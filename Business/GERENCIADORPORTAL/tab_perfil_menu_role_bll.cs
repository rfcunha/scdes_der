using System;
using System.Collections.Generic;
using System.Linq;
using Dao.GERENCIADORPORTAL;
using Entity.GERENCIADORPORTAL;
using Entity.GERENCIADORPORTAL.Utils.Filtro;
using Suporte.Exceptions;
using Suporte.MsgBox;
using Suporte.Transaction;

namespace Business.GERENCIADORPORTAL
{
    public class tab_perfil_menu_role_bll
    {
        #region PARAMETROS
        private readonly tab_perfil_menu_role_dao _objDal;
        #endregion PARAMETROS

        #region CONSTRUTOR
        public tab_perfil_menu_role_bll()
        {
            _objDal = new tab_perfil_menu_role_dao();
        }

        #endregion CONSTRUTOR

        #region METODOS
        public int INSERT(tab_perfil_menu_role obj)
        {
            var retorno = 0;
            try
            {
                using (var trans = TransactionScopeUtils.CreateTransactionScope())
                {
                    retorno = _objDal.INSERT(obj);
                    trans.Complete();
                }
            }
            catch (CustomException ce)
            {
                throw ce;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            return retorno;
        }

        public int UPDATE(tab_perfil_menu_role obj)
        {
            var retorno = 0;
            try
            {
                using (var trans = TransactionScopeUtils.CreateTransactionScope())
                {
                    retorno = _objDal.UPDATE(obj);
                    trans.Complete();
                }
            }
            catch (CustomException ce)
            {
                throw ce;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            return retorno;
        }

        public int DELETE(tab_perfil_menu_role obj)
        {
            var retorno = 0;
            try
            {
                using (var trans = TransactionScopeUtils.CreateTransactionScope())
                {
                    retorno = _objDal.DELETE(obj);
                    trans.Complete();
                }
            }
            catch (CustomException ce)
            {
                throw ce;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            return retorno;
        }

        public int DELETE_ALL_ROLE(int per_id)
        {
            var retorno = 0;
            try
            {
                using (var trans = TransactionScopeUtils.CreateTransactionScope())
                {
                    retorno = _objDal.DELETE_ALL_ROLE(per_id);
                    trans.Complete();
                }
            }
            catch (CustomException ce)
            {
                throw ce;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            return retorno;
        }

        public bool STATUS(tab_perfil_menu_role obj)
        {
            var retorno = false;
            try
            {
                using (var trans = TransactionScopeUtils.CreateTransactionScope())
                {
                    retorno = _objDal.STATUS(obj);
                    trans.Complete();
                }
            }
            catch (CustomException ce)
            {
                throw ce;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            return retorno;
        }

        public tab_perfil_menu_role GET_BY(int id)
        {
            tab_perfil_menu_role retorno;
            try
            {
                retorno = _objDal.GET_BY(id);
            }
            catch (CustomException ce)
            {
                throw ce;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            return retorno;
        }

        public ICollection<tab_perfil_menu_role> GET_ALL(tab_perfil_menu_role_filtro obj)
        {
            ICollection<tab_perfil_menu_role> retorno;
            try
            {

                retorno = _objDal.GET_ALL(obj);
                foreach (var item in retorno)
                {
                    item.tab_menu_role = new tab_menu_role_bll().GET_BY(item.mro_id);
                }
            }
            catch (CustomException ce)
            {
                throw ce;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            return retorno;
        }

        public IEnumerable<string> RETORNA_OBJETO_HTML_ROLES_PERFIL(int per_id, string url)
        {
            if (url == "/")
                url = "/Home/Index";
          
            return GET_ALL(new tab_perfil_menu_role_filtro() { per_id = per_id }).ToList().Where(role => url.Contains(role.tab_menu_role.tab_menu.men_url + "" + role.tab_menu_role.tab_menu.men_pagina)).Select(role => role.tab_menu_role.mro_controle);
        }
        #endregion METODOS
    }
}