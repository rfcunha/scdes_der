using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using Dao.GERENCIADORPORTAL;
using Entity.GERENCIADORPORTAL;
using Entity.GERENCIADORPORTAL.Utils.Filtro;
using Suporte.Exceptions;
using Suporte.MsgBox;
using Suporte.MsgBox.Mvc;
using Suporte.Transaction;

namespace Business.GERENCIADORPORTAL
{
    public class tab_menu_bll
    {
        #region PARAMETROS
        private readonly tab_menu_dao _objDal;
        #endregion PARAMETROS

        #region CONSTRUTOR
        public tab_menu_bll()
        {
            _objDal = new tab_menu_dao();
        }

        #endregion CONSTRUTOR

        #region METODOS
        public int INSERT(tab_menu obj)
        {
            var retorno = 0;
            try
            {
                using (var trans = TransactionScopeUtils.CreateTransactionScope())
                {
                    retorno = _objDal.INSERT(obj);
                    trans.Complete();
                }
            }
            catch (CustomException ce)
            {
                throw ce;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            return retorno;
        }

        public int UPDATE(tab_menu obj)
        {
            var retorno = 0;
            try
            {
                using (var trans = TransactionScopeUtils.CreateTransactionScope())
                {
                    retorno = _objDal.UPDATE(obj);
                    trans.Complete();
                }
            }
            catch (CustomException ce)
            {
                throw ce;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            return retorno;
        }

        public int DELETE(tab_menu obj)
        {
            var retorno = 0;
            try
            {
                using (var trans = TransactionScopeUtils.CreateTransactionScope())
                {
                    retorno = _objDal.DELETE(obj);
                    trans.Complete();
                }
            }
            catch (CustomException ce)
            {
                throw ce;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            return retorno;
        }

        public bool STATUS(tab_menu obj)
        {
            var retorno = false;
            try
            {
                using (var trans = TransactionScopeUtils.CreateTransactionScope())
                {
                    retorno = _objDal.STATUS(obj);
                    trans.Complete();
                }
            }
            catch (CustomException ce)
            {
                throw ce;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            return retorno;
        }

        public tab_menu GET_BY(int id)
        {
            tab_menu retorno;
            try
            {
                retorno = _objDal.GET_BY(id);
            }
            catch (CustomException ce)
            {
                throw ce;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            return retorno;
        }

        public ICollection<tab_menu> GET_ALL(tab_menu_filtro obj)
        {
            ICollection<tab_menu> retorno;
            try
            {
                retorno = _objDal.GET_ALL(obj);
            }
            catch (CustomException ce)
            {
                throw ce;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            return retorno;
        }
        #endregion METODOS

        #region METODOS XML

        public void CreateMenu(int id)
        {
            try
            {
                if (System.IO.File.Exists(System.Web.HttpContext.Current.Server.MapPath(@"~\Xml\Perfil\" + id + ".xml")))
                    System.IO.File.Delete(System.Web.HttpContext.Current.Server.MapPath(@"~\Xml\Perfil\" + id + ".xml"));

                CreateXmlPai(id).Save(System.Web.HttpContext.Current.Server.MapPath(@"~\Xml\Perfil\" + id + ".xml"));
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao criar o menu.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        private XmlDocument CreateXmlPai(int id)
        {
            try
            {
                var doc = new XmlDocument();
                doc.CreateXmlDeclaration("1.0", "UTF-8", null);

                XmlAttribute atributoNode;
                XmlNode menuNode;

                ////DEFINE N� M�E
                menuNode = doc.CreateElement("MenuItens");
                doc.AppendChild(menuNode);

                var menuNovo = CreateListaMenuPai(id);

                foreach (var menu in menuNovo.OrderBy(pagina => pagina.men_ordem).Where(pagina => pagina.men_pai == null))
                {
                    XmlNode node;
                    node = doc.CreateElement("Menu");

                    atributoNode = doc.CreateAttribute("Text");
                    atributoNode.Value = menu.men_menu;
                    node.Attributes.Append(atributoNode);


                    atributoNode = doc.CreateAttribute("Url");
                    atributoNode.Value = menu.men_url + menu.men_pagina;
                    node.Attributes.Append(atributoNode);

                    atributoNode = doc.CreateAttribute("Icon");
                    atributoNode.Value = menu.men_icon;
                    node.Attributes.Append(atributoNode);


                    menuNode.AppendChild(node);

                    CreateXmlMenuFilho(menuNovo, menu.men_id, node, doc);
                }

                
                return doc;
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void CreateXmlMenuFilho(List<tab_menu> listaMenu, int? idPai, XmlNode noPai, XmlDocument doc)
        {
            try
            {
                foreach (var menu in listaMenu.OrderBy(pagina => pagina.men_ordem).Where(pagina => pagina.men_pai == idPai))
                {
                    XmlAttribute atributoNode;
                    XmlNode node;

                    node = doc.CreateElement("SubMenu");

                    atributoNode = doc.CreateAttribute("Text");
                    atributoNode.Value = menu.men_menu;

                    node.Attributes.Append(atributoNode);


                    atributoNode = doc.CreateAttribute("Url");
                    atributoNode.Value = menu.men_url + menu.men_pagina;
                    node.Attributes.Append(atributoNode);

                    atributoNode = doc.CreateAttribute("Icon");
                    atributoNode.Value = menu.men_icon;
                    node.Attributes.Append(atributoNode);

                    noPai.AppendChild(node);

                    if (menu.men_pai != null)
                        CreateXmlMenuFilho(listaMenu, menu.men_id, node, doc);
                }
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private List<tab_menu> CreateListaMenuPai(int id)
        {
            try
            {
                var menuPerfil = new tab_perfil_menu_role_bll().GET_ALL(new tab_perfil_menu_role_filtro() { per_id = id });
                
                System.Web.HttpContext.Current.Session["listaMenu"] = menuPerfil.Select(tabMenuPagina => tabMenuPagina.tab_menu_role.tab_menu).GroupBy(x => x.men_id).Select(x => x.First()).ToList();

                foreach (var item in menuPerfil)
                {
                    CreateListaMenuFilho(item.tab_menu_role.tab_menu.men_pai);
                }

                return ((List<tab_menu>)System.Web.HttpContext.Current.Session["listaMenu"]).Where(t => t.men_exibir).OrderBy(pagina => pagina.men_ordem).ToList();

            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void CreateListaMenuFilho(int? idPai)
        {
            try
            {
                var menuNovo = new tab_menu_bll().GET_ALL(new tab_menu_filtro { men_ativo = true }).OrderBy(pagina => pagina.men_ordem).Where(pagina => pagina.men_id == idPai).ToList();

                foreach (var item in menuNovo)
                {
                    var listaPlano = System.Web.HttpContext.Current.Session["listaMenu"] != null ? (List<tab_menu>)System.Web.HttpContext.Current.Session["listaMenu"] : new List<tab_menu>();

                    if (listaPlano.Count(pagina => pagina.men_id == item.men_id) <= 0)
                    {
                        listaPlano.Add(item);
                        System.Web.HttpContext.Current.Session["listaMenu"] = listaPlano;

                        CreateListaMenuFilho(item.men_pai);
                    }
                }
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion
    }
}
