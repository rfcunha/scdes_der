using System;
using System.Collections.Generic;
using System.Linq;
using Dao.GERENCIADORPORTAL;
using Entity.GERENCIADORPORTAL;
using Entity.GERENCIADORPORTAL.Utils.Filtro;
using Suporte.Exceptions;
using Suporte.MsgBox;
using Suporte.Transaction;

namespace Business.GERENCIADORPORTAL
{
    public class tab_perfil_bll
    {
        #region PARAMETROS
        private readonly tab_perfil_dao _objDal;

        #endregion PARAMETROS

        #region CONSTRUTOR
        public tab_perfil_bll()
        {
            _objDal = new tab_perfil_dao();
        }
        #endregion CONSTRUTOR

        #region METODOS

        public bool UPDATE_XML(int per_id)
        {
            var retorno = true;
            try
            {
                using (var trans = TransactionScopeUtils.CreateTransactionScope())
                {
                    retorno = _objDal.UPDATE_XML(per_id);

                    trans.Complete();
                }

                return retorno;
            }
            catch (CustomException ce)
            {
                throw ce;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
        }


        public int INSERT(tab_perfil obj)
        {
            var retorno = 0;
            try
            {
                using (var trans = TransactionScopeUtils.CreateTransactionScope())
                {
                    retorno = _objDal.INSERT(obj);
                    foreach (var item in obj.tab_perfil_menu_role.Where(item => item.acao == Enumeradores.Acao.Insert))
                    {
                        item.per_id = retorno;
                        new tab_perfil_menu_role_bll().INSERT(item);
                    }
                    trans.Complete();
                }
            }
            catch (CustomException ce)
            {
                throw ce;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            return retorno;
        }

        public int UPDATE(tab_perfil obj)
        {
            var retorno = 0;
            try
            {
                using (var trans = TransactionScopeUtils.CreateTransactionScope())
                {
                    retorno = _objDal.UPDATE(obj);
                    foreach (var item in obj.tab_perfil_menu_role)
                    {
                        item.per_id = obj.per_id;
                        switch (item.acao)
                        {
                            case Enumeradores.Acao.Insert:
                                new tab_perfil_menu_role_bll().INSERT(item);
                                break;
                            case Enumeradores.Acao.Delete:
                                new tab_perfil_menu_role_bll().DELETE(new tab_perfil_menu_role { pmr_id = item.pmr_id });
                                break;
                        }
                    }
                    trans.Complete();
                }
            }
            catch (CustomException ce)
            {
                throw ce;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            return retorno;
        }

        public int DELETE(tab_perfil obj)
        {
            var retorno = 0;
            try
            {
                using (var trans = TransactionScopeUtils.CreateTransactionScope())
                {
                    retorno = _objDal.DELETE(obj);
                    new tab_perfil_menu_role_bll().DELETE_ALL_ROLE(obj.per_id);
                    trans.Complete();
                }
            }
            catch (CustomException ce)
            {
                throw ce;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            return retorno;
        }

        public bool STATUS(tab_perfil obj)
        {
            var retorno = false;
            try
            {
                using (var trans = TransactionScopeUtils.CreateTransactionScope())
                {
                    retorno = _objDal.STATUS(obj);
                    trans.Complete();
                }
            }
            catch (CustomException ce)
            {
                throw ce;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            return retorno;
        }

        public tab_perfil GET_BY(int id)
        {
            tab_perfil retorno;
            try
            {
                retorno = _objDal.GET_BY(id);
                retorno.tab_perfil_menu_role = new tab_perfil_menu_role_bll().GET_ALL(new tab_perfil_menu_role_filtro { per_id = retorno.per_id }).ToList();
               
            }
            catch (CustomException ce)
            {
                throw ce;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            return retorno;
        }

        public ICollection<tab_perfil> GET_ALL(tab_perfil_filtro obj)
        {
            ICollection<tab_perfil> retorno;
            try
            {
                retorno = _objDal.GET_ALL(obj);
            }
            catch (CustomException ce)
            {
                throw ce;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            return retorno;
        }

        public ICollection<tab_perfil> GET_GRID(tab_perfil_filtro obj)
        {
            ICollection<tab_perfil> retorno;
            try
            {
                retorno = _objDal.GET_GRID(obj);
            }
            catch (CustomException ce)
            {
                throw ce;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            return retorno;
        }
        #endregion METODOS
    }
}
