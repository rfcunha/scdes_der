using System;
using System.Collections.Generic;
using Dao.GERENCIADORPORTAL;
using Entity.GERENCIADORPORTAL;
using Entity.GERENCIADORPORTAL.Utils.Filtro;
using Suporte.Exceptions;
using Suporte.MsgBox;
using Suporte.Transaction;

namespace Business.GERENCIADORPORTAL
{
    public class tab_menu_role_bll
    {
        #region PARAMETROS
        private readonly tab_menu_role_dao _objDal;

        #endregion PARAMETROS

        #region CONSTRUTOR
        public tab_menu_role_bll()
        {
            _objDal = new tab_menu_role_dao();
        }
         
  
        #endregion CONSTRUTOR

        #region METODOS
        public int INSERT(tab_menu_role obj)
        {
            var retorno = 0;
            try
            {
                using (var trans = TransactionScopeUtils.CreateTransactionScope())
                {
                    retorno = _objDal.INSERT(obj);
                    trans.Complete();
                }
            }
            catch (CustomException ce)
            {
                throw ce;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message,bootStrapMessageType.TYPE_DANGER);
            }
            return retorno;
        }

        public int UPDATE(tab_menu_role obj)
        {
            var retorno = 0;
            try
            {
                using (var trans = TransactionScopeUtils.CreateTransactionScope())
                {
                    retorno = _objDal.UPDATE(obj);
                    trans.Complete();
                }
            }
            catch (CustomException ce)
            {
                throw ce;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message,bootStrapMessageType.TYPE_DANGER);
            }
            return retorno;
        }

        public int DELETE(tab_menu_role obj)
        {
            var retorno = 0;
            try
            {
                using (var trans = TransactionScopeUtils.CreateTransactionScope())
                {
                    retorno = _objDal.DELETE(obj);
                    trans.Complete();
                }
            }
            catch (CustomException ce)
            {
                throw ce;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message,bootStrapMessageType.TYPE_DANGER);
            }
            return retorno;
        }

        public bool STATUS(tab_menu_role obj)
        {
            var retorno = false;
            try
            {
                using (var trans = TransactionScopeUtils.CreateTransactionScope())
                {
                    retorno = _objDal.STATUS(obj);
                    trans.Complete();
                }
            }
            catch (CustomException ce)
            {
                throw ce;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message,bootStrapMessageType.TYPE_DANGER);
            }
            return retorno;
        }

        public tab_menu_role GET_BY(int id)
        {
            tab_menu_role retorno;
            try
            {
                var item = _objDal.GET_BY(id);
                item.tab_menu = new tab_menu_bll().GET_BY(item.men_id);
                return item;
            }
            catch (CustomException ce)
            {
                throw ce;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message,bootStrapMessageType.TYPE_DANGER);
            }
            return retorno;
        }

        public ICollection<tab_menu_role> GET_ALL(tab_menu_role_filtro obj)
        {
            ICollection<tab_menu_role> retorno;
            try
            {
                retorno = _objDal.GET_ALL(obj);
            }
            catch (CustomException ce)
            {
                throw ce;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message,bootStrapMessageType.TYPE_DANGER);
            }
            return retorno;
        }
        #endregion METODOS

    }
}
