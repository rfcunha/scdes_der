using System; 
using Dao.GERENCIADORPORTAL;
using Suporte.Exceptions;
using Suporte.MsgBox;
using Suporte.Transaction;

namespace Business.GERENCIADORPORTAL
{
    public sealed class tab_configuracao_sistema_bll
    {
         #region PARAMETROS
        private readonly tab_configuracao_sistema_dao _objDal;
        #endregion PARAMETROS

        #region CONSTRUTOR
        public tab_configuracao_sistema_bll()
        {
            _objDal = new tab_configuracao_sistema_dao();
        }

        #endregion CONSTRUTOR

        #region METODOS
         
            public int STP_GET_BY_ID_SISTEMA()
            {
                var retorno = 0;
                try
                {
                  retorno = _objDal.STP_GET_BY_ID_SISTEMA();
                }
                catch (CustomException ce)
                {
                    throw ce;
                }
                catch (Exception ex)
                {
                    throw new CustomException(ex.Message,bootStrapMessageType.TYPE_DANGER);
                }
                return retorno;
            } 

        #endregion METODOS
    } 
}
