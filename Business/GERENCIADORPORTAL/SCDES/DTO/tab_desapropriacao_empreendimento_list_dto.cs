﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business.SCDES.DTO
{
    public class tab_desapropriacao_empreendimento_list_dto
    {
        public string dem_auto_decreto { get; set; }

        public List<short> dem_lote_list { get; set; }
    }
}
