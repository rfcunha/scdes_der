using System;
using System.Collections.Generic;
using System.Linq;
using Business.SCDES.DTO;
using Dao.SCDES;
using Entity.SCDES.Models;
using Suporte.Exceptions;
using Suporte.MsgBox;
using Entity.SCDES.Utils.Filtro;

namespace Business.SCDES
{
    public class tab_desapropriacao_empreendimento_bll
    {
        public List<tab_desapropriacao_empreendimento_list_dto> LISTAR_EMPREENDIMENTOS()
        {
            try
            {
                using (var dao = new tab_desapropriacao_empreendimento_dao())
                {
                    var demList = dao.LISTAR_EMPREENDIMENTOS();

                    var list = demList.GroupBy(d => d.dem_auto_decreto)
                        .Select(d => new tab_desapropriacao_empreendimento_list_dto()
                        {
                            dem_auto_decreto = d.Key,
                            dem_lote_list = d.Select(x => x.dem_lote).ToList()
                        }).ToList();

                    return list;
                }
            }
            catch (Exception e)
            {
                throw new CustomException("Erro: " + e, bootStrapMessageType.TYPE_DANGER);
            }
        }

        public tab_desapropriacao_empreendimento BUSCAR_EMPREENDIMENTO(string numAutoDecreto, short lote)
        {
            try
            {
                var filtro = new tab_desapropriacao_empreendimento_filtro()
                {
                    dem_auto_decreto = numAutoDecreto,
                    dem_lote = lote
                };

                var dem = new tab_desapropriacao_empreendimento();

                using (var dao = new tab_desapropriacao_empreendimento_dao())
                {
                    dem = dao.BUSCAR_EMPREENDIMENTO(filtro);
                }

                return dem;
            }
            catch (Exception e)
            {
                throw new CustomException("Erro: " + e, bootStrapMessageType.TYPE_DANGER);
            }
        }

    }
}
