using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Dao.SCDES;
using Entity.SCDES.Models;
using Entity.SCDES.Utils.Filtro;
using Entity.SCDES.Utils.Gdv;
using Suporte.Exceptions;
using Suporte.Formatacao;
using Suporte.MsgBox;
using Suporte.Transaction;

namespace Business.SCDES
{
    public class tab_emitente_bll
    {
        public int INSERT(tab_emitente obj)
        {
            try
            {
                int retorno;

                ValidaInsert(obj);

                using (var scope = TransactionScopeUtils.CreateTransactionScope())
                {
                    using (var dao = new tab_emitente_dao())
                    {
                        obj.emi_telefone = obj.emi_telefone.FormatarTelefone(true);
                        obj.emi_celular = obj.emi_celular.FormatarCelular(true);

                        retorno = dao.INSERT(obj).emi_id;
                    }

                    scope.Complete();
                }

                return retorno;
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao salvar os dados do emitente.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public bool UPDATE(tab_emitente obj)
        {
            try
            {
                ValidaUpdate(obj);

                using (var scope = TransactionScopeUtils.CreateTransactionScope())
                {
                    using (var dao = new tab_emitente_dao())
                    {
                        dao.UPDATE(obj);
                    }

                    scope.Complete();
                }

                return true;
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao alterar os dados do emitente.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public bool DELETE(int id, string userDelete)
        {
            try
            {
                ValidaDelete(id);

                using (var scope = TransactionScopeUtils.CreateTransactionScope())
                {
                    var obj = FIND(id);
                    obj.userDelete = userDelete;
                    obj.dtDelete = DateTime.Now;

                    using (var dao = new tab_emitente_dao())
                    {
                        dao.DELETE(obj);
                    }

                    scope.Complete();
                }

                return true;
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao excluir o emitente.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public bool STATUS(int id, bool status, string userLastUpdate)
        {
            try
            {
                var obj = new tab_emitente { emi_id = id, emi_ativo = status, userLastUpdate = userLastUpdate, dtLastUpdate = DateTime.Now };


                using (var scope = TransactionScopeUtils.CreateTransactionScope())
                {
                    using (var dao = new tab_emitente_dao())
                    {
                        dao.UPDATE(obj, x => x.emi_ativo, x => x.userLastUpdate, x => x.dtLastUpdate);
                    }

                    scope.Complete();
                }

                return true;
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao " + (status ? "desativar" : "ativar") + " o emitente.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public tab_emitente FIND(int id)
        {
            try
            {
                using (var dao = new tab_emitente_dao())
                {
                    return dao.FIND(id);
                }
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao obter os dados do emitente.</br>Entre em contato com o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public IEnumerable<tab_emitente> FIND(Expression<Func<tab_emitente, bool>> Where = null)
        {
            try
            {
                using (var dao = new tab_emitente_dao())
                {
                    return dao.FIND(Where).ToList();
                }
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao pesquisar os dados do emitente.</br>Entre em contato com o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public List<tab_emitente_gdv> PESQUISA_GDV(tab_emitente_filtro filtro)
        {
            try
            {
                using (var dao = new tab_emitente_dao())
                {
                    return dao.PESQUISA_GDV(filtro);
                }
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao pesquisar os dados do emitente.</br>Entre em contato com o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        #region | VALIDACAO |

        private void ValidaInsert(tab_emitente obj)
        {
            if (string.IsNullOrEmpty(obj.emi_nome))
                throw new CustomException("- digite o nome.", bootStrapMessageType.TYPE_WARNING);

            using (var dao = new tab_emitente_dao())
            {
                if (dao.VALIDATE(x => x.emi_nome == obj.emi_nome))
                    throw new CustomException("J� exite um emitente cadastrado com este nome.", bootStrapMessageType.TYPE_WARNING);
            }
        }

        private void ValidaUpdate(tab_emitente obj)
        {
            if (string.IsNullOrEmpty(obj.emi_nome))
                throw new CustomException("- digite o nome.", bootStrapMessageType.TYPE_DANGER);

            using (var dao = new tab_emitente_dao())
            {
                if (dao.VALIDATE(x => x.emi_nome == obj.emi_nome && x.emi_id != obj.emi_id))
                    throw new CustomException("J� exite um emitente cadastrado com este nome.", bootStrapMessageType.TYPE_WARNING);
            }
        }

        private void ValidaDelete(int id)
        {
            using (var dao = new tab_desapropriacao_dao())
            {
                if (dao.VALIDATE(x => x.tab_desapropriacao_emitente.Any(y => y.emp_id == id)))
                    throw new CustomException("N�o � possivel excluir este emitente.</br></br>- existe uma ou mais desapropria��o associada a este emitente.</br>", bootStrapMessageType.TYPE_WARNING);
            }
        }

        #endregion
    }
}
