using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Dao.SCDES;
using Entity.SCDES.Models;
using Entity.SCDES.Utils.Filtro;
using Entity.SCDES.Utils.Gdv;
using Suporte.Exceptions;
using Suporte.Formatacao;
using Suporte.MsgBox;
using Suporte.Transaction;

namespace Business.SCDES
{
    public class tab_desapropriacao_individual_perito_judicial_bll
    {
        public int INSERT(tab_desapropriacao_individual_perito_judicial obj)
        {
            try
            {
                int retorno;

                ValidaInsert(obj);

                using (var scope = TransactionScopeUtils.CreateTransactionScope())
                {
                    using (var dao = new tab_desapropriacao_individual_perito_judicial_dao())
                    {
                        obj.ipj_telefone = obj.ipj_telefone.FormatarTelefone(true);
                        obj.ipj_celular = obj.ipj_celular.FormatarCelular(true);

                        retorno = dao.INSERT(obj).ipj_id;
                    }

                    scope.Complete();
                }

                return retorno;
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao salvar os dados do perito judicial.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public bool UPDATE(tab_desapropriacao_individual_perito_judicial obj)
        {
            try
            {
                ValidaUpdate(obj);

                using (var scope = TransactionScopeUtils.CreateTransactionScope())
                {
                    using (var dao = new tab_desapropriacao_individual_perito_judicial_dao())
                    {
                        obj.ipj_telefone = obj.ipj_telefone.FormatarTelefone(true);
                        obj.ipj_celular = obj.ipj_celular.FormatarCelular(true);

                        dao.UPDATE(obj);
                    }

                    scope.Complete();
                }

                return true;
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao alterar os dados do perito judicial.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public bool DELETE(int id, string userDelete)
        {
            try
            {
                ValidaDelete(id);

                using (var scope = TransactionScopeUtils.CreateTransactionScope())
                {
                    var obj = FIND(id);
                    obj.userDelete = userDelete;
                    obj.dtDelete = DateTime.Now;

                    using (var dao = new tab_desapropriacao_individual_perito_judicial_dao())
                    {
                        dao.DELETE(obj);
                    }

                    scope.Complete();
                }

                return true;
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao excluir a condu��o da desapropria��o.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public bool STATUS(int id, bool status, string userLastUpdate)
        {
            try
            {
                var obj = new tab_desapropriacao_individual_perito_judicial { ipj_id = id, ipj_ativo = status, userLastUpdate = userLastUpdate, dtLastUpdate = DateTime.Now };


                using (var scope = TransactionScopeUtils.CreateTransactionScope())
                {
                    using (var dao = new tab_desapropriacao_individual_perito_judicial_dao())
                    {
                        dao.UPDATE(obj, x => x.ipj_ativo, x => x.userLastUpdate, x => x.dtLastUpdate);
                    }

                    scope.Complete();
                }

                return true;
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao " + (status ? "desativar" : "ativar") + " o perito judicial.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public tab_desapropriacao_individual_perito_judicial FIND(int id)
        {
            try
            {
                using (var dao = new tab_desapropriacao_individual_perito_judicial_dao())
                {
                    return dao.FIND(id);
                }
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao obter os dados do perito judicial.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public IEnumerable<tab_desapropriacao_individual_perito_judicial> FIND(Expression<Func<tab_desapropriacao_individual_perito_judicial, bool>> Where = null)
        {
            try
            {
                using (var dao = new tab_desapropriacao_individual_perito_judicial_dao())
                {
                    return dao.FIND(Where).ToList();
                }
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao pesquisar os dados do perito judicial.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public List<tab_desapropriacao_individual_perito_judicial_gdv> PESQUISA_GDV(tab_desapropriacao_individual_perito_judicial_filtro filtro)
        {
            try
            {
                using (var dao = new tab_desapropriacao_individual_perito_judicial_dao())
                {
                    return dao.PESQUISA_GDV(filtro);
                }
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao pesquisar os dados do perito judicial.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        #region | VALIDACAO |

        private void ValidaInsert(tab_desapropriacao_individual_perito_judicial obj)
        {
            if (string.IsNullOrEmpty(obj.ipj_nome))
                throw new CustomException("- digite o nome.", bootStrapMessageType.TYPE_WARNING);

            using (var dao = new tab_desapropriacao_individual_perito_judicial_dao())
            {
                if (dao.VALIDATE(x => x.ipj_nome == obj.ipj_nome))
                    throw new CustomException("J� exite um perito judicial cadastrado com este nome.", bootStrapMessageType.TYPE_WARNING);
            }
        }

        private void ValidaUpdate(tab_desapropriacao_individual_perito_judicial obj)
        {
            if (string.IsNullOrEmpty(obj.ipj_nome))
                throw new CustomException("- digite o nome.", bootStrapMessageType.TYPE_DANGER);

            using (var dao = new tab_desapropriacao_individual_perito_judicial_dao())
            {
                if (dao.VALIDATE(x => x.ipj_nome == obj.ipj_nome && x.ipj_id != obj.ipj_id))
                    throw new CustomException("J� exite um perito judicial cadastrado com este nome.", bootStrapMessageType.TYPE_WARNING);
            }
        }

        private void ValidaDelete(int id)
        {
            using (var dao = new tab_desapropriacao_individual_dao())
            {
                if (dao.VALIDATE(x => x.din_conducao_ipj_id == id))
                    throw new CustomException("N�o � possivel excluir este perito judicial.</br></br>- existe uma ou mais desapropria��o individual associada a este perito judicial.</br>", bootStrapMessageType.TYPE_WARNING);
            }
        }

        #endregion
    }
}
