﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entity.SCDES.Models;
using Suporte.Exceptions;
using Dao.SCDES;
using System.Linq.Expressions;
using Suporte.MsgBox;

namespace Business.SCDES
{
    public class tab_select_helper_bll
    {


        public List<tab_select_helper> FIND(Expression<Func<tab_select_helper, bool>> Where = null)
        {
            try
            {
                using (var dao = new tab_select_helper_dao())
                {
                    return dao.FIND(Where).ToList();
                }
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao pesquisar os dados da condução da desapropriação.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public Dictionary<string, List<tab_select_helper>> FIND(int guia_id)
        {
            try
            {
                using (var dao = new tab_select_helper_dao())
                {
                    var data = dao.LISTAR_GUIA_HELPERS(guia_id);

                    var list = data.GroupBy(t => t.seh_nome_campo).ToDictionary(t => t.Key, t => t.ToList());

                    return list;
                }
            }
            catch (Exception ex)
            {
                throw new CustomException("Erro ao recuperar os dados");
            }
        }
    }
}
