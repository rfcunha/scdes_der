﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Dao.SCDES;
using Entity.SCDES.Models;
using Entity.SCDES.Utils.Filtro;
using Entity.SCDES.Utils.Gdv;
using Suporte.Exceptions;
using Suporte.MsgBox;
using Suporte.Transaction;

namespace Business.SCDES
{
    public class tab_desapropriacao_individual_status_evento_tipo_bll
    {
        public int INSERT(tab_desapropriacao_individual_status_evento_tipo obj)
        {
            try
            {
                int retorno;

                ValidaInsert(obj);

                using (var scope = TransactionScopeUtils.CreateTransactionScope())
                {
                    using (var dao = new tab_desapropriacao_individual_status_evento_tipo_dao())
                    {
                        retorno = dao.INSERT(obj).set_id;
                    }

                    scope.Complete();
                }

                return retorno;
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao salvar os dados do tipo de evento.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public bool UPDATE(tab_desapropriacao_individual_status_evento_tipo obj)
        {
            try
            {
                ValidaUpdate(obj);

                using (var scope = TransactionScopeUtils.CreateTransactionScope())
                {
                    using (var dao = new tab_desapropriacao_individual_status_evento_tipo_dao())
                    {
                        dao.UPDATE(obj);
                    }

                    scope.Complete();
                }

                return true;
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao alterar os dados do tipo de evento.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public bool DELETE(int id, string userDelete)
        {
            try
            {
                ValidaDelete(id);

                using (var scope = TransactionScopeUtils.CreateTransactionScope())
                {
                    var obj = FIND(id);
                    obj.userDelete = userDelete;
                    obj.dtDelete = DateTime.Now;

                    using (var dao = new tab_desapropriacao_individual_status_evento_tipo_dao())
                    {
                        dao.DELETE(obj);
                    }

                    scope.Complete();
                }

                return true;
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao excluir os dados do tipo de evento.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public bool STATUS(int id, bool status, string userLastUpdate)
        {
            try
            {
                var obj = new tab_desapropriacao_individual_status_evento_tipo { set_id = id, set_ativo = status, userLastUpdate = userLastUpdate, dtLastUpdate = DateTime.Now };


                using (var scope = TransactionScopeUtils.CreateTransactionScope())
                {
                    using (var dao = new tab_desapropriacao_individual_status_evento_tipo_dao())
                    {
                        dao.UPDATE(obj, x => x.set_ativo, x => x.userLastUpdate, x => x.dtLastUpdate);
                    }

                    scope.Complete();
                }

                return true;
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao " + (status ? "desativar" : "ativar") + " o tipo de evento.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public tab_desapropriacao_individual_status_evento_tipo FIND(int id)
        {
            try
            {
                using (var dao = new tab_desapropriacao_individual_status_evento_tipo_dao())
                {
                    return dao.FIND(id);
                }
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao obter os dados do tipo de evento.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public List<tab_desapropriacao_individual_status_evento_tipo> LISTAR_ATIVOS()
        {
            try
            {
                using (var dao = new tab_desapropriacao_individual_status_evento_tipo_dao())
                {
                    return dao.FIND_ASSETS();
                }
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception ex)
            {
                
                throw new CustomException("Ocorreu um erro ao obter os dados do tipo de evento.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }
           

        public IEnumerable<tab_desapropriacao_individual_status_evento_tipo> FIND(Expression<Func<tab_desapropriacao_individual_status_evento_tipo, bool>> Where = null)
        {
            try
            {
                using (var dao = new tab_desapropriacao_individual_status_evento_tipo_dao())
                {
                    return dao.FIND(Where).ToList();
                }
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao pesquisar os dados do tipo de evento.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public List<tab_desapropriacao_individual_status_evento_tipo_gdv> PESQUISA_GDV(tab_desapropriacao_individual_status_evento_tipo_filtro filtro)
        {
            try
            {
                using (var dao = new tab_desapropriacao_individual_status_evento_tipo_dao())
                {
                    return dao.PESQUISA_GDV(filtro);
                }
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao pesquisar os dados do tipo de evento.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        #region | VALIDACAO |

        private void ValidaInsert(tab_desapropriacao_individual_status_evento_tipo obj)
        {
            if (string.IsNullOrEmpty(obj.set_nome))
                throw new CustomException("- digite o nome.", bootStrapMessageType.TYPE_WARNING);

            using (var dao = new tab_desapropriacao_individual_status_evento_tipo_dao())
            {
                if (dao.VALIDATE(x => x.set_nome == obj.set_nome))
                    throw new CustomException("Já exite um tipo de evento cadastrado com este nome.", bootStrapMessageType.TYPE_WARNING);
            }
        }

        private void ValidaUpdate(tab_desapropriacao_individual_status_evento_tipo obj)
        {
            if (string.IsNullOrEmpty(obj.set_nome))
                throw new CustomException("- digite o nome.", bootStrapMessageType.TYPE_WARNING);

            using (var dao = new tab_desapropriacao_individual_status_evento_tipo_dao())
            {
                if (dao.VALIDATE(x => x.set_nome == obj.set_nome && x.set_id != obj.set_id))
                    throw new CustomException("Já exite um tipo de evento cadastrado com este nome.", bootStrapMessageType.TYPE_WARNING);
            }
        }

        private void ValidaDelete(int id)
        {
            //using (var dao = new tab_desapropriacao_individual_status_evento_tipo_dao())
            //{
            //    if (dao.VALIDATE(x => x.set_id == id))
            //        throw new CustomException("Não é possivel excluir este tipo de evento.</br></br>- existe uma ou mais desapropriação individual associada a este tipo de evento.</br>", bootStrapMessageType.TYPE_WARNING);
            //}
        }

        #endregion
    }
}
