﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dao.SCDES;
using Entity.SCDES.Utils.Gdv;

namespace Business.SCDES
{
    public class tab_desapropriacao_emitente_bll
    {


        public List<tab_desapropriacao_emitente_gdv> FIND(int desId)
        {
            //TODO: completa os dados de emitente

            using (var emitenteDao = new tab_desapropriacao_emitente_dao())
            {
                return emitenteDao.PESQUISA_GDV_EMITENTES(desId);
            }
        }
    }
}
