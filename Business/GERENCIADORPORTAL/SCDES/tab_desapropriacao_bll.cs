using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using Dao.SCDES;
using Entity.SCDES.Enum;
using Entity.SCDES.Models;
using Entity.SCDES.Utils.Filtro;
using Entity.SCDES.Utils.Gdv;
using Suporte.Exceptions;
using Suporte.Formatacao;
using Suporte.MsgBox;
using Suporte.Transaction;

namespace Business.SCDES
{
    public class tab_desapropriacao_bll
    {
        public int INSERT(tab_desapropriacao obj)
        {
            try
            {
                int retorno;

                ValidaInsert(obj);

                using (var scope = TransactionScopeUtils.CreateTransactionScope())
                {
                    obj.tab_desapropriacao_empreendimento = UploadAnexosEmpreendimento(obj.tab_desapropriacao_empreendimento);

                    using (var dao = new tab_desapropriacao_dao())
                    {
                        retorno = dao.INSERT(obj).des_id;
                    }

                    scope.Complete();
                }

                return retorno;
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao salvar os dados da desapropria��o.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public bool UPDATE(tab_desapropriacao obj)
        {
            try
            {
                ValidaUpdate(obj);

                using (var scope = TransactionScopeUtils.CreateTransactionScope())
                {

                    obj.tab_desapropriacao_empreendimento = UploadAnexosEmpreendimento(obj.tab_desapropriacao_empreendimento);

                    using (var dao = new tab_desapropriacao_dao())
                    {
                        dao.UPDATE(obj);
                    }

                    scope.Complete();
                }

                return true;
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao alterar os dados da desapropria��o.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public bool DELETAR(int id, string user_delete)
        {
            try
            {
                using (var dao = new tab_desapropriacao_dao())
                {
                    return dao.DELETAR(id, user_delete);
                }
            }
            catch (Exception e)
            {
                throw new CustomException("Erro: " + e, bootStrapMessageType.TYPE_DANGER);
            }
        }

        //public bool STATUS(int id, bool status, string userName, string descricaoMotivo)
        //{
        //    try
        //    {
        //        var obj = new tab_desapropriacao { des_id = id, boa_ativo = status };


        //        using (var scope = TransactionScopeUtils.CreateTransactionScope())
        //        {
        //            using (var dao = new tab_desapropriacao_dao())
        //            {
        //                dao.UPDATE(obj, x => x.boa_ativo);
        //            }

        //            using (var daoHistorico = new tab_desapropriacao_historico_status_dao())
        //            {
        //                daoHistorico.INSERT(new tab_desapropriacao_historico_status
        //                {
        //                    boa_id = id,
        //                    hst_dt_cadastro = DateTime.Now,
        //                    hst_status = status ? "Ativo" : "Cancelado",
        //                    hst_usuario = userName,
        //                    hst_motivo = descricaoMotivo
        //                });
        //            }

        //            scope.Complete();
        //        }

        //        return true;
        //    }
        //    catch (CustomException)
        //    {
        //        throw;
        //    }
        //    catch (Exception)
        //    {
        //        throw new CustomException("Ocorreu um erro ao " + (status ? "desativar" : "ativar") + " o boatr.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
        //    }
        //}

        //public bool AUDITAR(int id, short boa_auditado, string userLastUpdade)
        //{
        //    try
        //    {
        //        var obj = new tab_desapropriacao { boa_id = id, boa_auditado = boa_auditado, boa_auditado_dt = DateTime.Now, boa_auditado_nome = userLastUpdade, userLastUpdate = userLastUpdade, dtLastUpdate = DateTime.Now };

        //        using (var scope = TransactionScopeUtils.CreateTransactionScope())
        //        {
        //            using (var dao = new tab_desapropriacao_dao())
        //            {
        //                dao.UPDATE(obj, boatr => boatr.boa_auditado, boatr => boatr.boa_auditado_dt, boatr => boatr.boa_auditado_nome, boatr => boatr.userLastUpdate, boatr => boatr.dtLastUpdate);
        //            }

        //            scope.Complete();
        //        }

        //        return true;
        //    }
        //    catch (CustomException)
        //    {
        //        throw;
        //    }
        //    catch (Exception)
        //    {
        //        throw new CustomException("Ocorreu um erro ao auditar o boatr.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
        //    }
        //}

        public tab_desapropriacao FIND(int id)
        {
            try
            {
                using (var dao = new tab_desapropriacao_dao())
                {
                    var item = dao.FIND(x => x.des_id == id)

                    .Include(x => x.tab_desapropriacao_departamento)
                    .Include(x => x.tab_desapropriacao_empreendimento)
                    .Include(x => x.tab_desapropriacao_status)
                    .Include(x => x.tab_desapropriacao_localidade)
                    .Include(x => x.tab_desapropriacao_observacao)
                    .Include(x => x.tab_desapropriacao_recurso_fonte)
                    .Include(x => x.tab_desapropriacao_emitente)
                    .Include(x => x.tab_desapropriacao_historico).Single();

                    return item;
                }
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new CustomException("Ocorreu um erro ao obter os dados.Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public IEnumerable<tab_desapropriacao> FIND(Expression<Func<tab_desapropriacao, bool>> Where = null)
        {
            try
            {
                using (var dao = new tab_desapropriacao_dao())
                {
                    return dao.FIND(Where).Where(t => string.IsNullOrEmpty(t.userDelete));
                }
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao pesquisar os dados.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public List<tab_desapropriacao_gdv> PESQUISA_GDV_SIMPLES(tab_desapropriacao_filtro filtro)
        {
            try
            {
                using (var dao = new tab_desapropriacao_dao())
                {
                    return dao.PESQUISA_GDV_SIMPLES(filtro);
                }
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao pesquisar os dados.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public List<tab_desapropriacao_gdv> PESQUISA_GDV(tab_desapropriacao_filtro filtro)
        {
            try
            {
                using (var dao = new tab_desapropriacao_dao())
                {
                    return dao.PESQUISA_GDV(filtro);
                }
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao carregar a lista de desapropria��o.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }


        /// <summary>
        /// SALVAR OS ARQUIVOS E RETONA A LISTA COM ATUALIZADA
        /// </summary>
        /// <param name="listaEmpreendimentos"></param>
        /// <returns></returns>
        private ICollection<tab_desapropriacao_empreendimento> UploadAnexosEmpreendimento(ICollection<tab_desapropriacao_empreendimento> listaEmpreendimentos)
        {
            try
            {

                listaEmpreendimentos.Where(empreendimento => empreendimento.dem_acao == dem_acao.Insert || empreendimento.dem_update_anexo)
                    .Where(item => item.dem_file_upload != null).ToList().ForEach(item =>
                    {
                        item.dem_anexo = Path.GetFileNameWithoutExtension(item.dem_file_upload.FileName.RemoveCaracteresSpeciaisFileUpload()) + "_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + Path.GetExtension(item.dem_file_upload.FileName);
                        item.dem_anexo_nome = item.dem_file_upload.FileName;
                        item.dem_anexo_pasta = @"\Upload\Anexo\";
                        item.dem_file_upload.SaveAs(System.Web.HttpContext.Current.Server.MapPath(@"~" + item.dem_anexo_pasta) + item.dem_anexo);
                    });

                return listaEmpreendimentos;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao realizar o upload do anexo. <br/>Entre em contato com o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        #region | VALIDACAO |

        private void ValidaInsert(tab_desapropriacao obj)
        {
            using (var dao = new tab_desapropriacao_dao())
            {
                //if (dao.VALIDATE(x => x.boa_numero_geral == obj.boa_numero_geral))
                //    throw new CustomException("J� exite uma desapropria��o cadastrada com este n�mero, bcp e data do fato. ", bootStrapMessageType.TYPE_WARNING);
            }
        }

        private void ValidaUpdate(tab_desapropriacao obj)
        {
            using (var dao = new tab_desapropriacao_dao())
            {
                //if (dao.VALIDATE(x => x.boa_numero_geral == obj.boa_numero_geral && x.boa_id != obj.boa_id))
                //    throw new CustomException("J� exite uma desapropria��o cadastrado com este n�mero, bcp e data do fato. ", bootStrapMessageType.TYPE_WARNING);
            }
        }

        //private void ValidaDelete(int id)
        //{
        //    using (var dao = new tab_desapropriacao_dao())
        //    {
        //        if (dao.VALIDATE(x => x.boa_deletado == false && x.boa_id == id))
        //            throw new CustomException("N�o � possivel excluir este tipo de acidente.</br></br>- existe um ou mais boatr associado a este tipo de acidente.</br>", bootStrapMessageType.TYPE_DANGER);
        //    }
        //}

        #endregion
    }
}
