using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dao.SCDES;
using Entity.DER;
using Entity.DER.Utils.Filtro;
using Suporte.Exceptions;
using Suporte.MsgBox;

namespace Business.DER
{
    public class report_bll
    {
        private readonly report_dao _dao;

        public report_bll()
        {
            _dao = new report_dao();
        }

        public System.Data.DataSet RelatorioDesaprioriacao(string numeroAutoDecreto, short? lote)
        {
            try
            {
                return _dao.RelatorioDesapropriacao(numeroAutoDecreto, lote);
            }
            catch (CustomException ex)
            {
                throw ex;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao carregar os dados do Contrato.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }
    }
}
