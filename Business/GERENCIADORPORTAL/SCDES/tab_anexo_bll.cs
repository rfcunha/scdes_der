﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using Dao.SCDES;
using Entity.SCDES.Models;
using Entity.SCDES.Utils.Filtro;
using Entity.SCDES.Utils.Gdv;
using Suporte.Exceptions;
using Suporte.Formatacao;
using Suporte.MsgBox;
using Suporte.Transaction;

namespace Business.SCDES
{
    public class tab_anexo_bll
    {
        public int INSERT(tab_anexo obj)
        {
            try
            {
                int retorno;

                //ValidaInsert(obj);

                using (var scope = TransactionScopeUtils.CreateTransactionScope())
                {
                    using (var dao = new tab_anexo_dao())
                    {
                        retorno = dao.INSERT(obj).ane_id;
                    }

                    scope.Complete();
                }

                return retorno;
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao salvar os dados da desapropriação.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public bool DELETE(int id)
        {
            try
            {
                var obj = new tab_anexo { ane_id = id };

                using (var scope = TransactionScopeUtils.CreateTransactionScope())
                {
                    using (var dao = new tab_anexo_dao())
                    {
                        dao.DELETE(obj);
                    }

                    scope.Complete();
                }

                return true;
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao excluir os dados do boatr.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public tab_anexo FIND(int id)
        {
            try
            {
                using (var dao = new tab_anexo_dao())
                {
                    var item = dao.FIND(x => x.ane_id == id).Include(x => x.tab_anexo_identificador).Single();

                    return item;
                }
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao obter os dados.Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public IEnumerable<tab_anexo> FIND(Expression<Func<tab_anexo, bool>> Where = null)
        {
            try
            {
                using (var dao = new tab_anexo_dao())
                {
                    return dao.FIND(Where);
                }
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao pesquisar os dados.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        private ICollection<tab_anexo> Upload(ICollection<tab_anexo> lista)
        {
            try
            {
                lista.ToList().ForEach(item =>
                {
                    item.ane_nome_original = Path.GetFileName(item.ane_arquivo.FileName);
                    item.ane_nome = new Guid().ToString() + "." + Path.GetExtension(item.ane_arquivo.FileName);
                    item.ane_pasta = @"\Upload\Anexo\";
                    item.ane_arquivo.SaveAs(System.Web.HttpContext.Current.Server.MapPath(@"~" + item.ane_pasta) + item.ane_nome);
                });

                return lista;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao realizar o upload do anexo. <br/>Entre em contato com o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }
    }
}
