using System;
using System.Collections.Generic;
using Dao.GERENCIADORPORTAL;
using Entity.GERENCIADORPORTAL;
using Entity.GERENCIADORPORTAL.Utils.Filtro;
using Suporte.Exceptions;
using Suporte.MsgBox;
using Suporte.Transaction;

namespace Business.GERENCIADORPORTAL
{
    public class tab_usuario_bll
    {
        #region PARAMETROS
        private readonly tab_usuario_dao _objDal;
        #endregion PARAMETROS

        #region CONSTRUTOR
        public tab_usuario_bll()
        {
            _objDal = new tab_usuario_dao();
        } 
        #endregion CONSTRUTOR

        #region METODOS
        public int INSERT(tab_usuario obj)
        {
            var retorno = 0;
            try
            {
                using (var trans = TransactionScopeUtils.CreateTransactionScope())
                {
                    retorno = _objDal.INSERT(obj);
                    trans.Complete();
                }
            }
            catch (CustomException ce)
            {
                throw ce;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            return retorno;
        }

        public int UPDATE(tab_usuario obj)
        {
            var retorno = 0;
            try
            {
                using (var trans = TransactionScopeUtils.CreateTransactionScope())
                {
                    retorno = _objDal.UPDATE(obj);
                    trans.Complete();
                }
            }
            catch (CustomException ce)
            {
                throw ce;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            return retorno;
        }


        public int STP_UPD_USUARIO_SENHA(int usu_id, string usu_senha, string userLastUpdate, bool usu_trocar_senha)
        {
            var retorno = 0;
            try
            {
                using (var trans = TransactionScopeUtils.CreateTransactionScope())
                {
                    retorno = _objDal.STP_UPD_USUARIO_SENHA(usu_id,usu_senha,userLastUpdate,usu_trocar_senha);
                    trans.Complete();
                }
            }
            catch (CustomException ce)
            {
                throw ce;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            return retorno;
        }

        public int DELETE(tab_usuario obj)
        {
            var retorno = 0;
            try
            {
                using (var trans = TransactionScopeUtils.CreateTransactionScope())
                {
                    retorno = _objDal.DELETE(obj);
                    trans.Complete();
                }
            }
            catch (CustomException ce)
            {
                throw ce;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            return retorno;
        }

        public bool STATUS(tab_usuario obj)
        {
            var retorno = false;
            try
            {
                using (var trans = TransactionScopeUtils.CreateTransactionScope())
                {
                    retorno = _objDal.STATUS(obj);
                    trans.Complete();
                }
            }
            catch (CustomException ce)
            {
                throw ce;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            return retorno;
        }

        public tab_usuario GET_BY(int id)
        {
            tab_usuario retorno;
            try
            {
                retorno = _objDal.GET_BY(id);
            }
            catch (CustomException ce)
            {
                throw ce;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            return retorno;
        }
         
        public ICollection<tab_usuario> GET_ALL(tab_usuario_filtro obj)
        {
            ICollection<tab_usuario> retorno;
            try
            {
                retorno = _objDal.GET_ALL(obj);
            }
            catch (CustomException ce)
            {
                throw ce;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            return retorno;
        }
         
        public ICollection<tab_usuario> GET_GRID(tab_usuario_filtro obj)
        {
            ICollection<tab_usuario> retorno;
            try
            {
                retorno = _objDal.GET_GRID(obj);
            }
            catch (CustomException ce)
            {
                throw ce;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            return retorno;
        }

        public tab_usuario VALIDAR_USUARIO(tab_usuario_filtro obj)
        {
            tab_usuario retorno;
            try
            {
                retorno = _objDal.VALIDAR_USUARIO(obj);
            }
            catch (CustomException ce)
            {
                throw ce;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            return retorno;
        }
        #endregion METODOS
    }
}
