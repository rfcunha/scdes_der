using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Dao.SCDES;
using Entity.SCDES.Models;
using Entity.SCDES.Utils.Filtro;
using Entity.SCDES.Utils.Gdv;
using Suporte.Exceptions;
using Suporte.MsgBox;
using Suporte.Transaction;

namespace Business.SCDES
{
    public class tab_desapropriacao_departamento_bll
    {
        public int INSERT(tab_desapropriacao_departamento obj)
        {
            try
            {
                int retorno;

                ValidaInsert(obj);

                using (var scope = TransactionScopeUtils.CreateTransactionScope())
                {
                    using (var dao = new tab_desapropriacao_departamento_dao())
                    {
                        retorno = dao.INSERT(obj).dde_id;
                    }

                    scope.Complete();
                }

                return retorno;
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao salvar os dados do departamento respons�vel.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public bool UPDATE(tab_desapropriacao_departamento obj)
        {
            try
            {
                ValidaUpdate(obj);

                using (var scope = TransactionScopeUtils.CreateTransactionScope())
                {
                    using (var dao = new tab_desapropriacao_departamento_dao())
                    {
                        dao.UPDATE(obj);
                    }

                    scope.Complete();
                }

                return true;
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao alterar os dados do departamento respons�vel.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public bool DELETE(int id, string userDelete)
        {
            try
            {
                ValidaDelete(id);

                using (var scope = TransactionScopeUtils.CreateTransactionScope())
                {
                    var obj = FIND(id);
                    obj.userDelete = userDelete;
                    obj.dtDelete = DateTime.Now;

                    using (var dao = new tab_desapropriacao_departamento_dao())
                    {
                        dao.DELETE(obj);
                    }

                    scope.Complete();
                }

                return true;
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao excluir o departamento respons�vel.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public bool STATUS(int id, bool status, string userLastUpdate)
        {
            try
            {
                var obj = new tab_desapropriacao_departamento { dde_id = id, dde_ativo = status, userLastUpdate = userLastUpdate, dtLastUpdate = DateTime.Now };


                using (var scope = TransactionScopeUtils.CreateTransactionScope())
                {
                    using (var dao = new tab_desapropriacao_departamento_dao())
                    {
                        dao.UPDATE(obj, x => x.dde_ativo, x => x.userLastUpdate, x => x.dtLastUpdate);
                    }

                    scope.Complete();
                }

                return true;
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao " + (status ? "desativar" : "ativar") + " o departamento respons�vel.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public tab_desapropriacao_departamento FIND(int id)
        {
            try
            {
                using (var dao = new tab_desapropriacao_departamento_dao())
                {
                    return dao.FIND(id);
                }
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao obter os dados do departamento respons�vel.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public IEnumerable<tab_desapropriacao_departamento> FIND(Expression<Func<tab_desapropriacao_departamento, bool>> Where = null)
        {
            try
            {
                using (var dao = new tab_desapropriacao_departamento_dao())
                {
                    return dao.FIND(Where).ToList();
                }
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao pesquisar os dados do departamento respons�vel.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public List<tab_desapropriacao_departamento_gdv> PESQUISA_GDV(tab_desapropriacao_departamento_filtro filtro)
        {
            try
            {
                using (var dao = new tab_desapropriacao_departamento_dao())
                {
                    return dao.PESQUISA_GDV(filtro);
                }
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao pesquisar os dados do departamento respons�vel.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        #region | VALIDACAO |

        private void ValidaInsert(tab_desapropriacao_departamento obj)
        {
            if (string.IsNullOrEmpty(obj.dde_nome))
                throw new CustomException("- digite o nome.", bootStrapMessageType.TYPE_WARNING);

            using (var dao = new tab_desapropriacao_departamento_dao())
            {
                if (dao.VALIDATE(x => x.dde_nome == obj.dde_nome))
                    throw new CustomException("J� exite um departamento respons�vel cadastrado com este nome.", bootStrapMessageType.TYPE_WARNING);
            }
        }

        private void ValidaUpdate(tab_desapropriacao_departamento obj)
        {
            if (string.IsNullOrEmpty(obj.dde_nome))
                throw new CustomException("- digite o nome.", bootStrapMessageType.TYPE_WARNING);

            using (var dao = new tab_desapropriacao_departamento_dao())
            {
                if (dao.VALIDATE(x => x.dde_nome == obj.dde_nome && x.dde_id != obj.dde_id))
                    throw new CustomException("J� exite um departamento respons�vel cadastrado com este nome.", bootStrapMessageType.TYPE_WARNING);
            }
        }

        private void ValidaDelete(int id)
        {
            using (var dao = new tab_desapropriacao_dao())
            {
                if (dao.VALIDATE(x => x.dde_id == id))
                    throw new CustomException("N�o � possivel excluir este departamento respons�vel.</br></br>- existe uma ou mais desapropria��o-projeto associada a este departamento respons�vel.</br>", bootStrapMessageType.TYPE_WARNING);
            }
        }

        #endregion
    }
}
