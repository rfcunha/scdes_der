using System;
using System.Collections.Generic;
using System.Linq;
using Business.SCDES.DTO;
using Dao.SCDES;
using Entity.SCDES.Models;
using Suporte.Exceptions;
using Suporte.MsgBox;

namespace Business.SCDES
{
    public class tab_desapropriacao_empreendimento_bll
    {
        public List<tab_desapropriacao_empreendimento_list_dto> LISTAR_EMPREENDIMENTOS()
        {
            try
            {
                using (var dao = new tab_desapropriacao_empreendimento_dao())
                {
                    var demList = dao.LISTAR_EMPREENDIMENTOS();

                    var list = demList.GroupBy(d => d.dem_auto_decreto)
                        .Select(d => new tab_desapropriacao_empreendimento_list_dto()
                        {
                            dem_auto_decreto = d.Key,
                            dem_lote_list = d.Select(x => x.dem_lote).ToList()
                        }).ToList();

                    return list;
                }
            }
            catch (Exception e)
            {
                throw new CustomException("Erro: " + e, bootStrapMessageType.TYPE_DANGER);
            }
        }
    }
}
