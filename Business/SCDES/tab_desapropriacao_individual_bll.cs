using System;
using System.Data.Entity.Validation;
using System.Diagnostics.Eventing.Reader;
using Dao.SCDES;
using Entity.SCDES.Models;
using Entity.SCDES.Utils.Filtro;
using Suporte.Exceptions;
using Suporte.MsgBox;
using Entity.SCDES.Utils.Gdv;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using System.IO;
using System.Web;
using Suporte.Transaction;

namespace Business.SCDES
{
    public class tab_desapropriacao_individual_bll
    {
        public tab_desapropriacao_individual CREATE(string numAutoDecreto, short lote)
        {

            try
            {
                var filtro = new tab_desapropriacao_empreendimento_filtro()
                {
                    dem_auto_decreto = numAutoDecreto,
                    dem_lote = lote
                };

                var dem = new tab_desapropriacao_empreendimento();

                using (var dao = new tab_desapropriacao_empreendimento_dao())
                {
                    dem = dao.BUSCAR_EMPREENDIMENTO(filtro);
                }


                return new tab_desapropriacao_individual()
                {
                    des_id = dem.des_id,
                    din_guid = Guid.NewGuid().ToString(),
                    din_empreendimento_auto_decreto = filtro.dem_auto_decreto,
                    din_empreendimento_rodovia_nome = dem.dem_rodovia_nome,
                    din_empreendimento_rodovia_sp = dem.dem_rodovia_sp,
                    din_empreendimento_rodovia_trecho = dem.dem_rodovia_trecho,
                    din_empreendimento_dup_codigo = dem.dem_dup_codigo,
                    din_empreendimento_dup_dt = dem.dem_dup_dt,
                    din_empreendimento_lote = dem.dem_lote,
                    din_empreendimento_rodovia_km_final = dem.dem_rodovia_km_final,
                    din_empreendimento_rodovia_km_inicial = dem.dem_rodovia_km_inicial,
                    din_cadastro_dt = DateTime.Now,
                    dtCreate = DateTime.Now
                };


            }
            catch (Exception e)
            {
                throw ;
            }

        }

        public bool INSERT(tab_desapropriacao_individual desapropriacaoIndividual)
        {

            try
            {
                using (var dao = new tab_desapropriacao_individual_dao())
                {
                    dao.INSERT(SaveAttachments(desapropriacaoIndividual));
                }
            }
            catch (Exception e)
            {
                throw new CustomException("Erro: " + e, bootStrapMessageType.TYPE_DANGER);
            }

            return true;
        }

        private tab_desapropriacao_individual SaveAttachments(tab_desapropriacao_individual din)
        {

            try
            {
                // Salvar tab_desapropriacao_individual_anexos
                din.tab_desapropriacao_individual_anexos.Select(t => t.tab_anexo_identificador)
                    .Union(din.tab_desapropriacao_individual_divisa.Select(t => t.tab_anexo_identificador))
                    .Union(din.tab_desapropriacao_individual_entrada.Select(t => t.tab_anexo_identificador))
                    .Union(din.tab_desapropriacao_individual_instalacao.Select(t => t.tab_anexo_identificador))
                    .Union(din.tab_desapropriacao_individual_cultura.Select(t => t.tab_anexo_identificador))
                    .Union(din.tab_desapropriacao_individual_estrada_acesso.Select(t => t.tab_anexo_identificador))
                    .Union(din.tab_desapropriacao_individual_planta_propriedade.Select(t => t.tab_anexo_identificador))
                    .Union(din.tab_desapropriacao_individual_registro_fotografico.Select(t => t.tab_anexo_identificador))
                    .ToList().ForEach(item =>
                {
                    var folderPath = @"\Upload\Anexo\PROJETO\" + din.des_id + @"\" + item.aid_tipo + @"\" + din.din_guid + @"\" + item.aid_guia ;

                    if (!Directory.Exists(HttpContext.Current.Server.MapPath(@"~" + folderPath)))
                        Directory.CreateDirectory(HttpContext.Current.Server.MapPath(@"~" + folderPath));
                    
                    item.tab_anexo.Where(d => d.ane_id == 0).ToList().ForEach(t =>
                    {
                        t.ane_pasta =  folderPath + @"\" + t.ane_nome;
                        t.ane_arquivo.SaveAs(HttpContext.Current.Server.MapPath(@"~" + t.ane_pasta));
                    });
                });


                return din;
            }
            catch (Exception e)
            {
                throw new CustomException("Erro: " + e, bootStrapMessageType.TYPE_DANGER);
            }
        }

        public bool DELETAR(int id, string userDelete)
        {
            try
            {
                using (var dao = new tab_desapropriacao_individual_dao())
                {
                    return dao.DELETAR(id, userDelete);
                }
            }
            catch (Exception e)
            {
                throw new CustomException("Erro: " + e, bootStrapMessageType.TYPE_DANGER);
            }
        }


        public bool UPDATE(tab_desapropriacao_individual desapropriacaoIndividual)
        {

            try
            {

                using (var dao = new tab_desapropriacao_individual_dao())
                {
                    dao.UPDATE(SaveAttachments(desapropriacaoIndividual));
                }
            }
            catch (Exception e)
            {
                throw new CustomException("Erro: " + e, bootStrapMessageType.TYPE_DANGER);
            }


            return true;
        }

        public List<tab_desapropriacao_individual_gdv> PESQUISA_GDV(tab_desapropriacao_individual_filtro filtro)
        {
            try
            {
                using (var dao = new tab_desapropriacao_individual_dao())
                {
                    return dao.PESQUISA_GDV(filtro);
                }
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao carregar a lista de desapropriação.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public tab_desapropriacao_individual FIND(int id)
        {
            try
            {
                using (var dao = new tab_desapropriacao_individual_dao())
                {
                    return dao.FIND(id);
                }
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao obter os dados do departamento responsável.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }


    }
}
