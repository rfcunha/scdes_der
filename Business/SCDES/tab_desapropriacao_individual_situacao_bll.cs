using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Dao.SCDES;
using Entity.SCDES.Models;
using Entity.SCDES.Utils.Filtro;
using Entity.SCDES.Utils.Gdv;
using Suporte.Exceptions;
using Suporte.MsgBox;
using Suporte.Transaction;

namespace Business.SCDES
{
    public class tab_desapropriacao_individual_situacao_bll
    {
        public int INSERT(tab_desapropriacao_individual_situacao obj)
        {
            try
            {
                int retorno;

                ValidaInsert(obj);

                using (var scope = TransactionScopeUtils.CreateTransactionScope())
                {
                    using (var dao = new tab_desapropriacao_individual_situacao_dao())
                    {
                        retorno = dao.INSERT(obj).isi_id;
                    }

                    scope.Complete();
                }

                return retorno;
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao salvar os dados da situa��o da desapropria��o individual.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public bool UPDATE(tab_desapropriacao_individual_situacao obj)
        {
            try
            {
                ValidaUpdate(obj);

                using (var scope = TransactionScopeUtils.CreateTransactionScope())
                {
                    using (var dao = new tab_desapropriacao_individual_situacao_dao())
                    {
                        dao.UPDATE(obj);
                    }

                    scope.Complete();
                }

                return true;
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao alterar os dados da situa��o da desapropria��o individual.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public bool DELETE(int id, string userDelete)
        {
            try
            {
                ValidaDelete(id);

                using (var scope = TransactionScopeUtils.CreateTransactionScope())
                {
                    var obj = FIND(id);
                    obj.userDelete = userDelete;
                    obj.dtDelete = DateTime.Now;

                    using (var dao = new tab_desapropriacao_individual_situacao_dao())
                    {
                        dao.DELETE(obj);
                    }

                    scope.Complete();
                }

                return true;
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao excluir a situa��o da desapropria��o individual.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public bool STATUS(int id, bool status, string userLastUpdate)
        {
            try
            {
                var obj = new tab_desapropriacao_individual_situacao { isi_id = id, isi_ativo = status, userLastUpdate = userLastUpdate, dtLastUpdate = DateTime.Now };


                using (var scope = TransactionScopeUtils.CreateTransactionScope())
                {
                    using (var dao = new tab_desapropriacao_individual_situacao_dao())
                    {
                        dao.UPDATE(obj, x => x.isi_ativo, x => x.userLastUpdate, x => x.dtLastUpdate);
                    }

                    scope.Complete();
                }

                return true;
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao " + (status ? "desativar" : "ativar") + " a situa��o da desapropria��o individual.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public tab_desapropriacao_individual_situacao FIND(int id)
        {
            try
            {
                using (var dao = new tab_desapropriacao_individual_situacao_dao())
                {
                    return dao.FIND(id);
                }
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao obter os dados da situa��o da desapropria��o individual.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public IEnumerable<tab_desapropriacao_individual_situacao> FIND(Expression<Func<tab_desapropriacao_individual_situacao, bool>> Where = null)
        {
            try
            {
                using (var dao = new tab_desapropriacao_individual_situacao_dao())
                {
                    var list = dao.FIND(Where).ToList();
                    return list;
                }
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao pesquisar os dados da situa��o da desapropria��o individual.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public List<tab_desapropriacao_individual_situacao_gdv> PESQUISA_GDV(tab_desapropriacao_individual_situacao_filtro filtro)
        {
            try
            {
                using (var dao = new tab_desapropriacao_individual_situacao_dao())
                {
                    return dao.PESQUISA_GDV(filtro);
                }
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao pesquisar os dados da situa��o da desapropria��o individual.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        #region | VALIDACAO |

        private void ValidaInsert(tab_desapropriacao_individual_situacao obj)
        {
            if (string.IsNullOrEmpty(obj.isi_nome))
                throw new CustomException("- digite o nome.", bootStrapMessageType.TYPE_WARNING);

            using (var dao = new tab_desapropriacao_individual_situacao_dao())
            {
                if (dao.VALIDATE(x => x.isi_nome == obj.isi_nome))
                    throw new CustomException("J� exite uma situa��o cadastrada com este nome.", bootStrapMessageType.TYPE_WARNING);
            }
        }

        private void ValidaUpdate(tab_desapropriacao_individual_situacao obj)
        {
            if (string.IsNullOrEmpty(obj.isi_nome))
                throw new CustomException("- digite o nome.", bootStrapMessageType.TYPE_WARNING);

            using (var dao = new tab_desapropriacao_individual_situacao_dao())
            {
                if (dao.VALIDATE(x => x.isi_nome == obj.isi_nome && x.isi_id != obj.isi_id))
                    throw new CustomException("J� exite uma situa��o cadastrada com este nome.", bootStrapMessageType.TYPE_WARNING);
            }
        }

        private void ValidaDelete(int id)
        {
            using (var dao = new tab_desapropriacao_individual_dao())
            {
                if (dao.VALIDATE(x => x.din_conducao_isi_id == id))
                    throw new CustomException("N�o � possivel excluir esta situa��o.</br></br>- existe uma ou mais desapropria��o individual associada a esta situa��o.</br>", bootStrapMessageType.TYPE_WARNING);
            }
        }

        #endregion
    }
}
