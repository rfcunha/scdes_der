using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Dao.SCDES;
using Entity.SCDES.Utils.Filtro;
using Entity.SCDES.Utils.Gdv;
using Suporte.Exceptions;
using Suporte.MsgBox;

namespace Business.SCDES
{
    public class tab_audit_bll
    {
        public IEnumerable FIND_ALL_TABLENAME()
        {
            try
            {
                using (var dao = new tab_audit_dao())
                {
                    var lista = dao.FIND(null).ToList().Select(li => new { li.TableName }).Distinct();

                    return lista;
                }
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao pesquisar os dados.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public IEnumerable FIND_ALL_TYPE(string tableName)
        {
            try
            {
                using (var dao = new tab_audit_dao())
                {
                    return dao.FIND(audit => audit.TableName == tableName).ToList().Select(li => new { li.Type }).Distinct();
                }
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao pesquisar os dados.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public IEnumerable FIND_ALL_USERNAME(string tableName)
        {
            try
            {
                using (var dao = new tab_audit_dao())
                {
                    return dao.FIND(audit => audit.TableName == tableName).ToList().Select(li => new { li.UserName }).Distinct();
                }
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao pesquisar os dados.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public List<tab_audit_gdv> PESQUISA_GDV(tab_audit_filtro filtro)
        {
            try
            {
                using (var dao = new tab_audit_dao())
                {
                    return dao.PESQUISA_GDV(filtro);
                }
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao pesquisar os dados.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }
    }
}
