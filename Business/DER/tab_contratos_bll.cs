using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dao.DER;
using Entity.DER;
using Entity.DER.Utils.Filtro;
using Suporte.Exceptions;
using Suporte.MsgBox;

namespace Business.DER
{
    public class tab_contratos_bll
    {
        private readonly tab_contratos_dao _dao;

        public tab_contratos_bll()
        {
            _dao = new tab_contratos_dao();
        }

        public tab_contratos GET_BY(string numContr)
        {
            try
            {
                return _dao.GET_BY(numContr);
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao carregar os dados do Contrato.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }
    }
}
