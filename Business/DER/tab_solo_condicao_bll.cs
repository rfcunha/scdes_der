using System;
using System.Collections.Generic;
using Dao.DER;
using Entity.DER;
using Entity.DER.Utils.Filtro;
using Suporte.Exceptions;
using Suporte.MsgBox;

namespace Business.DER
{
    public class tab_solo_condicao_bll
    {
        private readonly tab_solo_condicao_dao _dao;

        public tab_solo_condicao_bll()
        {
            _dao = new tab_solo_condicao_dao();
        }

        public tab_solo_condicao FIND(int id)
        {
            try
            {
                return _dao.GET_BY(id);
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao carregar os dados da condi��o do solo.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public IEnumerable<tab_solo_condicao> FIND(tab_solo_condicao_filtro filtro)
        {
            try
            {
                return _dao.PESQUISA(filtro);
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao carregar a lista da condi��o do solo.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }
    }
}
