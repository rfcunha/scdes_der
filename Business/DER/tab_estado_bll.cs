﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dao.DER;
using Entity.DER;
using Entity.DER.Utils;
using Suporte.Exceptions;
using Suporte.MsgBox;

namespace Business.DER
{
    public class tab_estado_bll
    {
        private readonly tab_estado_dao _dao;

        public tab_estado_bll()
        {
            _dao = new tab_estado_dao();
        }

        public List<tab_estado> FIND(tab_estado_filtro filtro)
        {
            try
            {
                return _dao.PESQUISA(filtro);
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao carregar a lista de estados.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        //public List<tab_estado> FIND()
        //{
        //    try
        //    {
        //        var filtro = new tab_estado_filtro();

        //        return _dao.PESQUISA(filtro);
        //    }
        //    catch (CustomException)
        //    {
        //        throw;
        //    }
        //    catch (Exception)
        //    {
        //        throw new CustomException("Ocorreu um erro ao carregar a lista de estados.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
        //    }
        //}

        public tab_estado FIND(string est_uf)
        {
            try
            {
                var filtro = new tab_estado_filtro { est_uf = est_uf };

                return _dao.PESQUISA(filtro).FirstOrDefault();
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao carregar os dados do estado.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public tab_estado FIND(int id)
        {
            try
            {
                return _dao.GET_BY(id);
            }
            catch (CustomException ex)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao carregar os dados do estado.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }
    }
}
