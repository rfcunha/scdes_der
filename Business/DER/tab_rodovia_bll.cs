﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dao.DER;
using Entity.DER;
using Entity.DER.Utils.Filtro;
using Suporte.Exceptions;
using Suporte.MsgBox;

namespace Business.DER
{
    public class tab_rodovia_bll
    {
        private readonly tab_rodovia_dao _dao;

        public tab_rodovia_bll()
        {
            _dao = new tab_rodovia_dao();
        }

        public tab_rodovia GET_BY_RODOVIA(string rod_sp, decimal rod_km_inicial, decimal rod_km_final)
        {
            try
            {
                return _dao.GET_BY_RODOVIA(rod_sp, rod_km_inicial, rod_km_final);
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao carregar a lista das rodovias.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public IEnumerable<string> PESQUISA_RODOVIA_SP(string rod_sp)
        {
            try
            {
                return _dao.PESQUISA_RODOVIA_SP(rod_sp);
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao carregar a lista das sp's.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public IEnumerable<tab_rodovia> PESQUISA_RODOVIA(tab_rodovia_filtro filtro)
        {
            try
            {
                return _dao.PESQUISA_RODOVIA(filtro);
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao carregar a lista das rodovias.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }
    }
}
