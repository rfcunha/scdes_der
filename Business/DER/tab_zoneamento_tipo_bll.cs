using System;
using System.Collections.Generic;
using Dao.DER;
using Entity.DER;
using Entity.DER.Utils.Filtro;
using Suporte.Exceptions;
using Suporte.MsgBox;

namespace Business.DER
{
    public class tab_zoneamento_tipo_bll
    {
        private readonly tab_zoneamento_tipo_dao _dao;

        public tab_zoneamento_tipo_bll()
        {
            _dao = new tab_zoneamento_tipo_dao();
        }

        public tab_zoneamento_tipo FIND(int id)
        {
            try
            {
                return _dao.GET_BY(id);
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao carregar os dados do tipo de zoneamento.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public IEnumerable<tab_zoneamento_tipo> FIND(tab_zoneamento_tipo_filtro filtro)
        {
            try
            {
                return _dao.PESQUISA(filtro);
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao carregar a lista de tipo de zoneamento.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }
    }
}
