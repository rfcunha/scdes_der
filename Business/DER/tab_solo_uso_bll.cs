using System;
using System.Collections.Generic;
using Dao.DER;
using Entity.DER;
using Entity.DER.Utils.Filtro;
using Suporte.Exceptions;
using Suporte.MsgBox;

namespace Business.DER
{
    public class tab_solo_uso_bll
    {
        private readonly tab_solo_uso_dao _dao;

        public tab_solo_uso_bll()
        {
            _dao = new tab_solo_uso_dao();
        }

        public tab_solo_uso FIND(int id)
        {
            try
            {
                return _dao.GET_BY(id);
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao carregar os dados do uso do solo.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public IEnumerable<tab_solo_uso> FIND(tab_solo_uso_filtro filtro)
        {
            try
            {
                return _dao.PESQUISA(filtro);
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao carregar a lista do uso do solo.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }
    }
}
