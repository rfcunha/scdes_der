﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dao.DER;
using Entity.DER;
using Entity.DER.Utils.Filtro;
using Suporte.Exceptions;
using Suporte.MsgBox;

namespace Business.DER
{
    public class tab_regional_bll
    {
        private readonly tab_regional_dao _dao;

        public tab_regional_bll()
        {
            _dao = new tab_regional_dao();
        }

        public tab_regional FIND(int id)
        {
            try
            {
                return _dao.GET_BY(id);
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao carregar os dados da regional.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public IEnumerable<tab_regional> FIND(tab_regional_filtro filtro)
        {
            try
            {
                return _dao.PESQUISA(filtro);
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao carregar a lista das regionais.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }
    }
}
