using System;
using System.Collections.Generic;
using Dao.DER;
using Entity.DER;
using Entity.DER.Utils.Filtro;
using Suporte.Exceptions;
using Suporte.MsgBox;

namespace Business.DER
{
    public class tab_comarca_bll
    {
        private readonly tab_comarca_dao _dao;

        public tab_comarca_bll()
        {
            _dao = new tab_comarca_dao();
        }

        public tab_comarca FIND(int id)
        {
            try
            {
                return _dao.GET_BY(id);
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao carregar os dados da comarca.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public IEnumerable<tab_comarca> FIND(tab_comarca_filtro filtro)
        {
            try
            {
                return _dao.PESQUISA(filtro);
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao carregar a lista das comarcas.</br>Entre em contato com  o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }
    }
}
