﻿var onBeginLogin = function () {
    waitingDialog.show('Verificando autenticidade...', { dialogSize: 'sm' });
};
var onSuccessLogin = function (result) {
    waitingDialog.hide();
};
var onFailureLogin = function () {
    waitingDialog.hide();
    //Mensagens.Flutuante('Erro', 'Falha na tentativa de logar', 'error');
    Mensagens.Modal('Erro', 'Ocorreu um erro ao acessar o sistema.<br/>Entre em contato com o suporte.', BootstrapDialog.TYPE_DANGER);
};


var onBeginSenha = function () { 
    waitingDialog.show('Verificando autenticidade...', { dialogSize: 'sm' });
};
var onSuccessSenha = function (result) { 
    waitingDialog.hide();
};
var onFailureSenha = function () {
    waitingDialog.hide();
    //Mensagens.Flutuante('Erro', 'Falha na tentativa de logar', 'error');
    Mensagens.Modal('Erro', 'Ocorreu um erro ao acessar o sistema.<br/>Entre em contato com o suporte.', BootstrapDialog.TYPE_DANGER);
};

function verificarSenhasIguais() {
    $("#form_nova_senha").submit(function (e) {
        var _erro = "";
        if ($("#txtSenhaNova").val() == "") {
            _erro += "- digite a nova senha.<br/>";
        }
        if ($("#txtSenhaNovaConfirmar").val() == "") {
            _erro += "- digite a confirmação de senha.<br/>";
        }

        if ($("#txtSenhaNova").val().length < 8) {
            _erro += "- nova senha deve possuir no minimo 8 caracteres.<br/>";
        }

        if (!($("#txtSenhaNovaConfirmar").val() === $("#txtSenhaNova").val())) {
            _erro += "- o campo confirmação de senha deve ser identico ao campo nova senha.<br/>";
        }

        if ($("#txtSenhaNova").val() === $("#usu_senha_hidden").val()) {
            _erro += "- a senha atual não pode ser identica ao campo nova senha.<br/>";
        } 

        if (_erro) {
            Mensagens.Modal("Atenção", _erro, BootstrapDialog.TYPE_WARNING);
            e.preventDefault();
            e.stopImmediatePropagation();
            return false;
        } else {
            return true;
        }
    });
}


$(document).ready(function () {
    Requirido();
    validaFormulario("#form_login","#form_nova_senha"); 
    verificarSenhasIguais();
});
 