﻿var timeout = 60 * 60 * 1000;
var timer = setInterval(
            function () {
                timeout -= 1000;
                window.status = ContadorSessao(timeout);

//                console.log(window.status);
//                console.log(timeout);

                if (timeout == 0) { clearInterval(timer); }
            }, 1000
        );

function ContadorSessao(ms) {
    var sec = Math.floor(ms / 1000);
    var min = Math.floor(sec / 60);
    sec = sec % 60;
    var hr = Math.floor(min / 60);
    min = min % 60;
    if (hr == 0 && min == 0 && sec == 0) { MensagemLogin('show'); };
}

function MensagemLogin(exibirModal) {
    $("#div_login").modal(exibirModal);
    if (exibirModal == 'hide') {
        timeout = 60 * 60 * 1000;
        var timer = setInterval(
            function () {
                timeout -= 1000;
                window.status = ContadorSessao(timeout);
                if (timeout == 0) { clearInterval(timer); }
            }, 1000
        );
    }
}
function IniciarMaster() {
    
}

var onBeginLogin = function () {
    waitingDialog.show('Verificando autenticidade...', { dialogSize: 'sm' });
};

var onSuccessLogin = function (result) {
    waitingDialog.hide();
};

var onFailureLogin = function () {
    waitingDialog.hide();
    //Mensagens.Flutuante('Erro', 'Falha na tentativa de logar', 'error');
};


var onBeginAlterarSenha = function () {
    waitingDialog.show('Alterando a senha...', { dialogSize: 'sm' });
};
var onSuccessAlterarSenha = function (result) {
    waitingDialog.hide();
};
var onFailureAlterarSenha = function (result) {
    waitingDialog.hide();
    Mensagens.Flutuante('Erro', 'Falha na de alterar a senha', 'error');
};




function verificarSenhasIguais() {
    $("#form_alterar_senha").submit(function (e) {
        var _erro = "";
        if ($("#txtSenhaNova").val() == "") {
            _erro += "- digite a nova senha.<br/>";
        }
        if ($("#txtSenhaNovaConfirmar").val() == "") {
            _erro += "- digite a confirmação de senha.<br/>";
        }

        if ($("#txtSenhaNova").val().length < 8) {
            _erro += "- nova senha deve possuir no minimo 8 caracteres.<br/>";
        }

        if (!($("#txtSenhaNovaConfirmar").val() === $("#txtSenhaNova").val())) {
            _erro += "- o campo confirmação de senha deve ser identico ao campo nova senha.<br/>";
        }

        if ($("#txtSenhaNova").val() === $("#usu_senha_hidden").val()) {
            _erro += "- a senha atual não pode ser identica ao campo nova senha.<br/>";
        }

        if (_erro) {
            Mensagens.Modal("Atenção", _erro, BootstrapDialog.TYPE_WARNING);
            e.preventDefault();
            e.stopImmediatePropagation();
            return false;
        } else {
            return true;
        }
    });
}

function ValidaLogin() {

    $("#btnLogin").click(function (e) {

        var msg = "";
        var isvalid = true;

        if ($("#usu_login").val().trim().length === 0) { msg += "- digite o usuário.<br/>"; isvalid = false; }
        if ($("#usu_senha").val().trim().length === 0) { msg += "- digite a senha.<br/>"; isvalid = false; }


        if (!isvalid) {
            Mensagens.Modal("Atenção", msg, BootstrapDialog.TYPE_WARNING);
            e.preventDefault();
        
                    } else {

                $.ajax({
                    url: "~/../../../MasterPage/Login",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify({
                        usu_login: $('#usu_login').val(),
                        usu_senha: $('#usu_senha').val()
                    }),
                    success: function (result) {

                        MensagemLogin('hide');
                    },
                    error: function (result) {
                        //alert(result);
                    }
                });
            }

        return isvalid;
    });
}

function ValidaTrocaSenha() {

    $("#btnAlterarSenha").click(function (e) {

        var msg = "";
        var isvalid = true;

        if ($("#txtSenhaNova").val().trim().length === 0) { msg += "- digite a nova senha.<br/>"; isvalid = false; }
        if ($("#txtSenhaNovaConfirmar").val().trim().length === 0) { msg += "- digite confirmar senha.<br/>"; isvalid = false; }


        if (!isvalid) {
            Mensagens.Modal("Atenção", msg, BootstrapDialog.TYPE_WARNING);
            e.preventDefault();
        }

        return isvalid;
    });
}

function ajaxError(xhr) {

    var err = eval("(" + xhr.responseText + ")");
    //alert(err.Message);
    Mensagens.Modal("Erro", err.Message, BootstrapDialog.TYPE_DANGER);
}


$(document).ready(function () {
    //Requirido();
    validaFormulario("#form_login", "#form_alterar_senha");
    verificarSenhasIguais();
    IniciarMaster();
    ValidaLogin();
    ValidaTrocaSenha();
});
 