﻿var onBegin = function () {
    waitingDialog.show('Processando...', { dialogSize: 'sm' });
};
var onSuccess = function (result) {
    limpar.Form("#form_cadastro");
    waitingDialog.hide();
};
var onFailure = function () {
    waitingDialog.hide();
    //Mensagens.Flutuante('Erro', 'Houve uma falha ao cadastrar os dados. <br/>Entre em contato com suporte.', 'error');
};


function helpOnline() {
    var intro = introJs();
    intro.exit(); 
        intro.setOptions({
            steps: [
                {
                    element: '#form_auxiliar',
                    intro: "Para salvar um departamento responsável preencha todos <b>campos</b> que contêm <font color='red'>*</font>."
                }, 
                {
                    element: '#contentePlace_ucCadastro_btnSalvar',
                    intro: "Ok, clique em <b>CADASTRAR</b>",
                    position: 'right'
                },
                {
                    element: '#contentePlace_ucCadastro_btnLimpar',
                    intro: "Para limpar todos os campos do formulário clique em <b>LIMPAR</b>",
                    position: 'right'
                },
                {
                    element: '#contentePlace_ucCadastro_btnConsultar',
                    intro: "Clique em <b>PESQUISAR</b> para pesquisar os departamentos responsáveis cadastrados",
                    position: 'right'
                }
            ]
        }); 
    intro.start();
}

function Limpar() { 
    $("#btnLimpar").click(function(e) { 
        limpar.Form("#form_cadastro"); 
    }); 
}

function Iniciar() { 
    validaFormulario("#form_cadastro"); 
    DropDownAutoComplete.Ini();
}
 

 jQuery(document).ready(function () {
    Iniciar();
    Limpar();
    Mask.Ini();
    Requirido();
 });  