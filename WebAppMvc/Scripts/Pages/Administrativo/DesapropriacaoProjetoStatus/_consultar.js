﻿var columnDefs = [ 
       {
           render: function (data, type, row, dadosLinha) {
               if (row["dst_ativo"] === true) {
                   return   '<span title="Editar" class="fa fa-pencil-square-o OpcaoGrid"  id="btnEditar" data-roles="true" style="margin:0 2px 0 2px; cursor:pointer" onclick="window.location.href=\'Editar?args=' + row["args"] + '\'"></span>' +
                            '<span title="Desativar" class="fa fa-check text-success OpcaoGrid" id="btnStatus" data-roles="true" style="margin:0 2px 0 2px; cursor:pointer" href="Status/" data-id="' + row["dst_id"] + '" data-status="false"  onclick="Mensagens.ModalStatus(this, \'Deseja desativar este item?\');"></span> ' +
                            '<span title="Deletar" class="fa fa-trash"  id="btnDeletar" OpcaoGrid" data-roles="true" href="Deletar/" data-id="' + row["dst_id"] + '" style="margin:0 2px 0 2px; cursor:pointer" onclick="Mensagens.ModalDelete(this,\'Deseja realmente deletar este item?\');"></span>'; 
                           
               } else {
                   return   '<span title="Editar" class="fa fa-pencil-square-o text-danger OpcaoGrid" id="btnEditar" data-roles="true" style="margin:0 2px 0 2px; cursor:pointer"></span> ' +
                            '<span title="Ativar" class="fa fa-remove text-danger OpcaoGrid" id="btnStatus" data-roles="true" style="margin:0 2px 0 2px; cursor:pointer" href="Status/" data-id="' + row["dst_id"] + '" data-status="true"  onclick="Mensagens.ModalStatus(this,\'Deseja ativar este item?\');"></span> ' +
                            '<span title="Deletar" class="fa fa-trash"  id="btnDeletar" OpcaoGrid" data-roles="true" href="Deletar/" data-id="' + row["dst_id"] + '" style="margin:0 2px 0 2px; cursor:pointer" onclick="Mensagens.ModalDelete(this,\'Deseja realmente deletar este item?\');"></span>'; 
               }

           },
           width: "80px",
           targets: [3]
       },
       {
           render: function (data, type, row, dadosLinha) {
               return row["dst_ativo"] === true ? "Ativo" : "Inativo";
           },
           width: "80px",
           targets: [1]
       } 
];

var columns = [ 
    { data: "dst_nome", title: "Nome" },
    { data: "dst_ativo", title: "Status" },
    { data: "dst_cadastro_dt", title: "Cadastro" },  
    { data: null, title: "Opções"}];   



function Status(controle) {
    $.ajax({
        type: "POST",
        url: '~/../' + jQuery(controle).attr("href"),
        data: { __RequestVerificationToken: $('input[name="__RequestVerificationToken"').val(), id: jQuery(controle).attr("data-id"), status: jQuery(controle).attr("data-status") },
        dataType: "html",
        cache: false,
        success: function (data) {
            eval(data);
            CriarTabela();
        }, beforeSend: function () {

        },
        error: function (xhr) {  
            eval(xhr.responseText);
        }
    });
}

function Delete(controle) {
    $.ajax({
        type: "POST",
        url: '~/../' + jQuery(controle).attr("href"),
        data: { __RequestVerificationToken: $('input[name="__RequestVerificationToken"').val(), id: jQuery(controle).attr("data-id") },
        dataType: "html",
        cache: false,
        success: function (data) {
            eval(data);
            CriarTabela();
        }, beforeSend: function () {

        },
        error: function (xhr) { // if error occured 
            eval(xhr.responseText);
        }
    });
}


function Iniciar() {  
   $("#btnPesquisar").click(function(e) {
       CriarTabela();
   });  

         $("#btnLimpar").click(function(e) {
          limpar.Form('#form_consulta');
      });

   DropDownAutoComplete.Ini();
} 


function CriarTabela() {
    Grids.MontarDataTable("#table_consulta", "~/../../../Administrativo/DesapropriacaoProjetoStatus/Consultar", columnDefs, columns);

    $("#divGrid").css({ "display": "inline" });
}

function helpOnline() {
    var intro = introJs();
    intro.exit(); 
        intro.setOptions({
            steps: [
                {
                    element: '#form_auxiliar',
                    intro: "Para salvar um sistema preencha todos <b>campos</b> que contêm <font color='red'>*</font>.",
                }, 
                {
                    element: '#contentePlace_ucCadastro_btnSalvar',
                    intro: "Ok, clique em <b>CADASTRAR</b>",
                    position: 'right'
                },
                {
                    element: '#contentePlace_ucCadastro_btnLimpar',
                    intro: "Para limpar todos os campos do formulário clique em <b>LIMPAR</b>",
                    position: 'right'
                },
                {
                    element: '#contentePlace_ucCadastro_btnConsultar',
                    intro: "Clique em <b>PESQUISAR</b> para gerar uma  pesquisa",
                    position: 'right'
                },
                {
                    element: '#btnNovoConsultar',
                     intro: "Clique em <b>VOLTAR</b> para voltar a tela anterior",
                    position: 'right'
                }
            ]
        }); 
    intro.start();
}


 
 

 jQuery(document).ready(function () {
    Iniciar();
    Requirido();  
 });  