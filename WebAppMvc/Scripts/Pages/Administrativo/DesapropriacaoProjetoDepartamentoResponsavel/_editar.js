﻿var onBegin = function () {
    waitingDialog.show('Processando...', { dialogSize: 'sm' });
};
var onSuccess = function (result) {
    limpar.Form("#form_editar"); 
    waitingDialog.hide();
};
var onFailuren = function () {
    waitingDialog.hide();
    Mensagens.Flutuante('Erro', 'Houve uma falha ao alterar os dados. <br/>Entre em contato com suporte.', 'error');
};



function helpOnline() {
    var intro = introJs();
    intro.exit(); 
        intro.setOptions({
            steps: [
                {
                    element: '#form_auxiliar',
                    intro: "Para alterar os dados de um departamento responsável alterar algum dos <b>campos</b> que contêm <font color='red'>*</font>.",
                }, 
                {
                    element: '#contentePlace_ucCadastro_btnSalvar',
                    intro: "Ok, clique em <b>ALTERAR</b>",
                    position: 'right'
                },
                {
                    element: '#contentePlace_ucCadastro_btnLimpar',
                    intro: "Para limpar todos os campos do formulário clique em <b>LIMPAR</b>",
                    position: 'right'
                },
                {
                    element: '#contentePlace_ucCadastro_btnConsultar',
                    intro: "Clique em <b>PESQUISAR</b> para pesquisar os departamentos responsáveis cadastrados",
                    position: 'right'
                }
            ]
        }); 
    intro.start();
}

function Limpar() { 
    $("#btnLimpar").click(function(e) { 
        limpar.Form("#form_editar"); 
    }); 
}

function Iniciar() { 
    validaFormulario("#form_editar");
}
 

 jQuery(document).ready(function () {
    Iniciar(); 
    Limpar();
    Requirido();
 });  