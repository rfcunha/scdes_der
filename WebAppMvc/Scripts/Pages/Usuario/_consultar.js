﻿var columnDefs = [ 
       {
           render: function (data, type, row, dadosLinha) {
               if (row["usu_ativo"]==true) {
                   return '<span class="fa fa-pencil OpcaoGrid"  id="btnEditar" data-roles="true" title="Editar"  style="cursor:pointer" onclick="window.location.href=\'Editar?args=' + row["args"] + '\'"></span>' +
                       ' <span class="fa fa-check text-success OpcaoGrid" id="btnStatus" data-roles="true" style="cursor:pointer" href="Status/" data-id="' + row["usu_id"] + '" data-status="false"  onclick="Mensagens.ModalStatus(this, \'Deseja desativar este item?\');"></span> ' +
                       ' <span class="fa fa-trash"  id="btnDeletar" OpcaoGrid" data-roles="true" href="Deletar/" data-id="' + row["usu_id"] + '" style="cursor:pointer" onclick="Mensagens.ModalDelete(this,\'Deseja remover este item?\');"></span>'; 
                           
               } else {
                   return ' <span class="fa fa-pencil text-danger OpcaoGrid" id="btnEditar" data-roles="true" title="Editar"></span> ' +
                          ' <span class="fa fa-remove text-danger OpcaoGrid" id="btnStatus" data-roles="true"  style="cursor:pointer" href="Status/" data-id="' + row["usu_id"] + '" data-status="true"  onclick="Mensagens.ModalStatus(this,\'Deseja ativar este item?\');"></span> ' +
                          ' <span class="fa fa-trash OpcaoGrid" id="btnDeletar" data-roles="true"   href="Deletar/" data-id="' + row["usu_id"] + '"  style="cursor:pointer" onclick="Mensagens.ModalDelete(this,=\'Deseja remover este item?\');"></span>';
               }

           },
           width: "80px",
           targets: [3]
       },
       {
           render: function (data, type, row, dadosLinha) { 
               if (row["usu_ativo"]==true) {
                   return 'Ativo';
               } else {
                   return 'Inativo';
               }
           },
           width: "80px",
           targets: [2]
       } 
];

var columns = [ 
    { data: "usu_nome", title: "Nome" },
    { data: "usu_login", title: "Login" }, 
    { data: "usu_ativo", title: "Status" },  
    { data: null, title: "Opções"}];   



function Status(controle) {
    $.ajax({
        type: "POST",
        url: '~/../' + jQuery(controle).attr("href"),
        data: { __RequestVerificationToken: $('input[name="__RequestVerificationToken"').val(), id: jQuery(controle).attr("data-id"), status: jQuery(controle).attr("data-status") },
        dataType: "html",
        cache: false,
        success: function (data) {
            eval(data);
            CriarTabela();
        }, beforeSend: function () {

        },
        error: function (xhr) {  
            eval(xhr.responseText);
        }
    });
}

function Delete(controle) {
    $.ajax({
        type: "POST",
        url: '~/../' + jQuery(controle).attr("href"),
        data: { __RequestVerificationToken: $('input[name="__RequestVerificationToken"').val(), id: jQuery(controle).attr("data-id") },
        dataType: "html",
        cache: false,
        success: function (data) {
            eval(data);
            CriarTabela();
        }, beforeSend: function () {

        },
        error: function (xhr) { // if error occured 
            eval(xhr.responseText);
        }
    });
}


function Iniciar() {  
   $("#btnPesquisar").click(function(e) {
       CriarTabela();
   });  
   DropDownAutoComplete.Ini();
   CriarTabela(); 
} 


function CriarTabela() {
     Grids.MontarDataTable("#table_consulta", "/Administrativo/Usuario/PostConsulta",columnDefs,columns);
     
}

function helpOnline() {
    var intro = introJs();
    intro.exit(); 
        intro.setOptions({
            steps: [
                {
                    element: '#form_auxiliar',
                    intro: "Para salvar um sistema preencha todos <b>campos</b> que contêm <font color='red'>*</font>.",
                }, 
                {
                    element: '#contentePlace_ucCadastro_btnSalvar',
                    intro: "Ok, clique em <b>CADASTRAR</b>",
                    position: 'right'
                },
                {
                    element: '#contentePlace_ucCadastro_btnLimpar',
                    intro: "Para limpar todos os campos do formulário clique em <b>LIMPAR</b>",
                    position: 'right'
                },
                {
                    element: '#contentePlace_ucCadastro_btnConsultar',
                    intro: "Clique em <b>PESQUISAR</b> para gerar uma  pesquisa",
                    position: 'right'
                },
                {
                    element: '#btnNovoConsultar',
                     intro: "Clique em <b>VOLTAR</b> para voltar a tela anterior",
                    position: 'right'
                }
            ]
        }); 
    intro.start();
}


 
 

 jQuery(document).ready(function () {
    Iniciar();
    Requirido();  
 });  