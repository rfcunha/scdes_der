﻿var onBegin = function () {
    waitingDialog.show('Alterando...', { dialogSize: 'sm' });
};
var onSuccess = function (result) {
    waitingDialog.hide();
};
var onFailuren = function () {
    waitingDialog.hide();
    Mensagens.Flutuante('Erro', 'Falha na tentativa de editar', 'error');
};



function helpOnline() {
    var intro = introJs();
    intro.exit(); 
        intro.setOptions({
            steps: [
                {
                    element: '#form_auxiliar',
                    intro: "Para salvar um sistema preencha todos <b>campos</b> que contêm <font color='red'>*</font>.",
                }, 
                {
                    element: '#contentePlace_ucCadastro_btnSalvar',
                    intro: "Ok, clique em <b>CADASTRAR</b>",
                    position: 'right'
                },
                {
                    element: '#contentePlace_ucCadastro_btnLimpar',
                    intro: "Para limpar todos os campos do formulário clique em <b>LIMPAR</b>",
                    position: 'right'
                },
                {
                    element: '#contentePlace_ucCadastro_btnConsultar',
                    intro: "Clique em <b>PESQUISAR</b> para gerar uma  pesquisa",
                    position: 'right'
                },
                {
                    element: '#btnNovoConsultar',
                     intro: "Clique em <b>VOLTAR</b> para voltar a tela anterior",
                    position: 'right'
                }
            ]
        }); 
    intro.start();
}

function Limpar() { 
    $("#btnLimpar").click(function(e) { 
        limparForm("#form_editar"); 
    }); 
}

function Iniciar() { 
    validaFormulario("#form_editar");
    DropDownAutoComplete.Ini();
}
 

 jQuery(document).ready(function () {
    Iniciar(); 
    Limpar();
 });  