var Mensagens = {
    Flutuante: function (titulo, mensagem, type) {
        var icon = "";
        switch(type) {
            case 'error':
                icon = 'fa fa-exclamation';
                break;
            case 'info':
                icon = 'fa fa-info';
                break;
            case 'warning':
                icon = 'fa fa-exclamation-triangle';
                break;
            case 'success':
                icon = 'glyphicon glyphicon-ok';
                    break;
        }
        $.growl({
            title: ' ' + titulo,
            icon: icon,
            message: '<br>' + mensagem,
        }, {
            type: type,
            delay: 2000,
            icon_type: 'class',
            placement: { 
                align: "left"
            },
            animate: {
                enter: 'animated rollIn',
                exit: 'animated rollOut'
            }
        });
    },
    Modal: function (titulo, mensagem, type) {
            //BootstrapDialog.TYPE_DEFAULT = 'type-default';
            //BootstrapDialog.TYPE_INFO = 'type-info';
            //BootstrapDialog.TYPE_PRIMARY = 'type-primary';
            //BootstrapDialog.TYPE_SUCCESS = 'type-success';
            //BootstrapDialog.TYPE_WARNING = 'type-warning';
            //BootstrapDialog.TYPE_DANGER = 'type-danger'; 
        BootstrapDialog.show({
                    closable: false,
                    title: titulo,
                    message: mensagem,
                    type: type,
                    buttons: [{
                        id: "btn-ok",
                        icon: "glyphicon glyphicon-check",
                        label: "OK",
                        cssClass: "btn btn-default active",
                        autospin: false,
                        action: function (dialogRef) {
                            dialogRef.close();
                        }
                    }]
                });
     },
    ModalStatus: function (controle,mensagem) {  
                BootstrapDialog.show({
                    title: "<b>Confirma&ccedil;&atilde;o</b>",
                    message: mensagem,
                    type: BootstrapDialog.TYPE_PRIMARY,
                    buttons: [
                        {
                            label: "N&atilde;o",
                            cssClass: "btn btn-default active",
                            action: function(dialog) {
                                dialog.close();
                            }
                        },
                        {
                            label: "Sim",
                            cssClass: "btn btn-default active",
                            action: function(dialog) {
                                dialog.close();
                                Status(controle);
                            }
                        }
                    ]
                }); 
    },

    ModalDelete: function (controle, mensagem) {  
            BootstrapDialog.show({
                title: "<b>Confirma&ccedil;&atilde;o</b>",
                message: mensagem,
                type: BootstrapDialog.TYPE_PRIMARY,
                buttons: [
                    {
                        label: "N&atilde;o",
                        cssClass: "btn btn-default active",
                        action: function(dialog) {
                            dialog.close();
                        }
                    },
                    {
                        label: "Sim",
                        cssClass: "btn btn-default active",
                        action: function (dialog) {
                            $("input[name=hfExcluir]").val("true");
                            dialog.close();
                            Delete(controle);
                        }
                    }
                ]
            }); 
    },
    ModalCancelar: function (controle) {
        var titulo = jQuery(controle).attr("alt");
        var mensagem = jQuery(controle).attr("title");
        $.msgbox({
            type: 'error',
            title: titulo,
            content: mensagem,
            width: 437,
            height: 200,
            buttons: ['Sim', 'Cancelar'],
            buttonEvents: {
                'Sim': function () {
                    $.msgbox.closeAll();
                    Cancelar(controle);
                },
                'Cancelar': function () {
                    $.msgbox.closeAll();
                }
            },
            id: 'valida' + $.now()
        });
    }
}; 

var Grids = {
    MontarDataTable: function(id, url, columnDefs, columns) {
        if (typeof table !== "undefined") { 
            table.DataTable().destroy();
        }
        table = $(id).dataTable({
            language: {
                url: "~/../../../Scripts/Jquery.Datatable/pt-BR.txt"
            }, 
            serverSide: true,
            searching: false,
            responsive: true,
            ordering: false, 
            bAutoWidth: false,
            bProcessing: true,
            dom: '<"row toolbar" <"col-md-6 left"><"col-md-6 right">>'+
                 '<"datatable-container"t<"processing-datatable"<"processing-datatable-overflow"r>>>'+
                 '<"footer-datatable"<"row"<"col-md-7"<"pull-left"p><"pull-left"l>><"col-md-5"<"pull-right bold"i>>>>', 
            sPaginationType: "extStyle",
            //Scrolling-------------- 
            sScrollX: "100%",
            sScrollXInner: "100%",
            bScrollCollapse: true,
            ajax: function (data, callback, settings) {
                $.ajax({
                    url: url,
                    type: "POST",
                    data: Grids.datatableAjaxData(data) 
                }).done(function (data, textStatus, jqXHR) { 
                        callback(data);
                        Grids.verificarRetornoEdicao();
                });
            },
            columnDefs: columnDefs,
            columns: columns
        }); 
        table.on('draw.dt', function () { 
           setTimeout('permission_roles_client();',0);
           setTimeout('Grids.verificarColuna()',100);   
        }); 
    },
     datatableAjaxData: function (dataTable) { 
        var form = $('form.search'); 
        var formulario = form.serialize() + "&pageIndex=" + (table.DataTable().page.info().page+1) + "&pageSize=" + dataTable.length;  
        return formulario;
    }, 
    verificarColuna: function () {
        if (!$('.OpcaoGrid').is(':visible')) {
              table.DataTable().columns(table.DataTable().columns().data().length - 1).visible(false);  
        }
    },
    verificarRetornoEdicao: function() {
        if ($("#tamanhoPagina").val() != "1") {  
             table._fnLengthChange($("#tamanhoCombo").val(),true);
             table.fnPageChange(($("#tamanhoPagina").val() - 1),true); 
            $("#tamanhoPagina").val("1");
            $("#tamanhoCombo").val("10");
        }    
    }
}  

var FuncoesHelp = {
    abrirHelp: function (page) {
        window.open(page, "", "width=900,height=700,scrollbars=no,status=yes,menubar=no,toolbar=no,location=no,directories=no");
    },
    disableFunctionKeys: function (e) {
        //F5 -- 116
        var functionKeys = new Array(112, 113, 114, 115, 117, 118, 119, 120, 121);
        if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {
            e.preventDefault();
            var key = (e.keyCode ? e.keyCode : e.charCode);
            switch (key) {
                case 112:
                    helpOnline();
                    break;
                default:;
            }
        }
    },
    resolucaoMonitor: function() {
           var tam = $(window).width(); 
           if (tam > 1024 ) {
              $("body").removeClass("sidebar-collapse");
           }else{
              $("body").addClass("sidebar-collapse");
           } 
    }
}
 
var Money = {
    getMoney: function(str) {
        return parseInt(str.replace(/[\D]+/g, ''));
    },
    formatReal: function (int) {
        var tmp = int + '';
        tmp = tmp.replace(/([0-9]{2})$/g, ",$1");
        if (tmp.length > 6)
            tmp = tmp.replace(/([0-9]{3}),([0-9]{2}$)/g, ".$1,$2"); 
        return tmp;
    }
}

var DropDownAutoComplete = {
    Ini: function () {
        try {
            $("select[name!='table_consulta_length']").addClass('chosen-select');
            $('.chosen-select').chosen({disable_search_threshold: 10});
            $('.chosen-container').css("width", "100%");

        } catch (e) {
           console.log('Falha ao iniciar componente chosen. Verifique se h� referencias.');
             throw e;
       }
    },
    Upd: function() {

        $('select').trigger("chosen:updated");

    }
}

var Autocomplete = {
    Ini: function (field, url, hiddenField) {
        $(field).typeahead({
            autoSelect: true,
            minLength: 3,
            delay: 400,
            source: function (query, process) {
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: url,
                    data: JSON.stringify({ query: query }),
                    dataType: 'json',
                    async: false
                })
                .done(function (data) { 
                    return process(data.d);
                });
            }
        });

        $(field).change(function () {
            var current = $(field).typeahead("getActive");
            if (current) {
                if (current.name == $(field).val()) {
                    $(hiddenField).val(current.id);

                } else {
                    $(hiddenField).val('');
                    $(field).val('');
                }
            } else {
                console.log('Nothing is active so it is a new value (or maybe empty value)');
            }
        });
    }
}

var Guid = {
    Generate: function () {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    }
}

var ScroolBar = {
    PerfectScroolbar: function() {
        for (i = 0; i < arguments.length; i++) {
           $(arguments[i]).perfectScrollbar();
        }
    }
};

var ShowModalPopUp = {
    VisualizarDetalhes: function(title, controller, id) {

        waitingDialog.show("Carregando...", { dialogSize: "sm" });

        var divId = $('.modal').length;

        var $dialog = $('<div class="modal fade" id=' + divId + ' role="dialog" data-backdrop="static" data-keyboard="false">' +
            '<div class="modal-dialog modal-lg">' +
            '<div class="modal-content">' +
            '<div class="modal-header" style="background-color: #3498db;">' +
            '<button type="button" class="close" onclick=$("#' + divId + '").modal("hide");>&times;</button>' +
            '<h4 class="modal-title" style="color: white;"><i class="fa fa-search" style="margin-right:5px"></i> ' + title + '</h4>' +
            '</div>' +
            '<div class="modal-body">' +
            '</div>' +
            '<div class="modal-footer">' +
            '<button type="button" class="btn btn-primary" onclick=$("#' + divId + '").modal("hide");>Fechar</button>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>');


        $.get(controller, { id: id }, function(data) {

            $dialog.find('.modal-body').html(data);
        });


        $dialog.on('hidden.bs.modal', function() {

            $dialog.remove();

            if ($('.modal-backdrop').length > 1) {
                $('.modal-backdrop')[0].remove();
            }
        });

        $dialog.modal();

        waitingDialog.hide();
    },

    Modal: function(title, controller) {

        waitingDialog.show("Carregando...", { dialogSize: "sm" });

        var divId = $('.modal').length;

        var $dialog = $('<div class="modal fade" id=' + divId + ' role="dialog" data-backdrop="static" data-keyboard="false">' +
            '<div class="modal-dialog modal-lg">' +
            '<div class="modal-content">' +
            '<div class="modal-header" style="background-color: #3498db;">' +
            '<button type="button" class="close" onclick=$("#' + divId + '").modal("hide");>&times;</button>' +
            '<h4 class="modal-title" style="color: white;"><i class="fa fa-file-text-o"></i> ' + title + '</h4>' +
            '</div>' +
            '<div class="modal-body">' +
            '</div>' +
            '<div class="modal-footer">' +
            '<button type="button" class="btn btn-primary" onclick=$("#' + divId + '").modal("hide");>Fechar</button>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>');


        jQuery.get(controller, function(data) {

            $dialog.find('.modal-body').html(data);
        });


        $dialog.on('hidden.bs.modal', function() {

            $dialog.remove();

            if ($('.modal-backdrop').length > 1) {
                $('.modal-backdrop')[0].remove();
            }
        });

        $dialog.modal();

        waitingDialog.hide();
    }

}

$(document).ready(function () {
    $(document).ready(function () { 
        $(document).on('keydown', FuncoesHelp.disableFunctionKeys);
        FuncoesHelp.resolucaoMonitor();
    });
});

