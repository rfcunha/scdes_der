﻿var onBegin = function () {
    waitingDialog.show('Processando...', { dialogSize: 'sm' });
};
var onSuccess = function (result) {
    waitingDialog.hide();
};
var onFailure = function () {
    waitingDialog.hide();
    //Mensagens.Flutuante('Erro', 'Houve uma falha ao editar os dados. <br/>Entre em contato com suporte.', 'error');
};


function helpOnline() {
    var intro = introJs();
    intro.exit();
    intro.setOptions({
        steps: [
                {
                    element: '#form_auxiliar',
                    intro: "Para salvar um departamento responsável preencha todos <b>campos</b> que contêm <font color='red'>*</font>."
                },
                {
                    element: '#contentePlace_ucCadastro_btnSalvar',
                    intro: "Ok, clique em <b>CADASTRAR</b>",
                    position: 'right'
                },
                {
                    element: '#contentePlace_ucCadastro_btnLimpar',
                    intro: "Para limpar todos os campos do formulário clique em <b>LIMPAR</b>",
                    position: 'right'
                },
                {
                    element: '#contentePlace_ucCadastro_btnConsultar',
                    intro: "Clique em <b>PESQUISAR</b> para pesquisar os departamentos responsáveis cadastrados",
                    position: 'right'
                }
            ]
    });
    intro.start();
}

function ModoDefault() {

    limpar.Div("#tabGerais");

    $("#table_localidade > tbody").html("");
    $("#divGridLocalidade").css({ "display": "none" });

    $("#table_fonte_recurso > tbody").html("");
    $("#divGridFonteRecurso").css({ "display": "none" });

    $("#table_empreendimento > tbody").html("");
    $("#divGridEmpreendimento").css({ "display": "none" });

    $("#table_pendencia > tbody").html("");
    $("#divGridPendencia").css({ "display": "none" });

    $(".panel-heading > ul > li.active").removeClass("active");

    $(".panel-heading > ul > li:eq(0)").addClass("active");

    $("#tabLocalidade,#tabFonteRecurso,#tabEmpreendimento,#tabPendencia").removeClass("active in");

    $("#tabGerais").addClass("active in");
}

function Limpar() {
    $("#btnLimpar").click(function (e) {
        limpar.Form("#form_cadastro");
    });
}



function Iniciar() {

    waitingDialog.show('Carregando...', { dialogSize: 'sm' });

    $("#divAutoDecreto").modal("show");
    Requirido();
    $("#btnSalvar").click(function () { return validaFormularioDiv('tabGerais'); });
    DropDownAutoComplete.Ini();

    waitingDialog.hide();
}


jQuery(document).ready(function () {
    Iniciar();
    Limpar();
    Mask.Ini();
});  