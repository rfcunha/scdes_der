﻿var onBegin = function () {
    waitingDialog.show('Processando...', { dialogSize: 'sm' });
};

var onSuccess = function (result) {
    waitingDialog.hide();
};

var onFailure = function (e) {
    console.log(e);
    waitingDialog.hide();
    Mensagens.Flutuante('Erro', 'Houve uma falha ao cadastrar os dados! <br/>Entre em contato com suporte.', 'error');
};

function multiOnChange(tipo) {
    var box = $("#multi_select_" + tipo).val();

    var value = "";

    if (box) {
        for (var i = 0; i < box.length; i++) {
            value += box[i] + "|";
        }
    }   

    if (tipo === "infra") {
        $("#din_imovel_infraestrutura").val(value);
    }
    else if (tipo === "equi") {
        $("#din_imovel_equipamento").val(value);
    }
    else if (tipo === "serv") {
        $("#din_imovel_servico").val(value);
    }
    else if (tipo === "sus") {
        $("#din_imovel_sus").val(value);
    }
}

function statusDesapropriacaoChange() {
    var ist_id = $("#din_conducao_ist_id").val();

    if (ist_id === "7") {
        $("#divPeritoAssistenteData").css({ "display": "inline" });
    } else {
        $("#divPeritoAssistenteData").css({ "display": "none" });
    }

}

function MultiHasValueOnInit() {

    var value = $("#din_imovel_infraestrutura").val();
    if (value) {
        $("#multi_select_infra").val(value.split("|"));
    }

    value = $("#din_imovel_equipamento").val();
    if (value) {
        $("#multi_select_equi").val(value.split("|"));
    }

    value = $("#din_imovel_servico").val();
    if (value) {
        $("#multi_select_serv").val(value.split("|"));
    }

    value = $("#din_imovel_sus").val();
    if (value) {
        $("#multi_select_sus").val(value.split("|"));
    }
}

function helpOnline() {
    var intro = introJs();
    intro.exit(); 
        intro.setOptions({
            steps: [
                {
                    element: '#form_auxiliar',
                    intro: "Para salvar um departamento responsável preencha todos <b>campos</b> que contêm <font color='red'>*</font>."
                }, 
                {
                    element: '#contentePlace_ucCadastro_btnSalvar',
                    intro: "Ok, clique em <b>CADASTRAR</b>",
                    position: 'right'
                },
                {
                    element: '#contentePlace_ucCadastro_btnLimpar',
                    intro: "Para limpar todos os campos do formulário clique em <b>LIMPAR</b>",
                    position: 'right'
                },
                {
                    element: '#contentePlace_ucCadastro_btnConsultar',
                    intro: "Clique em <b>PESQUISAR</b> para pesquisar os departamentos responsáveis cadastrados",
                    position: 'right'
                }
            ]
        }); 
    intro.start();
}



function CarregarLocalidadeMunicipio() {

    if (jQuery("#din_propriedade_est_id option:selected").val() !== "") {

        var dataString = JSON.stringify({
            est_id: jQuery("#din_propriedade_est_id option:selected").val()
        });

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "~/../CarregarMunicipio",
            data: dataString,
            dataType: "json",
            async: true,
            error: function (xhr) {

                Mensagens.Modal("Erro", xhr.responseJSON.msgErro, BootstrapDialog.TYPE_DANGER);
            },
            success: function (data) {

                if (data.length > 0) {

                    var obj = data;

                    $("#din_propriedade_mun_id option").remove();
                    $("#din_propriedade_mun_id").append("<option value=''>--selecione--</option>");

                    for (var i = 0; i < obj.length; i++) {
                        $("#din_propriedade_mun_id").append("<option value=" + obj[i].mun_id + ">" + obj[i].mun_nome + "</option>");
                    }

                    $("#din_propriedade_mun_id").attr('disabled', false);
                    $('#din_propriedade_mun_id').trigger("chosen:updated");
                }
                else {
                    Mensagens.Modal("Informativo", "Não existe nenhum municipio cadastrada para este estado, volte e cadastre pelo menos um municipio ou entre em contato com o administrador do sistema.", BootstrapDialog.TYPE_INFO);

                    $("#din_propriedade_mun_id").empty();
                    $("#din_propriedade_mun_id").append("<option value=''>--selecione--</option>");
                    $("#din_propriedade_mun_id").attr('disabled', true);
                    $('#din_propriedade_mun_id').trigger("chosen:updated");
                }
            }
        });
    } else {

        $("#mun_id").empty();
        $("#mun_id").append("<option value=''>--selecione--</option>");
        $("#mun_id").attr('disabled', true);
        $('#mun_id').trigger("chosen:updated");
    }
}

function Limpar() { 
    $("#btnLimpar").click(function(e) { 
        limpar.Form("#form_cadastro"); 
    });
}

function TextEditor() {
    $('.textarea-editor').summernote(
        {
            height: 600,                 // define a altura do editor
            //width: $("#divTextEditor").width() - 200,
            minHeight: null,           // define a altura minima
            maxHeight: null,          // define a altura máxima 
            minWidth: null,
            maxWidth: 1200,
            disableDragAndDrop: true,
            disableResizeEditor: false,
            //focus: true,                  // define o foco na área editável apos a inicialização
            fontNames: ['Arial', 'Arial Black', 'Calibri', 'Comic Sans MS', 'Courier New'],
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontname', 'fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['utils', ['table', 'height']]
            ],
            lang: 'pt-BR'
        });

        $("#din_laudo_avaliacao").summernote('disable');
        $("#din_memorial_descritivo").summernote('disable');

}

function Iniciar() {
    //validaFormularioDiv("tabGerais"); 
    TextEditor();
    DropDownAutoComplete.Ini();
    MultiHasValueOnInit();   



    $("[data-toggle='toggle']").bootstrapSwitch({
        onText: "Sim",
        offText: "Não",
        animate: false,
        onSwitchChange: function (event, value) {
            var guia = $(this).closest("div[id]").attr("id");
            var tab = '#' + guia;
            var toggleValue = !value;

            $(tab).find('input:not(.validar), textarea, button, select, .btn').attr('disabled', toggleValue);
            $(tab).find('select').trigger("chosen:updated");
            
            if (guia === "tabLaudoAvaliacao") {
                if (toggleValue === true) {
                    $("#din_laudo_avaliacao").summernote('disable');
                } else {
                    $("#din_laudo_avaliacao").summernote('enable');
                }
            }

            if (guia === "tabMemorialDescritivo") {
                if (toggleValue === true) {
                    $("#din_memorial_descritivo").summernote('disable');
                } else {
                    $("#din_memorial_descritivo").summernote('enable');
                }

            }

            if (guia === "tabCaracteristicaImovel") {
                if (toggleValue === false) {
                    $(".chosen-container-multi .chosen-choices").css("background-color", "#fff");
                } else {
                    $(".chosen-container-multi .chosen-choices").css("background-color", "#eee");
                }
                
            } 
        }
    });
}

jQuery(document).ready(function () {
    Iniciar();
    Limpar();
    Mask.Ini();
});  