﻿var columnDefs = [ 
       {
           render: function (data, type, row, dadosLinha) {
               
                   var op =  
                   '<span title="Visualizar Detalhes" class="fa fa-search OpcaoGrid" id="btnPesquisar" data-roles="true" style="margin:0 2px 0 2px; cursor:pointer;" onclick="ShowModalPopUp.VisualizarDetalhes(\'Desapropriação Individual\', \'~/../../../Desapropriacao/DesapropriacaoIndividual/VisualizarDetalhes\','+ row["din_id"] +');"></span>' +
                   '<span title="Editar" class="fa fa-pencil-square-o text-black OpcaoGrid" id="btnEditar" data-roles="true" style="margin:0 2px 0 2px; cursor:pointer;" onclick="window.location.href=\'Editar?args=' + row["args"] + '\'"></span>' +
                   '<span title="Deletar" class="fa fa-trash"  id="btnDeletar" OpcaoGrid" data-roles="true" href="Deletar/" data-id="' + row["din_id"] + '" style="margin:0 2px 0 2px; cursor:pointer" onclick="Mensagens.ModalDelete(this,\'Deseja remover este item?\');"></span>';

               return op;

           },
           width: "80px",
           targets: [5]
       }
];

var columns = [ 
    { data: "din_geral_documento_numero", title: "Código do Documento" },
    { data: "din_geral_objeto", title: "Objeto do Cadastro" },
    { data: "din_geral_emissao_dt", title: "Dt. Emissão" },
    { data: "din_empreendimento_rodovia_sp", title: "Sp" },
    { data: "din_empreendimento_rodovia_nome", title: "Rodovia" },
    { data: null, title: "Opções"}];

function Status(controle) {
    $.ajax({
        type: "POST",
        url: '~/../' + jQuery(controle).attr("href"),
        data: { __RequestVerificationToken: $('input[name="__RequestVerificationToken"').val(), id: jQuery(controle).attr("data-id"), status: jQuery(controle).attr("data-status") },
        dataType: "html",
        cache: false,
        success: function (data) {
            eval(data);
            CriarTabela();
        }, beforeSend: function () {

        },
        error: function (xhr) {  
            eval(xhr.responseText);
        }
    });
}

function Delete(controle) {
    $.ajax({
        type: "POST",
        url: '~/../' + jQuery(controle).attr("href"),
        data: { __RequestVerificationToken: $('input[name="__RequestVerificationToken"').val(), id: jQuery(controle).attr("data-id") },
        dataType: "html",
        cache: false,
        success: function (data) {
            eval(data);
            CriarTabela();
        }, beforeSend: function () {

        },
        error: function (xhr) { // if error occured 
            eval(xhr.responseText);
        }
    });
}

function Iniciar() {  
   $("#btnPesquisar").click(function(e) {
       CriarTabela();
   });

   DropDownAutoComplete.Ini();
} 

function CriarTabela() {
     Grids.MontarDataTable("#table_consulta", "~/../../../Desapropriacao/DesapropriacaoIndividual/Consultar", columnDefs, columns);

     $("#divGrid").css({ "display": "inline" });
}

function helpOnline() {
    var intro = introJs();
    intro.exit(); 
        intro.setOptions({
            steps: [
                {
                    element: '#form_auxiliar',
                    intro: "Para salvar um sistema preencha todos <b>campos</b> que contêm <font color='red'>*</font>.",
                }, 
                {
                    element: '#contentePlace_ucCadastro_btnSalvar',
                    intro: "Ok, clique em <b>CADASTRAR</b>",
                    position: 'right'
                },
                {
                    element: '#contentePlace_ucCadastro_btnLimpar',
                    intro: "Para limpar todos os campos do formulário clique em <b>LIMPAR</b>",
                    position: 'right'
                },
                {
                    element: '#contentePlace_ucCadastro_btnConsultar',
                    intro: "Clique em <b>PESQUISAR</b> para gerar uma  pesquisa",
                    position: 'right'
                },
                {
                    element: '#btnNovoConsultar',
                     intro: "Clique em <b>VOLTAR</b> para voltar a tela anterior",
                    position: 'right'
                }
            ]
        }); 
    intro.start();
}

function Limpar() { 
    $("#btnLimpar").click(function(e) { 
        limpar.Form("#form_consulta"); 
    }); 
}

 jQuery(document).ready(function () {
    Iniciar();
    Limpar();
    Requirido();  
 });

jQuery(document).ready(function () {
    Iniciar();
    Limpar();
    Requirido();  
});  