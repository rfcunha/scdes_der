﻿//***************************************************
//* FUNÇÃO QUE PERMITE SOMENTE LETRAS  *
//***************************************************
function SomenteLetras(e) {
    if (!(e.which < 48 || e.which > 57)) {
        return false;
    }
    else {
        return true;
    }
};

//***************************************************
//* FUNÇÃO QUE PERMITE SOMENTE LETRAS E NUMEROS *
//***************************************************
function SomenteLetraseNumeros(controle) { 
    if (!(event.keyCode >= 63 && event.keyCode <= 93) && !(event.keyCode >= 97 && event.keyCode <= 122) && !(event.keyCode >= 32 && event.keyCode <= 59) && !(event.keyCode == 231) && !(event.keyCode >= 192)) {
        if (event.keyCode === 13) {
            controle.focus();
        }
        else {
            return false;
        }
    }
}

//***************************************************
//* FUNÇÃO PARA LIBERAR SOMENTE NUMEROS *
//***************************************************
function SomenteNumero(e) {
    var retorno = true;
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) { retorno = false; }
    return retorno;
};


function DesabilitaEspaco() {
    if (event.keyCode === 32) {
        return false;
    }
};

var Valid = {
    Required: function (obj) {
        var value = "";
        var msgError = $(obj).attr("data-val-required");
        
        if ($(obj).val() != null) { value = $(obj).val().trim(); }
        
        if ($(obj).is("[type=text],[type=password], select, textarea") && value === "" && msgError != undefined) {
           
            $(obj).addClass("has-error");
            
            if ($(obj).is("select.chosen-select")) {
                $("#" + $(obj).attr("id") + "_chosen a").addClass("has-error");
            }

            return msgError + "<br>";

        } else {
            if ($(obj).is("select.chosen-select")) {
                $("#" + $(obj).attr("id") + "_chosen a").removeClass("has-error");
            }

            $(obj).removeClass("has-error");

            return "";
        }
    },
    Regex: function (obj) {
        var regex = $(obj).attr("data-val-regex-pattern"); 
        if (regex != undefined) {
            
            var msgError = $(obj).attr("data-val-regex");
            
            if (msgError != undefined) {
                
                if (new RegExp(regex).test($(obj).val()) === false) {
                    return msgError + "<br>";
                } else {
                    return "";
                }

            }
        } else {
            return "";
        }
    }
};



function validaFormulario() {
    $('input[data-val]:not([readonly]):not([disabled]),select[data-val],textarea[data-val]').focusout(function () {

        if (Valid.Regex(this) == false) {
            return false;
        }

        return true;
    });

    for (i = 0; i < arguments.length; i++) {

        $(arguments[i]).submit(function (e) {

            var _erro = "";

            $("#" + e.target.id + " input[data-val=true]:not([readonly]):not([disabled])," +
                "#" + e.target.id + " select[data-val=true]:not([readonly]):not([disabled])," +
                "#" + e.target.id + " textarea[data-val=true]:not([readonly]):not([disabled])").each(function () {

                    _erro += Valid.Required(this);

            });

            if (_erro) {
                Mensagens.Modal("Atenção", _erro, BootstrapDialog.TYPE_WARNING);
                e.preventDefault();
                e.stopImmediatePropagation();
                return false;
            } else { 
                return true;
            }
        });
    }
}


function validaFormularioDiv() {


    for (i = 0; i < arguments.length; i++) {

        var divPai = arguments[i];

        var _erro = "";

        $("input[data-val=true]:not([readonly]):not([disabled]),select[data-val=true]:not([readonly]):not([disabled]),textarea[data-val=true]:not([readonly]):not([disabled])", $("#" + divPai)).each(function () {

            _erro += Valid.Required(this);
            _erro += Valid.Regex(this);


            //REMOVE A CLASS ERRO NO FOCUS
            $(this).focus(function () { $(this).removeClass("has-error"); });

            if ($(this).is('select.chosen-select')) {
                if ($(this).next().is("div.chosen-container")) {
                    $(this).next().click(function () {
                        $("#" + $(this).attr("id") + " a").removeClass("has-error");
                    });
                }
            }

        });
    }

    if (_erro) {

        Mensagens.Modal("Atenção", _erro, BootstrapDialog.TYPE_WARNING);

        return false;

    } else {
        return true;
    }
}


function validaEmail(controle) {
    var $email = $(controle).val().split(";");
    for (var i = 0; i < $email.length; i++) {
        var $valor = $email[i];
        if ($valor) {
            var $erro = "";
            var expReg = /^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$/i;
            if (!$valor.match(expReg)) { $erro = "email inválido."; }
        } else {
            return $(this);
        }
    }
    if ($erro) {
        Mensagens.Modal("Atenção", $erro, BootstrapDialog.TYPE_WARNING);
        jQuery(controle).val("");
        setTimeout(function () { $(this).focus(); }, 50);
    } else {
        return $(this);
    }

    return $(this);
}

function validaData(controle) {
    var $valor = $(controle).val();
    if ($valor) {
        var $erro = "";
        var expReg = /^((0[1-9]|[12][0-9]|3[01])\/(0[1-9]|1[012])\/[1-2][0-9]\d{2})$/;
        if ($valor.match(expReg)) {
            var $dia = parseFloat($valor.substring(0, 2));
            var $mes = parseFloat($valor.substring(3, 5));
            var $ano = parseFloat($valor.substring(6, 10));
            if (($mes === 4 && $dia > 30) || ($mes === 6 && $dia > 30) || ($mes === 9 && $dia > 30) || ($mes === 11 && $dia > 30)) {
                $erro = "Data incorreta! O mês especificado na data " + $valor + " contém 30 dias.";
            } else {
                if ($ano % 4 !== 0 && $mes === 2 && $dia > 28) {
                    $erro = "Data incorreta! O mês especificado na data " + $valor + " contém 28 dias.";
                } else {
                    if ($ano % 4 === 0 && $mes === 2 && $dia > 29) { $erro = "Data incorreta! O mês especificado na data " + $valor + " contém 29 dias."; }
                }
            }
        } else {
            $erro = "Formato de Data para " + $valor + " é inválido";
        }

        if ($erro) {

            $(controle).val("");
            Mensagens.Modal("Atenção", $erro, BootstrapDialog.TYPE_WARNING);

        } else {
            return $(this);
        }
    } else {
        return $(this);
    }
}

function validaHora(controle) {
    var value = $(controle).val();
    if (value.length > 0) {
        var colonCount = 0;
        var hasMeridian = false;
        var erro = false;
        for (var i = 0; i < value.length; i++) {
            var ch = value.substring(i, i + 1);
            if ((ch < "0") || (ch > "9")) {
                if ((ch !== ":") && (ch !== " ") && (ch !== "a") && (ch !== "A") && (ch !== "p") && (ch !== "P") && (ch !== "m") && (ch !== "M")) {
                    return false;
                }
            }
            if (ch === ":") { colonCount++; }
            if ((ch === "p") || (ch === "P") || (ch === "a") || (ch === "A")) { hasMeridian = true; }
        }

        if ((colonCount < 1) || (colonCount > 2)) {
            erro = true;
        }

        var hh = value.substring(0, value.indexOf(":"));

        if ((parseFloat(hh) < 0) || (parseFloat(hh) > 23)) {
            erro = true;
        }

        if (hasMeridian) {
            if ((parseFloat(hh) < 1) || (parseFloat(hh) > 12)) {
                erro = true;
            }
        }
        if (colonCount === 2) {
            var mm = value.substring(value.indexOf(":") + 1, value.lastIndexOf(":"));
        } else {
            var mm = value.substring(value.indexOf(":") + 1, value.length);
        }

        if ((parseFloat(mm) < 0) || (parseFloat(mm) > 59)) {
            erro = true;
        }
        if (colonCount === 2) {
            var ss = value.substring(value.lastIndexOf(":") + 1, value.length);
        } else {
            var ss = "00";
        }

        if ((parseFloat(ss) < 0) || (parseFloat(ss) > 59)) {
            erro = true;
        }

        if (erro) {

            Mensagens.Modal("Atenção", "Formato da hora " + value + " é inválido", BootstrapDialog.TYPE_WARNING);
            jQuery(controle).val("");
            setTimeout(function () { $(this).focus(); }, 50);

        } else {
            return $(this);
        }
    }
}

function validaDataHora(controle) {
    var erro = false;
    var msg = "";
    if ($(controle).val().length > 0) {
        if ($(controle).val().length === 16) {
            var $valor = $(controle).val().split(" ")[0];
            var expReg = /^((0[1-9]|[12][0-9]|3[01])\/(0[1-9]|1[012])\/[1-2][0-9]\d{2})$/;
            if ($valor.match(expReg)) {

                var $dia = parseFloat($valor.substring(0, 2));
                var $mes = parseFloat($valor.substring(3, 5));
                var $ano = parseFloat($valor.substring(6, 10));

                if (($mes === 4 && $dia > 30) || ($mes === 6 && $dia > 30) || ($mes === 9 && $dia > 30) || ($mes === 11 && $dia > 30)) {
                    msg = "Data incorreta! O mês especificado na data " + $valor + " contém 30 dias.<br/>";
                    erro = true;
                } else {
                    if ($ano % 4 !== 0 && $mes === 2 && $dia > 28) {
                        msg = "Data incorreta! O mês especificado na data " + $valor + " contém 28 dias.<br/>";
                        erro = true;
                    } else {
                        if ($ano % 4 === 0 && $mes === 2 && $dia > 29) {
                            msg = "Data incorreta! O mês especificado na data " + $valor + " contém 29 dias.<br/>";
                            erro = true;
                        }
                    }
                }
            } else {
                msg = "Formato da data para " + $valor + " é inválido. <br/>";
                erro = true;
            }


            var value = $(controle).val().split(" ")[1];
            var colonCount = 0;
            var hasMeridian = false;

            for (var i = 0; i < value.length; i++) {
                var ch = value.substring(i, i + 1);
                if ((ch < "0") || (ch > "9")) { if ((ch !== ":") && (ch !== " ") && (ch !== "a") && (ch !== "A") && (ch !== "p") && (ch !== "P") && (ch !== "m") && (ch !== "M")) { return false; } }
                if (ch === ":") { colonCount++; }
                if ((ch === "p") || (ch === "P") || (ch === "a") || (ch === "A")) { hasMeridian = true; }
            }

            if ((colonCount < 1) || (colonCount > 2)) { erro = true; }
            var hh = value.substring(0, value.indexOf(":"));
            if ((parseFloat(hh) < 0) || (parseFloat(hh) > 23)) { msg += "Formato da hora " + value + " é inválido"; erro = true; }
            if (hasMeridian) { if ((parseFloat(hh) < 1) || (parseFloat(hh) > 12)) { msg += "Formato da hora " + value + " é inválido"; erro = true; } }
            if (colonCount === 2) { var mm = value.substring(value.indexOf(":") + 1, value.lastIndexOf(":")); } else { var mm = value.substring(value.indexOf(":") + 1, value.length); }
            if ((parseFloat(mm) < 0) || (parseFloat(mm) > 59)) { msg += "Formato da hora " + value + " é inválido"; erro = true; }
            if (colonCount === 2) { var ss = value.substring(value.lastIndexOf(":") + 1, value.length); } else { var ss = "00"; }
            if ((parseFloat(ss) < 0) || (parseFloat(ss) > 59)) { msg += "Formato da hora " + value + " é inválido"; erro = true; }

        } else {
            msg += "Formato da data/hora para " + $(controle).val() + " é inválido. <br/>"; erro = true;
        }

        if (erro) {
            Mensagens.Modal("Atenção", msg, BootstrapDialog.TYPE_WARNING);
            jQuery(controle).val("");
            setTimeout(function () { $(this).focus(); }, 50);
            return false;
        }
    }
}


function valida_cpf(objcpf) {
    var value = objcpf.val();
    value = value.replace(".", "");
    value = value.replace(".", "");
    var cpf = value.replace("-", "");

    while (cpf.length < 11) cpf = "0" + cpf;
    var expReg = /^0+$|^1+$|^2+$|^3+$|^4+$|^5+$|^6+$|^7+$|^8+$|^9+$/;
    var a = [];
    var b = new Number;
    var c = 11;

    for (i = 0; i < 11; i++) {
        a[i] = cpf.charAt(i);
        if (i < 9) b += (a[i] * --c);
    }
    if ((x = b % 11) < 2) { a[9] = 0 } else { a[9] = 11 - x }
    b = 0;
    c = 11;

    for (y = 0; y < 10; y++) b += (a[y] * c--);
    if ((x = b % 11) < 2) { a[10] = 0; } else { a[10] = 11 - x; }
    if ((cpf.charAt(9) != a[9]) || (cpf.charAt(10) != a[10]) || cpf.match(expReg)) {

        Mensagens.Modal("Atenção", "- cpf invalido.", BootstrapDialog.TYPE_WARNING);
        objcpf.val("");

    } else {
        return $(this);
    }
}

function valida_cnpj(objCnpj) {

    var cnpj = objCnpj.val();

    cnpj = jQuery.trim(cnpj); // retira espaços em branco
    // DEIXA APENAS OS NÚMEROS
    cnpj = cnpj.replace("/", "");
    cnpj = cnpj.replace(".", "");
    cnpj = cnpj.replace(".", "");
    cnpj = cnpj.replace("-", "");

    var numeros, digitos, soma, i, resultado, pos, tamanho, digitos_iguais;
    digitos_iguais = 1;

    if (cnpj.length < 14 && cnpj.length < 15) {
        return false;
    }

    for (i = 0; i < cnpj.length - 1; i++) {
        if (cnpj.charAt(i) != cnpj.charAt(i + 1)) {
            digitos_iguais = 0;
            break;
        }
    }

    if (!digitos_iguais) {
        tamanho = cnpj.length - 2
        numeros = cnpj.substring(0, tamanho);
        digitos = cnpj.substring(tamanho);
        soma = 0;
        pos = tamanho - 7;

        for (i = tamanho; i >= 1; i--) {
            soma += numeros.charAt(tamanho - i) * pos--;
            if (pos < 2) {
                pos = 9;
            }
        }
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(0)) {
            return false;
        }
        tamanho = tamanho + 1;
        numeros = cnpj.substring(0, tamanho);
        soma = 0;
        pos = tamanho - 7;
        for (i = tamanho; i >= 1; i--) {
            soma += numeros.charAt(tamanho - i) * pos--;
            if (pos < 2) {
                pos = 9;
            }
        }
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;

        if (resultado !== digitos.charAt(1)) {
            Mensagens.Modal("Atenção", "- cnpj invalido.", BootstrapDialog.TYPE_WARNING);
            objCnpj.val("");
        }

        return true;

    } else {
        Mensagens.Modal("Atenção", "- cnpj invalido.", BootstrapDialog.TYPE_WARNING);
        objCnpj.val("");
    }
};


function validaPlacaVeiculo(controle) { 
    var $valor = $(controle).val(); 
    if ($valor) {
        var $erro = "";
        var expReg = /[a-z]{3}?\d{4}/gim;

        if (!$valor.match(expReg)) { $erro = "placa inválida."; }

    } else {
        return $(this);
    } 
    if ($erro) {
        Mensagens.Modal("Atenção", $erro, BootstrapDialog.TYPE_WARNING);
        jQuery(controle).val("");
        setTimeout(function () { $(this).focus(); }, 50);

    } else {
        return $(this);
    }
    return $(this);
}


function textArea() {
    $("textarea[max-length]").keyup(function () {
        //get the limit from maxlength attribute  
        var limit = parseInt($(this).attr("max-length"));
        //get the current text inside the textarea  
        var text = $(this).val();
        //count the number of characters in the text  
        var chars = text.length;

        //check if there are more characters then allowed  
        if (chars > limit) {
            //and if there are use substr to get the text before the limit  
            var new_text = text.substr(0, limit);
            //and change the current text with the new text  
            $(this).val(new_text);
        }
    });
}

function Requirido() {

    $("input[data-val-length-max]").each(function () {
        var length = parseInt($(this).attr("data-val-length-max")); $(this).attr('maxlength', length);
    });

    $("textarea[data-val-length-max]").each(function () {
        var length = parseInt($(this).attr("data-val-length-max")); $(this).attr('maxlength', length);
        $(this).keyup(function () {
            var limit = parseInt($(this).attr('maxlength'));
            var text = $(this).val();
            var chars = text.length;
            if (chars > limit) {
                var new_text = text.substr(0, limit);
                $(this).val(new_text);
            }
        });
    });

//    $("input[type='text'][data-val=true],input[type='password'][data-val=true], select[data-val=true], textarea[data-val=true], input[type='radio'][data-val=true], input[type='checkbox'][data-val=true], input[type='checkbox'][data-val=true], input[type='checkbox'][data-val=true], input[type='file'][data-val=true]").each(function () {
//        
//        if ($(this).parent().prev().is("label")) {
//            $(this).parent().prev().attr('for', $(this).attr('id'));
//        }

//        $("label[for='" + $(this).attr('id') + "']").addClass("required");


//        //REMOVE A CLASS ERRO NO FOCUS////
//        $(this).focus(function () { $(this).removeClass("has-error");});

//        if ($(this).is('select.chosen-select')) {
//            if ($(this).next().is("div.chosen-container")) {
//                $(this).next().click(function () {
//                    alert($("#" + $(this).attr("id")));
//                    $("#" + $(this).attr("id") + " a").removeClass("has-error");
//                });
//            }
//        }
    //    }); 

    $("input[data-val-required],select[data-val-required],textarea[data-val-required]").each(function () {

        if ($(this).parent().prev().is("label")) {
            $(this).parent().prev().attr('for', $(this).attr('id'));
        }
        $("label[for='" + $(this).attr('id') + "']").addClass("required");



        $(this).focus(function () { $(this).removeClass("has-error"); });

        if ($(this).is('select.chosen-select')) {
            if ($(this).next().is("div.chosen-container")) {
                $(this).next().click(function () {
                    $("#" + $(this).attr("id") + " a").removeClass("has-error");
                });
            }
        }
    });

    Mask.Ini();
    textArea();
}


var Mask = {
    Ini: function () {
        $("input[data-only-letras-maiusculas=true]").keyup(function () { $(this).val($(this).val().toUpperCase()); });
        $("input[data-only-number=true]").keypress(SomenteNumero);
        $("input[data-only-letras=true]").keypress(SomenteLetras);
        $("input[data-only-number-letras=true], textarea[data-only-number-letras=true]").keypress(SomenteLetraseNumeros);
        $("input[data-remove-space=true]").keypress(DesabilitaEspaco);
        $("input[data-datetime-mask=true]").mask("00/00/0000 00:00").change(function () { validaDataHora(this); });
        $("input[data-date-mask=true]").mask("00/00/0000").change(function () { validaData(this); });
        $("input[data-time-mask=true]").mask("00:00").change(function () { validaHora(this); });
        $("input[data-number-points-mask=true]").mask("#.##0", { reverse: true });
        $("input[data-money-mask=true]").mask("#.##0,00", { reverse: true });
        $("input[data-email-mask=true]").change(function () { validaEmail(this); });
        $("input[data-placa-veiculo-mask=true]").mask("AAA9999").change(function () { validaPlacaVeiculo(this); }); ;
        $("input[data-telefone-mask=true]").mask("(99)9999-9999");
        //$("input[data-celular-mask=true]").focusout(function () {var element = $(this); element.unmask(); var phone = element.val().replace(/\D/g, ""); if (phone.length > 10) { element.mask("(99) 99999-999?9"); } else { element.mask("(99) 9999-9999?9"); } }).trigger("focusout");
        $("input[data-celular-mask=true]").focusout(function () {
            var phone, element;
            element = $(this);
            element.unmask();

            phone = element.val().replace(/\D/g, "");

            if (phone.length > 10) { element.mask("(00) 00000-0009"); } else { element.mask("(00) 0000-00009"); }
        }).trigger("focusout");
        
        $('[data-cpf-mask]').mask('000.000.000-00', { reverse: true }).blur(function () {valida_cpf($(this))});
        $('[data-cnpj-mask]').mask('00.000.000/0000-00', { reverse: true }).blur(function() { valida_cnpj($(this)) });
        $('[data-cep-mask]').mask('00000-000').blur(function () {
            if ($(this).val().length > 0 && $(this).val().length < 9) {
                Mensagens.Modal("Atenção", 'CEP é inválido', BootstrapDialog.TYPE_WARNING);
                return false;
            }
        });
        $("input[data-autosdodecreto-mask=true]").mask("000000/00/DER/0000");
        $("input[data-cod-documento-mask=true]").mask("AA-AA0000000-000.000-000-A00/000");
    }
}

var limpar = {
    Form: function(id) {
        $(id).find(":input").each(function() {
            switch (this.type) {
                case "password":
                case "file":
                case "text":
                case "textarea":
                    $(this).val("");
                    break;
                case "checkbox":
                case "radio":
                    this.checked = false;
                    this.checked[0] = true;
                    break;
                case "select-multiple":
                case "select-one":
                case "select":
                    this.selectedIndex = 0;
                    DropDownAutoComplete.Upd();
                    break;
            }
        });
    },

    Div: function() {

        for (i = 0; i < arguments.length; i++) {
            var divPai = arguments[i];

            $($(divPai)).find(":input").each(function() {
                switch (this.type) {
                case "password":
                case "file":
                case "text":
                case "textarea":
                    $(this).val("");
                    break;
                case "checkbox":
                case "radio":

                    this.checked = false;
                    this.checked[0] = true;
                    break;
                case "select-multiple":
                case "select-one":
                case "select":
                    this.selectedIndex = 0;
                    DropDownAutoComplete.Upd();
                    break;
                }
            });
        }
    }

}