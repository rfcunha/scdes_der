
function permission_roles_client() {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "~/../../../MasterPage/RolesPerfil",
        data: JSON.stringify({ url: window.location.pathname }),
        dataType: 'json',
        async: false
    }).success(function (data) {
     
        $("div[data-roles=true],span[data-roles=true],button[data-roles=true]").each(function () {
            var idElement = $(this).attr("id");
            $.each(data, function (i, obj) { 
                if (jQuery.inArray(idElement, obj) >= 0) {
                    $('[id^="' + idElement + '"]').css('display', 'inline'); 
                } else {
                    $('[id^="' + idElement + '"]').css('display', 'none');
                }  
            });
        });

    });
}



$(document).ready(function () { 
    permission_roles_client();
});
 