﻿var onBegin = function () {
    waitingDialog.show('Processando...', { dialogSize: 'sm' });
};
var onSuccess = function (result) {
    Limpar();
    waitingDialog.hide();
};
var onFailure = function () {
    waitingDialog.hide();
};

function Iniciar() {
    validaFormulario("form");
    DropDownAutoComplete.Ini();
}

function Limpar() {
    limpar.Form("form");
}

jQuery(document).ready(function () {
    Iniciar();
    Limpar();
    Mask.Ini();
    Requirido();
});