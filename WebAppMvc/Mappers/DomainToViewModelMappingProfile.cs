﻿using AutoMapper;
using Entity.SCDES.Models;
using Entity.SCDES.Utils.Filtro;
using Entity.SCDES.Utils.Gdv;
using WebAppMvc.Areas.Administrativo.Models.DesapropriacaoIndividualAssistenteTecnico;
using WebAppMvc.Areas.Administrativo.Models.DesapropriacaoIndividualAssistenteTecnico.Filtro;
using WebAppMvc.Areas.Administrativo.Models.DesapropriacaoIndividualPeritoJudicial;
using WebAppMvc.Areas.Administrativo.Models.DesapropriacaoIndividualPeritoJudicial.Filtro;
using WebAppMvc.Areas.Administrativo.Models.DesapropriacaoIndividualPredominanteEntorno;
using WebAppMvc.Areas.Administrativo.Models.DesapropriacaoIndividualSituacao;
using WebAppMvc.Areas.Administrativo.Models.DesapropriacaoIndividualSituacao.Filtro;
using WebAppMvc.Areas.Administrativo.Models.DesapropriacaoIndividualStatus;
using WebAppMvc.Areas.Administrativo.Models.DesapropriacaoIndividualStatus.Filtro;
using WebAppMvc.Areas.Administrativo.Models.DesapropriacaoIndividualStatusEventoTipo;
using WebAppMvc.Areas.Administrativo.Models.DesapropriacaoIndividualStatusEventoTipo.Filtro;
using WebAppMvc.Areas.Administrativo.Models.DesapropriacaoProjetoDepartamentoResponsavel;
using WebAppMvc.Areas.Administrativo.Models.DesapropriacaoProjetoDepartamentoResponsavel.Filtro;
using WebAppMvc.Areas.Administrativo.Models.DesapropriacaoProjetoStatus;
using WebAppMvc.Areas.Administrativo.Models.DesapropriacaoProjetoStatus.Filtro;
using WebAppMvc.Areas.Desapropriacao.Models;
using System.Collections.Generic;
using WebAppMvc.Areas.Administrativo.Models.Emitente;

namespace WebAppMvc.Mappers
{
    public class DomainToViewModelMappingProfile : Profile
    {

        protected override void Configure()
        {
            //Mapper.CreateMap<tab_desapropriacao, model_desapropriacao>().ForMember(x=> x.list_model_desapropriacao_pendencia, o=> o.Ignore());

            #region | FILTRO |

            Mapper.CreateMap<tab_desapropriacao_departamento_filtro, model_desapropriacao_departamento_filtro>();
            Mapper.CreateMap<tab_desapropriacao_status_filtro, model_desapropriacao_status_filtro>();

            Mapper.CreateMap<tab_desapropriacao_individual_predominante_entorno, model_desapropriacao_individual_predominante_entorno>();
            Mapper.CreateMap<tab_desapropriacao_individual_perito_judicial_filtro, model_desapropriacao_individual_perito_judicial_filtro>();
            Mapper.CreateMap<tab_desapropriacao_individual_assistente_tecnico_filtro, model_desapropriacao_individual_assistente_tecnico_filtro>();
            Mapper.CreateMap<tab_desapropriacao_individual_situacao_filtro, model_desapropriacao_individual_situacao_filtro>();
            Mapper.CreateMap<tab_desapropriacao_individual_status_filtro, model_desapropriacao_individual_status_filtro>();
            Mapper.CreateMap<tab_desapropriacao_individual_status_evento_tipo_filtro, model_desapropriacao_individual_status_evento_tipo_filtro>();

            #endregion

            Mapper.CreateMap<tab_desapropriacao_departamento, model_desapropriacao_departamento>();
            Mapper.CreateMap<tab_desapropriacao_status, model_desapropriacao_status>();
            Mapper.CreateMap<tab_desapropriacao_observacao, model_desapropriacao_observacao>();
            Mapper.CreateMap<tab_desapropriacao_localidade, model_desapropriacao_localidade>().ForMember(x => x.est_nome, y => y.Ignore()).ForMember(x => x.mun_nome, y => y.Ignore());
            Mapper.CreateMap<tab_desapropriacao_recurso_fonte, model_desapropriacao_recurso_fonte>();
            Mapper.CreateMap<tab_emitente, model_emitente>();
            Mapper.CreateMap<tab_desapropriacao_emitente, model_desapropriacao_emitente>().ForMember(x => x.emp_nome, y => y.Ignore());
            Mapper.CreateMap<tab_desapropriacao_empreendimento, model_desapropriacao_empreendimento>();

            Mapper.CreateMap<tab_desapropriacao_individual_predominante_entorno, model_desapropriacao_individual_predominante_entorno>();
            Mapper.CreateMap<tab_desapropriacao_individual_perito_judicial, model_desapropriacao_individual_perito_judicial>();
            Mapper.CreateMap<tab_desapropriacao_individual_assistente_tecnico, model_desapropriacao_individual_assistente_tecnico>();
            Mapper.CreateMap<tab_desapropriacao_individual_situacao, model_desapropriacao_individual_situacao>();
            Mapper.CreateMap<tab_desapropriacao_individual_status, model_desapropriacao_individual_status>();
            Mapper.CreateMap<tab_desapropriacao_individual_status_evento, model_desapropriacao_individual_status_evento>();
            Mapper.CreateMap<tab_desapropriacao_individual_status_evento_tipo, model_desapropriacao_individual_status_evento_tipo>();

            Mapper.CreateMap<tab_desapropriacao_individual_planta_propriedade, model_desapropriacao_individual_planta_propriedade>()
                .ForMember(x => x.model_anexo_identificador, o => o.MapFrom(y => y.tab_anexo_identificador));

            Mapper.CreateMap<tab_anexo, model_anexo>();
            Mapper.CreateMap<tab_anexo_identificador, model_anexo_identificador>()
                .ForMember(x => x.list_model_anexo, o => o.MapFrom(y => y.tab_anexo));

            Mapper.CreateMap<tab_desapropriacao_individual_revisao, model_desapropriacao_individual_revisao>();

            Mapper.CreateMap<tab_desapropriacao_individual_anexos, model_desapropriacao_individual_anexos>()
                .ForMember(x => x.model_anexo_identificador, o => o.MapFrom(y => y.tab_anexo_identificador));

            Mapper.CreateMap<tab_desapropriacao_individual_cultura, model_desapropriacao_individual_cultura>()
                .ForMember(x => x.model_anexo_identificador, o => o.MapFrom(y => y.tab_anexo_identificador));

            Mapper.CreateMap<tab_desapropriacao_individual_divisa, model_desapropriacao_individual_divisa>()
                .ForMember(x => x.model_anexo_identificador, o => o.MapFrom(y => y.tab_anexo_identificador));

            Mapper.CreateMap<tab_desapropriacao_individual_edificacao_material, model_desapropriacao_individual_edificacao_material>();

            Mapper.CreateMap<tab_desapropriacao_individual_edificacao, model_desapropriacao_individual_edificacao>()
                .ForMember(x => x.list_model_desapropriacao_individual_edificacao_material, o => o.MapFrom(y => y.tab_desapropriacao_individual_edificacao_material));

            Mapper.CreateMap<tab_desapropriacao_individual_entrada, model_desapropriacao_individual_entrada>()
                .ForMember(x => x.model_anexo_identificador, o => o.MapFrom(y => y.tab_anexo_identificador));

            Mapper.CreateMap<tab_desapropriacao_individual_instalacao, model_desapropriacao_individual_instalacao>()
                .ForMember(x => x.model_anexo_identificador, o => o.MapFrom(y => y.tab_anexo_identificador));

            Mapper.CreateMap<tab_desapropriacao_individual_pendencia, model_desapropriacao_individual_pendencia>();

            Mapper.CreateMap<tab_desapropriacao_individual_registro_fotografico, model_desapropriacao_individual_registro_fotografico>()
                .ForMember(x => x.model_anexo_identificador, o => o.MapFrom(y => y.tab_anexo_identificador));

            Mapper.CreateMap<tab_desapropriacao_individual_estrada_acesso, model_desapropriacao_individual_estrada_acesso>()
                .ForMember(x => x.model_anexo_identificador, o => o.MapFrom(y => y.tab_anexo_identificador));

            Mapper.CreateMap<tab_desapropriacao_individual_interessado, model_desapropriacao_individual_interessado>();

            Mapper.CreateMap<tab_desapropriacao_emitente_gdv, model_desapropriacao_emitente>();
            Mapper.CreateMap<tab_desapropriacao_emitente, model_desapropriacao_emitente>();

            Mapper.CreateMap<tab_select_helper, model_select_helper>();
            Mapper.CreateMap<tab_select_helper_guia, model_select_helper_guia>();

            Mapper.CreateMap<tab_desapropriacao, model_desapropriacao>()
                .ForMember(x => x.model_desapropriacao_departamento, o => o.MapFrom(y => y.tab_desapropriacao_departamento))
                .ForMember(x => x.model_desapropriacao_status, o => o.MapFrom(y => y.tab_desapropriacao_status))

                .ForMember(x => x.list_model_desapropriacao_observacao, o => o.MapFrom(y => y.tab_desapropriacao_observacao))
                .ForMember(x => x.list_model_desapropriacao_localidade, o => o.MapFrom(y => y.tab_desapropriacao_localidade))
                .ForMember(x => x.list_model_desapropriacao_recurso_fonte, o => o.MapFrom(y => y.tab_desapropriacao_recurso_fonte))
                .ForMember(x => x.list_model_desapropriacao_empreendimento, o => o.MapFrom(y => y.tab_desapropriacao_empreendimento))
                .ForMember(x => x.list_model_desapropriacao_emitente, o => o.MapFrom(y => y.tab_desapropriacao_emitente));

            Mapper.CreateMap<tab_desapropriacao_individual, model_desapropriacao_individual>()
                .ForMember(x => x.model_desapropriacao_empreendimento, o => o.MapFrom(y => y.tab_desapropriacao_empreendimento))

                .ForMember(x => x.din_conducao_situacao_desapropriacao, o => o.MapFrom(y => y.din_conducao_isi_id))
                .ForMember(x => x.din_conducao_situacao_desapropriacao_obs, o => o.MapFrom(y => y.din_conducao_isi_obs))
                .ForMember(x => x.model_desapropriacao_individual_predominante_entorno, o => o.MapFrom(y => y.tab_desapropriacao_individual_predominante_entorno))
                .ForMember(x => x.model_desapropriacao_individual_perito_judicial, o => o.MapFrom(y => y.tab_desapropriacao_individual_perito_judicial))
                .ForMember(x => x.model_desapropriacao_individual_assistente_tecnico, o => o.MapFrom(y => y.tab_desapropriacao_individual_assistente_tecnico))
                .ForMember(x => x.model_desapropriacao_individual_situacao, o => o.MapFrom(y => y.tab_desapropriacao_individual_situacao))
                .ForMember(x => x.model_desapropriacao_individual_status, o => o.MapFrom(y => y.tab_desapropriacao_individual_status))

                .ForMember(x => x.list_model_desapropriacao_individual_revisao, o => o.MapFrom(y => y.tab_desapropriacao_individual_revisao))
                .ForMember(x => x.list_model_desapropriacao_individual_status_evento, o => o.MapFrom(y => y.tab_desapropriacao_individual_status_evento))
                .ForMember(x => x.list_model_desapropriacao_individual_anexos, o => o.MapFrom(y => y.tab_desapropriacao_individual_anexos))
                .ForMember(x => x.list_model_desapropriacao_individual_cultura, o => o.MapFrom(y => y.tab_desapropriacao_individual_cultura))
                .ForMember(x => x.list_model_desapropriacao_individual_divisa, o => o.MapFrom(y => y.tab_desapropriacao_individual_divisa))
                .ForMember(x => x.list_model_desapropriacao_individual_edificacao, o => o.MapFrom(y => y.tab_desapropriacao_individual_edificacao))
                .ForMember(x => x.list_model_desapropriacao_individual_entrada, o => o.MapFrom(y => y.tab_desapropriacao_individual_entrada))
                .ForMember(x => x.list_model_desapropriacao_individual_pendencia, o => o.MapFrom(y => y.tab_desapropriacao_individual_pendencia))
                .ForMember(x => x.list_model_desapropriacao_individual_planta_propriedade, o => o.MapFrom(y => y.tab_desapropriacao_individual_planta_propriedade))

                .ForMember(x => x.list_model_desapropriacao_individual_registro_fotografico, o => o.MapFrom(y => y.tab_desapropriacao_individual_registro_fotografico))
                .ForMember(x => x.list_model_desapropriacao_individual_instalacao, o => o.MapFrom(y => y.tab_desapropriacao_individual_instalacao))
                .ForMember(x => x.list_model_desapropriacao_individual_estrada_acesso, o => o.MapFrom(y => y.tab_desapropriacao_individual_estrada_acesso))
                .ForMember(x => x.list_model_desapropriacao_individual_interessado, o => o.MapFrom(y => y.tab_desapropriacao_individual_interessado))
                .ForMember(x => x.list_model_desapropriacao_individual_status_evento, o => o.MapFrom(y => y.tab_desapropriacao_individual_status_evento))
                ;




        }
    }
}