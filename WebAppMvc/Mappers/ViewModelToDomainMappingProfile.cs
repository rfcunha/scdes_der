﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Entity.SCDES.Models;
using Entity.SCDES.Utils.Filtro;
using Entity.SCDES.Utils.Gdv;
using WebAppMvc.Areas.Administrativo.Models.DesapropriacaoIndividualAssistenteTecnico;
using WebAppMvc.Areas.Administrativo.Models.DesapropriacaoIndividualAssistenteTecnico.Filtro;
using WebAppMvc.Areas.Administrativo.Models.DesapropriacaoIndividualPeritoJudicial;
using WebAppMvc.Areas.Administrativo.Models.DesapropriacaoIndividualPeritoJudicial.Filtro;
using WebAppMvc.Areas.Administrativo.Models.DesapropriacaoIndividualPredominanteEntorno;
using WebAppMvc.Areas.Administrativo.Models.DesapropriacaoIndividualPredominanteEntorno.Filtro;
using WebAppMvc.Areas.Administrativo.Models.DesapropriacaoIndividualSituacao;
using WebAppMvc.Areas.Administrativo.Models.DesapropriacaoIndividualSituacao.Filtro;
using WebAppMvc.Areas.Administrativo.Models.DesapropriacaoIndividualStatus;
using WebAppMvc.Areas.Administrativo.Models.DesapropriacaoIndividualStatus.Filtro;
using WebAppMvc.Areas.Administrativo.Models.DesapropriacaoIndividualStatusEventoTipo;
using WebAppMvc.Areas.Administrativo.Models.DesapropriacaoIndividualStatusEventoTipo.Filtro;
using WebAppMvc.Areas.Administrativo.Models.DesapropriacaoProjetoDepartamentoResponsavel;
using WebAppMvc.Areas.Administrativo.Models.DesapropriacaoProjetoDepartamentoResponsavel.Filtro;
using WebAppMvc.Areas.Administrativo.Models.DesapropriacaoProjetoStatus;
using WebAppMvc.Areas.Administrativo.Models.DesapropriacaoProjetoStatus.Filtro;
using WebAppMvc.Areas.Desapropriacao.Models;
using WebAppMvc.Areas.Administrativo.Models.Emitente;

namespace WebAppMvc.Mappers
{
    public class ViewModelToDomainMappingProfile:Profile
    {

	        protected override void Configure()
	        {
                #region | FILTRO |

                Mapper.CreateMap<model_desapropriacao_departamento_filtro, tab_desapropriacao_departamento_filtro>();
                Mapper.CreateMap<model_desapropriacao_status_filtro, tab_desapropriacao_status_filtro>();

                Mapper.CreateMap<model_desapropriacao_individual_predominante_entorno_filtro, tab_desapropriacao_individual_predominante_entorno_filtro>();
                Mapper.CreateMap<model_desapropriacao_individual_perito_judicial_filtro, tab_desapropriacao_individual_perito_judicial_filtro>();
                Mapper.CreateMap<model_desapropriacao_individual_assistente_tecnico_filtro, tab_desapropriacao_individual_assistente_tecnico_filtro>();
                Mapper.CreateMap<model_desapropriacao_individual_situacao_filtro, tab_desapropriacao_individual_situacao_filtro>();
                Mapper.CreateMap<model_desapropriacao_individual_status_filtro, tab_desapropriacao_individual_status_filtro>();
                Mapper.CreateMap<model_desapropriacao_individual_status_evento_tipo_filtro, tab_desapropriacao_individual_status_evento_tipo_filtro>();

                #endregion
                
	            Mapper.CreateMap<model_desapropriacao_departamento, tab_desapropriacao_departamento>();
                Mapper.CreateMap<model_desapropriacao_status, tab_desapropriacao_status>();
                Mapper.CreateMap<model_desapropriacao_observacao, tab_desapropriacao_observacao>();
                Mapper.CreateMap<model_desapropriacao_localidade, tab_desapropriacao_localidade>();
                Mapper.CreateMap<model_desapropriacao_recurso_fonte, tab_desapropriacao_recurso_fonte>();
                Mapper.CreateMap<model_desapropriacao_empreendimento, tab_desapropriacao_empreendimento>();
                Mapper.CreateMap<model_emitente, tab_emitente>();
                Mapper.CreateMap<model_desapropriacao_emitente, tab_desapropriacao_emitente>();

                Mapper.CreateMap<model_desapropriacao_individual_predominante_entorno, tab_desapropriacao_individual_predominante_entorno>();
                Mapper.CreateMap<model_desapropriacao_individual_perito_judicial, tab_desapropriacao_individual_perito_judicial>();
                Mapper.CreateMap<model_desapropriacao_individual_assistente_tecnico, tab_desapropriacao_individual_assistente_tecnico>();
                Mapper.CreateMap<model_desapropriacao_individual_situacao, tab_desapropriacao_individual_situacao>();
                    
                Mapper.CreateMap<model_desapropriacao_individual_status, tab_desapropriacao_individual_status>();
                Mapper.CreateMap<model_desapropriacao_individual_status_evento, tab_desapropriacao_individual_status_evento>();
                Mapper.CreateMap<model_desapropriacao_individual_status_evento_tipo, tab_desapropriacao_individual_status_evento_tipo>();

	            

                Mapper.CreateMap<model_anexo, tab_anexo>();
                Mapper.CreateMap<model_anexo_identificador, tab_anexo_identificador>()
	                .ForMember(x => x.tab_anexo, o => o.MapFrom(y => y.list_model_anexo));

	            Mapper.CreateMap<model_desapropriacao_individual_revisao, tab_desapropriacao_individual_revisao>();
                Mapper.CreateMap<model_desapropriacao_individual_anexos, tab_desapropriacao_individual_anexos>()
	                .ForMember(x => x.tab_anexo_identificador, o => o.MapFrom(y => y.model_anexo_identificador));

                Mapper.CreateMap<model_desapropriacao_individual_cultura, tab_desapropriacao_individual_cultura>()
                    .ForMember(x => x.tab_anexo_identificador, o => o.MapFrom(y => y.model_anexo_identificador));

                Mapper.CreateMap<model_desapropriacao_individual_divisa, tab_desapropriacao_individual_divisa>()
                    .ForMember(x => x.tab_anexo_identificador, o => o.MapFrom(y => y.model_anexo_identificador));

                Mapper.CreateMap<model_desapropriacao_individual_edificacao_material, tab_desapropriacao_individual_edificacao_material>();

                Mapper.CreateMap<model_desapropriacao_individual_edificacao, tab_desapropriacao_individual_edificacao>()
                    .ForMember(x => x.tab_desapropriacao_individual_edificacao_material, o => o.MapFrom(y => y.list_model_desapropriacao_individual_edificacao_material));

	            Mapper.CreateMap<model_desapropriacao_individual_entrada, tab_desapropriacao_individual_entrada>()
                    .ForMember(x => x.tab_anexo_identificador, o => o.MapFrom(y => y.model_anexo_identificador));

                Mapper.CreateMap<model_desapropriacao_individual_instalacao, tab_desapropriacao_individual_instalacao>()
                    .ForMember(x => x.tab_anexo_identificador, o => o.MapFrom(y => y.model_anexo_identificador));

	            Mapper.CreateMap<model_desapropriacao_individual_pendencia, tab_desapropriacao_individual_pendencia>();
                Mapper.CreateMap<model_desapropriacao_individual_registro_fotografico, tab_desapropriacao_individual_registro_fotografico>()
                    .ForMember(x => x.tab_anexo_identificador, o => o.MapFrom(y => y.model_anexo_identificador));

	            Mapper.CreateMap<model_desapropriacao_individual_estrada_acesso, tab_desapropriacao_individual_estrada_acesso>()
                        .ForMember(x => x.tab_anexo_identificador, o => o.MapFrom(y => y.model_anexo_identificador));

	            Mapper.CreateMap<model_desapropriacao_individual_planta_propriedade, tab_desapropriacao_individual_planta_propriedade>()
	                .ForMember(x => x.tab_anexo_identificador, o => o.MapFrom(y => y.model_anexo_identificador));

	            Mapper.CreateMap<model_desapropriacao_individual_interessado, tab_desapropriacao_individual_interessado>();



	            Mapper.CreateMap<model_desapropriacao_emitente, tab_desapropriacao_emitente_gdv>();
                Mapper.CreateMap<model_desapropriacao_emitente, tab_desapropriacao_emitente>();

                Mapper.CreateMap<model_select_helper, tab_select_helper>();
                Mapper.CreateMap<model_select_helper_guia, tab_select_helper_guia>();


	            Mapper.CreateMap<model_desapropriacao, tab_desapropriacao>()
                    .ForMember(x => x.tab_desapropriacao_observacao, o => o.MapFrom(y => y.list_model_desapropriacao_observacao))
	                .ForMember(x => x.tab_desapropriacao_localidade,o => o.MapFrom(y => y.list_model_desapropriacao_localidade))
                    .ForMember(x => x.tab_desapropriacao_recurso_fonte,o => o.MapFrom(y => y.list_model_desapropriacao_recurso_fonte))
                    .ForMember(x => x.tab_desapropriacao_empreendimento, o => o.MapFrom(y => y.list_model_desapropriacao_empreendimento))
                    .ForMember(x => x.tab_desapropriacao_emitente, o => o.MapFrom(y => y.list_model_desapropriacao_emitente));


                Mapper.CreateMap<model_desapropriacao_individual, tab_desapropriacao_individual>()
                    .ForMember(x => x.din_conducao_isi_id, o => o.MapFrom(y => y.din_conducao_situacao_desapropriacao))
                    .ForMember(x => x.din_conducao_isi_obs, o => o.MapFrom(y => y.din_conducao_situacao_desapropriacao_obs))
                    .ForMember(x => x.tab_desapropriacao_individual_predominante_entorno, o => o.MapFrom(y => y.model_desapropriacao_individual_predominante_entorno))
                    .ForMember(x => x.tab_desapropriacao_individual_perito_judicial, o => o.MapFrom(y => y.model_desapropriacao_individual_perito_judicial))
                    .ForMember(x => x.tab_desapropriacao_individual_assistente_tecnico, o => o.MapFrom(y => y.model_desapropriacao_individual_assistente_tecnico))
                    .ForMember(x => x.tab_desapropriacao_individual_situacao, o => o.MapFrom(y => y.model_desapropriacao_individual_situacao))
                    .ForMember(x => x.tab_desapropriacao_individual_status, o => o.MapFrom(y => y.model_desapropriacao_individual_status))

                    .ForMember(x => x.tab_desapropriacao_individual_revisao, o => o.MapFrom(y => y.list_model_desapropriacao_individual_revisao))
                    .ForMember(x => x.tab_desapropriacao_individual_status_evento, o => o.MapFrom(y => y.list_model_desapropriacao_individual_status_evento))
                    .ForMember(x => x.tab_desapropriacao_individual_anexos, o => o.MapFrom(y => y.list_model_desapropriacao_individual_anexos))
                    .ForMember(x => x.tab_desapropriacao_individual_cultura, o => o.MapFrom(y => y.list_model_desapropriacao_individual_cultura))
                    .ForMember(x => x.tab_desapropriacao_individual_divisa, o => o.MapFrom(y => y.list_model_desapropriacao_individual_divisa))
                    .ForMember(x => x.tab_desapropriacao_individual_edificacao, o => o.MapFrom(y => y.list_model_desapropriacao_individual_edificacao))
                    .ForMember(x => x.tab_desapropriacao_individual_entrada, o => o.MapFrom(y => y.list_model_desapropriacao_individual_entrada))
                    .ForMember(x => x.tab_desapropriacao_individual_pendencia, o => o.MapFrom(y => y.list_model_desapropriacao_individual_pendencia))
                    .ForMember(x => x.tab_desapropriacao_individual_planta_propriedade, o => o.MapFrom(y => y.list_model_desapropriacao_individual_planta_propriedade))

                    .ForMember(x => x.tab_desapropriacao_individual_registro_fotografico, o => o.MapFrom(y => y.list_model_desapropriacao_individual_registro_fotografico))
                    .ForMember(x => x.tab_desapropriacao_individual_instalacao, o => o.MapFrom(y => y.list_model_desapropriacao_individual_instalacao))
                    .ForMember(x => x.tab_desapropriacao_individual_estrada_acesso, o => o.MapFrom(y => y.list_model_desapropriacao_individual_estrada_acesso))
                    .ForMember(x => x.tab_desapropriacao_individual_interessado, o => o.MapFrom(y => y.list_model_desapropriacao_individual_interessado));
	        }
    }
}

