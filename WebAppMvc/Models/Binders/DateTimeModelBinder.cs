﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebAppMvc.Models.Binders
{
    public class DateTimeModelBinder : DefaultModelBinder
    {
        //public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        //{
        //    var displayFormat = bindingContext.ModelMetadata.DisplayFormatString;
        //    var value = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);

        //    if (value != null)
        //    {
        //        DateTime date;

        //        if (displayFormat != null)
        //        {
        //            if (DateTime.TryParseExact(value.AttemptedValue, displayFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
        //            {
        //                return date;
        //            }

        //            bindingContext.ModelState.AddModelError(bindingContext.ModelName, string.Format("{0} is an invalid date format", value.AttemptedValue));
        //        }
        //        else
        //        {
        //            // use the format specified in the DisplayFormat attribute to parse the date
        //            if (DateTime.TryParse(value.AttemptedValue, new CultureInfo("pt-BR"), DateTimeStyles.None, out date))
        //            {
        //                return date;
        //            }

        //            bindingContext.ModelState.AddModelError(bindingContext.ModelName, string.Format("{0} is an invalid date format", value.AttemptedValue));
        //        }
        //    }

        //    return base.BindModel(controllerContext, bindingContext);
        //}



        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var underlyingType = Nullable.GetUnderlyingType(bindingContext.ModelType) ?? bindingContext.ModelType;
            var value = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
            var submittedValue = value == null ? null : value.AttemptedValue;
            var formatString = bindingContext.ModelMetadata.DisplayFormatString;

            if (underlyingType != typeof(DateTime) || string.IsNullOrEmpty(formatString) || string.IsNullOrEmpty(submittedValue))
                return base.BindModel(controllerContext, bindingContext);

            DateTime parsedDate;

            if (!DateTime.TryParseExact(submittedValue, formatString.Replace("{0:", "").Replace("}", ""), new DateTimeFormatInfo(), DateTimeStyles.None, out parsedDate))
            {
                bindingContext.ModelState.AddModelError(bindingContext.ModelName, string.Format("The value '{0}' is not valid for {1}.", submittedValue, bindingContext.ModelMetadata.DisplayName ?? bindingContext.ModelMetadata.PropertyName));
                bindingContext.ModelMetadata.Model = bindingContext.ModelType.IsValueType ? Activator.CreateInstance(bindingContext.ModelType) : null;

                return bindingContext.ModelMetadata.Model;
            }

            bindingContext.ModelMetadata.Model = parsedDate;
            return parsedDate;
        }
    }
}