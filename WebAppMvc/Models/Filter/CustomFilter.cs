﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Suporte.Criptografia;

namespace WebAppMvc.Models.Filter
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public sealed class NoCacheAttribute : ActionFilterAttribute
    {
        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            filterContext.HttpContext.Response.Cache.SetExpires(DateTime.UtcNow.AddDays(-1));
            filterContext.HttpContext.Response.Cache.SetValidUntilExpires(false);
            filterContext.HttpContext.Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
            filterContext.HttpContext.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            filterContext.HttpContext.Response.Cache.SetNoStore();

            base.OnResultExecuting(filterContext);
        }
    }

    public class HandleErrorJsonAttribute : FilterAttribute, IExceptionFilter
    {
        public void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;
            filterContext.HttpContext.Response.Clear();
            filterContext.HttpContext.Response.StatusCode = 500;
            filterContext.HttpContext.Response.TrySkipIisCustomErrors = true;

            filterContext.Result = new JsonResult
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                ContentType = "application/json",
                Data = new
                {
                    ExceptionStack = filterContext.Exception.StackTrace,
                    ExceptionSource = filterContext.Exception.Source,
                    ExceptionHelpLink = filterContext.Exception.HelpLink,
                    //msgErro = filterContext.Exception.ToString()
                    msgErro = filterContext.Exception.Message,
                }
            };
        }
    }

    public class HandleErrorActionAttribute : FilterAttribute, IExceptionFilter
    {
        public void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;
            filterContext.HttpContext.Response.Clear();
            filterContext.HttpContext.Response.StatusCode = 500;
            filterContext.HttpContext.Response.TrySkipIisCustomErrors = true;

            if (filterContext.HttpContext.Request.HttpMethod.Equals("POST"))
            {
                if (filterContext.HttpContext.Request.Url != null)
                {
                    var hasQuerystring = (filterContext.HttpContext.Request.QueryString.Count > 0);
                    var additionalQueryString = (hasQuerystring)
                        ? "&erro=true&msgErro=" + Cripto.Encrypt(filterContext.Exception.Message)
                        : "?erro=true&msgErro=" + Cripto.Encrypt(filterContext.Exception.Message);



                    filterContext.Result = new RedirectResult(filterContext.HttpContext.Request.Url + additionalQueryString);
                }
            }
            else if (filterContext.HttpContext.Request.HttpMethod.Equals("GET"))
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary
                    {
                        {"controller", "CustomError"},
                        {"action", "PageError"},
                        {"area", ""},
                        {"msgErro", Cripto.Encrypt(filterContext.Exception.Message)}
                    });

                //filterContext.Controller.TempData.Add("errorMsg", filterContext.Exception.Message);
            }
        }
    }



}