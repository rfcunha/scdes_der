﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Xml;
using Business.GERENCIADORPORTAL;
using Entity.GERENCIADORPORTAL;
using Entity.GERENCIADORPORTAL.Utils.Filtro;
using Suporte.Criptografia;
using Suporte.Exceptions;
using Suporte.Imagem;
using Suporte.MsgBox;
using Suporte.MsgBox.Mvc;
using WebAppMvc.Helpers;

namespace WebAppMvc.Controllers.MasterPage
{
    public class MasterPageController : BaseController
    {
        #region FUNCTION MASTER

        [HttpPost]
        public ActionResult AlterarSenha(string txtSenhaNova, string txtSenhaNovaConfirmar)
        {
            try
            {
                var obj = new tab_usuario_bll().GET_BY(((tab_usuario)Session["objUsuario"]).usu_id);
                obj.usu_senha = Cripto.Encrypt(txtSenhaNova);
                obj.usu_trocar_senha = false;
                new tab_usuario_bll().STP_UPD_USUARIO_SENHA(obj.usu_id, obj.usu_senha, obj.usu_login, obj.usu_trocar_senha);
                return new bootStrapDialog().Show("Sucesso!", "Senha alterada com sucesso", "function(dialog) {dialog.close();$('#div_trocar_senha').modal('hide');}", bootStrapMessageType.TYPE_SUCCESS);

            }
            catch (Exception ex)
            {
                return new bootStrapDialog().Show("Erro!", ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
        }

        [HttpGet]
        public ActionResult Login()
        {
            return PartialView("_Login");
        }

        //[HttpPost]
        //public ActionResult Login(string usu_login, string usu_senha)
        //{
        //    return ValidarLogin(usu_login, Cripto.Encrypt(usu_senha));
        //}

        [HttpPost]
        public ActionResult Login(string usu_login, string usu_senha)
        {
            try
            {
                var _sis_id = new tab_configuracao_sistema_bll().STP_GET_BY_ID_SISTEMA();
                if (_sis_id > 0)
                {
                    //VALIDANDO USUARIO GERENCIADOR DE CONTAS
                    var tabUsuario = new tab_usuario_bll().VALIDAR_USUARIO(new tab_usuario_filtro() { usu_login = usu_login, usu_senha = Cripto.Encrypt(usu_senha)});

                    if (tabUsuario.usu_id > 0)
                    {
                        //Verificando Imagem do Usuario
                        CreateUpdateImageUser(tabUsuario.usu_id);
                        tabUsuario.usu_url_imagem = @"~\Content\Images\User\" + tabUsuario.usu_id + "\\" + tabUsuario.usu_id + ".png";

                        //VALIDANDO USUARIO SE EXISTE PERFIL NO GERENCIADOR DE CONTAS
                        var itemUsuarioPerfilSistema = new tab_usuario_perfil_sistema_bll().GET_ALL(new tab_usuario_perfil_sistema_filtro() { usu_id = tabUsuario.usu_id, sis_id = _sis_id }).FirstOrDefault();
                        if (itemUsuarioPerfilSistema != null)
                        {
                            //VALIDANDO SE PERFIL DO USUARIO EXISTENTE
                            tabUsuario.tab_perfil = new tab_perfil_bll().GET_BY(itemUsuarioPerfilSistema.per_id);
                            if (tabUsuario.tab_perfil.per_id > 0)
                            {
                                if (!tabUsuario.tab_perfil.per_update_xml)
                                {
                                    //CRIANDO XML DE MENU
                                    tabUsuario.tab_perfil.per_update_xml = true;
                                    tabUsuario.tab_perfil.userLastUpdate = tabUsuario.usu_login;
                                    new tab_perfil_bll().UPDATE(tabUsuario.tab_perfil);
                                    new tab_menu_bll().CreateMenu(tabUsuario.tab_perfil.per_id);
                                }
                                Session["objUsuario"] = tabUsuario;
                                return JavaScript("MensagemLogin('hide')");
                            }
                            else
                            {
                                return new bootStrapDialog().Show("Informativo", "Perfil inativo ou não existe no sistema.<br/>Entre em contato com o administrador do sistema.", "function(dialog) {dialog.close();MensagemLogin('show');}", bootStrapMessageType.TYPE_WARNING);
                            }
                        }
                        else
                        {

                            return new bootStrapDialog().Show("Informativo", "Usuário inativo ou não existe no sistema.<br/>Entre em contato com o administrador do sistema.", "function(dialog) {dialog.close();MensagemLogin('show');}", bootStrapMessageType.TYPE_WARNING);
                        }
                    }
                    else
                    {

                        return new bootStrapDialog().Show("Informativo", "Usuário inativo ou não existe.<br/>Entre em contato com o administrador do sistema.", "function(dialog) {dialog.close();MensagemLogin('show');}", bootStrapMessageType.TYPE_WARNING);
                    }
                }
                else
                {
                    return new bootStrapDialog().Show("Informativo", "Sistema não está configurado corretamente. Entre em contato com o administrador do sistema.", bootStrapMessageType.TYPE_WARNING);
                }
            }
            catch (Exception ex)
            {
                return new bootStrapDialog().Show("Erro!", ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
        }

        public JsonResult RolesPerfil(string url)
        {
            var roles = new tab_perfil_menu_role_bll().RETORNA_OBJETO_HTML_ROLES_PERFIL(idPerfil, url);
            return Json(new { roles });
        }

        #endregion

        #region METODOS
        
        #region | METODOS IMAGEM |

        private void CreateUpdateImageUser(int usu_id)
        {

            if (!Directory.Exists(Server.MapPath(@"~\Content\Images\User\" + usu_id)))
                Directory.CreateDirectory(Server.MapPath(@"~\Content\Images\User\" + usu_id));


            if (System.IO.File.Exists(Server.MapPath(@"~\Content\Images\User\" + usu_id + "\\" + usu_id + ".png")))
                System.IO.File.Delete(Server.MapPath(@"~\Content\Images\User\" + usu_id + "\\" + usu_id + ".png"));

            var imagem = new Imagem().byteArrayToImage(new tab_usuario_bll().GET_BY(usu_id).usu_imagem);


            imagem.Save(Server.MapPath(@"~\Content\Images\User\" + usu_id + "\\" + usu_id + ".png"));
        }

        #endregion

        #endregion
    }
}
