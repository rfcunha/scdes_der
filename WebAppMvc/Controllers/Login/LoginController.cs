﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Xml;
using Business.GERENCIADORPORTAL;
using Entity.GERENCIADORPORTAL;
using Entity.GERENCIADORPORTAL.Utils.Filtro;
using Suporte.Criptografia;
using Suporte.Exceptions;
using Suporte.Imagem;
using Suporte.MsgBox;
using Suporte.MsgBox.Mvc;

namespace WebAppMvc.Controllers.Login
{
    public class LoginController : Controller
    {
        [HttpGet]
        public ActionResult Login(string argsLogin)
        {
            if (argsLogin == null) return View(); 
            var args = new EncryptQueryString(argsLogin);
            return ValidarLogin(args["usu_id"]);  
        } 
         
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ValidarLogin(string usu_login, string usu_senha, string RedirectUrl)
        {
            try
            {
                //VALIDANDO USUARIO GERENCIADOR DE CONTAS 
                usu_senha = Cripto.Encrypt(usu_senha);
                var tabUsuario = new tab_usuario_bll().VALIDAR_USUARIO(new tab_usuario_filtro() { usu_login = usu_login, usu_senha = usu_senha });
                if (tabUsuario.usu_id > 0)
                {
                    if (tabUsuario.usu_trocar_senha)
                    {
                        return JavaScript("$('#usu_senha_hidden').val('" + usu_senha + "');$('#usu_login_hidden').val('" + usu_login + "');$('#form_login').css('display','none');$('#form_nova_senha').css('display','block');");
                    }
                    else
                    {
                        return RealizarLogin(tabUsuario, RedirectUrl);
                    }
                }
                else
                {
                    return new bootStrapDialog().Show("Informativo", "Usuário inativo ou não existe.<br/>Entre em contato com o administrador do sistema.", bootStrapMessageType.TYPE_WARNING);
                }
            }
            catch (Exception ex)
            {
                return new bootStrapDialog().Show("Erro!", ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
        }

        [HttpGet]
        public ActionResult ValidarLogin(string usu_id)
        {
            try
            {
                //VALIDANDO USUARIO GERENCIADOR DE CONTAS
                var tabUsuario = new tab_usuario_bll().GET_BY(int.Parse(usu_id));
                if (tabUsuario.usu_id > 0)
                {
                    if (tabUsuario.usu_trocar_senha)
                    {
                        return JavaScript("$('#usu_senha_hidden').val('123Mudar');$('#usu_senha_hidden').val('" + tabUsuario.usu_login + "');$('#form_login').css('display','none');$('#form_nova_senha').css('display','block');"); 
                    }
                    else
                    {
                        return RealizarLogin(tabUsuario, "");
                    }
                }
                else
                {
                    return new bootStrapDialog().Show("Informativo", "Usuário inativo ou não existe.<br/>Entre em contato com o administrador do sistema.", bootStrapMessageType.TYPE_WARNING);
                }
            }
            catch (Exception ex)
            {
                return new bootStrapDialog().Show("Erro!", ex.Message, bootStrapMessageType.TYPE_DANGER);
            } 
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AlterarSenha(string usu_login_hidden, string usu_senha_hidden, string txtSenhaNova)
        {
            try
            {
                //VALIDANDO USUARIO GERENCIADOR DE CONTAS
                var obj = new tab_usuario_bll().VALIDAR_USUARIO(new tab_usuario_filtro() { usu_login = usu_login_hidden, usu_senha = usu_senha_hidden });
                obj = new tab_usuario_bll().GET_BY(obj.usu_id);
                obj.usu_senha = Cripto.Encrypt(txtSenhaNova);
                obj.usu_trocar_senha = false;
                new tab_usuario_bll().STP_UPD_USUARIO_SENHA(obj.usu_id, obj.usu_senha, obj.usu_login,obj.usu_trocar_senha);
                return RealizarLogin(obj, "");
            }
            catch (Exception ex)
            {
                return new bootStrapDialog().Show("Erro!", ex.Message, bootStrapMessageType.TYPE_DANGER);
            } 
        }

        public ActionResult RealizarLogin(tab_usuario tabUsuario, string RedirectUrl)
        {
            //VALIDANDO USUARIO SE EXISTE PERFIL NO GERENCIADOR DE CONTAS
            var _sis_id = new tab_configuracao_sistema_bll().STP_GET_BY_ID_SISTEMA();
            if (_sis_id > 0)
            {
                var itemUsuarioPerfilSistema = new tab_usuario_perfil_sistema_bll().GET_ALL(new tab_usuario_perfil_sistema_filtro() { usu_id = tabUsuario.usu_id, sis_id = _sis_id }).FirstOrDefault();
                if (itemUsuarioPerfilSistema != null)
                {
                    //Verificando Imagem do Usuario
                    CreateUpdateImageUser(tabUsuario.usu_id);
                    tabUsuario.usu_url_imagem = @"~\Content\Images\User\" + tabUsuario.usu_id + "\\" + tabUsuario.usu_id + ".png";
                    //VALIDANDO SE PERFIL DO USUARIO EXISTENTE
                    tabUsuario.tab_perfil = new tab_perfil_bll().GET_BY(itemUsuarioPerfilSistema.per_id);
                    if (tabUsuario.tab_perfil.per_id > 0)
                    {
                        if (!tabUsuario.tab_perfil.per_update_xml)
                        {
                            //CRIANDO XML DE MENU
                            tabUsuario.tab_perfil.per_update_xml = true;
                            tabUsuario.tab_perfil.userLastUpdate = tabUsuario.usu_login;

                            new tab_perfil_bll().UPDATE_XML(tabUsuario.tab_perfil.per_id);
                            new tab_menu_bll().CreateMenu(tabUsuario.tab_perfil.per_id);
                        }

                        Session["objUsuario"] = tabUsuario;

                        var paginaRedirecionar = RedirectUrl;
                        var args = Request.QueryString["args"];

                        if (string.IsNullOrEmpty(paginaRedirecionar))
                        {
                            return JavaScript("window.location.href ='" + Url.Action("Index", "Home") + "'"); 
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(args))
                            {
                                return JavaScript("window.location.href ='" + paginaRedirecionar + "'");  
                            }
                            else
                            {
                                return JavaScript("window.location.href ='" + paginaRedirecionar + "?args=" + args + "'");  
                            }
                        }

                        return null;
                    }
                    else
                    {
                        return new bootStrapDialog().Show("Informativo", "Perfil inativo ou não existe no sistema.<br/>Entre em contato com o administrador do sistema.", bootStrapMessageType.TYPE_WARNING);
                    }
                }
                else
                {
                    return new bootStrapDialog().Show("Informativo", "Usuário inativo ou não existe no sistema.<br/>Entre em contato com o administrador do sistema.", bootStrapMessageType.TYPE_WARNING);
                }
            }
            else
            {
                return new bootStrapDialog().Show("Informativo", "Sistema não está configurado corretamente. Entre em contato com o administrador do sistema.", bootStrapMessageType.TYPE_WARNING);
            } 
        }

        #region METODOS  
 
        

        #region METODOS IMAGEM

        private void CreateUpdateImageUser(int usu_id)
        {

            if (!Directory.Exists(Server.MapPath(@"~\Content\Images\User\" + usu_id)))
                Directory.CreateDirectory(Server.MapPath(@"~\Content\Images\User\" + usu_id));

            if (System.IO.File.Exists(Server.MapPath(@"~\Content\Images\User\"+usu_id + "\\" + usu_id + ".png")))
                System.IO.File.Delete(Server.MapPath(@"~\Content\Images\User\"+usu_id + "\\" + usu_id + ".png"));

            var imagem = new Imagem().byteArrayToImage(new tab_usuario_bll().GET_BY(usu_id).usu_imagem);

            imagem.Save(Server.MapPath(@"~\Content\Images\User\" + usu_id + "\\" + usu_id + ".png"));
        }

        #endregion

        #endregion

        [HttpGet]
        public ActionResult Logoff()
        {
            Session["objUsuario"] = null;
            return View("Login");
        } 

    }
}
