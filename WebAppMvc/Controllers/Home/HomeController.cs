﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Xml;
using Business.GERENCIADORPORTAL;
using Entity.GERENCIADORPORTAL;
using Entity.GERENCIADORPORTAL.Utils.Filtro;
using Suporte.Criptografia;
using Suporte.Exceptions;
using Suporte.Imagem;
using Suporte.MsgBox.Mvc;
using Suporte.MsgBox;
using WebAppMvc.Helpers;
using WebAppMvc.Models.Filter;

namespace WebAppMvc.Controllers.Home
{
    public class HomeController : BaseController
    {
        [HttpGet]
        //[CustomActionFilter(isModal = false)]
        public ActionResult Index()
        {
            return View();
        }
       
    }
}
