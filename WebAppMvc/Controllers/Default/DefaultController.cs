﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebAppMvc.Controllers.Default
{
    public class DefaultController : Controller
    {
        [HttpGet]
        public ActionResult Index(string argsLogin)
        {
            if (argsLogin == null)
            {
                return RedirectToAction("Login", "Login");
            }
            else
            {
                return RedirectToAction("Login", "Login", new {  argsLogin });
            }
        }

    }
}
