﻿using System.Web.Mvc;

namespace WebAppMvc.Controllers.CustomError
{
    public class CustomErrorController : Controller
    {
        public ActionResult PageAcessDenied()
        {
            return View();
        }

        public ActionResult PageError()
        {
            return View();
        }

        public ActionResult PageNotFound()
        {
            return View();
        } 
    }
}
