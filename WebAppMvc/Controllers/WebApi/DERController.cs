﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Business.DER;
using Entity.DER.Utils;
using Entity.DER.Utils.Filtro;


namespace WebAppMvc.Controllers.WebApi
{
    public class DERController : ApiController
    {
        [HttpGet]
        public HttpResponseMessage CarregarEstado()
        {
            var list = new tab_estado_bll().FIND(new tab_estado_filtro()).Select(x => new { x.est_id, x.est_nome, x.est_uf });

            return Request.CreateResponse(HttpStatusCode.OK, list, Configuration.Formatters.JsonFormatter);
        }

        [HttpGet]
        public HttpResponseMessage CarregarMunicipio(int est_id)
        {
            var list = new tab_municipio_bll().FIND(new tab_municipio_filtro { est_id = est_id }).Select(x => new { x.mun_id, x.mun_nome, x.mun_uf });

            return Request.CreateResponse(HttpStatusCode.OK, list, Configuration.Formatters.JsonFormatter);
        }

        [HttpGet]
        public HttpResponseMessage CarregarComarca(bool com_ativo)
        {
            var list = new tab_comarca_bll().FIND(new tab_comarca_filtro { com_ativo = com_ativo}).Select(x => new { x.com_id, x.com_nome });

            return Request.CreateResponse(HttpStatusCode.OK, list, Configuration.Formatters.JsonFormatter);
        }

        [HttpGet]
        public HttpResponseMessage CarregarSoloUso(bool sus_ativo)
        {
            var list = new tab_solo_uso_bll().FIND(new tab_solo_uso_filtro { sus_ativo = sus_ativo }).Select(x => new { x.sus_id, x.sus_nome });

            return Request.CreateResponse(HttpStatusCode.OK, list, Configuration.Formatters.JsonFormatter);
        }

    }
}
