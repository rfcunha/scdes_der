﻿using System.Web;
using System.Web.Optimization;

namespace WebAppMvc
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            //bundles.Clear();

                 

            bundles.Add(new ScriptBundle("~/Script/Jquery").Include("~/Scripts/Jquery/jquery-2.0.0.js"));
             
            bundles.Add(new ScriptBundle("~/Script/JqueryIE9").Include("~/Scripts/Jquery/jquery-1.9.0.js", "~/Scripts/plugins/html5shiv.js", "~/Scripts/plugins/respond.min.js"));

            bundles.Add(new ScriptBundle("~/Script/JqueryUi").Include("~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/Script/Unobtrusive").Include("~/Scripts/Jquery.Unobtrusive/jquery.unobtrusive-ajax.min.js"));

            bundles.Add(new ScriptBundle("~/Script/Boot").Include(
                "~/Scripts/Jquery.Boostrap/bootstrap-min.js", 
                "~/Scripts/Jquery.Boostrap/boostrap.menu-min.js",
                "~/Scripts/Jquery.Boostrap/bootStrap.Dialog/js/bootstrap-dialog.js"
                ));

            bundles.Add(new ScriptBundle("~/Script/Panels").Include(
               "~/Scripts/jQuery.Panels/snippets.js",
                "~/Scripts/JQuery.Typeaheadjs/bootstrap3-typeahead.js"));


            bundles.Add(new ScriptBundle("~/Script/Perfect-Scrollbar").Include(
             "~/Scripts/jQuery.Panels/snippets.js",
              "~/jQuery.Perfect-Scrollbar/js/perfect-scrollbar.js"));
 

            bundles.Add(new ScriptBundle("~/Script/JqueryUi").Include("~/Scripts/JqueryUI/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/Script/Unobtrusive").Include("~/Scripts/Jquery.Unobtrusive/jquery.unobtrusive-ajax-min.js"));

            bundles.Add(new ScriptBundle("~/Script/Modal").Include("~/Scripts/Jquery.MessageBox/jquery.msgbox.i18n.js", "~/Scripts/Jquery.MessageBox/jquery.msgbox.js"));


            bundles.Add(new ScriptBundle("~/Script/Grid").Include("~/Scripts/Jquery.Datatable/jquery.dataTables.js",
                                                                  "~/Scripts/Jquery.Datatable/dataTables.responsive.js",
                                                                  "~/Scripts/Jquery.Datatable/dataTables.tableTools.js",
                                                                  "~/Scripts/Jquery.Datatable/dataTables.semanticui-min.js",
                                                                  "~/Scripts/Jquery.Datatable/dataTables.buttons-min.js",
                                                                  "~/Scripts/Jquery.Datatable/buttons.semanticui-min.js",
                                                                  "~/Scripts/Jquery.Datatable/jszip-min.js",
                                                                  "~/Scripts/Jquery.Datatable/pdfmake-min.js",
                                                                  "~/Scripts/Jquery.Datatable/vfs_fonts.js",
                                                                  "~/Scripts/Jquery.Datatable/buttons.html5-min.js",
                                                                  "~/Scripts/Jquery.Datatable/buttons.print-min.js",
                                                                  "~/Scripts/Jquery.Datatable/buttons.colVis-min.js",
                                                                  "~/Scripts/Jquery.Datatable/dataTables.responsive.js"));

            bundles.Add(new ScriptBundle("~/Script/Growl").Include("~/Scripts/Jquery.Growl/bootstrap-growl-min.js"));

            bundles.Add(new ScriptBundle("~/Script/Intro").Include("~/Scripts/Jquery.Intro/intro.min.js"));


            bundles.Add(new ScriptBundle("~/Script/Charts").Include(
                "~/Scripts/Jquery.Highcharts/highcharts.js",
                "~/Scripts/Jquery.Highcharts/highcharts-more.js",
                "~/Scripts/Jquery.Highcharts/highcharts-3d.js",
                "~/Scripts/Jquery.Highcharts/modules/exporting.js",
                 "~/Scripts/Jquery.Highcharts/themes/sand-signika.js"));

            bundles.Add(new ScriptBundle("~/Script/Intro").Include(
                "~/Scripts/jQuery.Chosen/chosen.proto.js",
               "~/Scripts/jQuery.Chosen/chosen.jquery.js"));

            bundles.Add(new ScriptBundle("~/Script/Plugins").Include(
                "~/Scripts/plugins/app.min.js",
                "~/Scripts/jQuery.Waiting.Dialog/waitingDialog.js",
                "~/Scripts/plugins/moment.min.js",
                "~/Scripts/plugins/moment-with-locales.min.js",
                "~/Scripts/plugins/summernote/text-editor.min.js"
                ));

            bundles.Add(new ScriptBundle("~/Script/Multiselect").Include("~/Scripts/Plugins/bootstrap-multiselect.js"));

            bundles.Add(new ScriptBundle("~/Script/Mask").Include("~/Scripts/plugins/masked-input-plugin-min.js"));



            bundles.Add(new StyleBundle("~/Content/Css").Include(
                "~/Content/Style/bootstrap.css",
                "~/Content/Style/style.css",
                "~/Content/Style/Fonts.css",
                "~/Content/Style/css-base-min.css",
                "~/Content/Style/all-skins.css",
                "~/Content/Style/animate-min.css",
                "~/Content/Style/bootstrap-multiselect-min.css",
                "~/Scripts/Jquery.Datatable/dataTables.bootstrap.css",
                "~/Scripts/Jquery.Datatable/dataTables.responsive.css",
                "~/Scripts/Jquery.Datatable/dataTables.tableTools.css",
                "~/Scripts/Jquery.Boostrap/bootStrap.Dialog/css/bootstrap-dialog.css",
                "~/Scripts/Jquery.Datatable/buttons.dataTables.min.css",
                "~/Scripts/Jquery.Intro/introjs.min.css",
                "~/Scripts/jQuery.Chosen/bootstrap-chosen.css",
                "~/Scripts/Jquery.MessageBox/css/jquery.msgbox.css",
                "~/Scripts/jQuery.Panels/css/style.css",
                "~/jQuery.Perfect-Scrollbar/css/perfect-scrollbar.min.css",
                "~/Scripts/plugins/summernote/text-editor.min.css",
                "~/Content/Style/lightbox.css"
                ));
        }
    }
}