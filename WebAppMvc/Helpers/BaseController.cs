﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;
using System.Xml;
using Entity.GERENCIADORPORTAL;
using Suporte.Exceptions;
using Suporte.MsgBox;

namespace WebAppMvc.Helpers
{
    public class BaseController : Controller
    {
        private int _idUsuario;
        private string _nomeloginUsuario;
        private int _idPerfil;

        protected string nomeloginUsuario
        {
            get { return ((tab_usuario)Session["objUsuario"]).usu_nome_login; ; }
            set { _nomeloginUsuario = value; }
        }

        protected int idPerfil
        {
            get
            {
                if (System.Web.HttpContext.Current.Session == null || System.Web.HttpContext.Current.Session["objUsuario"] == null)
                {
                    return 1;
                    //throw new CustomException("Erro ao recuperar o perfil; <br/> Acione a equipe de TI!", bootStrapMessageType.TYPE_DANGER);
                }
                else
                {
                    return ((tab_usuario)System.Web.HttpContext.Current.Session["objUsuario"]).tab_perfil.per_id;
                }
                
            }
            set { _idPerfil = value; }
        }

        public int idUsuario
        {
            get { return _idUsuario; }
            set { _idUsuario = ((tab_usuario)System.Web.HttpContext.Current.Session["objUsuario"]).usu_id; }
        }

        protected BaseController()
        {
            CriarMenu();
        }

        #region METODOS




        #endregion

        #region METODOS DROPDOWNLIST

        protected SelectList ListStatusConsulta()
        {
            try
            {
                IList<SelectListItem> iSelectListItem = new List<SelectListItem>
                {
                    new SelectListItem {Text = "Todos", Value = "null"},
                    new SelectListItem {Text = "Ativo", Value = "true"},
                    new SelectListItem {Text = "Desativo", Value = "false"},
                };

                return new SelectList(iSelectListItem, "Value", "Text");
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
        }
         
        #endregion


        #region GERAR MENU

        private void CriarMenu()
        {
            if (System.Web.HttpContext.Current.Session["objUsuario"] == null)
            {
                @ViewBag.Menu = "";

                
            }
            else
            {
                var stringBuilder = new StringBuilder();
                var diretorio = System.Web.HttpContext.Current.Server.MapPath(@"~\Xml\Perfil\") + idPerfil  + ".xml";
                var reader = new XmlTextReader(diretorio);
                var pathOriginal = System.Web.HttpContext.Current.Request.ApplicationPath == "/" ? "" : System.Web.HttpContext.Current.Request.ApplicationPath;

                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element && reader.Name == "Menu")
                    {
                        if (string.IsNullOrEmpty(reader.GetAttribute("Url")))
                        {
                            stringBuilder.Append("<li class='treeview'>");
                            stringBuilder.Append("<a href='javascript:void(0)'><i class='" + reader.GetAttribute("Icon") + "'></i> <span> " + reader.GetAttribute("Text") + " </span> <i class='fa fa-angle-left pull-right'></i></a>");
                            stringBuilder.Append("<ul class='treeview-menu'>");
                        }
                        else
                        {
                            stringBuilder.Append("<li class='treeview'>");
                            stringBuilder.Append("<a href='" + pathOriginal + reader.GetAttribute("Url") + "'><i class='" + reader.GetAttribute("Icon") + "'></i> <span> " + reader.GetAttribute("Text") + " </span></a>");
                            stringBuilder.Append("<ul></ul>");
                        }
                    }
                    if (reader.NodeType == XmlNodeType.Element && reader.Name == "SubMenu")
                    {
                        stringBuilder.Append("<li><a href='" + pathOriginal + reader.GetAttribute("Url") + "'>" + reader.GetAttribute("Text") + "</a></li>");
                    }
                    if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "Menu")
                    {
                        stringBuilder.Append("</ul>");
                        stringBuilder.Append("</li>");
                    }
                }

                @ViewBag.Menu = stringBuilder;
            } 
        }

        #endregion
    }
}