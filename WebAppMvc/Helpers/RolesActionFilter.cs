﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Business.GERENCIADORPORTAL;
using Entity.GERENCIADORPORTAL;
using Entity.GERENCIADORPORTAL.Utils.Filtro;
using Suporte.MsgBox;
using Suporte.MsgBox.Mvc;


namespace WebAppMvc.Helpers
{
    public class RolesActionFilter : ActionFilterAttribute, IActionFilter
    {

        void IActionFilter.OnActionExecuting(ActionExecutingContext filterContext)
        {
            var area = GetAreaName(filterContext.RouteData.Route);
            area = area == "" ? null : "/" + area;
            var controller = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            controller = controller == "" ? null : "/" + controller;
            var action = filterContext.ActionDescriptor.ActionName;
            action = action == "" ? null : "/" + action;
            var currentPage = area + controller + action;

            if (HttpContext.Current.Session["objUsuario"] == null)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary { { "Area", "" }, { "Controller", "Login" }, { "action", "Login" }, { "args", "" }, { "RedirectUrl", currentPage } });
            }
            else
            { 
                var perId = ((tab_usuario)(HttpContext.Current.Session["objUsuario"])).tab_perfil.per_id;
                var roles = new tab_perfil_menu_role_bll().GET_ALL(new tab_perfil_menu_role_filtro() { per_id = perId }).Where(role => currentPage.Contains(role.tab_menu_role.tab_menu.men_url + "" + role.tab_menu_role.tab_menu.men_pagina)).Select(role => role.tab_menu_role.mro_controle).ToList();


                if (!(roles.Count > 0))
                {
                    filterContext.Result = isModal ? new bootStrapDialog().Show("Oops! Acesso negado.", "Entre em contato com o adminstrador do sistema.", bootStrapMessageType.TYPE_DANGER) : new RedirectToRouteResult(new RouteValueDictionary { { "Area", "" }, { "Controller", "CustomError" }, { "Action", "PageAcessDenied" } });
                }
            }
        }


        private static string GetAreaName(RouteBase route)
        {
            var area = route as IRouteWithArea;
            if (area != null) { return area.Area; }
            var route2 = (Route)route;
            if ((route2 != null) && (route2.DataTokens != null)) { return Convert.ToString(route2.DataTokens["area"]); }
            return "";
        }

        /// <summary>
        /// Parametro a se usar SEMPRE
        /// Caso o evento a ser utilizado seja um POST AJAX o isModal = True
        /// Caso o evento a ser utilizado seja um redirecionamento o isModal = False
        /// </summary>
        public bool isModal { get; set; }

    }
}