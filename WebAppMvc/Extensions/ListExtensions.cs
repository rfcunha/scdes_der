﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using WebGrease.Css.Extensions;

namespace WebAppMvc.Extensions
{
    public static class ListExtensions
    {
        internal static List<SelectListItem> ToSelectList(this JsonResult json)
        {
            var lista = new List<SelectListItem>();

            var jsonSerializer = new JavaScriptSerializer();

            var jsonSerialized = jsonSerializer.Serialize(json.Data);
            var jsonItems = JsonConvert.DeserializeObject<Dictionary<string, string>[]>(jsonSerialized);

            jsonItems.ForEach(x => lista.Add(new SelectListItem { Text = x["Text"], Value = x["Value"] }));

            return lista;
        }

        internal static List<SelectListItem> ToSelectList(this JsonResult json, string Text, string Value)
        {
            var lista = new List<SelectListItem>();

            var jsonSerializer = new JavaScriptSerializer();

            var jsonSerialized = jsonSerializer.Serialize(json.Data);
            var jsonItems = JsonConvert.DeserializeObject<Dictionary<string, string>[]>(jsonSerialized);

            jsonItems.ForEach(x => lista.Add(new SelectListItem { Text = x[Text], Value = x[Value] }));

            return lista;
        }

        internal static List<SelectListItem> ToSelectList(this JsonResult json, string Text, string Value, int Select)
        {
            var lista = new List<SelectListItem>();

            var jsonSerializer = new JavaScriptSerializer();

            var jsonSerialized = jsonSerializer.Serialize(json.Data);
            var jsonItems = JsonConvert.DeserializeObject<Dictionary<string, string>[]>(jsonSerialized);

            jsonItems.ForEach(x =>
            {
                lista.Add(x[Value].Equals(@Select.ToString())
                    ? new SelectListItem { Text = x[Text], Value = x[Value], Selected = true }
                    : new SelectListItem { Text = x[Text], Value = x[Value] });
            });



            return lista;
        }

        internal static List<SelectListItem> ToSelectList(this ActionResult json, string Text, string Value)
        {
            var lista = new List<SelectListItem>();

            var jsonSerializer = new JavaScriptSerializer();

            var jsonSerialized = jsonSerializer.Serialize(((JsonResult)json).Data);
            var jsonItems = JsonConvert.DeserializeObject<Dictionary<string, string>[]>(jsonSerialized);

            jsonItems.ForEach(x => lista.Add(new SelectListItem { Text = x[Text], Value = x[Value] }));

            return lista;
        }

        internal static List<SelectListItem> ToSelectList(this ActionResult json, string Text, string Value, int Select)
        {
            var lista = new List<SelectListItem>();

            var jsonSerializer = new JavaScriptSerializer();

            var jsonSerialized = jsonSerializer.Serialize(((JsonResult)json).Data);
            var jsonItems = JsonConvert.DeserializeObject<Dictionary<string, string>[]>(jsonSerialized);

            jsonItems.ForEach(x =>

                                lista.Add(x[Value].Equals(@Select.ToString())
                    ? new SelectListItem { Text = x[Text], Value = x[Value], Selected = true }
                    : new SelectListItem { Text = x[Text], Value = x[Value] }

                ));

            return lista;
        }
    }
}