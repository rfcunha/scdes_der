﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using AutoMapper;
using Business.DER;
using Business.SCDES;
using Entity.DER.Utils;
using Entity.SCDES.Enum;
using Entity.SCDES.Models;
using Entity.SCDES.Utils.Filtro;
using Entity.SCDES.Utils.Gdv;
using Suporte.Criptografia;
using Suporte.Exceptions;
using Suporte.MsgBox;
using Suporte.MsgBox.Mvc;
using WebAppMvc.Areas.Desapropriacao.Models;
using WebAppMvc.Areas.Desapropriacao.Models.Filtro;
using WebAppMvc.Areas.Desapropriacao.Models.Helpers;
using WebAppMvc.Extensions;
using WebAppMvc.Helpers;
using WebAppMvc.Models.Filter;
using WebAppMvc.Areas.Administrativo.Models.DesapropriacaoIndividualStatus;
using Entity.DER.Utils.Filtro;

namespace WebAppMvc.Areas.Desapropriacao.Controllers
{
    public class DesapropriacaoIndividualController : BaseController
    {
        #region | GET |

        [HttpGet]
        [HandleErrorAction]
        //[RolesActionFilter(isModal = false)]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult PreCadastro()
        {
            return View();
        }

        [HttpGet]
        [HandleErrorAction]
        //[RolesActionFilter(isModal = false)]
        public ActionResult Cadastrar()
        {
            ViewBag.CarregarPropriedadeEstado = CarregarPropriedadeEstado().ToSelectList("est_nome", "est_id");
            ViewBag.CarregarPropriedadeContribuinteTipo = CarregarPropriedadeContribuinteTipo().ToSelectList("din_propriedade_contribuinte_tipo_nome", "din_propriedade_contribuinte_tipo");
            ViewBag.CarregarStatusEventoTipo = CarregarStatusEventoTipo().ToSelectList("set_nome", "set_id");

            return View();
        }

        [HttpGet]
        [HandleErrorAction]
        public ActionResult Editar(string args)
        {
            return OBTER_DADOS(args);
        }

        [HttpGet]
        [HandleErrorAction]
        public ActionResult EdicaoCadastro(int din_id) // Editar a partir do primeiro salvamento no cadastro
        {
            return OBTER_DADOS(din_id);
        }

        [HttpGet]
        [HandleErrorAction]
        //[RolesActionFilter(isModal = false)]
        public ActionResult Consultar(string args)
        {
            ViewBag.CarregarRegional = CarregarRegional(true).ToSelectList("reg_nome", "reg_id");
            ViewBag.CarregarAgenteFinanciador = CarregarAgenteFinanciador(true).ToSelectList("afi_nome", "afi_id");
            ViewBag.CarregarStatus = CarregarStatus(true).ToSelectList("dst_nome", "dst_id");
            ViewBag.CarregarRodoviaSp = CarregarRodoviaSp().ToSelectList("rod_sp", "rod_sp");

            var listHelpers = CarregarHelpersGuia();

            ViewBag.CarregarPrioridade = listHelpers["din_propriedade_prioridade"].Select(t => new SelectListItem
            {
                Text = t.seh_nome_exibicao,
                Value = t.seh_value == null ? t.seh_id.ToString() : t.seh_value
            }).ToList();

            return string.IsNullOrEmpty(args) ? View(new model_desapropriacao_individual_filtro()) : View(RetornarPesquisa(args));
        }

        [HttpGet]
        [HandleErrorAction]
        public ActionResult VisualizarDetalhes(string id)
        {
            try
            {
                var objTab = new tab_desapropriacao_individual_bll().FIND(int.Parse(id));

                var din_obj = Mapper.Map<tab_desapropriacao_individual, model_desapropriacao_individual>(objTab);

                PreencherViewBag(din_obj);

                ViewBag.ActionAtual = "Visualizar";

                return View("Cadastrar", din_obj);

            }
            catch (Exception e)
            {
                throw new CustomException("Erro ao encontrar os dados. Erro: <br/>" + e, bootStrapMessageType.TYPE_DANGER);
            }
        }

        #endregion

        #region | POST |

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PreCadastro(model_desapropriacao_individual_precadastro objPrecadastro)
        {
            try
            {
                if (string.IsNullOrEmpty(objPrecadastro.dpc_empreendimento_auto_decreto) || objPrecadastro.dpc_empreendimento_lote <= 0)
                {
                    return new bootStrapDialog().Show("Atenção", "Todos os campos são obrigatórios!", bootStrapMessageType.TYPE_WARNING);
                }

                tab_desapropriacao_empreendimento dem = new tab_desapropriacao_empreendimento_bll().BUSCAR_EMPREENDIMENTO(
                    objPrecadastro.dpc_empreendimento_auto_decreto,
                    objPrecadastro.dpc_empreendimento_lote
                );

                var entityObj = new tab_desapropriacao_individual(dem);

                var din_obj = Mapper.Map<model_desapropriacao_individual>(entityObj);

                PreencherViewBag(din_obj);

                din_obj.din_geral_emissao_dt = null;

                ViewBag.ActionAtual = "Cadastrar";

                return View("Cadastrar", din_obj);
            }
            catch (Exception e)
            {
                throw new CustomException("Erro ao encontrar os dados. Erro: <br/>" + e, bootStrapMessageType.TYPE_DANGER);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Cadastrar(model_desapropriacao_individual obj)
        {
            return SALVAR(obj);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Editar(model_desapropriacao_individual obj, string args)
        {
            return ALTERAR(obj, args);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Consultar(tab_desapropriacao_individual_filtro obj)
        {
            return PESQUISAR(obj);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Status(int id, bool status)
        {
            return STATUS(id, status, nomeloginUsuario);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Deletar(int id)
        {
            try
            {
                return DELETAR(id, nomeloginUsuario);
            }
            catch (CustomException ex)
            {
                return new bootStrapDialog().Show(ex);
            }
            catch (Exception ex)
            {
                return new bootStrapDialog().Show("Erro", ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
        }

        #endregion

        #region | METODOS |

        private ActionResult SALVAR(model_desapropriacao_individual obj)
        {
            try
            {
                ValidaModel(obj);

                ActionResult retorno = null;

                var tabDesapropriacaoIndividual = Mapper.Map<tab_desapropriacao_individual>(obj);

                if (new tab_desapropriacao_individual_bll().INSERT(tabDesapropriacaoIndividual))
                {
                    retorno = new bootStrapDialog().Show("Sucesso", "Desapropriação individual salva com sucesso!", Url.Action("EdicaoCadastro", "DesapropriacaoIndividual", new { din_id = tabDesapropriacaoIndividual.din_id }), bootStrapMessageType.TYPE_SUCCESS, false);
                }

                return retorno;
            }
            catch (CustomException ex)
            {
                return new bootStrapDialog().Show(ex);
            }
        }

        private ActionResult ALTERAR(model_desapropriacao_individual obj, string args)
        {
            try
            {
                ValidaModel(obj);

                ActionResult retorno = null;

                var tab_din_atual = new tab_desapropriacao_individual_bll().FIND(obj.din_id);

                if (tab_din_atual == null)
                    throw new CustomException("Desapropriação Individual não encontrado!");

                // TODO: Atualizar os Dados
                #region Dados Atualizados

                tab_din_atual.din_geral_documento_numero = obj.din_geral_documento_numero;
                tab_din_atual.din_guid = obj.din_guid ?? Guid.NewGuid().ToString();
                tab_din_atual.din_geral_emissao_dt = obj.din_geral_emissao_dt ?? DateTime.Now;
                tab_din_atual.din_geral_revisao = obj.din_geral_revisao;
                tab_din_atual.din_geral_elaborador = obj.din_geral_elaborador;
                tab_din_atual.din_geral_responsavel_tecnico = obj.din_geral_responsavel_tecnico;
                tab_din_atual.din_geral_objeto = obj.din_geral_objeto;

                tab_din_atual.tab_desapropriacao_individual_revisao =
                    Mapper.Map<ICollection<tab_desapropriacao_individual_revisao>>(obj.list_model_desapropriacao_individual_revisao);

                if (obj.din_propriedade_validar)
                {
                    tab_din_atual.din_propriedade_cep = obj.din_propriedade_cep;
                    tab_din_atual.din_propriedade_logradouro = obj.din_propriedade_logradouro;
                    tab_din_atual.din_propriedade_logradouro_complemento = obj.din_propriedade_logradouro_complemento;
                    tab_din_atual.din_propriedade_mun_id = obj.din_propriedade_mun_id;
                    tab_din_atual.din_propriedade_bairro = obj.din_propriedade_bairro;
                    tab_din_atual.din_propriedade_contribuinte_tipo = obj.din_propriedade_contribuinte_tipo;
                    tab_din_atual.din_propriedade_contribuinte_numero = obj.din_propriedade_contribuinte_numero;
                    tab_din_atual.din_propriedade_cartorio = obj.din_propriedade_cartorio;
                    tab_din_atual.din_propriedade_matricula = obj.din_propriedade_matricula;
                    tab_din_atual.din_propriedade_desapropriacao = obj.din_propriedade_desapropriacao;
                    tab_din_atual.din_propriedade_comarca = obj.din_propriedade_comarca;
                    tab_din_atual.din_propriedade_estaca_inicial = obj.din_propriedade_estaca_inicial;
                    tab_din_atual.din_propriedade_estaca_final = obj.din_propriedade_estaca_final;
                    tab_din_atual.din_propriedade_km_inicial = obj.din_propriedade_km_inicial;
                    tab_din_atual.din_propriedade_km_final = obj.din_propriedade_km_final;
                    tab_din_atual.din_propriedade_latitude = obj.din_propriedade_latitude;
                    tab_din_atual.din_propriedade_longitude = obj.din_propriedade_longitude;
                    tab_din_atual.din_propriedade_dominio = obj.din_propriedade_dominio;
                    tab_din_atual.din_propriedade_prioridade = obj.din_propriedade_prioridade;
                    tab_din_atual.din_propriedade_autos_numero = obj.din_propriedade_autos_numero;
                    tab_din_atual.din_propriedade_autos_dt = obj.din_propriedade_autos_dt;
                }

                if (obj.din_conducao_validar)
                {
                    tab_din_atual.din_conducao_anuencia = obj.din_conducao_anuencia;
                    tab_din_atual.din_conducao_valor_total = obj.din_conducao_valor_total;
                    tab_din_atual.din_conducao_isi_id = obj.din_conducao_situacao_desapropriacao;
                    tab_din_atual.din_conducao_numero_processo_unificado = obj.din_conducao_numero_processo_unificado;
                    tab_din_atual.din_conducao_isi_obs = obj.din_conducao_situacao_desapropriacao_obs;
                    tab_din_atual.din_conducao_ist_id = obj.din_conducao_ist_id;
                    tab_din_atual.din_conducao_ipj_id = obj.din_conducao_ipj_id;
                    tab_din_atual.din_conducao_iat_id = obj.din_conducao_iat_id;

                    tab_din_atual.tab_desapropriacao_individual_status_evento =
                        Mapper.Map<ICollection<tab_desapropriacao_individual_status_evento>>(obj.list_model_desapropriacao_individual_status_evento);
                }

                if (obj.din_interessados_validar)
                {
                    tab_din_atual.tab_desapropriacao_individual_interessado =
                        Mapper.Map<ICollection<tab_desapropriacao_individual_interessado>>(obj.list_model_desapropriacao_individual_interessado);
                }

                if (obj.din_imovel_validar)
                {
                    tab_din_atual.din_imovel_ipe_id = obj.din_imovel_ipe_id;
                    tab_din_atual.din_imovel_zti_id = obj.din_imovel_zti_id;
                    tab_din_atual.din_imovel_infraestrutura = obj.din_imovel_infraestrutura;
                    tab_din_atual.din_imovel_equipamento = obj.din_imovel_equipamento;
                    tab_din_atual.din_imovel_servico = obj.din_imovel_servico;
                    tab_din_atual.din_imovel_area_terreno = obj.din_imovel_area_terreno;
                    tab_din_atual.din_imovel_area_necessaria = obj.din_imovel_area_necessaria;
                    tab_din_atual.din_imovel_area_porcentagem = obj.din_imovel_area_porcentagem;
                    tab_din_atual.din_imovel_perimetro = obj.din_imovel_perimetro;
                    tab_din_atual.din_imovel_top_id = obj.din_imovel_top_id;
                    tab_din_atual.din_imovel_sco_id = obj.din_imovel_sco_id;
                    tab_din_atual.din_imovel_restricao_uso = obj.din_imovel_restricao_uso;
                    tab_din_atual.din_imovel_curso_dagua = obj.din_imovel_curso_dagua;
                    tab_din_atual.din_imovel_recurso_minerais = obj.din_imovel_recurso_minerais;
                    tab_din_atual.din_imovel_terreno_ocupacoes = obj.din_imovel_terreno_ocupacoes;
                    tab_din_atual.din_imovel_sus = obj.din_imovel_sus;
                    tab_din_atual.din_imovel_delimitacao = obj.din_imovel_delimitacao;
                    tab_din_atual.din_imovel_atividade_economica = obj.din_imovel_atividade_economica;
                    tab_din_atual.din_imovel_posicionamento = obj.din_imovel_posicionamento;
                    tab_din_atual.din_imovel_frontal = obj.din_imovel_frontal;
                    tab_din_atual.din_imovel_fundo = obj.din_imovel_fundo;
                    tab_din_atual.din_imovel_lateral_esquerda = obj.din_imovel_lateral_esquerda;
                    tab_din_atual.din_imovel_lateral_direita = obj.din_imovel_lateral_direita;
                }

                if (obj.din_anexos_validar)
                {
                    tab_din_atual.tab_desapropriacao_individual_anexos =
                        Mapper.Map<ICollection<tab_desapropriacao_individual_anexos>>(obj.list_model_desapropriacao_individual_anexos);
                }

                if (obj.din_benfeitorias_validar)
                {

                    tab_din_atual.tab_desapropriacao_individual_divisa =
                        Mapper.Map<ICollection<tab_desapropriacao_individual_divisa>>(obj.list_model_desapropriacao_individual_divisa);

                    tab_din_atual.tab_desapropriacao_individual_entrada =
                        Mapper.Map<ICollection<tab_desapropriacao_individual_entrada>>(obj.list_model_desapropriacao_individual_entrada);

                    tab_din_atual.tab_desapropriacao_individual_instalacao =
                        Mapper.Map<ICollection<tab_desapropriacao_individual_instalacao>>(obj.list_model_desapropriacao_individual_instalacao);

                    tab_din_atual.tab_desapropriacao_individual_cultura =
                        Mapper.Map<ICollection<tab_desapropriacao_individual_cultura>>(obj.list_model_desapropriacao_individual_cultura);

                    tab_din_atual.tab_desapropriacao_individual_edificacao =
                        Mapper.Map<ICollection<tab_desapropriacao_individual_edificacao>>(obj.list_model_desapropriacao_individual_edificacao);

                    tab_din_atual.tab_desapropriacao_individual_estrada_acesso =
                        Mapper.Map<ICollection<tab_desapropriacao_individual_estrada_acesso>>(obj.list_model_desapropriacao_individual_estrada_acesso);
                }

                if (obj.din_memorial_descritivo_validar)
                {
                    tab_din_atual.din_memorial_descritivo = obj.din_memorial_descritivo;
                }

                if (obj.din_laudo_avaliacao_validar)
                {
                    tab_din_atual.din_laudo_avaliacao = obj.din_laudo_avaliacao;
                }

                if (obj.din_registro_fotograficos_validar)
                {
                    tab_din_atual.tab_desapropriacao_individual_registro_fotografico =
                        Mapper.Map<ICollection<tab_desapropriacao_individual_registro_fotografico>>(obj.list_model_desapropriacao_individual_registro_fotografico);
                }

                //if (obj.din_pendencias_validar)
                //{
                    tab_din_atual.tab_desapropriacao_individual_pendencia =
                        Mapper.Map<ICollection<tab_desapropriacao_individual_pendencia>>(obj.list_model_desapropriacao_individual_pendencia);
                //}

                if (obj.din_planta_propriedade_validar)
                {
                    tab_din_atual.tab_desapropriacao_individual_planta_propriedade =
                        Mapper.Map<ICollection<tab_desapropriacao_individual_planta_propriedade>>(obj.list_model_desapropriacao_individual_planta_propriedade);
                }

                #endregion

                if (new tab_desapropriacao_individual_bll().UPDATE(tab_din_atual))
                {
                    retorno = new bootStrapDialog().Show("Sucesso", "Desapropriação individual salva com sucesso!", Url.Action("EdicaoCadastro", "DesapropriacaoIndividual", new { din_id = obj.din_id }), bootStrapMessageType.TYPE_SUCCESS, false);
                }

                return retorno;
            }
            catch (CustomException ex)
            {
                return new bootStrapDialog().Show(ex);
            }
        }

        private ActionResult OBTER_DADOS(int din_id)
        {
            try
            {
                var objTab = new tab_desapropriacao_individual_bll().FIND(din_id);

                var din_obj = Mapper.Map<tab_desapropriacao_individual, model_desapropriacao_individual>(objTab);

                PreencherViewBag(din_obj);

                ViewBag.ActionAtual = "Editar";

                return View("Cadastrar", din_obj);
            }
            catch (Exception e)
            {
                throw new CustomException("Erro ao encontrar os dados. Erro: <br/>" + e, bootStrapMessageType.TYPE_DANGER);
            }
        }

        private void PreencherViewBag(model_desapropriacao_individual din_obj)
        {
            var emitentes_obj = new tab_desapropriacao_emitente_bll().FIND(din_obj.model_desapropriacao_empreendimento.des_id);
            var dem_list = Mapper.Map<ICollection<tab_desapropriacao_emitente_gdv>, ICollection<model_desapropriacao_emitente>>(emitentes_obj);

            ViewBag.CarregarPropriedadeContribuinteTipo = CarregarPropriedadeContribuinteTipo().ToSelectList("din_propriedade_contribuinte_tipo_nome", "din_propriedade_contribuinte_tipo");
            ViewBag.CarregarStatusIndividual = Mapper.Map<List<model_desapropriacao_individual_status>>(new tab_desapropriacao_individual_status_bll().LISTAR_ATIVOS());
            ViewBag.CarregarStatusEventoTipo = CarregarStatusEventoTipo().ToSelectList("set_nome", "set_id");
            ViewBag.CarregarSituacao = CarregarSituacao().ToSelectList("isi_nome", "isi_id");
            ViewBag.CarregarAssistenteTecnico = CarregarAssistenteTecnico().ToSelectList("iat_nome", "iat_id");
            ViewBag.CarregarPeritoJudicial = CarregarPeritoJudical().ToSelectList("ipj_nome", "ipj_id");
            ViewBag.CarregarPredominanteEntorno = CarregarPredominanteEntorno().ToSelectList("ipe_nome", "ipe_id");
            ViewBag.CarregarEmitentes = dem_list;
            ViewBag.CarregarMunicipio = CarregarMunicipio("25").ToSelectList("mun_nome", "mun_id");

            ViewBag.CarregarHelpers = CarregarHelpersGuia();

            ViewBag.ListModelRevisao = din_obj.list_model_desapropriacao_individual_revisao;
            ViewBag.ListModelDivisas = din_obj.list_model_desapropriacao_individual_divisa;
            ViewBag.ListModelEntradas = din_obj.list_model_desapropriacao_individual_entrada;
            ViewBag.ListModelInstalacoes = din_obj.list_model_desapropriacao_individual_instalacao;
            ViewBag.ListModelCulturas = din_obj.list_model_desapropriacao_individual_cultura;
            ViewBag.ListModelEstradasAcessos = din_obj.list_model_desapropriacao_individual_estrada_acesso;
            ViewBag.ListModelEdificacoes = din_obj.list_model_desapropriacao_individual_edificacao;
            ViewBag.ListModelInteressados = din_obj.list_model_desapropriacao_individual_interessado;
            ViewBag.ListModelStatusEvento = din_obj.list_model_desapropriacao_individual_status_evento;
            ViewBag.ListModelRegistroFotografico = din_obj.list_model_desapropriacao_individual_registro_fotografico;
            ViewBag.ListModelPendencias = din_obj.list_model_desapropriacao_individual_pendencia;
            ViewBag.ListModelAnexos = din_obj.list_model_desapropriacao_individual_anexos;
            ViewBag.ListModelPlantaPropriedade = din_obj.list_model_desapropriacao_individual_planta_propriedade;

            ViewBag.AttachmentsFolder = string.Format(@"\Upload\Anexo\DesapropriacaoProjeto\{0}\Individual\", din_obj.model_desapropriacao_empreendimento.des_id);
        }
        private ActionResult OBTER_DADOS(string requestQueryString)
        {
            try
            {
                var args = new EncryptQueryString(requestQueryString);

                if (!string.IsNullOrEmpty(args["din_id"]))
                {
                    return OBTER_DADOS(int.Parse(args["din_id"]));
                }

                return View("Cadastrar", RetornarPesquisa(requestQueryString));
            }
            catch (Exception e)
            {
                throw new CustomException("Erro ao encontrar os dados. Erro: <br/>" + e, bootStrapMessageType.TYPE_DANGER);
            }
        }

        private ActionResult STATUS(int id, bool status, string userLastUpdate)
        {
            try
            {
                ActionResult retorno = null;

                if (new tab_desapropriacao_departamento_bll().STATUS(id, status, userLastUpdate))
                {
                    retorno = new bootStrapDialog().Show("Sucesso", "Departamento responsável " + (status ? "ativado" : "desativado") + " com sucesso!", bootStrapMessageType.TYPE_SUCCESS);
                }

                return retorno;
            }
            catch (CustomException ex)
            {
                return new bootStrapDialog().Show(ex);
            }
        }

        private ActionResult DELETAR(int id, string userLastUpdate)
        {
            try
            {
                ActionResult retorno = null;

                if (new tab_desapropriacao_individual_bll().DELETAR(id, userLastUpdate))
                {
                    retorno = new bootStrapDialog().Show("Sucesso", "Desapropriação individual excluida com sucesso!", bootStrapMessageType.TYPE_SUCCESS);
                }

                return retorno;
            }
            catch (CustomException ex)
            {
                return new bootStrapDialog().Show(ex);
            }
        }

        public ActionResult PESQUISAR(tab_desapropriacao_individual_filtro obj)
        {
            try
            {
                var list = new tab_desapropriacao_individual_bll().PESQUISA_GDV(obj).Select(t => new
                {
                    t.din_id,
                    t.din_geral_documento_numero,
                    t.din_geral_objeto,
                    din_geral_emissao_dt = t.din_geral_emissao_dt == null ? string.Empty : t.din_geral_emissao_dt.Value.ToString("d"),
                    t.din_conducao_ist_id,
                    t.din_empreendimento_rodovia_sp,
                    t.din_empreendimento_rodovia_nome,
                    args = GerarQueryString(t.din_id, obj).ToString()
                });

                return Json(new { data = list, recordsFiltered = obj.pageCount });
            }
            catch (CustomException ex)
            {
                return new bootStrapDialog().Show(ex);
            }
        }

        private Dictionary<string, List<model_select_helper>> CarregarHelpersGuia()
        {
            var dictionary = new tab_select_helper_bll().FIND(t => t.seh_ativo)
                .GroupBy(t => t.seh_nome_campo)
                .ToDictionary(t => t.Key, t => t.OrderBy(d => d.seh_nome_exibicao).ToList());

            return Mapper.Map<Dictionary<string, List<model_select_helper>>>(dictionary);
        }

        private void ValidaModel(model_desapropriacao_individual obj)
        {
            Dictionary<string, List<string>> validateDictionary = new Dictionary<string, List<string>>();

            // TODO: esse dictionary pode ser uma tabela de controle no banco
            Dictionary<string, string> abasDictionary = new Dictionary<string, string>();

            abasDictionary.Add("dados_gerais", "Dados Gerais");
            abasDictionary.Add("empreendimento", "Empreendimento");
            abasDictionary.Add("propriedade", "Propriedade");
            abasDictionary.Add("conducao", "Status desapropriação");
            abasDictionary.Add("interessados", "Interessados");
            abasDictionary.Add("anexos", "Anexos");
            abasDictionary.Add("imovel", "Caracterização do Imóvel");
            abasDictionary.Add("benfeitoria", "Benfeitorias");
            abasDictionary.Add("memorial_descritivo", "Memorial descritivo");
            abasDictionary.Add("planta_propriedade", "Planta da propriedade");
            abasDictionary.Add("laudo_avaliacao", "Laudo de avaliação");
            abasDictionary.Add("registro_fotografico", "Registro fotográfico");
            abasDictionary.Add("pendencias", "Pendências");

            List<string> errorList = new List<string>();

            #region DADOS GERAIS

            // Validar Dados gerais
            List<string> dadosGeraisErrorList = new List<string>();

            if (string.IsNullOrEmpty(obj.din_geral_documento_numero))
                dadosGeraisErrorList.Add("Código do Documento");

            if (obj.din_geral_emissao_dt == null)
                dadosGeraisErrorList.Add("Data de emissão");

            if (obj.din_geral_emp_id == 0)
                dadosGeraisErrorList.Add("Emitente");

            if (string.IsNullOrEmpty(obj.din_geral_revisao))
                dadosGeraisErrorList.Add("Revisão");

            if (string.IsNullOrEmpty(obj.din_geral_elaborador))
                dadosGeraisErrorList.Add("Elaborador");

            if (string.IsNullOrEmpty(obj.din_geral_responsavel_tecnico))
                dadosGeraisErrorList.Add("Responsável técnico");

            if (string.IsNullOrEmpty(obj.din_geral_objeto))
                dadosGeraisErrorList.Add("Objeto do Cadastro de Desapropriação");

            if (dadosGeraisErrorList.Count > 0)
                validateDictionary.Add("dados_gerais", dadosGeraisErrorList);

            #endregion

            #region EMPREENDIMENTO
            // Empreendimento
            if (errorList.Count > 0)
            {
                validateDictionary.Add("empreendimento", errorList);
                errorList.Clear();
            }
            #endregion

            #region PROPRIEDADE
            if (obj.din_propriedade_validar)
            {
                List<string> propriedadeErrorList = new List<string>();

                if (!string.IsNullOrEmpty(obj.din_propriedade_cep))
                    obj.din_propriedade_cep = obj.din_propriedade_cep.Remove(5, 1);

                if (string.IsNullOrEmpty(obj.din_propriedade_km_inicial))
                    propriedadeErrorList.Add("Km Inicial");

                if (string.IsNullOrEmpty(obj.din_propriedade_km_final))
                    propriedadeErrorList.Add("Km Final");

                if (string.IsNullOrEmpty(obj.din_propriedade_estaca_inicial))
                    propriedadeErrorList.Add("Estaca Inicial");

                if (string.IsNullOrEmpty(obj.din_propriedade_estaca_final))
                    propriedadeErrorList.Add("Estaca Final");

                if (obj.din_propriedade_prioridade == null)
                    propriedadeErrorList.Add("Prioridade");

                if (propriedadeErrorList.Count > 0)
                    validateDictionary.Add("propriedade", propriedadeErrorList);
            }

            #endregion

            #region STATUS DESAPROPRIACAO
            if (obj.din_conducao_validar)
            {
                List<string> conducaoErrorList = new List<string>();

                if (obj.din_conducao_anuencia == null)
                    conducaoErrorList.Add("Anuência");

                if (obj.din_conducao_situacao_desapropriacao == null)
                    conducaoErrorList.Add("Situação");

                if (obj.din_conducao_ist_id == null)
                    conducaoErrorList.Add("Status desapropriação");

                if (obj.list_model_desapropriacao_individual_status_evento.Count > 0)
                {
                    foreach (var item in obj.list_model_desapropriacao_individual_status_evento)
                    {
                        if (string.IsNullOrEmpty(item.ise_data))
                            conducaoErrorList.Add("Datas Status\\Data");

                        if (item.set_id == 0)
                            conducaoErrorList.Add("Datas Status\\Tipo");
                    }
                }

                if (conducaoErrorList.Count > 0)
                    validateDictionary.Add("conducao", conducaoErrorList);
            }

            #endregion

            #region INTERESSADOS
            List<string> interessadoErrorList = new List<string>();
            if (obj.din_interessados_validar)
            {
                if (obj.list_model_desapropriacao_individual_interessado.Count > 0)
                {
                    foreach (var item in obj.list_model_desapropriacao_individual_interessado)
                    {
                        //if (item.iin_responsavel == null)
                        //    errorList.Add("- Datas Status\\Data");

                        if (!string.IsNullOrEmpty(item.iin_celular))
                            item.iin_celular = new string(item.iin_celular.Where(c => char.IsDigit(c)).ToArray());

                        if (!string.IsNullOrEmpty(item.iin_telefone))
                            item.iin_telefone = new string(item.iin_telefone.Where(c => char.IsDigit(c)).ToArray());

                        if (!string.IsNullOrEmpty(item.iin_cpf_cnpj))
                            item.iin_cpf_cnpj = new string(item.iin_cpf_cnpj.Where(c => char.IsDigit(c)).ToArray());

                        if (string.IsNullOrEmpty(item.iin_proprietario))
                            interessadoErrorList.Add("Interessados\\Proprietário");

                        if (string.IsNullOrEmpty(item.iin_logradouro))
                            interessadoErrorList.Add("Interessados\\Logradouro");
                    }

                    if (obj.list_model_desapropriacao_individual_interessado.Where(d => d.iin_responsavel && d.iin_acao != acao.Delete).ToList().Count > 1)
                        interessadoErrorList.Add("Datas Status\\Responsável (Possui mais de 1 responsável)");
                }
                else
                {
                    interessadoErrorList.Add("É necessário clicar no botão Adicionar para salvar um ou mais Interessados");
                }
            }

            if (interessadoErrorList.Count > 0)
                validateDictionary.Add("interessados", interessadoErrorList);

            #endregion

            #region IMOVEL
            if (obj.din_imovel_validar)
            {
                List<string> imovelErrorList = new List<string>();

                if (string.IsNullOrEmpty(obj.din_imovel_area_terreno))
                    imovelErrorList.Add("Área do terreno");

                if (string.IsNullOrEmpty(obj.din_imovel_area_necessaria))
                    imovelErrorList.Add("Área necessária");

                if (obj.din_imovel_top_id == null)
                    imovelErrorList.Add("Topografia");


                if (imovelErrorList.Count > 0)
                    validateDictionary.Add("imovel", imovelErrorList);
            }

            #endregion

            #region BENFEITORIAS
            if (errorList.Count > 0)
            {
                validateDictionary.Add("benfeitoria", errorList);
                errorList.Clear();
            }
            #endregion

            #region REGISTRO FOTOGRAFICO
            if (obj.din_registro_fotograficos_validar)
            {
                var registroErrorList = new List<string>();

                if (obj.list_model_desapropriacao_individual_registro_fotografico.Count > 0)
                {
                    foreach (var item in obj.list_model_desapropriacao_individual_registro_fotografico)
                    {
                        if (item.irf_data < new DateTime(1900, 1, 1))
                            registroErrorList.Add("Registro Fotográfico\\Data");

                        if (string.IsNullOrEmpty(item.irf_descricao))
                            registroErrorList.Add("Registro Fotográfico\\Descrição");
                    }

                    if (registroErrorList.Count > 0)
                        validateDictionary.Add("registro_fotografico", registroErrorList);
                }
                else
                {
                    registroErrorList.Add("É necessário clicar no botão Adicionar para salvar um ou mais Registros Fotográficos");
                }

                if (registroErrorList.Count > 0)
                    validateDictionary.Add("registro_fotografico", registroErrorList);
            }

            #endregion

            #region PLANTA PROPRIEDADE
            if (obj.din_planta_propriedade_validar)
            {
                var plantaPropriedadeErrorList = new List<string>();

                if (obj.list_model_desapropriacao_individual_planta_propriedade.Count > 0)
                {
                    foreach (var item in obj.list_model_desapropriacao_individual_planta_propriedade)
                    {
                        if (item.ipp_anexo_dt < new DateTime(1900, 1, 1) || item.ipp_anexo_dt == null)
                            item.ipp_anexo_dt = DateTime.Now;
                    }
                }
                else
                {
                    plantaPropriedadeErrorList.Add("É necessário clicar no botão Adicionar para salvar uma ou mais Plantas da Propriedade");
                }

                if (plantaPropriedadeErrorList.Count > 0)
                    validateDictionary.Add("planta_propriedade", plantaPropriedadeErrorList);
            }

            #endregion

            #region ANEXOS
            if (errorList.Count > 0)
            {
                validateDictionary.Add("anexos", errorList);
                errorList.Clear();
            }

            #endregion

            if (validateDictionary.Count > 0)
            {
                var retorno = new StringBuilder();
                retorno.Append("<ol>");

                foreach (var aba in validateDictionary)
                {
                    retorno.Append("<li>");

                    retorno.Append(abasDictionary[aba.Key]);
                    retorno.Append("<ul>");

                    retorno.Append(string.Join(" ", aba.Value.Select(d => "<li>" + d + "</li>").ToArray()));

                    retorno.Append("</ul>");
                    retorno.Append("</li>");
                }

                retorno.Append("</ol>");

                throw new CustomException("Preencha os campos obrigatorios! <br/>" + retorno, bootStrapMessageType.TYPE_WARNING);
            }

        }

        private model_desapropriacao_individual_filtro RetornarPesquisa(string requestQueryString)
        {
            var args = new EncryptQueryString(requestQueryString);

            return new model_desapropriacao_individual_filtro
            {
                pageIndex = int.Parse(args["pageIndex"]),
                pageSize = int.Parse(args["pageSize"]),
            };
        }

        private EncryptQueryString GerarQueryString(int din_id, tab_desapropriacao_individual_filtro obj)
        {
            return new EncryptQueryString
            {
                { "din_id",   din_id.ToString()  }, 
                { "pageIndex", obj.pageIndex.ToString()},
                { "pageSize",   obj.pageSize.ToString()}
            };
        }
        #endregion

        #region | JSON |

        [HandleErrorJson]
        public JsonResult CarregarPropriedadeEstado()
        {
            var list = new tab_estado_bll().FIND(new tab_estado_filtro()).Select(x => new { x.est_id, x.est_nome, x.est_uf });

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [HandleErrorJson]
        public JsonResult CarregarPropriedadeContribuinteTipo()
        {
            var list = Enum.GetNames(typeof(tab_desapropriacao_individual_contribuinte_tipo)).Select(name =>
                new
                {
                    din_propriedade_contribuinte_tipo = (int)Enum.Parse(typeof(tab_desapropriacao_individual_contribuinte_tipo), name),
                    din_propriedade_contribuinte_tipo_nome = name
                }).ToList();

            var retorno = Json(list, JsonRequestBehavior.AllowGet);

            return retorno;
        }

        [HandleErrorJson]
        public JsonResult CarregarStatusEventoTipo()
        {
            var list = new tab_desapropriacao_individual_status_evento_tipo_bll().FIND(x => x.set_ativo).Select(x => new { x.set_id, x.set_nome });

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CarregarEmpreendimento()
        {
            var list = new tab_desapropriacao_empreendimento_bll().LISTAR_EMPREENDIMENTOS();

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CarregarSituacao()
        {
            var list = new tab_desapropriacao_individual_situacao_bll().FIND(x => x.isi_ativo).Select(x => new { x.isi_nome, x.isi_id });

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CarregarPredominanteEntorno()
        {
            var list = new tab_desapropriacao_individual_predominante_entorno_bll().FIND(x => x.ipe_ativo).Select(x => new { x.ipe_nome, x.ipe_id });

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CarregarAssistenteTecnico()
        {
            var list = new tab_desapropriacao_individual_assistente_tecnico_bll().FIND(x => x.iat_ativo).Select(x => new { x.iat_nome, x.iat_id });

            return Json(list, JsonRequestBehavior.AllowGet);
        }


        public JsonResult CarregarPeritoJudical()
        {
            var list = new tab_desapropriacao_individual_perito_judicial_bll().FIND(x => x.ipj_ativo).Select(x => new { x.ipj_nome, x.ipj_id });

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [HandleErrorJson]
        public JsonResult CarregarAgenteFinanciador(bool afi_ativo)
        {
            var list = new tab_agente_financiador_bll().FIND(new tab_agente_financiador_filtro { afi_ativo = afi_ativo }).Select(x => new { x.afi_id, x.afi_nome });

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [HandleErrorJson]
        public JsonResult CarregarStatus(bool? dst_ativo)
        {
            var list = new tab_desapropriacao_status_bll().FIND(x => x.dst_ativo == dst_ativo).Select(x => new { x.dst_id, x.dst_nome });

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [HandleErrorJson]
        public JsonResult CarregarRegional(bool? reg_ativo)
        {
            var list = new tab_regional_bll().FIND(new tab_regional_filtro { reg_ativo = reg_ativo }).Select(x => new { x.reg_id, x.reg_nome });

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [HandleErrorJson]
        public JsonResult CarregarRodoviaSp()
        {
            var retorno = new tab_rodovia_bll().PESQUISA_RODOVIA_SP(string.Empty).OrderByDescending(s => s.ToString()).Select(x => new { rod_sp = x.ToString() });

            return Json(retorno, JsonRequestBehavior.AllowGet);
        }

        [HandleErrorJson]
        public JsonResult CarregarMunicipio(string est_id)
        {
            var list = new tab_municipio_bll().FIND(new tab_municipio_filtro { est_id = int.Parse(est_id) }).Select(x => new { x.mun_id, x.mun_nome, x.mun_uf });

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}
