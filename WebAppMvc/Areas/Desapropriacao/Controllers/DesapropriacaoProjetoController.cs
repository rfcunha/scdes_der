﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Business.DER;
using Business.GERENCIADORPORTAL;
using Business.SCDES;
using Entity.DER.Utils;
using Entity.DER.Utils.Filtro;
using Entity.SCDES.Models;
using Entity.SCDES.Utils.Filtro;
using Entity.SCDES.Utils.Gdv;
using Suporte.Criptografia;
using Suporte.Exceptions;
using Suporte.MsgBox;
using Suporte.MsgBox.Mvc;
using WebAppMvc.Areas.Desapropriacao.Models;
using WebAppMvc.Areas.Desapropriacao.Models.Filtro;
using WebAppMvc.Extensions;
using WebAppMvc.Helpers;
using WebAppMvc.Models.Filter;

namespace WebAppMvc.Areas.Desapropriacao.Controllers
{
    public class DesapropriacaoProjetoController : BaseController
    {
        #region | GET |

        [HttpGet]
        [HandleErrorAction]
        //[RolesActionFilter(isModal = false)]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [HandleErrorAction]
        //[RolesActionFilter(isModal = false)]
        public ActionResult Cadastrar()
        {
            ViewBag.CarregarDepartamentoResponsavel = CarregarDepartamentoResponsavel(true).ToSelectList("dde_nome", "dde_id");
            ViewBag.CarregarRegional                = CarregarRegional(true).ToSelectList("reg_nome", "reg_id");
            ViewBag.CarregarAgenteFinanciador       = CarregarAgenteFinanciador(true).ToSelectList("afi_nome", "afi_id");
            ViewBag.CarregarEmitentes               = CarregarEmitentes(true).ToSelectList("emi_nome", "emi_id");
            ViewBag.CarregarStatus                  = CarregarStatus(true).ToSelectList("dst_nome", "dst_id");
            ViewBag.CarregarEstado                  = CarregarEstado().ToSelectList("est_nome", "est_id");

            return View();
        }

        [HttpGet]
        [NoCache]
        [HandleErrorAction]
        public ActionResult Editar(string args)
        {
            return OBTER_DADOS(args);
        }

        [HttpGet]
        [NoCache]
        [HandleErrorAction]
        //[RolesActionFilter(isModal = false)]
        public ActionResult Consultar(string args)
        {
            ViewBag.CarregarDepartamentoResponsavel = CarregarDepartamentoResponsavel(true).ToSelectList("dde_nome", "dde_id");
            ViewBag.CarregarRegional = CarregarRegional(true).ToSelectList("reg_nome", "reg_id");
            ViewBag.CarregarStatus = CarregarStatus(true).ToSelectList("dst_nome", "dst_id");
            ViewBag.CarregarAgenteFinanciador = CarregarAgenteFinanciador(true).ToSelectList("afi_nome", "afi_id");

            return string.IsNullOrEmpty(args) ? View(new model_desapropriacao_filtro()) : View(RetornarPesquisa(args));
        }

        [HttpGet]
        [HandleErrorAction]
        public ActionResult VisualizarDetalhes(string id)
        {
            try
            {
                var obj = new tab_desapropriacao_bll().FIND(int.Parse(id));

                var model = Mapper.Map<tab_desapropriacao, model_desapropriacao>(obj);

                model.reg_nome = new tab_regional_bll().FIND(model.reg_id).reg_nome;
                model.afi_nome = new tab_agente_financiador_bll().FIND(model.afi_id).afi_nome;
                   
                foreach (var item in model.list_model_desapropriacao_localidade)
                {
                    var mun = new tab_municipio_bll().FIND(item.mun_id);
                    var est = new tab_estado_bll().FIND(mun.est_id);

                    item.mun_nome = mun.mun_nome;
                    item.est_id = est.est_id;
                    item.est_nome = est.est_nome;
                }

                return PartialView("Partial/_Visualizar", model);
            }
            catch (CustomException ex)
            {
                return new bootStrapDialog().Show(ex);
            }
        }

        #endregion

        #region | POST |

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Cadastrar(model_desapropriacao obj)
        {
            return SALVAR(obj);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Editar(model_desapropriacao obj, string args)
        {
            return ALTERAR(obj, args);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Consultar(tab_desapropriacao_filtro obj)
        {
            return PESQUISAR(obj);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Status(int id, bool status)
        {
            return STATUS(id, status, nomeloginUsuario);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Deletar(int id)
        {
            return DELETAR(id, nomeloginUsuario);
        }

        #endregion

        #region METODOS

        private ActionResult SALVAR(model_desapropriacao obj)
        {
            try
            {
                ValidaModel();

                ActionResult retorno = null;

                if (new tab_desapropriacao_bll().INSERT(convert_model_to_table_insert(obj)) > 0)
                {
                    retorno = new bootStrapDialog().Show("Sucesso", "Cadastro realizado com sucesso!", Url.Action("Index", "DesapropriacaoProjeto", new { area = "Desapropriacao" }), bootStrapMessageType.TYPE_SUCCESS, true);
                }

                return retorno;
            }
            catch (CustomException ex)
            {
                return new bootStrapDialog().Show(ex);
            }
        }

        private ActionResult ALTERAR(model_desapropriacao obj, string args)
        {
            try
            {
                ValidaModel();

                ActionResult retorno = null;

                if (new tab_desapropriacao_bll().UPDATE(convert_model_to_table_update(obj)))
                {
                    retorno = new bootStrapDialog().Show("Sucesso", "Alteração realizada com sucesso!", Url.Action("Consultar", "DesapropriacaoProjeto", new { area = "Desapropriacao", args = args }), bootStrapMessageType.TYPE_SUCCESS, false);
                }

                return retorno;
            }
            catch (CustomException ex)
            {
                return new bootStrapDialog().Show(ex);
            }
        }
        
        
        private ActionResult OBTER_DADOS(string requestQueryString)
        {
            var args = new EncryptQueryString(requestQueryString);

            if (!string.IsNullOrEmpty(args["des_id"]))
            {
                var obj = new tab_desapropriacao_bll().FIND(int.Parse(args["des_id"]));

                var emitentesObj = new tab_desapropriacao_emitente_bll().FIND(obj.des_id);


                var model = Mapper.Map<tab_desapropriacao, model_desapropriacao>(obj);
                model.list_model_desapropriacao_emitente = Mapper.Map<ICollection<tab_desapropriacao_emitente_gdv>, ICollection<model_desapropriacao_emitente>>(emitentesObj);

                foreach (var item in model.list_model_desapropriacao_localidade)
                {
                    var mun = new tab_municipio_bll().FIND(item.mun_id);
                    var est = new tab_estado_bll().FIND(mun.est_id);

                    item.mun_nome = mun.mun_nome;
                    item.est_id = est.est_id;
                    item.est_nome = est.est_nome;
                }

                ViewBag.CarregarDepartamentoResponsavel = CarregarDepartamentoResponsavel(true).ToSelectList("dde_nome", "dde_id");
                ViewBag.CarregarAgenteFinanciador       = CarregarAgenteFinanciador(true).ToSelectList("afi_nome", "afi_id");
                ViewBag.CarregarRegional                = CarregarRegional(true).ToSelectList("reg_nome", "reg_id");
                ViewBag.CarregarEmitentes               = CarregarEmitentes(true).ToSelectList("emi_nome", "emi_id");
                ViewBag.CarregarStatus                  = CarregarStatus(true).ToSelectList("dst_nome", "dst_id");
                ViewBag.CarregarEstado                  = CarregarEstado().ToSelectList("est_nome", "est_id");

                ViewBag.ListModelEmitente = model.list_model_desapropriacao_emitente;
                ViewBag.ListModelPendencia = model.list_model_desapropriacao_observacao;
                ViewBag.ListModelLocalidade = model.list_model_desapropriacao_localidade;
                ViewBag.ListModelEmpreendimento = model.list_model_desapropriacao_empreendimento;
                ViewBag.ListModelRecursoFonte = model.list_model_desapropriacao_recurso_fonte;

                return View(model);
            }
            else
            {
                return View("Consultar", RetornarPesquisa(requestQueryString));
            }
        }

        private ActionResult STATUS(int id, bool status, string userLastUpdate)
        {
            try
            {
                ActionResult retorno = null;

                if (new tab_desapropriacao_departamento_bll().STATUS(id, status, userLastUpdate))
                {
                    retorno = new bootStrapDialog().Show("Sucesso", "Departamento responsável " + (status ? "ativado" : "desativado") + " com sucesso!", bootStrapMessageType.TYPE_SUCCESS);
                }
                    
                return retorno;
            }
            catch (CustomException ex)
            {
                return new bootStrapDialog().Show(ex);
            }
        }

        private ActionResult DELETAR(int id, string userLastUpdate)
        {
            try
            {
                ActionResult retorno = null;

                if (new tab_desapropriacao_bll().DELETAR(id, userLastUpdate))
                {
                    retorno = new bootStrapDialog().Show("Sucesso", "Desapropriação (projeto) excluida com sucesso!", bootStrapMessageType.TYPE_SUCCESS);
                }
                else
                {
                    throw new CustomException("Não foi possível excluír. Acione a");
                }

                return retorno;
            }
            catch (CustomException ex)
            {
                return new bootStrapDialog().Show(ex);
            }
        }

        public ActionResult PESQUISAR(tab_desapropriacao_filtro obj)
        {
            try
            {
                var list = new tab_desapropriacao_bll().PESQUISA_GDV(obj).Select(t => new
                {
                    t.des_id,
                    t.des_projeto_codigo,
                    t.des_nome,
                    t.dge_nome,
                    t.dde_nome,
                    des_valor_estimado = t.des_valor_estimado == null ? string.Empty : t.des_valor_estimado.Value.ToString("n"),
                    t.dst_nome,
                    des_cadastro_dt = t.des_cadastro_dt.ToString("d"),
                    //t.des_inicio,
                    //t.des_fim,
                    t.reg_nome,
                    args = GerarQueryString(t.des_id, obj).ToString()
                });

                return Json(new { data = list, recordsFiltered = obj.pageCount });
            }
            catch (CustomException ex)
            {
                return new bootStrapDialog().Show(ex);
            }
        }

        public tab_desapropriacao convert_model_to_table_insert(model_desapropriacao model)
        {
            try
            {
                var obj = Mapper.Map<model_desapropriacao, tab_desapropriacao>(model);

                obj.tab_desapropriacao_observacao = obj.tab_desapropriacao_observacao.Where(pendencia => pendencia.dob_acao == Entity.SCDES.Models.dob_acao.Insert).ToList();
                obj.tab_desapropriacao_localidade = obj.tab_desapropriacao_localidade.Where(pendencia => pendencia.dlo_acao == Entity.SCDES.Models.dlo_acao.Insert).ToList();
                obj.tab_desapropriacao_recurso_fonte = obj.tab_desapropriacao_recurso_fonte.Where(pendencia => pendencia.drf_acao == Entity.SCDES.Models.drf_acao.Insert).ToList();
                obj.tab_desapropriacao_empreendimento = obj.tab_desapropriacao_empreendimento.Where(pendencia => pendencia.dem_acao == Entity.SCDES.Models.dem_acao.Insert).ToList();

                return obj;
            }
            catch (CustomException)
            {
                throw new CustomException("Ocorreu um erro ao converter os dados da desapropriação",bootStrapMessageType.TYPE_DANGER);
            }
        }

        public tab_desapropriacao convert_model_to_table_update(model_desapropriacao model)
        {
            try
            {
                var obj = Mapper.Map<model_desapropriacao, tab_desapropriacao>(model);

                obj.tab_desapropriacao_observacao = obj.tab_desapropriacao_observacao.OrderBy(x => x.dob_acao).ToList();
                obj.tab_desapropriacao_localidade = obj.tab_desapropriacao_localidade.OrderBy(x => x.dlo_acao).ToList();
                obj.tab_desapropriacao_recurso_fonte = obj.tab_desapropriacao_recurso_fonte.OrderBy(x => x.drf_acao).ToList();
                obj.tab_desapropriacao_empreendimento = obj.tab_desapropriacao_empreendimento.OrderBy(x => x.dem_acao).ToList();
                obj.tab_desapropriacao_emitente = obj.tab_desapropriacao_emitente.OrderBy(x => x.dee_acao).ToList();
                
                return obj;
            }
            catch (CustomException)
            {
                throw new CustomException("Ocorreu um erro ao converter os dados da desapropriação", bootStrapMessageType.TYPE_DANGER);
            }
        }

        private void ValidaModel()
        {
            if (!ModelState.IsValid)
            {
                var retorno = string.Join("<br/>", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage));

                throw new CustomException(string.Format("Preencha os campos obrigatorios!\n\n{0}", retorno), bootStrapMessageType.TYPE_WARNING);
            }
        }

        private model_desapropriacao_filtro RetornarPesquisa(string args)
        {
            var retorno = new EncryptQueryString(args);

            return new model_desapropriacao_filtro
            {
                des_nome = !string.IsNullOrEmpty(retorno["des_nome"]) ? retorno["des_nome"] : string.Empty, 
                des_projeto_codigo = !string.IsNullOrEmpty(retorno["des_projeto_codigo"]) ? retorno["des_projeto_codigo"] : string.Empty, 
                dst_id = !string.IsNullOrEmpty(retorno["dst_id"]) ? int.Parse(retorno["dst_id"]) : (int?)null, 
                reg_id = !string.IsNullOrEmpty(retorno["reg_id"]) ? int.Parse(retorno["reg_id"]) : (int?)null, 
                afi_id =  !string.IsNullOrEmpty(retorno["afi_id"]) ?  int.Parse(retorno["afi_id"]) : (int?)null, 
                dem_dup_dt = !string.IsNullOrEmpty(retorno["dem_dup_dt"]) ?  DateTime.Parse(retorno["dem_dup_dt"]) : (DateTime?)null, 
                dem_dup_codigo = !string.IsNullOrEmpty(retorno["dem_dup_codigo"]) ? retorno["dem_dup_codigo"] : string.Empty, 
                dem_nota_reserva = !string.IsNullOrEmpty(retorno["dem_nota_reserva"]) ? retorno["dem_nota_reserva"] : string.Empty,
                pageIndex = int.Parse(retorno["pageIndex"]),
                pageSize = int.Parse(retorno["pageSize"]),
            };
        }

        private EncryptQueryString GerarQueryString(int id, tab_desapropriacao_filtro obj)
        {
            var qString= new EncryptQueryString
            {
                { "des_id",   id.ToString()  },
                { "des_nome", string.IsNullOrEmpty(obj.des_nome) ? "" : obj.des_nome  }, 
                { "des_projeto_codigo", string.IsNullOrEmpty(obj.des_projeto_codigo) ? "" : obj.des_projeto_codigo  }, 
                { "dst_id", obj.dst_id != null ?  obj.dst_id.ToString() : string.Empty  }, 
                { "reg_id", obj.reg_id != null ?  obj.reg_id.ToString() : string.Empty  }, 
                { "afi_id", obj.afi_id != null ?  obj.afi_id.ToString() : string.Empty  }, 
                { "dem_dup_dt", obj.dem_dup_dt != null ?  obj.dem_dup_dt.ToString() : string.Empty  }, 
                { "dem_dup_codigo", !string.IsNullOrEmpty(obj.dem_dup_codigo) ? obj.dem_dup_codigo : string.Empty  }, 
                { "dem_nota_reserva", !string.IsNullOrEmpty(obj.dem_nota_reserva) ? obj.dem_nota_reserva : string.Empty  }, 
                { "pageIndex", obj.pageIndex.ToString()},
                { "pageSize",   obj.pageSize.ToString()}
            };

            return qString;
        }

        #endregion


        #region | METODOS |


        #region | JSON |

        [HandleErrorJson]
        public JsonResult CarregarEstado()
        {
            var list = new tab_estado_bll().FIND(new tab_estado_filtro()).Select(x => new { x.est_id, x.est_nome,x.est_uf});

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [HandleErrorJson]
        public JsonResult CarregarMunicipio(string est_id)
        {
            var list = new tab_municipio_bll().FIND(new tab_municipio_filtro{est_id = int.Parse(est_id)}).Select(x => new { x.mun_id, x.mun_nome, x.mun_uf });

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [HandleErrorJson]
        public JsonResult CarregarAgenteFinanciador(bool afi_ativo)
        {
            var list = new tab_agente_financiador_bll().FIND(new tab_agente_financiador_filtro { afi_ativo = afi_ativo }).Select(x => new { x.afi_id, x.afi_nome});

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [HandleErrorJson]
        public JsonResult CarregarEmitentes(bool? emi_ativo)
        {
            var list = new tab_emitente_bll().FIND(x => x.emi_ativo == emi_ativo).Select(x => new { x.emi_id, x.emi_nome });

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [HandleErrorJson]
        public JsonResult CarregarStatus(bool? dst_ativo)
        {
            var list = new tab_desapropriacao_status_bll().FIND(x => x.dst_ativo == dst_ativo).Select(x => new { x.dst_id, x.dst_nome });

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [HandleErrorJson]
        public JsonResult CarregarRegional(bool? reg_ativo)
        {
            var list = new tab_regional_bll().FIND(new tab_regional_filtro { reg_ativo = reg_ativo }).Select(x => new { x.reg_id, x.reg_nome });

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [HandleErrorJson]
        public JsonResult CarregarDepartamentoResponsavel(bool? dde_ativo)
        {
            var retorno = new tab_desapropriacao_departamento_bll().FIND(x => x.dde_ativo == dde_ativo).Select(x => new { x.dde_id, x.dde_nome });

            return Json(retorno, JsonRequestBehavior.AllowGet);
        }

        [HandleErrorJson]
        public JsonResult CarregarRodoviaSp()
        {
            var retorno = new tab_rodovia_bll().PESQUISA_RODOVIA_SP(string.Empty).OrderByDescending(s => s.ToString()).Select(x => new { rod_sp = x.ToString() });

            return Json(retorno, JsonRequestBehavior.AllowGet);
        }

        [HandleErrorJson]
        public JsonResult ObterRodovia(string rod_sp, string rod_km_inicial, string rod_km_final)
        {
            var retorno = new tab_rodovia_bll().GET_BY_RODOVIA(rod_sp, decimal.Parse(rod_km_inicial), decimal.Parse(rod_km_final));

            return Json(retorno, JsonRequestBehavior.AllowGet);
        }

        [HandleErrorJson]
        public JsonResult CarregarContrato(string numContr)
        {
            var retorno = new tab_contratos_bll().GET_BY(numContr);

            string DataInicio = retorno.DataInicio == null ? null : retorno.DataInicio.Value.ToShortDateString();
            string ValContrOrigPI = retorno.ValContrOrigPI == null ? null : retorno.ValContrOrigPI.Value.ToString("n2");

            return Json(
                new { retorno.NumContr, ValContrOrigPI, retorno.PorcFin, DataInicio }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #endregion
    }
}
