﻿using System.Web.Mvc;

namespace WebAppMvc.Areas.Desapropriacao
{
    public class DesapropriacaoAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Desapropriacao";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Desapropriacao_default",
                "Desapropriacao/{controller}/{action}/{id}",
                new { id = UrlParameter.Optional }
            );
        }
    }
}
