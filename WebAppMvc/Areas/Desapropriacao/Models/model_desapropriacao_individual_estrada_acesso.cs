using System;
using System.ComponentModel.DataAnnotations;
using Entity.SCDES.Enum;
using Entity.SCDES.Models;

namespace WebAppMvc.Areas.Desapropriacao.Models
{
    public class model_desapropriacao_individual_estrada_acesso
    {
        public model_desapropriacao_individual_estrada_acesso()
        {
            model_anexo_identificador = new model_anexo_identificador()
            {
                aid_tipo = "INDIVIDUAL",
                aid_guia = "BENFEITORIAS",
                aid_form = "SERVID�ES/ESTRADAS E ACESSOS"
            };
        }

        [Key]
        public int iea_id { get; set; }
        [Display(Name = "Tipo")]
        public string iea_tipo { get; set; }
        [Display(Name = "Conserva��o")]
        public string iea_conservacao_estado { get; set; }
        [Display(Name = "Estaca Inicial")]
        public string iea_estaca_inicial { get; set; }
        [Display(Name = "Dimens�o")]
        public string iea_dimensao { get; set; }
        [Display(Name = "Pavimento")]
        public string iea_pavimento { get; set; }
        [Display(Name = "Observa��o")]
        public string iea_observacao { get; set; }
        [Display(Name = "Valor Total")]
        public decimal iea_valor_total { get; set; }
        public DateTime iea_cadastro_dt { get; set; }
        public int din_id { get; set; }

        public int? iea_anexo_identificador_id { get; set; }
        public model_anexo_identificador model_anexo_identificador { get; set; }

        public string userCreate { get; set; }
        public Nullable<System.DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<System.DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<System.DateTime> dtDelete { get; set; }

        public acao iea_acao { get; set; }
    }

}
