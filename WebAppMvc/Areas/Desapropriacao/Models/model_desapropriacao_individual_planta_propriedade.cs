﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web;
using Entity.SCDES.Enum;

namespace WebAppMvc.Areas.Desapropriacao.Models
{
    public class model_desapropriacao_individual_planta_propriedade
    {
        public model_desapropriacao_individual_planta_propriedade()
        {
            model_anexo_identificador = new model_anexo_identificador()
            {
                aid_tipo = "INDIVIDUAL",
                aid_guia = "PLANTA_PROPRIEDADE"
            };
        }

        public int ipp_id { get; set; }

        [Display(Name = "Descrição")]
        public string ipp_descricao { get; set; }

        [Display(Name = "Data")]
        public DateTime? ipp_anexo_dt { get; set; }

        public int? ipp_anexo_identificador_id { get; set; }

        public virtual model_anexo_identificador model_anexo_identificador { get; set; }

        public int din_id { get; set; }
        public model_desapropriacao_individual tab_desapropriacao_individual { get; set; }

        [Display(Name = "Upload")]
        public HttpPostedFileBase ipp_file_upload { get; set; }

        public string ipp_anexo { get; set; }

        public string ipp_anexo_nome { get; set; }

        public string ipp_anexo_pasta { get; set; }

        public acao ipp_acao { get; set; }

        public string userCreate { get; set; }
        public Nullable<System.DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<System.DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<System.DateTime> dtDelete { get; set; }
    }
}