﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAppMvc.Areas.Desapropriacao.Models
{
    public class model_select_helper
    {
        public int seh_id { get; set; }

        public string seh_value { get; set; }

        public string seh_nome_exibicao { get; set; }

        public string seh_nome_campo { get; set; }

        public bool seh_ativo { get; set; }

        public model_select_helper_guia tab_select_helper_guia { get; set; }
    }
}