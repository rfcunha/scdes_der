using System;
using System.ComponentModel.DataAnnotations;
using Entity.SCDES.Enum;
using Entity.SCDES.Models;

namespace WebAppMvc.Areas.Desapropriacao.Models
{
    public class model_desapropriacao_individual_cultura
    {
        public model_desapropriacao_individual_cultura()
        {
            model_anexo_identificador = new model_anexo_identificador()
            {
                aid_tipo = "INDIVIDUAL",
                aid_guia = "BENFEITORIAS",
                aid_form = "CULTURAS"
            };
        }

        [Key]
        public int icu_id { get; set; }
        [Display(Name = "Tipo")]
        public string icu_tipo { get; set; }
        [Display(Name = "Est�gio de Explora��o")]
        public string icu_exploracao_estagio { get; set; }
        [Display(Name = "Cultivo")]
        public string icu_cultivo { get; set; }
        [Display(Name = "�rea")]
        public string icu_area { get; set; }
        [Display(Name = "Valor Total")]
        public decimal icu_valor_total { get; set; }
        [Display(Name = "Observa��o")]
        public string icu_observacao { get; set; }
        public System.DateTime icu_cadastro_dt { get; set; }
        public Nullable<int> din_id { get; set; }

        public int? icu_anexo_identificador_id { get; set; }
        public model_anexo_identificador model_anexo_identificador { get; set; }

        public string userCreate { get; set; }
        public Nullable<System.DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<System.DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<System.DateTime> dtDelete { get; set; }

        public acao icu_acao { get; set; }
    }
}
