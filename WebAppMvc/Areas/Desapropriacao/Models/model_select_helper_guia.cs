﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAppMvc.Areas.Desapropriacao.Models
{
    public class model_select_helper_guia
    {
        public int shg_id { get; set; }

        public string shg_nome { get; set; }

        public bool shg_ativo { get; set; }

        public string shg_nome_exibicao { get; set; }
    }
}