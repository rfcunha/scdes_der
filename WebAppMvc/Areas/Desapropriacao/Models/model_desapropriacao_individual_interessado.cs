using System;
using System.ComponentModel.DataAnnotations;
using Entity.SCDES.Enum;

namespace WebAppMvc.Areas.Desapropriacao.Models
{
    public class model_desapropriacao_individual_interessado
    {
        [Key]
        public int iin_id { get; set; }
        
        [Display(Name = "Respons�vel")]
        public bool iin_responsavel { get; set; }

        [Display(Name = "Pessoa")]
        //[Required(ErrorMessage = "- selecione a pessoa.")]
        public string iin_pessoa { get; set; }

        [Display(Name = "Propriet�rio")]
        [StringLength(150, ErrorMessage = "O tamanho m�ximo s�o 150 caracteres.")]
        //[Required(ErrorMessage = "- digite o propriet�rio.")]
        public string iin_proprietario { get; set; }

        [Display(Name = "Logradouro")]
        [StringLength(200, ErrorMessage = "O tamanho m�ximo s�o 200 caracteres.")]
        //[Required(ErrorMessage = "- digite o logradouro.")]
        public string iin_logradouro { get; set; }

        [Display(Name = "Complemento")]
        [StringLength(200, ErrorMessage = "O tamanho m�ximo s�o 200 caracteres.")]
        //[Required(ErrorMessage = "- digite o complemento.")]
        public string iin_logradouro_complemento { get; set; }

        [Display(Name = "Rg/Rne")]
        [StringLength(20, ErrorMessage = "O tamanho m�ximo s�o 20 caracteres.")]
        //[Required(ErrorMessage = "- digite o rg/rne.")]
        public string iin_rg_rne { get; set; }

        [Display(Name = "Cpf/Cnpj")]
        [StringLength(25, ErrorMessage = "O tamanho m�ximo s�o 25 caracteres.")]
        //[Required(ErrorMessage = "- digite o cpf/cnpj.")]
        public string iin_cpf_cnpj { get; set; }

        [Display(Name = "Email")]
        [StringLength(200, ErrorMessage = "O tamanho m�ximo s�o 200 caracteres.")]
        //[Required(ErrorMessage = "- digite o email.")]
        public string iin_email { get; set; }

        [Display(Name = "Telefone")]
        [StringLength(14, ErrorMessage = "O tamanho m�ximo s�o 14 caracteres.")]
        //[Required(ErrorMessage = "- digite o telefone.")]
        public string iin_telefone { get; set; }

        [Display(Name = "Celular")]
        [StringLength(14, ErrorMessage = "O tamanho m�ximo s�o 14 caracteres.")]
        //[Required(ErrorMessage = "- digite o celular.")]
        public string iin_celular { get; set; }

        [Display(Name = "Observa��o")]
        [StringLength(20, ErrorMessage = "O tamanho m�ximo s�o 500 caracteres.")]
        //[Required(ErrorMessage = "- digite a observa��o.")]
        public string iin_observacao { get; set; }
        public DateTime iin_cadastro_dt { get; set; }
        public int din_id { get; set; }
        
        public string userCreate { get; set; }
        public Nullable<DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<DateTime> dtDelete { get; set; }

        public acao iin_acao { get; set; }
    }

}
