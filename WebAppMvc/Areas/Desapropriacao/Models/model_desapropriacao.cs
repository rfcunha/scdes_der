using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using WebAppMvc.Areas.Administrativo.Models.DesapropriacaoProjetoDepartamentoResponsavel;
using WebAppMvc.Areas.Administrativo.Models.DesapropriacaoProjetoStatus;

namespace WebAppMvc.Areas.Desapropriacao.Models
{
    public class model_desapropriacao
    {
        public model_desapropriacao()
        {
            list_model_desapropriacao_emitente = new List<model_desapropriacao_emitente>();
            list_model_desapropriacao_observacao = new List<model_desapropriacao_observacao>();
            list_model_desapropriacao_localidade = new List<model_desapropriacao_localidade>();
            list_model_desapropriacao_recurso_fonte = new List<model_desapropriacao_recurso_fonte>();
            list_model_desapropriacao_empreendimento = new List<model_desapropriacao_empreendimento>();
        }


        [Key]
        public int des_id { get; set; }

        [Display(Name = "N�mero do Decreto")]
        [StringLength(10, ErrorMessage = "O tamanho m�ximo s�o 10 caracteres.")]
        [Required(ErrorMessage = "- digite o n�mero do decreto.")]
        public string des_projeto_codigo { get; set; }
        
        [Display(Name = "Empreendimento")]
        [StringLength(70, ErrorMessage = "O tamanho m�ximo s�o 70 caracteres.")]
        [Required(ErrorMessage = "- digite o empreendimento.")]
        public string des_nome { get; set; }

        [Display(Name = "Departamento Respos�vel")]
        [Required(ErrorMessage = "- selecione o departamento respons�vel.")]
        public int dde_id { get; set; }
        
        [Display(Name = "Descri��o")]
        //[StringLength(500, ErrorMessage = "O tamanho m�ximo s�o 500 caracteres.")]
        [Required(ErrorMessage = "- digite a descri��o.")]
        public string des_descricao { get; set; }
        
        [Display(Name = "Justificativa T�cnica")]
        //[StringLength(500, ErrorMessage = "O tamanho m�ximo s�o 500 caracteres.")]
        [Required(ErrorMessage = "- digite a justificativa t�cnica.")]
        public string des_objetivo { get; set; }
       
        [Display(Name = "Dt. Inicial")]
        [Required(ErrorMessage = "- digite da data de inicio.")]
        public Nullable<DateTime> des_inicio { get; set; }
        
        [Display(Name = "Dt. Final ")]
        [Required(ErrorMessage = "- digite a data final.")]
        public Nullable<DateTime> des_fim { get; set; }
        
        [Display(Name = "Valor Estimado")]
        //[Required(ErrorMessage = "- digite o valor estimado.")]
        public Decimal? des_valor_estimado { get; set; }

        [Display(Name = "Status")]
        [Required(ErrorMessage = "- selecione o status.")]
        public int dst_id { get; set; }

        [Display(Name = "Agente Financiador")]
        [Required(ErrorMessage = "- selecione o agente financiador.")]
        public int afi_id { get; set; }
        public string afi_nome { get; set; }

        [Display(Name = "Regional")]
        [Required(ErrorMessage = "- selecione a regional.")]
        public int reg_id { get; set; }
        public string reg_nome { get; set; }

        [Display(Name = "Dt. Cadastro")]
        public DateTime des_cadastro_dt { get; set; }

        public string userCreate { get; set; }
        public DateTime? dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public DateTime? dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public DateTime? dtDelete { get; set; }

        public virtual model_desapropriacao_departamento model_desapropriacao_departamento { get; set; }
        public virtual model_desapropriacao_status model_desapropriacao_status { get; set; }

        public ICollection<model_desapropriacao_emitente> list_model_desapropriacao_emitente { get; set; }
        public ICollection<model_desapropriacao_empreendimento> list_model_desapropriacao_empreendimento { get; set; }
        public ICollection<model_desapropriacao_localidade> list_model_desapropriacao_localidade { get; set; }
        public ICollection<model_desapropriacao_observacao> list_model_desapropriacao_observacao { get; set; }
        public ICollection<model_desapropriacao_recurso_fonte> list_model_desapropriacao_recurso_fonte { get; set; }

    }
}
