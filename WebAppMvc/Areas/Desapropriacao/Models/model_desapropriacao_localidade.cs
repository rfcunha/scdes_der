using System;
using System.ComponentModel.DataAnnotations;

namespace WebAppMvc.Areas.Desapropriacao.Models
{
    public class model_desapropriacao_localidade
    {
        [Key]
        public int dlo_id { get; set; }
        
        [Display(Name = "Estado")]
        [Required(ErrorMessage = "- selecione o estado.")]
        public int est_id { get; set; }
        
        public string est_nome { get; set; }
        
        [Display(Name = "Municipio")]
        [Required(ErrorMessage = "- selecione o municipio.")]
        public int mun_id { get; set; }
        
        public string mun_nome { get; set; }

        [Display(Name = "Complemento")]
        [StringLength(500, ErrorMessage = "O tamanho m�ximo s�o 500 caracteres.")]
        public string dlo_complemento { get; set; }

        public DateTime dlo_cadastro_dt { get; set; }
        public int des_id { get; set; }

        public dlo_acao dlo_acao { get; set; }

        public string userCreate { get; set; }
        public DateTime? dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public DateTime? dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public DateTime? dtDelete { get; set; }
    }

    public enum dlo_acao
    {
        Insert = 1,
        Update = 2,
        Delete = 3
    }
}
