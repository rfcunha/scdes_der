using System;
using System.ComponentModel.DataAnnotations;
using Entity.SCDES.Enum;
using System.Web;
using System.Collections.Generic;

namespace WebAppMvc.Areas.Desapropriacao.Models
{
    public class model_anexo_identificador
    {
        public int aid_id { get; set; }
        public string aid_tipo { get; set; }
        public string aid_guia { get; set; }
        public string aid_form { get; set; }

        public List<model_anexo> list_model_anexo { get; set; }
    }
}
