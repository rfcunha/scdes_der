using System;
using System.ComponentModel.DataAnnotations;

namespace WebAppMvc.Areas.Desapropriacao.Models
{
    public class model_desapropriacao_recurso_fonte
    {
        public int drf_id { get; set; }
        
        [Display(Name = "Percentual")]
        [Required(ErrorMessage = "- selecione a porcentagem.")]
        public decimal drf_percentual { get; set; }
        
        [Display(Name = "Valor")]
        [Required(ErrorMessage = "- digite o valor.")]
        public decimal drf_valor { get; set; }
        
        [Display(Name = "N� Contrato")]
        [StringLength(10, ErrorMessage = "O tamanho m�ximo s�o 10 caracteres.")]
        [Required(ErrorMessage = "- digite o n�mero do contrato.")]
        public string drf_contrato_numero { get; set; }
        
        [Display(Name = "Data de Assinatura")]
        [Required(ErrorMessage = "- digite a data da assinatura.")]
        public DateTime drf_assinatura_dt { get; set; }
        
        public DateTime drf_cadastro_dt { get; set; }
        
        public int des_id { get; set; }

        public drf_acao drf_acao { get; set; }

        public string userCreate { get; set; }
        [Display(Name = "Dt. Create")]
        public DateTime? dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        [Display(Name = "Dt. Last Update")]
        public DateTime? dtLastUpdate { get; set; }
        [Display(Name = "Dt. Delete")]
        public string userDelete { get; set; }
        public DateTime? dtDelete { get; set; }
    }

    public enum drf_acao
    {
        Insert = 1,
        Update = 2,
        Delete = 3
    }
}
