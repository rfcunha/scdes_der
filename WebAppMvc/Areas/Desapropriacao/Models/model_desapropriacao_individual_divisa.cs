using System;
using System.ComponentModel.DataAnnotations;
using Entity.SCDES.Enum;
using System.Web;
using System.Collections.Generic;

namespace WebAppMvc.Areas.Desapropriacao.Models
{
    public class model_desapropriacao_individual_divisa
    {
        public model_desapropriacao_individual_divisa()
        {
            model_anexo_identificador = new model_anexo_identificador()
            {
                aid_tipo = "INDIVIDUAL",
                aid_guia = "BENFEITORIAS",
                aid_form = "DIVISAS"
            };
        }

        [Key]
        public int idi_id { get; set; }
        [Display(Name = "Tipo")]
        public string idi_tipo { get; set; }
        [Display(Name = "Descrição")]
        public string idi_descricao { get; set; }
        [Display(Name = "Quantidade")]
        public string idi_quantidade { get; set; }
        [Display(Name = "Unidade")]
        public string idi_unidade { get; set; }
        [Display(Name = "Observações")]
        public string idi_observacao { get; set; }
        [Display(Name = "Valor Total")]
        public decimal idi_valor_total { get; set; }
        public DateTime idi_cadastro_dt { get; set; }
        public int din_id { get; set; }

        public int? idi_anexo_identificador_id { get; set; }
        public model_anexo_identificador model_anexo_identificador { get; set; }

        public string userCreate { get; set; }
        public Nullable<DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<DateTime> dtDelete { get; set; }

        public acao idi_acao { get; set; }
    }
}
