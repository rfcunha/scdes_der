using System;
using System.ComponentModel.DataAnnotations;
using Entity.SCDES.Enum;
using Entity.SCDES.Models;
using System.Web;

namespace WebAppMvc.Areas.Desapropriacao.Models
{
    public class model_desapropriacao_individual_entrada
    {
        public model_desapropriacao_individual_entrada()
        {
            model_anexo_identificador = new model_anexo_identificador()
            {
                aid_tipo = "INDIVIDUAL",
                aid_guia = "BENFEITORIAS",
                aid_form = "ENTRADAS"
            };
        }

        [Key]
        public int ien_id { get; set; }
        [Display(Name = "Tipo")]
        public string ien_tipo { get; set; }
        [Display(Name = "Conservação")]
        public string ien_conservacao_estado { get; set; }
        [Display(Name = "Dimensões")]
        public string ien_dimensao { get; set; }
        [Display(Name = "Observações")]
        public string ien_observacao { get; set; }
        [Display(Name = "Valor Total")]
        public decimal ien_valor_total { get; set; }
        public DateTime ien_cadastro_dt { get; set; }
        public int din_id { get; set; }

        public int? ien_anexo_identificador_id { get; set; }
        public model_anexo_identificador model_anexo_identificador { get; set; }
        
        public string userCreate { get; set; }
        public Nullable<System.DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<System.DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<System.DateTime> dtDelete { get; set; }

        public acao ien_acao { get; set; }
    }

}
