using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using Entity.SCDES.Enum;

namespace WebAppMvc.Areas.Desapropriacao.Models
{
    public class model_desapropriacao_empreendimento
    {
        public model_desapropriacao_empreendimento()
        {
            list_model_desapropriacao_individual = new List<model_desapropriacao_individual>();
        }

        [Key]
        public int dem_id { get; set; }
        
        [Display(Name = "N� Autos do Decreto")]
        [StringLength(30, ErrorMessage = "O tamanho m�ximo s�o 30 caracteres.")]
        [Required(ErrorMessage = "- digite o autos do decreto.")]
        public string dem_auto_decreto { get; set; }

        [Display(Name = "Lote")]
        [Required(ErrorMessage = "- digite o lote.")]
        public short dem_lote { get; set; }
        
        [Display(Name = "SP")]
        [Required(ErrorMessage = "- digite a sp.")]
        public string dem_rodovia_sp { get; set; }
        
        [Display(Name = "Rodovia")]
        [StringLength(300, ErrorMessage = "O tamanho m�ximo s�o 300 caracteres.")]
        public string dem_rodovia_nome { get; set; }
       
        [Display(Name = "Trecho")]
        [StringLength(300, ErrorMessage = "O tamanho m�ximo s�o 300 caracteres.")]
        public string dem_rodovia_trecho { get; set; }
        
        [Display(Name = "Km Inicial")]
        [Required(ErrorMessage = "- digite o km inicial.")]
        public string dem_rodovia_km_inicial { get; set; }
        
        [Display(Name = "Km Final")]
        [Required(ErrorMessage = "- digite o km final.")]
        public string dem_rodovia_km_final { get; set; }
        
        [Display(Name = "Data do DUP")]
        [Required(ErrorMessage = "- digite a data do dup.")]
        public DateTime dem_dup_dt { get; set; }
        
        [Display(Name = "C�digo do DUP")]
        [StringLength(20, ErrorMessage = "O tamanho m�ximo s�o 20 caracteres.")]
        [Required(ErrorMessage = "- digite o c�digo do dup.")]
        public string dem_dup_codigo { get; set; }
        
        [Display(Name = "Nota de Reserva")]
        [StringLength(20, ErrorMessage = "O tamanho m�ximo s�o 20 caracteres.")]
        public string dem_nota_reserva { get; set; }
        
        public int des_id { get; set; }

        [Display(Name = "Upload")]
        public HttpPostedFileBase dem_file_upload { get; set; }
        public string dem_anexo { get; set; }
        public string dem_anexo_nome { get; set; }
        public string dem_anexo_pasta { get; set; } = @"\Upload\Anexo\";
        public DateTime dem_cadastro_dt { get; set; }
        
        public acao dem_acao { get; set; }
        public bool dem_update_anexo { get; set; }

        public string userCreate { get; set; }
        public DateTime? dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public DateTime? dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public DateTime? dtDelete { get; set; }

        public virtual ICollection<model_desapropriacao_individual> list_model_desapropriacao_individual { get; set; }
    }
}
