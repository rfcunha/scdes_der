using System;
using System.Collections.Generic;

namespace Entity.SCDES.Models
{
    public partial class tab_desapropriacao_indivisual_historico
    {
        public int ihi_id { get; set; }
        public string ihi_descricao { get; set; }
        public string ihi_usuario { get; set; }
        public System.DateTime ihi_cadastro_dt { get; set; }
        public int din_id { get; set; }
        public virtual tab_desapropriacao_individual tab_desapropriacao_individual { get; set; }
    }
}
