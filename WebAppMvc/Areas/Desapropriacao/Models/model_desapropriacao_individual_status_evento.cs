using System;
using System.ComponentModel.DataAnnotations;
using Entity.SCDES.Enum;

namespace WebAppMvc.Areas.Desapropriacao.Models
{
    public class model_desapropriacao_individual_status_evento
    {
        [Key]
        public int ise_id { get; set; }

        [Display(Name = "Descri��o")]
        //[Required(ErrorMessage = "- digite a data.")]
        public string ise_data { get; set; }

        [Display(Name = "Valor")]
        //[Required(ErrorMessage = "- digite a descri��o.")]
        public decimal? ise_valor { get; set; }

        public DateTime ise_cadastro_dt { get; set; }

        [Display(Name = "Tipo de Evento")]
        //[Required(ErrorMessage = "- selecione o tipo de evento.")]
        public int set_id { get; set; }

        public int din_id { get; set; }

        
        public string userCreate { get; set; }
        public Nullable<DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<DateTime> dtDelete { get; set; }

        public acao ise_acao { get; set; }
    }
}
