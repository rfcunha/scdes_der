using System;
using System.Collections.Generic;

namespace Entity.SCDES.Models
{
    public partial class tab_desapropriacao_historico
    {
        public int dhi_id { get; set; }
        public string dhi_descricao { get; set; }
        public string dhi_usuario { get; set; }
        public System.DateTime dhi_cadastro_dt { get; set; }
        public int des_id { get; set; }
        public virtual WebAppMvc.Areas.Desapropriacao.Models.model_desapropriacao tab_desapropriacao { get; set; }
    }
}
