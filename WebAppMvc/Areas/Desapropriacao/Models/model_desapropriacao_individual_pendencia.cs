﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebAppMvc.Areas.Desapropriacao.Models
{
    public class model_desapropriacao_individual_pendencia
    {
        [Key]
        public int ipe_id { get; set; }

        [Display(Name = "Observações")]
        [StringLength(500, ErrorMessage = "O tamanho máximo são 500 caracteres.")]
        //[Required(ErrorMessage = "- digite a descrição.")]
        public string ipe_descricao { get; set; }

        [Display(Name = "Status")]
        public short ipe_status { get; set; }

        public string ipe_cadastro_usuario { get; set; }
        public DateTime ipe_cadastro_dt { get; set; }

        public string ipe_concluido_usuario { get; set; }
        public DateTime? ipe_concluido_dt { get; set; }

        public ipe_acao ipe_acao { get; set; }



        public int din_id { get; set; }
        public int usu_id { get; set; }

        public string userCreate { get; set; }
        public Nullable<System.DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<System.DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<System.DateTime> dtDelete { get; set; }


        //public virtual WebAppMvc.Areas.Desapropriacao.Models.model_desapropriacao tab_desapropriacao { get; set; }
    }

    public enum ipe_acao
    {
        Insert = 1,
        Update = 2,
        Delete = 3
    }
}