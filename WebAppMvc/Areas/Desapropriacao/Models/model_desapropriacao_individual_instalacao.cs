using System;
using System.ComponentModel.DataAnnotations;
using Entity.SCDES.Enum;
using Entity.SCDES.Models;

namespace WebAppMvc.Areas.Desapropriacao.Models
{
    public class model_desapropriacao_individual_instalacao
    {
        public model_desapropriacao_individual_instalacao()
        {
            model_anexo_identificador = new model_anexo_identificador()
            {
                aid_tipo = "INDIVIDUAL",
                aid_guia = "BENFEITORIAS",
                aid_form = "INSTALAÇÕES"
            };
        }

        [Key]
        public int iin_id { get; set; }
        [Display(Name = "Tipo")]
        public string iin_tipo { get; set; }
        [Display(Name = "Conservação")]
        public string iin_conservacao_estado { get; set; }
        [Display(Name = "Extensão")]
        public string iin_extensao { get; set; }
        [Display(Name = "Oberservação")]
        public string iin_observacao { get; set; }
        [Display(Name = "Valor Total")]
        public decimal iin_valor_total { get; set; }
        public System.DateTime iin_cadastro_dt { get; set; }
        public int din_id { get; set; }

        public int? iin_anexo_identificador_id { get; set; }
        public model_anexo_identificador model_anexo_identificador { get; set; }
        
        public string userCreate { get; set; }
        public Nullable<System.DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<System.DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<System.DateTime> dtDelete { get; set; }
        
        public acao iin_acao { get; set; }
    }
}
