using System;
using System.ComponentModel.DataAnnotations;
using Entity.SCDES.Enum;
using System.Web;
using System.Collections.Generic;
using System.IO;

namespace WebAppMvc.Areas.Desapropriacao.Models
{
    public class model_anexo
    {
        public model_anexo()
        {
            ane_pasta = "\\Upload\\Anexo\\";
            dtCreate = DateTime.Now;
        }

        public int ane_id { get; set; }

        public int aid_id { get; set; }

        public string ane_nome_original { get; set; }
        public string ane_nome { get; set; }
        public string ane_pasta { get; set; }
        public string ane_mime_type { get; set; }
        public int ane_ordem { get; set; }
        public string ane_legenda { get; set; }

        private HttpPostedFileBase _ane_arquivo;

        public HttpPostedFileBase ane_arquivo
        {
            get
            {
                return _ane_arquivo;
            }
            set
            {
                _ane_arquivo = value;

                if (ane_id == 0)
                {
                    ane_nome_original = value == null ? null : Path.GetFileName(value.FileName);
                    ane_nome = value == null ? null : Guid.NewGuid() + Path.GetExtension(value.FileName);
                    ane_mime_type = value == null ? null : value.ContentType;
                }
            }
        }

        public string userCreate { get; set; }
        public Nullable<DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<DateTime> dtDelete { get; set; }
    }
}
