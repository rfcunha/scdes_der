using System;
using System.ComponentModel.DataAnnotations;
using System.Web;
using Entity.SCDES.Enum;

namespace WebAppMvc.Areas.Desapropriacao.Models
{
    public class model_desapropriacao_individual_registro_fotografico
    {
        public model_desapropriacao_individual_registro_fotografico()
        {
            model_anexo_identificador = new model_anexo_identificador()
            {
                aid_tipo = "INDIVIDUAL",
                aid_guia = "REGISTRO_FOTOGRAFICO"
            };    
        }
        
        public int irf_id { get; set; }

        [Display(Name = "Data")]
        public DateTime? irf_data { get; set; }

        [Display(Name = "Descri��o")]
        [StringLength(200, ErrorMessage = "O tamanho m�ximo s�o 200 caracteres.")]
        //[Required(ErrorMessage = "- digite a nota de reserva.")]
        public string irf_descricao { get; set; }

        public short irf_orgem { get; set; }

        public int? aid_id { get; set; }
        public model_anexo_identificador model_anexo_identificador { get; set; }

        [Display(Name = "Upload")]
        public HttpPostedFileBase irf_file_upload { get; set; }
        public string irf_anexo { get; set; }
        public string irf_anexo_nome { get; set; }
        
        public DateTime irf_cadastro_dt { get; set; }
        
        public int din_id { get; set; }
        
        public string userCreate { get; set; }
        public Nullable<DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<DateTime> dtDelete { get; set; }

        public acao irf_acao { get; set; }
    }
}
