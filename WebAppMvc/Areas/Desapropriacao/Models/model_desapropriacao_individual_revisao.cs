﻿using System;
using System.ComponentModel.DataAnnotations;
using Entity.SCDES.Enum;

namespace WebAppMvc.Areas.Desapropriacao.Models
{
    public class model_desapropriacao_individual_revisao
    {
        public int ire_id { get; set; }

        [Display(Name = "Descrição/Data")]
        [StringLength(500, ErrorMessage = "O tamanho máximo são 500 caracteres.")]
        //[Required(ErrorMessage = "- digite a descrição.")]
        public string ire_descricao { get; set; }

        public string ire_revisor { get; set; }

        public DateTime ire_cadastro_dt { get; set; }

        public int din_id { get; set; }
        //public int usu_id { get; set; }

        public acao ire_acao { get; set; }
    }
}