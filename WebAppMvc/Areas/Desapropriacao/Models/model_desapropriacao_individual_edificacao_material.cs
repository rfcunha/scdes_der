using System;
using System.ComponentModel.DataAnnotations;
using Entity.SCDES.Enum;

namespace WebAppMvc.Areas.Desapropriacao.Models
{
    public class model_desapropriacao_individual_edificacao_material
    {
        [Key]
        public int iem_id { get; set; }
        [Display(Name = "Material")]
        public string iem_material { get; set; }
        [Display(Name = "Quantidade")]
        public string iem_quantidade { get; set; }
        [Display(Name = "Descri��o")]
        public string iem_descricao { get; set; }
        [Display(Name = "Valor Total")]
        public decimal iem_valor_total { get; set; }
        public DateTime iem_cadastro_dt { get; set; }
        public int ied_id { get; set; }
        
        public string userCreate { get; set; }
        public Nullable<DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<DateTime> dtDelete { get; set; }

        public acao iem_acao { get; set; }
    }
}
