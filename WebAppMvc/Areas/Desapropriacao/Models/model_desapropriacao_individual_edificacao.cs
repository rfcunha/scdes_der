using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Entity.SCDES.Enum;

namespace WebAppMvc.Areas.Desapropriacao.Models
{
    public class model_desapropriacao_individual_edificacao
    {
        public model_desapropriacao_individual_edificacao()
        {
            //list_model_desapropriacao_individual_edificacao_material = new List<model_desapropriacao_individual_edificacao_material>();
        }

        [Key]
        public int ied_id { get; set; }
        [Display(Name = "Tipologia")]
        public string ied_tipologia { get; set; }
        [Display(Name = "N� Pavimento")]
        public string ied_pavimento_numero { get; set; }
        [Display(Name = "�rea Construida")]
        public string ied_area_construida { get; set; }
        [Display(Name = "Valor")]
        public decimal ied_valor { get; set; }
        [Display(Name = "Implanta��o")]
        public string ied_implantacao { get; set; }
        [Display(Name = "Conserva��o")]
        public string ied_conservacao_estado { get; set; }
        [Display(Name = "Padr�o Construtivo")]
        public string ied_padrao_construtivo { get; set; }
        public System.DateTime ied_cadastro_dt { get; set; }
        public int din_id { get; set; }
        
        public string userCreate { get; set; }
        public Nullable<System.DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<System.DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<System.DateTime> dtDelete { get; set; }

        public ICollection<model_desapropriacao_individual_edificacao_material> list_model_desapropriacao_individual_edificacao_material { get; set; }
        
        public acao ied_acao { get; set; }
    }
}
