﻿using System;
using System.ComponentModel.DataAnnotations;
using WebAppMvc.Areas.Administrativo.Models;

namespace WebAppMvc.Areas.Desapropriacao.Models.Filtro
{
    public class model_desapropriacao_filtro : PesquisaModelDefault
    {
        [Display(Name = "Código do Projeto: ")]
        [StringLength(100, ErrorMessage = "O tamanho máximo são 100 caracteres.")]
        public string des_projeto_codigo { get; set; }

        [Display(Name = "Nome ")]
        [StringLength(50, ErrorMessage = "O tamanho máximo são 50 caracteres.")]
        public string des_nome { get; set; }

        [Display(Name = "Departamento Resposável")]
        //[Required(ErrorMessage = "- selecione o departamento responsável.")]
        public int? dde_id { get; set; }

        [Display(Name = "Nota de Reserva")]
        [StringLength(50, ErrorMessage = "O tamanho máximo são 50 caracteres.")]
        public string dem_nota_reserva { get; set; }

        [Display(Name = "Dt. Dup ")]
        public DateTime? dem_dup_dt { get; set; }

        [Display(Name = "Código Dup")]
        [StringLength(50, ErrorMessage = "O tamanho máximo são 50 caracteres.")]
        public string dem_dup_codigo { get; set; }

        [Display(Name = "Status")]
        public int? dst_id { get; set; }

        [Display(Name = "Regional")]
        public int? reg_id { get; set; }

        [Display(Name = "Agente Financiador")]
        public int? afi_id { get; set; }

        [Display(Name = "Nº Autos do Decreto")]
        //[Required(ErrorMessage = "- digite o autos do decreto.")]
        public string dem_auto_decreto { get; set; }

        [Display(Name = "Cadastro")]
        public DateTime? des_cadastro_dt_inicial { get; set; }
        public DateTime? des_cadastro_dt_final { get; set; }
    }
}