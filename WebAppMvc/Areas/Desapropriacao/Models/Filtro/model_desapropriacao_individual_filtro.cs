﻿using System;
using System.ComponentModel.DataAnnotations;
using WebAppMvc.Areas.Administrativo.Models;

namespace WebAppMvc.Areas.Desapropriacao.Models.Filtro
{
    public class model_desapropriacao_individual_filtro : PesquisaModelDefault
    {
        [Display(Name = "Código do Documento")]
        [StringLength(35, ErrorMessage = "O tamanho máximo são 35 caracteres.")]
        public string din_geral_documento_numero { get; set; }

        [Display(Name = "Elaborador")]
        [StringLength(150, ErrorMessage = "O tamanho máximo são 150 caracteres.")]
        public string din_empreendimento_elaborador { get; set; }

        [Display(Name = "Nº Autos")]
        [StringLength(18, ErrorMessage = "O tamanho máximo são 18 caracteres.")]
        public string din_propriedade_autos_numero { get; set; }

        [Display(Name = "Prioridade")]
        public short? din_propriedade_prioridade { get; set; }

        [Display(Name = "Proprietário")]
        [StringLength(150, ErrorMessage = "O tamanho máximo são 150 caracteres.")]
        public string din_interessados_proprietario { get; set; }

        [Display(Name = "Objeto do Cadastro")]
        [StringLength(100, ErrorMessage = "O tamanho máximo são 100 caracteres.")]
        public string din_empreendimento_objeto { get; set; }

        [Display(Name = "Dt. Emissão")]
        public DateTime? din_geral_emissao_dt { get; set; }

        [Display(Name = "Agente Financiador")]
        public int? afi_id { get; set; }

        [Display(Name = "Nº Autos do Decreto")]
        [StringLength(35, ErrorMessage = "O tamanho máximo são 30 caracteres.")]
        public string din_empreendimento_auto_decreto { get; set; }

        [Display(Name = "Código Dup")]
        [StringLength(20, ErrorMessage = "O tamanho máximo são 20 caracteres.")]
        public string din_empreendimento_dup_codigo { get; set; }

        [Display(Name = "Dt. Dup ")]
        public DateTime? din_empreendimento_dup_dt { get; set; }

        [Display(Name = "Status da Desapropriação")]
        public int? din_conducao_ist_id { get; set; }

        [Display(Name = "Regional")]
        public int? reg_id { get; set; }

        [Display(Name = "Sp")]
        public string din_empreendimento_rodovia_sp { get; set; }

        [Display(Name = "Rodovia")]
        public string din_empreendimento_rodovia_nome { get; set; }

        [Display(Name = "Cadastro")]
        public DateTime? din_cadastro_dt_inicial { get; set; }
        public DateTime? din_cadastro_dt_final { get; set; }
    }
}