using System;
using System.ComponentModel.DataAnnotations;
using System.Web;
using Entity.SCDES.Enum;

namespace WebAppMvc.Areas.Desapropriacao.Models
{
    public class model_desapropriacao_individual_anexos
    {

        public model_desapropriacao_individual_anexos()
        {
            model_anexo_identificador = new model_anexo_identificador()
            {
                aid_tipo = "INDIVIDUAL",
                aid_guia = "ANEXO"
            };
        }

        public int ian_id { get; set; }
        public string ian_anexo { get; set; }
        public string dia_anexo_nome { get; set; }
        [Display(Name = "Tipo")]
        public short ian_tipo { get; set; }
        public DateTime ian_cadastro_dt { get; set; }
        public int din_id { get; set; }

        public int aid_id { get; set; }

        public acao ian_acao { get; set; }

        public string userCreate { get; set; }
        public Nullable<System.DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<System.DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<System.DateTime> dtDelete { get; set; }

        public model_anexo_identificador model_anexo_identificador { get; set; }

        [Display(Name = "Upload")]
        public HttpPostedFileBase ian_file_upload { get; set; }
    }
}
