using System;
using Entity.SCDES.Enum;
using System.ComponentModel.DataAnnotations;

namespace WebAppMvc.Areas.Desapropriacao.Models
{
    public class model_desapropriacao_emitente
    {
        [Key]
        public int dee_id { get; set; }
        
        public DateTime dee_cadastro_dt { get; set; }

        [Display(Name = "Emitente")]
        [Required(ErrorMessage = "- selecione o emitente.")]
        public int emp_id { get; set; }
        
        public string emp_nome { get; set; }

        public int des_id { get; set; }
        
        public string userCreate { get; set; }
        public Nullable<DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<DateTime> dtDelete { get; set; }

        public acao dee_acao { get; set; }
    }
}
