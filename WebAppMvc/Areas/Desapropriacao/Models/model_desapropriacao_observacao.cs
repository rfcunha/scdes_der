using System;
using System.ComponentModel.DataAnnotations;

namespace WebAppMvc.Areas.Desapropriacao.Models
{
    public class model_desapropriacao_observacao
    {
        [Key]
        public int dob_id { get; set; }
        
        [Display(Name = "Descri��o")]
        [StringLength(500, ErrorMessage = "O tamanho m�ximo s�o 500 caracteres.")]
        [Required(ErrorMessage = "- digite a descri��o.")]
        public string dob_descricao { get; set; }

        public string dob_cadastro_usuario { get; set; }
        public DateTime dob_cadastro_dt { get; set; }

        public int des_id { get; set; }
        public int usu_id { get; set; }
        
        public string userCreate { get; set; }
        public Nullable<System.DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<System.DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<System.DateTime> dtDelete { get; set; }


        public dob_acao dob_acao { get; set; }
    }

    public enum dob_acao
    {
        Insert = 1,
        Update = 2,
        Delete = 3
    }
}
