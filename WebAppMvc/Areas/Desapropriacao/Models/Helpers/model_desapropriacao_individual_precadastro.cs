﻿

using System.ComponentModel.DataAnnotations;
namespace WebAppMvc.Areas.Desapropriacao.Models.Helpers
{
    public class model_desapropriacao_individual_precadastro
    {
        [Display(Name = "Nº Auto do Decreto")]
        [Required(ErrorMessage = "- selecione o nº auto do decreto.")]
        public string dpc_empreendimento_auto_decreto { get; set; }

        [Display(Name = "Lote")]
        [Required(ErrorMessage = "- selecione o lote")]
        public short dpc_empreendimento_lote { get; set; }
    }
}