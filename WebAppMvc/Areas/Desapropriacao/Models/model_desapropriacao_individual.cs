using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using WebAppMvc.Areas.Administrativo.Models.DesapropriacaoIndividualAssistenteTecnico;
using WebAppMvc.Areas.Administrativo.Models.DesapropriacaoIndividualPeritoJudicial;
using WebAppMvc.Areas.Administrativo.Models.DesapropriacaoIndividualPredominanteEntorno;
using WebAppMvc.Areas.Administrativo.Models.DesapropriacaoIndividualSituacao;
using WebAppMvc.Areas.Administrativo.Models.DesapropriacaoIndividualStatus;

namespace WebAppMvc.Areas.Desapropriacao.Models
{
    public class model_desapropriacao_individual
    {
        public model_desapropriacao_individual()
        {
            list_model_desapropriacao_individual_interessado = new List<model_desapropriacao_individual_interessado>();
            list_model_desapropriacao_individual_anexos = new List<model_desapropriacao_individual_anexos>();
            list_model_desapropriacao_individual_cultura = new List<model_desapropriacao_individual_cultura>();
            list_model_desapropriacao_individual_divisa = new List<model_desapropriacao_individual_divisa>();
            list_model_desapropriacao_individual_edificacao = new List<model_desapropriacao_individual_edificacao>();
            list_model_desapropriacao_individual_entrada = new List<model_desapropriacao_individual_entrada>();
            list_model_desapropriacao_individual_instalacao = new List<model_desapropriacao_individual_instalacao>();
            list_model_desapropriacao_individual_pendencia = new List<model_desapropriacao_individual_pendencia>();
            list_model_desapropriacao_individual_registro_fotografico = new List<model_desapropriacao_individual_registro_fotografico>();
            list_model_desapropriacao_individual_revisao = new List<model_desapropriacao_individual_revisao>();
            list_model_desapropriacao_individual_estrada_acesso = new List<model_desapropriacao_individual_estrada_acesso>();
            list_model_desapropriacao_individual_status_evento = new List<model_desapropriacao_individual_status_evento>();
            list_model_desapropriacao_individual_planta_propriedade = new List<model_desapropriacao_individual_planta_propriedade>();
        }

        [Key]
        public int din_id { get; set; }

        [Display(Name = "C�digo do Documento")]
        [StringLength(35, ErrorMessage = "O tamanho m�ximo s�o 35 caracteres.")]
        public string din_geral_documento_numero { get; set; }

        public string din_guid { get; set; }
        
        [Display(Name = "Revis�o")]
        public string din_geral_revisao { get; set; }
        
        [Display(Name = "Data de Emiss�o")]
        public DateTime? din_geral_emissao_dt { get; set; }
        
        [Display(Name = "Emitente")]
        public int din_geral_emp_id { get; set; }

        [Display(Name = "Elabora��o")]
        [StringLength(150, ErrorMessage = "O tamanho m�ximo s�o 150 caracteres.")]
        public string din_geral_elaborador { get; set; }

        [Display(Name = "Respons�vel T�cnico")]
        [StringLength(150, ErrorMessage = "O tamanho m�ximo s�o 150 caracteres.")]
        public string din_geral_responsavel_tecnico { get; set; }

        [Display(Name = "Objeto do Cadastro")]
        [StringLength(100, ErrorMessage = "O tamanho m�ximo s�o 100 caracteres.")]
        public string din_geral_objeto { get; set; }

        public int dem_id { get; set; }
        
        [Display(Name = "Cep")]
        public string din_propriedade_cep { get; set; }
        
        [Display(Name = "N�mero")]
        public string din_propriedade_logradouro_numero { get; set; }
        
        [Display(Name = "Logradouro")]
        public string din_propriedade_logradouro { get; set; }
        
        [Display(Name = "Complemento")]
        public string din_propriedade_logradouro_complemento { get; set; }
        
        [Display(Name = "Municipio")]
        public int? din_propriedade_mun_id { get; set; }
        
        [Display(Name = "Estado")]
        public int? din_propriedade_est_id { get; set; }
        
        [Display(Name = "Bairro")]
        public string din_propriedade_bairro { get; set; }

        [Display(Name = "Tipo de Contribuinte")]
        public short? din_propriedade_contribuinte_tipo { get; set; }

        [Display(Name = "N�mero do Contribuinte")]
        public string din_propriedade_contribuinte_numero { get; set; }
        
        [Display(Name = "Cart�rio")]
        //[Required(ErrorMessage = "- Cart�rio � um campo obrigat�rio")]
        public string din_propriedade_cartorio { get; set; }
        
        [Display(Name = "Matricula")]
        public string din_propriedade_matricula { get; set; }
        
        [Display(Name = "Desapropria��o")]
        public Nullable<short> din_propriedade_desapropriacao { get; set; }
        
        [Display(Name = "Comarca")]
        //[Required(ErrorMessage = "- Comarca � um campo obrigat�rio")]
        public string din_propriedade_comarca { get; set; }
        
        [Display(Name = "Estaca da Propriedade")]
        public string din_propriedade_estaca_inicial { get; set; }
        
        [Display(Name = "Estaca da Propriedade")]
        public string din_propriedade_estaca_final { get; set; }
        
        [Display(Name = "Km da Propriedade")]
        public string din_propriedade_km_inicial { get; set; }
        
        [Display(Name = "Km da Propriedade")]
        public string din_propriedade_km_final { get; set; }

        [Display(Name = "Latitude/Longitude")]
        public string din_propriedade_latitude { get; set; }

        [Display(Name = "Latitude/Longitude")]
        public string din_propriedade_longitude { get; set; }
        
        [Display(Name = "Dominio")]
        public Nullable<short> din_propriedade_dominio { get; set; }
        
        [Display(Name = "Prioridade")]
        public Nullable<short> din_propriedade_prioridade { get; set; }
        
        [Display(Name = "N� Autos")]
        [StringLength(18, ErrorMessage = "O tamanho m�ximo s�o 18 caracteres.")]
        public string din_propriedade_autos_numero { get; set; }
        
        [Display(Name = "Dt. Autos")]
        public DateTime? din_propriedade_autos_dt { get; set; }

        [Display(Name = "Deseja adicionar dados de propriedade?")]
        public bool din_propriedade_validar { get; set; }

        [Display(Name = "Anu�ncia")]
        //[Required(ErrorMessage = "- Anu�ncia � um campo obrigat�rio")]
        public bool? din_conducao_anuencia { get; set; }

        [Display(Name = "Valor Total")]
        public Nullable<decimal> din_conducao_valor_total { get; set; }

        [Display(Name = "Previs�o de Pgto")]
        public DateTime? din_conducao_pgto_previsao_dt { get; set; }

        [Display(Name = "Status da Desapropria��o")]
        //[Required(ErrorMessage = "- Selecione um status da desapropria��o")]
        public int? din_conducao_ist_id { get; set; }

        [Display(Name = "Situa��o")]
        public Nullable<int> din_conducao_situacao_desapropriacao { get; set; }

        [Display(Name = "N�mero do Processo Unificado")]
        public string din_conducao_numero_processo_unificado { get; set; }

        [Display(Name = "Observa��es da Situa��o")]
        public string din_conducao_situacao_desapropriacao_obs { get; set; }

        [Display(Name = "Perito")]
        public Nullable<int> din_conducao_ipj_id { get; set; }

        [Display(Name = "Assistente t�cnico")]
        public Nullable<int> din_conducao_iat_id { get; set; }
        
        [Display(Name = "Deseja adicionar dados de status?")]
        public bool din_conducao_validar { get; set; }

        
        [Display(Name = "Propriet�rio")]
        [StringLength(150, ErrorMessage = "O tamanho m�ximo s�o 150 caracteres.")]
        public string din_interessados_proprietario { get; set; }
        [Display(Name = "Logradouro")]
        [StringLength(150, ErrorMessage = "O tamanho m�ximo s�o 150 caracteres.")]
        public string din_interessados_logradouro { get; set; }
        [Display(Name = "Complemento")]
        [StringLength(100, ErrorMessage = "O tamanho m�ximo s�o 100 caracteres.")]
        public string din_interessados_logradouro_complemento { get; set; }
        [Display(Name = "C�digo Contribuinte")]
        public string din_interessados_contribuinte_codigo { get; set; }
        [Display(Name = "Observa��es")]
        public string din_interessados_observacao { get; set; }

        [Display(Name = "Deseja adicionar dados de interessados?")]
        public bool din_interessados_validar { get; set; }

        [Display(Name = "Uso Predominante")]
        public int? din_imovel_ipe_id { get; set; }
        [Display(Name = "Tipo Zoneamento")]
        public int? din_imovel_zti_id { get; set; }
        [Display(Name = "Infraestruturas Disponiveis")]
        public string din_imovel_infraestrutura { get; set; }
        [Display(Name = "Equipamentos Disponiveis")]
        public string din_imovel_equipamento { get; set; }
        [Display(Name = "Servi�os Disponiveis")]
        public string din_imovel_servico { get; set; }
        [Display(Name = "�rea do Terreno")]
        public string din_imovel_area_terreno { get; set; }
        [Display(Name = "�rea Necess�ria")]
        public string din_imovel_area_necessaria { get; set; }
        [Display(Name = "Porcentagem da �rea")]
        public string din_imovel_area_porcentagem { get; set; }
        [Display(Name = "Perimetro")]
        public string din_imovel_perimetro { get; set; }
        [Display(Name = "Topografia")]
        public int? din_imovel_top_id { get; set; }
        [Display(Name = "Condi��es do Solo")]
        public int? din_imovel_sco_id { get; set; }
        [Display(Name = "Restri��es ao Uso")]
        public string din_imovel_restricao_uso { get; set; }
        [Display(Name = "Curso D'�gua")]
        public string din_imovel_curso_dagua { get; set; }
        [Display(Name = "Recursos Minerais")]
        public string din_imovel_recurso_minerais { get; set; }
        [Display(Name = "Ocupa��es no Terreno")]
        public string din_imovel_terreno_ocupacoes { get; set; } // TODO: LEMBRAR DE MUDAR NO MODEL
        [Display(Name = "Uso do Solo")]
        public string din_imovel_sus { get; set; } // TODO: LEMBRAR DE MUDAR NO MODEL
        [Display(Name = "Delimita��es de Divisas")]
        public string din_imovel_delimitacao { get; set; }
        [Display(Name = "Atividade/Aproveitamento Econ�mico")]
        public string din_imovel_atividade_economica { get; set; }
        [Display(Name = "Posicionamento")]
        public string din_imovel_posicionamento { get; set; }
        [Display(Name = "Frontal")]
        public string din_imovel_frontal { get; set; }
        [Display(Name = "Fundos")]
        public string din_imovel_fundo { get; set; }
        [Display(Name = "Lateral Esquerda")]
        public string din_imovel_lateral_esquerda { get; set; }
        [Display(Name = "Lateral Direita")]
        public string din_imovel_lateral_direita { get; set; }

        [Display(Name = "Deseja adicionar dados de priodade?")]
        public bool din_imovel_validar { get; set; }

        [AllowHtml]
        [Display(Name = "Benfeitorias")]
        public string din_benfeitorias { get; set; }

        [Display(Name = "Deseja adicionar dados de benfeitorias?")]
        public bool din_benfeitorias_validar { get; set; }

        [AllowHtml]
        [Display(Name = "Memorial Descritivo")]
        public string din_memorial_descritivo { get; set; }

        [Display(Name = "Deseja adicionar dados de memorial descritivo?")]
        public bool din_memorial_descritivo_validar { get; set; }

        [AllowHtml]
        [Display(Name = "Planta da Propriedade")]
        public string din_planta_propriedade { get; set; }

        [Display(Name = "Deseja adicionar dados de planta da propriedade?")]
        public bool din_planta_propriedade_validar { get; set; }

        [AllowHtml]
        [Display(Name = "Laudo de Avalia��o")]
        public string din_laudo_avaliacao { get; set; }

        [Display(Name = "Deseja adicionar dados de laudo avaliacao?")]
        public bool din_laudo_avaliacao_validar { get; set; }

        [Display(Name = "Deseja adicionar anexos?")]
        public bool din_anexos_validar { get; set; }

        [Display(Name = "Deseja adicionar pend�ncias?")]
        public bool din_pendencias_validar { get; set; }

        [Display(Name = "Deseja adicionar registros fotograficos?")]
        public bool din_registro_fotograficos_validar { get; set; }
        
        public DateTime din_cadastro_dt { get; set; }
        public string userCreate { get; set; }
        public DateTime? dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public DateTime? dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public DateTime? dtDelete { get; set; }

        public model_desapropriacao_empreendimento model_desapropriacao_empreendimento { get; set; }
        public ICollection<model_desapropriacao_individual_anexos> list_model_desapropriacao_individual_anexos { get; set; }
        public ICollection<model_desapropriacao_individual_cultura> list_model_desapropriacao_individual_cultura { get; set; }
        public ICollection<model_desapropriacao_individual_divisa> list_model_desapropriacao_individual_divisa { get; set; }
        public ICollection<model_desapropriacao_individual_edificacao> list_model_desapropriacao_individual_edificacao { get; set; }
        public ICollection<model_desapropriacao_individual_entrada> list_model_desapropriacao_individual_entrada { get; set; }
        public ICollection<model_desapropriacao_individual_instalacao> list_model_desapropriacao_individual_instalacao { get; set; }
        public ICollection<model_desapropriacao_individual_pendencia> list_model_desapropriacao_individual_pendencia { get; set; }
        public ICollection<model_desapropriacao_individual_registro_fotografico> list_model_desapropriacao_individual_registro_fotografico { get; set; }
        public ICollection<model_desapropriacao_individual_planta_propriedade> list_model_desapropriacao_individual_planta_propriedade { get; set; }
        public ICollection<model_desapropriacao_individual_revisao> list_model_desapropriacao_individual_revisao { get; set; }
        public ICollection<model_desapropriacao_individual_estrada_acesso> list_model_desapropriacao_individual_estrada_acesso { get; set; }
        public ICollection<model_desapropriacao_individual_status_evento> list_model_desapropriacao_individual_status_evento { get; set; }
        public ICollection<model_desapropriacao_individual_interessado> list_model_desapropriacao_individual_interessado { get; set; }      
        //public virtual ICollection<model_desapropriacao_indivisual_historico> model_desapropriacao_indivisual_historico { get; set; }

        public virtual model_desapropriacao_individual_assistente_tecnico model_desapropriacao_individual_assistente_tecnico { get; set; }
        public virtual model_desapropriacao_individual_perito_judicial model_desapropriacao_individual_perito_judicial { get; set; }
        public virtual model_desapropriacao_individual_predominante_entorno model_desapropriacao_individual_predominante_entorno { get; set; }
        public virtual model_desapropriacao_individual_situacao model_desapropriacao_individual_situacao { get; set; }
        public virtual model_desapropriacao_individual_status model_desapropriacao_individual_status { get; set; }
    }
}
