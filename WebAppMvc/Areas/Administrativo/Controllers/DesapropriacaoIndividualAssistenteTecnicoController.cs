﻿using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Business.SCDES;
using Entity.SCDES.Models;
using Entity.SCDES.Utils.Filtro;
using Suporte.Criptografia;
using Suporte.Exceptions;
using Suporte.Formatacao;
using Suporte.MsgBox;
using Suporte.MsgBox.Mvc;
using WebAppMvc.Areas.Administrativo.Models.DesapropriacaoIndividualAssistenteTecnico;
using WebAppMvc.Areas.Administrativo.Models.DesapropriacaoIndividualAssistenteTecnico.Filtro;
using WebAppMvc.Helpers;
using WebAppMvc.Models.Filter;

namespace WebAppMvc.Areas.Administrativo.Controllers
{
    public class DesapropriacaoIndividualAssistenteTecnicoController : BaseController
    {
        #region | GET |

        [HttpGet]
        [HandleErrorAction]
        [RolesActionFilter(isModal = false)]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [NoCache]
        [HandleErrorAction]
        [RolesActionFilter(isModal = false)]
        public ActionResult Cadastrar()
        {
            return View();
        }

        [HttpGet]
        [NoCache]
        [HandleErrorAction]
        public ActionResult Editar(string args)
        {
            return OBTER(args);
        }

        [HttpGet]
        [NoCache]
        [HandleErrorAction]
        [RolesActionFilter(isModal = false)]
        public ActionResult Consultar(string args)
        {
            return string.IsNullOrEmpty(args) ? View(new model_desapropriacao_individual_assistente_tecnico_filtro()) : View(RetornarPesquisa(args));
        }

        #endregion

        #region | POST |

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Cadastrar(model_desapropriacao_individual_assistente_tecnico obj)
        {
            return SALVAR(obj);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Editar(model_desapropriacao_individual_assistente_tecnico obj, string args)
        {
            return ALTERAR(obj, args);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Consultar(tab_desapropriacao_individual_assistente_tecnico_filtro obj)
        {
            return PESQUISAR(obj);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Status(int id, bool status)
        {
            return STATUS(id, status, nomeloginUsuario);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Deletar(int id)
        {
            return DELETAR(id, nomeloginUsuario);
        }

        #endregion

        #region METODOS

        private ActionResult SALVAR(model_desapropriacao_individual_assistente_tecnico model)
        {
            try
            {
                ValidaModel();

                ActionResult retorno = null;

                var obj = Mapper.Map<model_desapropriacao_individual_assistente_tecnico, tab_desapropriacao_individual_assistente_tecnico>(model);

                if (new tab_desapropriacao_individual_assistente_tecnico_bll().INSERT(obj) > 0)
                {
                    retorno = new bootStrapDialog().Show("Sucesso", "Cadastro realizado sucesso!", Url.Action("Index", "DesapropriacaoIndividualAssistenteTecnico", new { area = "Administrativo" }), bootStrapMessageType.TYPE_SUCCESS, true);
                }

                return retorno;
            }
            catch (CustomException ex)
            {
                return new bootStrapDialog().Show(ex);
            }
        }

        private ActionResult ALTERAR(model_desapropriacao_individual_assistente_tecnico model, string args)
        {
            try
            {
                ValidaModel();

                ActionResult retorno = null;

                var obj = Mapper.Map<model_desapropriacao_individual_assistente_tecnico, tab_desapropriacao_individual_assistente_tecnico>(model);

                if (new tab_desapropriacao_individual_assistente_tecnico_bll().UPDATE(obj))
                {
                    retorno = new bootStrapDialog().Show("Sucesso", "Alteração realizada com sucesso!", Url.Action("Consultar", "DesapropriacaoIndividualAssistenteTecnico", new { area = "Administrativo", args = args }), bootStrapMessageType.TYPE_SUCCESS, false);
                }

                return retorno;
            }
            catch (CustomException ex)
            {
                return new bootStrapDialog().Show(ex);
            }
        }

        private ActionResult OBTER(string queryString)
        {
            try
            {
                var args = new EncryptQueryString(queryString);

                if (!string.IsNullOrEmpty(args["iat_id"]))
                {
                    var model = Mapper.Map<tab_desapropriacao_individual_assistente_tecnico, model_desapropriacao_individual_assistente_tecnico>(new tab_desapropriacao_individual_assistente_tecnico_bll().FIND(int.Parse(args["iat_id"])));

                    return View(model);
                }
                else
                {
                    return View("Consultar", RetornarPesquisa(queryString));
                }
            }
            catch (CustomException ex)
            {
                return new bootStrapDialog().Show(ex);
            }
        }

        private ActionResult STATUS(int id, bool status, string userLastUpdate)
        {
            try
            {
                ActionResult retorno = null;

                if (new tab_desapropriacao_individual_assistente_tecnico_bll().STATUS(id, status, userLastUpdate))
                {
                    retorno = new bootStrapDialog().Show("Sucesso", "Assistente técnico " + (status ? "ativado" : "desativado") + " com sucesso!", bootStrapMessageType.TYPE_SUCCESS);
                }

                return retorno;
            }
            catch (CustomException ex)
            {
                return new bootStrapDialog().Show(ex);
            }
        }

        private ActionResult DELETAR(int id, string userLastUpdate)
        {
            try
            {
                ActionResult retorno = null;

                if (new tab_desapropriacao_individual_assistente_tecnico_bll().DELETE(id, userLastUpdate))
                {
                    retorno = new bootStrapDialog().Show("Sucesso", "Assistente técnico deletado com sucesso!", bootStrapMessageType.TYPE_SUCCESS);
                }

                return retorno;
            }
            catch (CustomException ex)
            {
                return new bootStrapDialog().Show(ex);
            }
        }

        public ActionResult PESQUISAR(tab_desapropriacao_individual_assistente_tecnico_filtro obj)
        {
            try
            {
                var list = new tab_desapropriacao_individual_assistente_tecnico_bll().PESQUISA_GDV(obj).Select(t => new
                {
                    t.iat_id,
                    t.iat_nome,
                    t.iat_email,
                    iat_telefone = t.iat_telefone.FormatarTelefone(false),
                    iat_celular = t.iat_celular.FormatarCelular(false),
                    t.iat_ativo,
                    iat_cadastro_dt = t.iat_cadastro_dt.ToString("d"),
                    args = GerarQueryString(t.iat_id, obj).ToString()
                });

                return Json(new { data = list, recordsFiltered = obj.pageCount });
            }
            catch (CustomException ex)
            {
                return new bootStrapDialog().Show(ex);
            }
        }


        private void ValidaModel()
        {
            if (!ModelState.IsValid)
                throw new CustomException("Preencha os campos obrigatorios!", bootStrapMessageType.TYPE_WARNING);
        }

        private model_desapropriacao_individual_assistente_tecnico_filtro RetornarPesquisa(string requestQueryString)
        {
            var args = new EncryptQueryString(requestQueryString);

            return new model_desapropriacao_individual_assistente_tecnico_filtro
            {
                iat_nome = !string.IsNullOrEmpty(args["iat_nome"]) ? args["iat_nome"] : null,
                iat_ativo = !string.IsNullOrEmpty(args["iat_ativo"]) ? bool.Parse(args["iat_ativo"]) : (bool?)null,
                pageIndex = int.Parse(args["pageIndex"]),
                pageSize = int.Parse(args["pageSize"]),
            };
        }

        private EncryptQueryString GerarQueryString(int iat_id, tab_desapropriacao_individual_assistente_tecnico_filtro obj)
        {
            return new EncryptQueryString
            {
                { "iat_id",   iat_id.ToString()  }, 
                { "iat_nome", string.IsNullOrEmpty(obj.iat_nome) ? "" : obj.iat_nome  }, 
                { "iat_ativo",  obj.iat_ativo == null ? string.Empty : obj.iat_ativo.ToString()},
                { "pageIndex", obj.pageIndex.ToString()},
                { "pageSize",   obj.pageSize.ToString()}
            };
        }

        #endregion
    }
}
