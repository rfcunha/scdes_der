﻿using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Business.SCDES;
using Entity.SCDES.Models;
using Entity.SCDES.Utils.Filtro;
using Suporte.Criptografia;
using Suporte.Exceptions;
using Suporte.MsgBox;
using Suporte.MsgBox.Mvc;
using WebAppMvc.Areas.Administrativo.Models.DesapropriacaoIndividualStatus;
using WebAppMvc.Areas.Administrativo.Models.DesapropriacaoIndividualStatus.Filtro;
using WebAppMvc.Helpers;
using WebAppMvc.Models.Filter;

namespace WebAppMvc.Areas.Administrativo.Controllers
{
    public class DesapropriacaoIndividualStatusController : BaseController
    {
        #region | GET |

        [HttpGet]
        [HandleErrorAction]
        [RolesActionFilter(isModal = false)]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [NoCache]
        [HandleErrorAction]
        [RolesActionFilter(isModal = false)]
        public ActionResult Cadastrar()
        {
            return View();
        }

        [HttpGet]
        [NoCache]
        [HandleErrorAction]
        public ActionResult Editar(string args)
        {
            return OBTER(args);
        }

        [HttpGet]
        [NoCache]
        [HandleErrorAction]
        [RolesActionFilter(isModal = false)]
        public ActionResult Consultar(string args)
        {
            return string.IsNullOrEmpty(args) ? View(new model_desapropriacao_individual_status_filtro()) : View(RetornarPesquisa(args));
        }

        #endregion

        #region | POST |

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Cadastrar(model_desapropriacao_individual_status obj)
        {
            return SALVAR(obj);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Editar(model_desapropriacao_individual_status obj, string args)
        {
            return ALTERAR(obj, args);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Consultar(tab_desapropriacao_individual_status_filtro obj)
        {
            return PESQUISAR(obj);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Status(int id, bool status)
        {
            return STATUS(id, status, nomeloginUsuario);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Deletar(int id)
        {
            return DELETAR(id, nomeloginUsuario);
        }

        #endregion

        #region METODOS

        private ActionResult SALVAR(model_desapropriacao_individual_status model)
        {
            try
            {
                ValidaModel();

                ActionResult retorno = null;

                var obj = Mapper.Map<model_desapropriacao_individual_status, tab_desapropriacao_individual_status>(model);

                if (new tab_desapropriacao_individual_status_bll().INSERT(obj) > 0)
                {
                    retorno = new bootStrapDialog().Show("Sucesso", "Cadastro realizado sucesso!", Url.Action("Index", "DesapropriacaoIndividualStatus", new { area = "Administrativo" }), bootStrapMessageType.TYPE_SUCCESS, true);
                }

                return retorno;
            }
            catch (CustomException ex)
            {
                return new bootStrapDialog().Show(ex);
            }
        }

        private ActionResult ALTERAR(model_desapropriacao_individual_status model, string queryString)
        {
            try
            {
                ValidaModel();

                ActionResult retorno = null;

                var obj = Mapper.Map<model_desapropriacao_individual_status, tab_desapropriacao_individual_status>(model);

                if (new tab_desapropriacao_individual_status_bll().UPDATE(obj))
                {
                    retorno = new bootStrapDialog().Show("Sucesso", "Alteração realizada com sucesso!", Url.Action("Consultar", "DesapropriacaoIndividualStatus", new { area = "Administrativo", args = queryString }), bootStrapMessageType.TYPE_SUCCESS, false);
                }

                return retorno;
            }
            catch (CustomException ex)
            {
                return new bootStrapDialog().Show(ex);
            }
        }

        private ActionResult OBTER(string queryString)
        {
            try
            {
                var args = new EncryptQueryString(queryString);

                if (!string.IsNullOrEmpty(args["ist_id"]))
                {
                    var model = Mapper.Map<tab_desapropriacao_individual_status, model_desapropriacao_individual_status>(new tab_desapropriacao_individual_status_bll().FIND(int.Parse(args["ist_id"])));

                    return View(model);
                }
                else
                {
                    return View("Consultar", RetornarPesquisa(queryString));
                }
            }
            catch (CustomException ex)
            {
                return new bootStrapDialog().Show(ex);
            }
        }

        private ActionResult STATUS(int id, bool status, string userLastUpdate)
        {
            try
            {
                ActionResult retorno = null;

                if (new tab_desapropriacao_individual_status_bll().STATUS(id, status, userLastUpdate))
                {
                    retorno = new bootStrapDialog().Show("Sucesso", "Status (Desapropriação Individual)" + (status ? "ativado" : "desativado") + " com sucesso!", bootStrapMessageType.TYPE_SUCCESS);
                }

                return retorno;
            }
            catch (CustomException ex)
            {
                return new bootStrapDialog().Show(ex);
            }
        }

        private ActionResult DELETAR(int id, string userLastUpdate)
        {
            try
            {
                ActionResult retorno = null;

                if (new tab_desapropriacao_individual_status_bll().DELETE(id, userLastUpdate))
                {
                    retorno = new bootStrapDialog().Show("Sucesso", "Status (Desapropriação Individual) deletado com sucesso!", bootStrapMessageType.TYPE_SUCCESS);
                }

                return retorno;
            }
            catch (CustomException ex)
            {
                return new bootStrapDialog().Show(ex);
            }
        }

        public ActionResult PESQUISAR(tab_desapropriacao_individual_status_filtro obj)
        {
            try
            {
                var list = new tab_desapropriacao_individual_status_bll().PESQUISA_GDV(obj).Select(t => new
                {
                    t.ist_id,
                    t.ist_nome,
                    t.ist_ativo,
                    ist_cadastro_dt = t.ist_cadastro_dt.ToString("d"),
                    args = GerarQueryString(t.ist_id, obj).ToString()
                });

                return Json(new { data = list, recordsFiltered = obj.pageCount });
            }
            catch (CustomException ex)
            {
                return new bootStrapDialog().Show(ex);
            }
        }


        private void ValidaModel()
        {
            if (!ModelState.IsValid)
                throw new CustomException("Preencha os campos obrigatorios!", bootStrapMessageType.TYPE_WARNING);
        }

        private model_desapropriacao_individual_status_filtro RetornarPesquisa(string requestQueryString)
        {
            var args = new EncryptQueryString(requestQueryString);

            return new model_desapropriacao_individual_status_filtro
            {
                ist_nome = !string.IsNullOrEmpty(args["ist_nome"]) ? args["ist_nome"] : null,
                ist_ativo = !string.IsNullOrEmpty(args["ist_ativo"]) ? bool.Parse(args["ist_ativo"]) : (bool?)null,
                pageIndex = int.Parse(args["pageIndex"]),
                pageSize = int.Parse(args["pageSize"]),
            };
        }

        private EncryptQueryString GerarQueryString(int ist_id, tab_desapropriacao_individual_status_filtro obj)
        {
            return new EncryptQueryString
            {
                { "ist_id",   ist_id.ToString()  }, 
                { "ist_nome", string.IsNullOrEmpty(obj.ist_nome) ? "" : obj.ist_nome  }, 
                { "ist_ativo",  obj.ist_ativo == null ? string.Empty : obj.ist_ativo.ToString()},
                { "pageIndex", obj.pageIndex.ToString()},
                { "pageSize",   obj.pageSize.ToString()}
            };
        }

        #endregion
    }
}