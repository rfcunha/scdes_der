﻿using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Business.SCDES;
using Entity.SCDES.Models;
using Entity.SCDES.Utils.Filtro;
using Suporte.Criptografia;
using Suporte.Exceptions;
using Suporte.MsgBox;
using Suporte.MsgBox.Mvc;
using WebAppMvc.Areas.Administrativo.Models.DesapropriacaoIndividualPredominanteEntorno;
using WebAppMvc.Areas.Administrativo.Models.DesapropriacaoIndividualPredominanteEntorno.Filtro;
using WebAppMvc.Helpers;
using WebAppMvc.Models.Filter;

namespace WebAppMvc.Areas.Administrativo.Controllers
{
    public class DesapropriacaoIndividualPredominanteEntornoController : BaseController
    {
        #region | GET |

        [HttpGet]
        [HandleErrorAction]
        [RolesActionFilter(isModal = false)]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [NoCache]
        [HandleErrorAction]
        [RolesActionFilter(isModal = false)]
        public ActionResult Cadastrar()
        {
            return View();
        }

        [HttpGet]
        [NoCache]
        [HandleErrorAction]
        public ActionResult Editar(string args)
        {
            return OBTER(args);
        }

        [HttpGet]
        [NoCache]
        [HandleErrorAction]
        [RolesActionFilter(isModal = false)]
        public ActionResult Consultar(string args)
        {
            return string.IsNullOrEmpty(args) ? View(new model_desapropriacao_individual_predominante_entorno_filtro()) : View(RetornarPesquisa(args));
        }

        #endregion

        #region | POST |

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Cadastrar(model_desapropriacao_individual_predominante_entorno obj)
        {
            return SALVAR(obj);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Editar(model_desapropriacao_individual_predominante_entorno obj, string args)
        {
            return ALTERAR(obj, args);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Consultar(tab_desapropriacao_individual_predominante_entorno_filtro obj)
        {
            return PESQUISAR(obj);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Status(int id, bool status)
        {
            return STATUS(id, status, nomeloginUsuario);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Deletar(int id)
        {
            return DELETAR(id, nomeloginUsuario);
        }

        #endregion

        #region METODOS

        private ActionResult SALVAR(model_desapropriacao_individual_predominante_entorno model)
        {
            try
            {
                ValidaModel();

                ActionResult retorno = null;

                var obj = Mapper.Map<model_desapropriacao_individual_predominante_entorno, tab_desapropriacao_individual_predominante_entorno>(model);

                if (new tab_desapropriacao_individual_predominante_entorno_bll().INSERT(obj) > 0)
                {
                    retorno = new bootStrapDialog().Show("Sucesso", "Cadastro realizado sucesso!", Url.Action("Index", "DesapropriacaoIndividualPredominanteEntorno", new { area = "Administrativo" }), bootStrapMessageType.TYPE_SUCCESS, true);
                }

                return retorno;
            }
            catch (CustomException ex)
            {
                return new bootStrapDialog().Show(ex);
            }
        }

        private ActionResult ALTERAR(model_desapropriacao_individual_predominante_entorno model, string queryString)
        {
            try
            {
                ValidaModel();

                ActionResult retorno = null;

                var obj = Mapper.Map<model_desapropriacao_individual_predominante_entorno, tab_desapropriacao_individual_predominante_entorno>(model);

                if (new tab_desapropriacao_individual_predominante_entorno_bll().UPDATE(obj))
                {
                    retorno = new bootStrapDialog().Show("Sucesso", "Alteração realizada com sucesso!", Url.Action("Consultar", "DesapropriacaoIndividualPredominanteEntorno", new { area = "Administrativo", args = queryString }), bootStrapMessageType.TYPE_SUCCESS, false);
                }

                return retorno;
            }
            catch (CustomException ex)
            {
                return new bootStrapDialog().Show(ex);
            }
        }

        private ActionResult OBTER(string queryString)
        {
            try
            {
                var args = new EncryptQueryString(queryString);

                if (!string.IsNullOrEmpty(args["ipe_id"]))
                {
                    var model = Mapper.Map<tab_desapropriacao_individual_predominante_entorno, model_desapropriacao_individual_predominante_entorno>(new tab_desapropriacao_individual_predominante_entorno_bll().FIND(int.Parse(args["ipe_id"])));

                    return View(model);
                }
                else
                {
                    return View("Consultar", RetornarPesquisa(queryString));
                }
            }
            catch (CustomException ex)
            {
                return new bootStrapDialog().Show(ex);
            }
        }

        private ActionResult STATUS(int id, bool status, string userLastUpdate)
        {
            try
            {
                ActionResult retorno = null;

                if (new tab_desapropriacao_individual_predominante_entorno_bll().STATUS(id, status, userLastUpdate))
                {
                    retorno = new bootStrapDialog().Show("Sucesso", "Uso predominante entorno " + (status ? "ativado" : "desativado") + " com sucesso!", bootStrapMessageType.TYPE_SUCCESS);
                }

                return retorno;
            }
            catch (CustomException ex)
            {
                return new bootStrapDialog().Show(ex);
            }
        }

        private ActionResult DELETAR(int id, string userLastUpdate)
        {
            try
            {
                ActionResult retorno = null;

                if (new tab_desapropriacao_individual_predominante_entorno_bll().DELETE(id, userLastUpdate))
                {
                    retorno = new bootStrapDialog().Show("Sucesso", "Uso predominante entorno deletado com sucesso!", bootStrapMessageType.TYPE_SUCCESS);
                }

                return retorno;
            }
            catch (CustomException ex)
            {
                return new bootStrapDialog().Show(ex);
            }
        }

        public ActionResult PESQUISAR(tab_desapropriacao_individual_predominante_entorno_filtro obj)
        {
            try
            {
                var list = new tab_desapropriacao_individual_predominante_entorno_bll().PESQUISA_GDV(obj).Select(t => new
                {
                    t.ipe_id,
                    t.ipe_nome,
                    t.ipe_ativo,
                    ipe_cadastro_dt = t.ipe_cadastro_dt.ToString("d"),
                    args = GerarQueryString(t.ipe_id, obj).ToString()
                });

                return Json(new { data = list, recordsFiltered = obj.pageCount });
            }
            catch (CustomException ex)
            {
                return new bootStrapDialog().Show(ex);
            }
        }


        private void ValidaModel()
        {
            if (!ModelState.IsValid)
                throw new CustomException("Preencha os campos obrigatorios!", bootStrapMessageType.TYPE_WARNING);
        }

        private model_desapropriacao_individual_predominante_entorno_filtro RetornarPesquisa(string requestQueryString)
        {
            var args = new EncryptQueryString(requestQueryString);

            return new model_desapropriacao_individual_predominante_entorno_filtro
            {
                ipe_nome = !string.IsNullOrEmpty(args["ipe_nome"]) ? args["ipe_nome"] : null,
                ipe_ativo = !string.IsNullOrEmpty(args["ipe_ativo"]) ? bool.Parse(args["ipe_ativo"]) : (bool?)null,
                pageIndex = int.Parse(args["pageIndex"]),
                pageSize = int.Parse(args["pageSize"]),
            };
        }

        private EncryptQueryString GerarQueryString(int ipe_id, tab_desapropriacao_individual_predominante_entorno_filtro obj)
        {
            return new EncryptQueryString
            {
                { "ipe_id",   ipe_id.ToString()  }, 
                { "ipe_nome", string.IsNullOrEmpty(obj.ipe_nome) ? "" : obj.ipe_nome  }, 
                { "ipe_ativo",  obj.ipe_ativo == null ? string.Empty : obj.ipe_ativo.ToString()},
                { "pageIndex", obj.pageIndex.ToString()},
                { "pageSize",   obj.pageSize.ToString()}
            };
        }

        #endregion
    }
}