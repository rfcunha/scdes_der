﻿using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Business.SCDES;
using Entity.SCDES.Models;
using Entity.SCDES.Utils.Filtro;
using Suporte.Criptografia;
using Suporte.Exceptions;
using Suporte.MsgBox;
using Suporte.MsgBox.Mvc;
using WebAppMvc.Areas.Administrativo.Models.DesapropriacaoIndividualStatusEventoTipo;
using WebAppMvc.Areas.Administrativo.Models.DesapropriacaoIndividualStatusEventoTipo.Filtro;
using WebAppMvc.Helpers;
using WebAppMvc.Models.Filter;
using System;

namespace WebAppMvc.Areas.Administrativo.Controllers
{
    public class DesapropriacaoIndividualStatusEventoTipoController : BaseController
    {
        #region | GET |

        [HttpGet]
        [HandleErrorAction]
        [RolesActionFilter(isModal = false)]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [NoCache]
        [HandleErrorAction]
        [RolesActionFilter(isModal = false)]
        public ActionResult Cadastrar()
        {
            return View();
        }

        [HttpGet]
        [NoCache]
        [HandleErrorAction]
        public ActionResult Editar(string args)
        {
            return OBTER(args);
        }

        [HttpGet]
        [NoCache]
        [HandleErrorAction]
        [RolesActionFilter(isModal = false)]
        public ActionResult Consultar(string args)
        {
            return string.IsNullOrEmpty(args) ? View(new model_desapropriacao_individual_status_evento_tipo_filtro()) : View(RetornarPesquisa(args));
        }

        #endregion

        #region | POST |

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Cadastrar(model_desapropriacao_individual_status_evento_tipo obj)
        {
            return SALVAR(obj);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Editar(model_desapropriacao_individual_status_evento_tipo obj, string args)
        {
            return ALTERAR(obj, args);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Consultar(tab_desapropriacao_individual_status_evento_tipo_filtro obj)
        {
            return PESQUISAR(obj);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Status(int id, bool status)
        {
            return STATUS(id, status, nomeloginUsuario);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Deletar(int id)
        {
            return DELETAR(id, nomeloginUsuario);
        }

        #endregion

        #region METODOS

        private ActionResult SALVAR(model_desapropriacao_individual_status_evento_tipo model)
        {
            try
            {
                ValidaModel();

                ActionResult retorno = null;

                var obj = Mapper.Map<model_desapropriacao_individual_status_evento_tipo, tab_desapropriacao_individual_status_evento_tipo>(model);

                if (new tab_desapropriacao_individual_status_evento_tipo_bll().INSERT(obj) > 0)
                {
                    retorno = new bootStrapDialog().Show("Sucesso", "Cadastro realizado sucesso!", Url.Action("Index", "DesapropriacaoIndividualStatusEventoTipo", new { area = "Administrativo" }), bootStrapMessageType.TYPE_SUCCESS, true);
                }

                return retorno;
            }
            catch (CustomException ex)
            {
                return new bootStrapDialog().Show(ex);
            }
        }

        private ActionResult ALTERAR(model_desapropriacao_individual_status_evento_tipo model, string queryString)
        {
            try
            {
                ValidaModel();

                ActionResult retorno = null;

                var obj = Mapper.Map<model_desapropriacao_individual_status_evento_tipo, tab_desapropriacao_individual_status_evento_tipo>(model);

                if (new tab_desapropriacao_individual_status_evento_tipo_bll().UPDATE(obj))
                {
                    retorno = new bootStrapDialog().Show("Sucesso", "Alteração realizada com sucesso!", Url.Action("Consultar", "DesapropriacaoIndividualStatusEventoTipo", new { area = "Administrativo", args = queryString }), bootStrapMessageType.TYPE_SUCCESS, false);
                }

                return retorno;
            }
            catch (CustomException ex)
            {
                return new bootStrapDialog().Show(ex);
            }
        }

        private ActionResult OBTER(string queryString)
        {
            try
            {
                var args = new EncryptQueryString(queryString);

                if (!string.IsNullOrEmpty(args["set_id"]))
                {
                    var model = Mapper.Map<tab_desapropriacao_individual_status_evento_tipo, model_desapropriacao_individual_status_evento_tipo>(new tab_desapropriacao_individual_status_evento_tipo_bll().FIND(int.Parse(args["set_id"])));

                    return View(model);
                }
                else
                {
                    return View("Consultar", RetornarPesquisa(queryString));
                }
            }
            catch (CustomException ex)
            {
                return new bootStrapDialog().Show(ex);
            }
        }

        private ActionResult STATUS(int id, bool status, string userLastUpdate)
        {
            try
            {
                ActionResult retorno = null;

                if (new tab_desapropriacao_individual_status_evento_tipo_bll().STATUS(id, status, userLastUpdate))
                {
                    retorno = new bootStrapDialog().Show("Sucesso", "Tipo de evento " + (status ? "ativado" : "desativado") + " com sucesso!", bootStrapMessageType.TYPE_SUCCESS);
                }

                return retorno;
            }
            catch (CustomException ex)
            {
                return new bootStrapDialog().Show(ex);
            }
        }

        private ActionResult DELETAR(int id, string userLastUpdate)
        {
            try
            {
                ActionResult retorno = null;

                if (new tab_desapropriacao_individual_status_evento_tipo_bll().DELETE(id, userLastUpdate))
                {
                    retorno = new bootStrapDialog().Show("Sucesso", "Tipo de evento deletado com sucesso!", bootStrapMessageType.TYPE_SUCCESS);
                }

                return retorno;
            }
            catch (CustomException ex)
            {
                return new bootStrapDialog().Show(ex);
            }
        }

        public ActionResult PESQUISAR(tab_desapropriacao_individual_status_evento_tipo_filtro obj)
        {
            try
            {
                var list = new tab_desapropriacao_individual_status_evento_tipo_bll().PESQUISA_GDV(obj).Select(t => new
                {
                    t.set_id,
                    t.set_nome,
                    t.set_valor,
                    set_data = t.set_data == null ? string.Empty : t.set_data.Value.ToString("d"),
                    t.set_ativo,
                    set_cadastro_dt = t.set_cadastro_dt.ToString("d"),
                    args = GerarQueryString(t.set_id, obj).ToString()
                });

                return Json(new { data = list, recordsFiltered = obj.pageCount });
            }
            catch (CustomException ex)
            {
                return new bootStrapDialog().Show(ex);
            }
        }

        private void ValidaModel()
        {
            if (!ModelState.IsValid)
                throw new CustomException("Preencha os campos obrigatorios!", bootStrapMessageType.TYPE_WARNING);
        }

        private model_desapropriacao_individual_status_evento_tipo_filtro RetornarPesquisa(string requestQueryString)
        {
            var args = new EncryptQueryString(requestQueryString);

            return new model_desapropriacao_individual_status_evento_tipo_filtro
            {
                set_nome = !string.IsNullOrEmpty(args["set_nome"]) ? args["set_nome"] : null,
                set_data = !string.IsNullOrEmpty(args["set_data"]) ? DateTime.Parse(args["set_data"]) : (DateTime?)null,
                set_ativo = !string.IsNullOrEmpty(args["set_ativo"]) ? bool.Parse(args["set_ativo"]) : (bool?)null,
                pageIndex = int.Parse(args["pageIndex"]),
                pageSize = int.Parse(args["pageSize"]),
            };
        }

        private EncryptQueryString GerarQueryString(int set_id, tab_desapropriacao_individual_status_evento_tipo_filtro obj)
        {
            return new EncryptQueryString
            {
                { "set_id",   set_id.ToString() },
                { "set_nome", string.IsNullOrEmpty(obj.set_nome) ? "" : obj.set_nome },
                { "set_data", obj.set_data == null ? string.Empty : obj.set_data.ToString() },
                { "set_valor", obj.set_valor == null ? string.Empty : obj.set_valor.ToString() },
                { "set_ativo",  obj.set_ativo == null ? string.Empty : obj.set_ativo.ToString() },
                { "pageIndex", obj.pageIndex.ToString() },
                { "pageSize",   obj.pageSize.ToString() }
            };
        }

        #endregion
    }
}
