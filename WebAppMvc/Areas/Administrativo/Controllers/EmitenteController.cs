﻿using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Business.SCDES;
using Entity.SCDES.Models;
using Entity.SCDES.Utils.Filtro;
using Suporte.Criptografia;
using Suporte.Exceptions;
using Suporte.Formatacao;
using Suporte.MsgBox;
using Suporte.MsgBox.Mvc;
using WebAppMvc.Helpers;
using WebAppMvc.Models.Filter;
using WebAppMvc.Areas.Administrativo.Models.Emitente;
using WebAppMvc.Areas.Administrativo.Models.Emitente.Filtro;

namespace WebAppMvc.Areas.Administrativo.Controllers
{
    public class EmitenteController : BaseController
    {
        #region | GET |

        [HttpGet]
        [HandleErrorAction]
        [RolesActionFilter(isModal = false)]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [NoCache]
        [HandleErrorAction]
        [RolesActionFilter(isModal = false)]
        public ActionResult Cadastrar()
        {
            return View();
        }

        [HttpGet]
        [NoCache]
        [HandleErrorAction]
        public ActionResult Editar(string args)
        {
            return OBTER(args);
        }

        [HttpGet]
        [NoCache]
        [HandleErrorAction]
        [RolesActionFilter(isModal = false)]
        public ActionResult Consultar(string args)
        {
            return string.IsNullOrEmpty(args) ? View(new model_emitente_filtro()) : View(RetornarPesquisa(args));
        }

        #endregion

        #region | POST |

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Cadastrar(model_emitente obj)
        {
            return SALVAR(obj);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Editar(model_emitente obj, string args)
        {
            return ALTERAR(obj, args);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Consultar(tab_emitente_filtro obj)
        {
            return PESQUISAR(obj);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Status(int id, bool status)
        {
            return STATUS(id, status, nomeloginUsuario);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Deletar(int id)
        {
            return DELETAR(id, nomeloginUsuario);
        }

        #endregion

        #region METODOS

        private ActionResult SALVAR(model_emitente model)
        {
            try
            {
                ValidaModel();

                ActionResult retorno = null;

                var obj = Mapper.Map<model_emitente, tab_emitente>(model);

                if (new tab_emitente_bll().INSERT(obj) > 0)
                {
                    retorno = new bootStrapDialog().Show("Sucesso", "Cadastro realizado sucesso!", Url.Action("Index", "Emitente", new { area = "Administrativo" }), bootStrapMessageType.TYPE_SUCCESS, true);
                }

                return retorno;
            }
            catch (CustomException ex)
            {
                return new bootStrapDialog().Show(ex);
            }
        }

        private ActionResult ALTERAR(model_emitente model, string args)
        {
            try
            {
                ValidaModel();

                ActionResult retorno = null;

                var obj = Mapper.Map<model_emitente, tab_emitente>(model);

                if (new tab_emitente_bll().UPDATE(obj))
                {
                    retorno = new bootStrapDialog().Show("Sucesso", "Alteração realizada com sucesso!", Url.Action("Consultar", "Emitente", new { area = "Administrativo", args = args }), bootStrapMessageType.TYPE_SUCCESS, false);
                }

                return retorno;
            }
            catch (CustomException ex)
            {
                return new bootStrapDialog().Show(ex);
            }
        }

        private ActionResult OBTER(string queryString)
        {
            try
            {
                var args = new EncryptQueryString(queryString);

                if (!string.IsNullOrEmpty(args["emi_id"]))
                {
                    var model = Mapper.Map<tab_emitente, model_emitente>(new tab_emitente_bll().FIND(int.Parse(args["emi_id"])));

                    return View(model);
                }
                else
                {
                    return View("Consultar", RetornarPesquisa(queryString));
                }
            }
            catch (CustomException ex)
            {
                return new bootStrapDialog().Show(ex);
            }
        }

        private ActionResult STATUS(int id, bool status, string userLastUpdate)
        {
            try
            {
                ActionResult retorno = null;

                if (new tab_emitente_bll().STATUS(id, status, userLastUpdate))
                {
                    retorno = new bootStrapDialog().Show("Sucesso", "Emitente " + (status ? "ativado" : "desativado") + " com sucesso!", bootStrapMessageType.TYPE_SUCCESS);
                }

                return retorno;
            }
            catch (CustomException ex)
            {
                return new bootStrapDialog().Show(ex);
            }
        }

        private ActionResult DELETAR(int id, string userLastUpdate)
        {
            try
            {
                ActionResult retorno = null;

                if (new tab_emitente_bll().DELETE(id, userLastUpdate))
                {
                    retorno = new bootStrapDialog().Show("Sucesso", "Emitente deletado com sucesso!", bootStrapMessageType.TYPE_SUCCESS);
                }

                return retorno;
            }
            catch (CustomException ex)
            {
                return new bootStrapDialog().Show(ex);
            }
        }

        public ActionResult PESQUISAR(tab_emitente_filtro obj)
        {
            try
            {
                var list = new tab_emitente_bll().PESQUISA_GDV(obj).Select(t => new
                {
                    t.emi_id,
                    t.emi_nome,
                    t.emi_email,
                    t.emi_telefone,
                    t.emi_celular,
                    t.emi_ativo,
                    args = GerarQueryString(t.emi_id, obj).ToString()
                });

                return Json(new { data = list, recordsFiltered = obj.pageCount });
            }
            catch (CustomException ex)
            {
                return new bootStrapDialog().Show(ex);
            }
        }


        private void ValidaModel()
        {
            if (!ModelState.IsValid)
                throw new CustomException("Preencha os campos obrigatorios!", bootStrapMessageType.TYPE_WARNING);
        }

        private model_emitente_filtro RetornarPesquisa(string requestQueryString)
        {
            var args = new EncryptQueryString(requestQueryString);

            return new model_emitente_filtro
            {
                emi_nome = !string.IsNullOrEmpty(args["emi_nome"]) ? args["emi_nome"] : null,
                emi_ativo = !string.IsNullOrEmpty(args["emi_ativo"]) ? bool.Parse(args["emi_ativo"]) : (bool?)null,
                pageIndex = int.Parse(args["pageIndex"]),
                pageSize = int.Parse(args["pageSize"]),
            };
        }

        private EncryptQueryString GerarQueryString(int emi_id, tab_emitente_filtro obj)
        {
            return new EncryptQueryString
            {
                { "emi_id",   emi_id.ToString()  }, 
                { "emi_nome", string.IsNullOrEmpty(obj.emi_nome) ? "" : obj.emi_nome  }, 
                { "emi_ativo",  obj.emi_ativo == null ? string.Empty : obj.emi_ativo.ToString()},
                { "pageIndex", obj.pageIndex.ToString()},
                { "pageSize",   obj.pageSize.ToString()}
            };
        }

        #endregion
    }
}
