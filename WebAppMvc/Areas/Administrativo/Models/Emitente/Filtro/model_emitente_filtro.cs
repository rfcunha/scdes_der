using System.ComponentModel.DataAnnotations;

namespace WebAppMvc.Areas.Administrativo.Models.Emitente.Filtro
{
    public class model_emitente_filtro : PesquisaModelDefault
    {
        [Display(Name = "Nome: ")]
        [StringLength(150, ErrorMessage = "O tamanho m�ximo s�o 150 caracteres.")]
        public string emi_nome { get; set; }
        
        [Display(Name = "Status: ")]
        public bool? emi_ativo { get; set; }
    }
}
