using System;
using System.ComponentModel.DataAnnotations;

namespace WebAppMvc.Areas.Administrativo.Models.Emitente
{
    public class model_emitente
    {
        public int emi_id { get; set; }

        [Display(Name = "Nome: ")]
        [StringLength(150, ErrorMessage = "O tamanho m�ximo s�o 150 caracteres.")]
        [Required(ErrorMessage = "- digite o nome.")]
        public string emi_nome { get; set; }

        [Display(Name = "Email: ")]
        [StringLength(150, ErrorMessage = "O tamanho m�ximo s�o 150 caracteres.")]
        public string emi_email { get; set; }

        [Display(Name = "Telefone: ")]
        public string emi_telefone { get; set; }

        [Display(Name = "Celular: ")]
        public string emi_celular { get; set; }

        [Display(Name = "Observa��o: ")]
        [StringLength(500, ErrorMessage = "O tamanho m�ximo s�o 500 caracteres.")]
        public string emi_observacao { get; set; }

        [Display(Name = "Status: ")]
        public bool emi_ativo { get; set; }

        public DateTime ipj_cadastro_dt { get; set; }

        public string userCreate { get; set; }
        public Nullable<DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<DateTime> dtDelete { get; set; }
    }
} 
