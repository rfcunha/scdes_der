using System;
using System.ComponentModel.DataAnnotations;

namespace WebAppMvc.Areas.Administrativo.Models.DesapropriacaoIndividualAssistenteTecnico
{
    public class model_desapropriacao_individual_assistente_tecnico
    {
        public int iat_id { get; set; }

        [Display(Name = "Nome: ")]
        [StringLength(150, ErrorMessage = "O tamanho m�ximo s�o 150 caracteres.")]
        [Required(ErrorMessage = "- digite o nome.")]
        public string iat_nome { get; set; }

        [Display(Name = "Email: ")]
        [StringLength(150, ErrorMessage = "O tamanho m�ximo s�o 150 caracteres.")]
        public string iat_email { get; set; }

        [Display(Name = "Telefone: ")]
        public string iat_telefone { get; set; }

        [Display(Name = "Celular: ")]
        public string iat_celular { get; set; }

        [Display(Name = "Observa��o: ")]
        [StringLength(500, ErrorMessage = "O tamanho m�ximo s�o 500 caracteres.")]
        public string iat_observacao { get; set; }

        [Display(Name = "Status: ")]
        public bool iat_ativo { get; set; }
        
        public DateTime iat_cadastro_dt { get; set; }
        
        public string userCreate { get; set; }
        public Nullable<DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<DateTime> dtDelete { get; set; }
    }
} 
