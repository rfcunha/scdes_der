using System.ComponentModel.DataAnnotations;

namespace WebAppMvc.Areas.Administrativo.Models.DesapropriacaoIndividualAssistenteTecnico.Filtro
{
    public class model_desapropriacao_individual_assistente_tecnico_filtro : PesquisaModelDefault
    {
        [Display(Name = "Nome: ")]
        [StringLength(150, ErrorMessage = "O tamanho m�ximo s�o 150 caracteres.")]
        public string iat_nome { get; set; }
        
        [Display(Name = "Status: ")]
        public bool? iat_ativo { get; set; }
    }
}
