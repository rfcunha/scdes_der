using System;
using System.ComponentModel.DataAnnotations;

namespace WebAppMvc.Areas.Administrativo.Models.DesapropriacaoIndividualStatusEventoTipo.Filtro
{
    public class model_desapropriacao_individual_status_evento_tipo_filtro : PesquisaModelDefault
    {
        [Display(Name = "Nome: ")]
        [StringLength(50, ErrorMessage = "O tamanho m�ximo s�o 50 caracteres.")]
        public string set_nome { get; set; }

        [Display(Name = "Possui Valor: ")]
        public bool? set_valor { get; set; }

        [Display(Name = "Status: ")]
        public bool? set_ativo { get; set; }

        [Display(Name = "Data do Evento: ")]
        public DateTime? set_data { get; set; }
    }
}
