using System;
using System.ComponentModel.DataAnnotations;

namespace WebAppMvc.Areas.Administrativo.Models.DesapropriacaoIndividualStatusEventoTipo
{
    public class model_desapropriacao_individual_status_evento_tipo
    {
        public int set_id { get; set; }

        [Display(Name = "Nome: ")]
        [StringLength(50, ErrorMessage = "O tamanho m�ximo s�o 50 caracteres.")]
        [Required(ErrorMessage = "- digite o nome.")]
        public string set_nome { get; set; }

        [Display(Name = "Possui Valor: ")]
        [Required(ErrorMessage = "- selecione se possui valor.")]
        public bool set_valor { get; set; }

        [Display(Name = "Status: ")]
        public bool set_ativo { get; set; }

        [Display(Name = "Data do Evento: ")]
        public DateTime? set_data { get; set; }

        public DateTime set_cadastro_dt { get; set; }
        
        public string userCreate { get; set; }
        public Nullable<DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<DateTime> dtDelete { get; set; }

    }
} 
