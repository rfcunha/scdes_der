using System.ComponentModel.DataAnnotations;

namespace WebAppMvc.Areas.Administrativo.Models.DesapropriacaoProjetoStatus.Filtro
{
    public class model_desapropriacao_status_filtro:PesquisaModelDefault
    {
        [Display(Name = "Nome: ")]
        [StringLength(50, ErrorMessage = "O tamanho m�ximo s�o 50 caracteres.")]
        public string dst_nome { get; set; }

        [Display(Name = "Status: ")]
        public bool? dst_ativo { get; set; }

    }
}
