using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Entity.SCDES.Models;

namespace WebAppMvc.Areas.Administrativo.Models.DesapropriacaoProjetoStatus
{
    public class model_desapropriacao_status
    {

        [Key]
        public int dst_id { get; set; }

        [Display(Name = "Nome: ")]
        [StringLength(50, ErrorMessage = "O tamanho m�ximo s�o 50 caracteres.")]
        [Required(ErrorMessage = "- digite o nome.")]
        public string dst_nome { get; set; }

        [Display(Name = "Status: ")]
        public bool dst_ativo { get; set; }
        public DateTime dst_cadastro_dt { get; set; }
        
        public string userCreate { get; set; }
        public Nullable<System.DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<System.DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<System.DateTime> dtDelete { get; set; }
        public virtual ICollection<tab_desapropriacao> tab_desapropriacao { get; set; }
    }
}
