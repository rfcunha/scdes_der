﻿using System.ComponentModel.DataAnnotations;

namespace WebAppMvc.Areas.Administrativo.Models.DesapropriacaoIndividualPredominanteEntorno.Filtro
{
    public class model_desapropriacao_individual_predominante_entorno_filtro : PesquisaModelDefault
    {
        [Display(Name = "Nome: ")]
        [StringLength(100, ErrorMessage = "O tamanho máximo são 100 caracteres.")]
        public string ipe_nome { get; set; }

        [Display(Name = "Status: ")]
        public bool? ipe_ativo { get; set; }
    }
}