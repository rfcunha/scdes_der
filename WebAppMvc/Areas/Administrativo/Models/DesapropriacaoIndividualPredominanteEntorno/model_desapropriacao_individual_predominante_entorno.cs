using System;
using System.ComponentModel.DataAnnotations;

namespace WebAppMvc.Areas.Administrativo.Models.DesapropriacaoIndividualPredominanteEntorno
{
    public class model_desapropriacao_individual_predominante_entorno
    {
        [Key]
        public int ipe_id { get; set; }

        [Display(Name = "Nome: ")]
        [StringLength(100, ErrorMessage = "O tamanho m�ximo s�o 100 caracteres.")]
        [Required(ErrorMessage = "- digite o nome.")]
        public string ipe_nome { get; set; }

        public bool ipe_ativo { get; set; }
        public System.DateTime ipe_cadastro_dt { get; set; }
        
        public string userCreate { get; set; }
        public Nullable<System.DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<System.DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<System.DateTime> dtDelete { get; set; }
    }
}
