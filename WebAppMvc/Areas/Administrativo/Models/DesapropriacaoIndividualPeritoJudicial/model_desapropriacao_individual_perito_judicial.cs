using System;
using System.ComponentModel.DataAnnotations;

namespace WebAppMvc.Areas.Administrativo.Models.DesapropriacaoIndividualPeritoJudicial
{
    public class model_desapropriacao_individual_perito_judicial
    {
        public int ipj_id { get; set; }

        [Display(Name = "Nome: ")]
        [StringLength(150, ErrorMessage = "O tamanho m�ximo s�o 150 caracteres.")]
        [Required(ErrorMessage = "- digite o nome.")]
        public string ipj_nome { get; set; }

        [Display(Name = "Email: ")]
        [StringLength(150, ErrorMessage = "O tamanho m�ximo s�o 150 caracteres.")]
        public string ipj_email { get; set; }

        [Display(Name = "Telefone: ")]
        public string ipj_telefone { get; set; }

        [Display(Name = "Celular: ")]
        public string ipj_celular { get; set; }

        [Display(Name = "Observa��o: ")]
        [StringLength(500, ErrorMessage = "O tamanho m�ximo s�o 500 caracteres.")]
        public string ipj_observacao { get; set; }

        [Display(Name = "Status: ")]
        public bool ipj_ativo { get; set; }

        public DateTime ipj_cadastro_dt { get; set; }
        
        public string userCreate { get; set; }
        public Nullable<DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<DateTime> dtDelete { get; set; }
    }
} 
