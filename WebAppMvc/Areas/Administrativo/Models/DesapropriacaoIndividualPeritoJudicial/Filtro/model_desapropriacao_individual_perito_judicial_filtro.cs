using System.ComponentModel.DataAnnotations;

namespace WebAppMvc.Areas.Administrativo.Models.DesapropriacaoIndividualPeritoJudicial.Filtro
{
    public class model_desapropriacao_individual_perito_judicial_filtro : PesquisaModelDefault
    {
        [Display(Name = "Nome: ")]
        [StringLength(150, ErrorMessage = "O tamanho m�ximo s�o 150 caracteres.")]
        public string ipj_nome { get; set; }
        
        [Display(Name = "Status: ")]
        public bool? ipj_ativo { get; set; }
    }
}
