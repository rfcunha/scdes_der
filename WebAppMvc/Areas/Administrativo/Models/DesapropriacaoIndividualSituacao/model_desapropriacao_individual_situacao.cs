using System;
using System.ComponentModel.DataAnnotations;

namespace WebAppMvc.Areas.Administrativo.Models.DesapropriacaoIndividualSituacao
{
    public class model_desapropriacao_individual_situacao
    {
        public int isi_id { get; set; }

        [Display(Name = "Nome: ")]
        [StringLength(50, ErrorMessage = "O tamanho m�ximo s�o 50 caracteres.")]
        [Required(ErrorMessage = "- digite o nome.")]
        public string isi_nome { get; set; }

        [Display(Name = "Status: ")]
        public bool isi_ativo { get; set; }
        
        public DateTime isi_cadastro_dt { get; set; }
        
        public string userCreate { get; set; }
        public Nullable<DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<DateTime> dtDelete { get; set; }
    }
} 
