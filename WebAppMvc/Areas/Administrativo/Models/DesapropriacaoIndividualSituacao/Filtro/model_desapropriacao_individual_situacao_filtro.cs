using System.ComponentModel.DataAnnotations;

namespace WebAppMvc.Areas.Administrativo.Models.DesapropriacaoIndividualSituacao.Filtro
{
    public class model_desapropriacao_individual_situacao_filtro : PesquisaModelDefault
    {
        [Display(Name = "Nome: ")]
        [StringLength(50, ErrorMessage = "O tamanho m�ximo s�o 50 caracteres.")]
        public string isi_nome { get; set; }
        
        [Display(Name = "Status: ")]
        public bool? isi_ativo { get; set; }
    }
}
