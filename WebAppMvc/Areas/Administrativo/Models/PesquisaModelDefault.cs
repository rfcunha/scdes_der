﻿namespace WebAppMvc.Areas.Administrativo.Models
{
    public class PesquisaModelDefault
    {
        public PesquisaModelDefault()
        {
            pageIndex = 1;
            pageSize = 10;
        }

        public int pageIndex { get; set; }
        public int pageSize { get; set; }
        public int pageCount { get; set; }
    }
}