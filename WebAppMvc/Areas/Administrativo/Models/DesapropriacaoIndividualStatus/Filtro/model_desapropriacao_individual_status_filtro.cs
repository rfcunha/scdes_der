using System.ComponentModel.DataAnnotations;

namespace WebAppMvc.Areas.Administrativo.Models.DesapropriacaoIndividualStatus.Filtro
{
    public class model_desapropriacao_individual_status_filtro : PesquisaModelDefault
    {
        [Display(Name = "Nome: ")]
        [StringLength(50, ErrorMessage = "O tamanho m�ximo s�o 50 caracteres.")]
        public string ist_nome { get; set; }
        
        [Display(Name = "Status: ")]
        public bool? ist_ativo { get; set; }
    }
}
