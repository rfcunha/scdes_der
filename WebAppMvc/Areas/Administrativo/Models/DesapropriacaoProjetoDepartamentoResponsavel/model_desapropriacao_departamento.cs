﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebAppMvc.Areas.Administrativo.Models.DesapropriacaoProjetoDepartamentoResponsavel
{
    public class model_desapropriacao_departamento
    {
        [Key]
        public int dde_id { get; set; }

        [Display(Name = "Nome: ")]
        [StringLength(100, ErrorMessage = "O tamanho máximo são 100 caracteres.")]
        [Required(ErrorMessage = "- digite o nome.")]
        public string dde_nome { get; set; }
        public bool dde_ativo { get; set; }
        public DateTime dde_cadastro_dt { get; set; }
        
        public string userCreate { get; set; }
        public Nullable<System.DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<System.DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<System.DateTime> dtDelete { get; set; }
    }
}