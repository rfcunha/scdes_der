﻿using System.ComponentModel.DataAnnotations;

namespace WebAppMvc.Areas.Administrativo.Models.DesapropriacaoProjetoDepartamentoResponsavel.Filtro
{
    public class model_desapropriacao_departamento_filtro : PesquisaModelDefault
    {
        [Display(Name = "Nome: ")]
        [StringLength(100, ErrorMessage = "O tamanho máximo são 100 caracteres.")]
        public string dde_nome { get; set; }

        [Display(Name = "Status: ")]
        public bool? dde_ativo { get; set; }
    }
}