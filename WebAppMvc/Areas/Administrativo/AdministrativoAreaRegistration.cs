﻿using System.Web.Mvc;

namespace WebAppMvc.Areas.Administrativo
{
    public class AdministrativoAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Administrativo";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "AdministrativoDefault",
                "Administrativo/{controller}/{action}/{id}", 
                new { id = UrlParameter.Optional }
                //new string[] { "WebAppMvc.Areas.Administrativo.Controllers" }
                );
            
        }
    }
}
