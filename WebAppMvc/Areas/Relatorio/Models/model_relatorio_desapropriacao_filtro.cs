﻿using System.ComponentModel.DataAnnotations;


namespace WebAppMvc.Areas.Relatorio.Models.Helpers
{
    public class model_relatorio_desapropriacao_filtro
    {
        [Display(Name = "Nº Auto do Decreto")]
        public string dpc_empreendimento_auto_decreto { get; set; }

        [Display(Name = "Lote")]
        public short? dpc_empreendimento_lote { get; set; }
    }
}