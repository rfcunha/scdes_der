﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebAppMvc.Helpers;
using WebAppMvc.Models.Filter;

namespace WebAppMvc.Areas.Relatorio.Controllers
{
    public class RelatorioDesapropriacaoController : BaseController
    {
        [HttpGet]
        [HandleErrorAction]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Gerar()
        {
            return Redirect("~/../../Areas/Relatorio/WebForms/RelatorioDesapropriacao.aspx");
        }
    }
}
