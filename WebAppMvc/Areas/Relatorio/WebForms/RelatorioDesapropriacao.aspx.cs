﻿using Business.DER;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAppMvc.Areas.Relatorio.WebForms
{
    public partial class RelatorioDesapropriacao1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                string numeroAutoDecreto = Request.QueryString["numeroAutoDecreto"];

                string stringLote = Request.QueryString["lote"]; short tempLote;
                short? lote = short.TryParse(Request.QueryString["lote"], out tempLote) ? short.Parse(Request.QueryString["lote"]) : (short?)null;

                var ds = new report_bll().RelatorioDesaprioriacao(numeroAutoDecreto, lote);

                ReportViewer.ProcessingMode = ProcessingMode.Local;
                ReportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"\Areas\Relatorio\Report\RelatorioDesapropriacao.rdlc";

                List<ReportParameter> parameters = new List<ReportParameter>();
                parameters.Add(new ReportParameter("NumeroAutoDecreto", numeroAutoDecreto));
                parameters.Add(new ReportParameter("Lote", stringLote));

                ReportViewer.LocalReport.SetParameters(parameters);
                ReportViewer.LocalReport.DataSources.Add(new ReportDataSource("DataSetRelatorioDesapropriacao", ds.Tables[0]));

                ReportViewer.SizeToReportContent = true;
                ReportViewer.Width = Unit.Percentage(100);
                ReportViewer.Height = Unit.Percentage(100);

                ReportViewer.LocalReport.Refresh();
            }
        }
    }
}