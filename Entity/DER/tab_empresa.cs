using System;
using System.Collections.Generic;
using Entity.SCDES.Models;

namespace Entity.DER
{
    public partial class tab_empresa
    {
        public tab_empresa()
        {
            this.tab_desapropriacao_individual = new List<tab_desapropriacao_individual>();
            this.tab_empresa_usuario = new List<tab_empresa_usuario>();
        }

        public int emp_id { get; set; }
        public string emp_razao_social { get; set; }
        public string emp_nome_fantasia { get; set; }
        public string emp_cnpj { get; set; }
        public bool emp_ativo { get; set; }
        public System.DateTime emp_dt_cadastro { get; set; }
        public bool emp_deletado { get; set; }
        public string userCreate { get; set; }
        public Nullable<System.DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<System.DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<System.DateTime> dtDelete { get; set; }
        public virtual ICollection<tab_desapropriacao_individual> tab_desapropriacao_individual { get; set; }
        public virtual ICollection<tab_empresa_usuario> tab_empresa_usuario { get; set; }
    }
}
