using System.Collections.Generic;

namespace Entity.DER
{
    public partial class tab_estado
    {
        public tab_estado()
        {
            this.tab_municipio = new List<tab_municipio>();
        }

        public int est_id { get; set; }
        public int est_codigo_ibge { get; set; }
        public string est_nome { get; set; }
        public string est_uf { get; set; }
        public string est_regiao { get; set; }
        public virtual ICollection<tab_municipio> tab_municipio { get; set; }
    }
}
