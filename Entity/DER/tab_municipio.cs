using Entity.SCDES.Models;

namespace Entity.DER
{
    public partial class tab_municipio
    {

        public int mun_id { get; set; }
        public string mun_nome { get; set; }
        public string mun_codigo_ibge { get; set; }
        public string mun_uf { get; set; }
        public bool mun_ativo { get; set; }
        public System.DateTime mun_dt_cadastro { get; set; }
        public bool mun_deletado { get; set; }
        public int est_id { get; set; }
        public virtual tab_estado tab_estado { get; set; }

        public int dlo_id { get; set; }
        public virtual tab_desapropriacao_localidade tab_desapropriacao_localidade { get; set; }
    }
}
