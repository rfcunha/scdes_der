using System;
using System.Collections.Generic;
using Entity.SCDES.Models;

namespace Entity.DER
{
    public partial class tab_agente_financiador
    {
        public tab_agente_financiador()
        {
            this.tab_desapropriacao_recurso_fonte = new List<tab_desapropriacao_recurso_fonte>();
        }

        public int afi_id { get; set; }
        public string afi_nome { get; set; }
        public bool afi_ativo { get; set; }
        public DateTime afi_dt_cadastro { get; set; }
        public bool afi_deletado { get; set; }
        public string userCreate { get; set; }
        public Nullable<System.DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<System.DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<System.DateTime> dtDelete { get; set; }
        public virtual ICollection<tab_desapropriacao_recurso_fonte> tab_desapropriacao_recurso_fonte { get; set; }
    }
}
