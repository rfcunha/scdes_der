﻿using System;
using System.Collections.Generic;
using Entity.SCDES.Models;

namespace Entity.DER
{
    public class tab_contratos
    {
        public int con_id { get; set; }
        public string AnoContr { get; set; }
        public string CodOrgContr { get; set; }
        public string NumContr { get; set; }
        public string FlagContrFct { get; set; }
        public string FlagRepact { get; set; }
        public string NumObra { get; set; }
        public string CodMun { get; set; }
        public string CodRegional { get; set; }
        public string NumCFP { get; set; }
        public string CodSituacao { get; set; }
        public string DescrObjetoContr1 { get; set; }
        public string DescrObjetoContr2 { get; set; }
        public string DescrObjetoContr3 { get; set; }
        public string DescrObjetoContr4 { get; set; }
        public string DescrRazaoSocial { get; set; }
        public string NumCGC { get; set; }
        public DateTime? DataInicio { get; set; }
        public DateTime? DataFim { get; set; }
        public short? PrazoMes { get; set; }
        public short? PrazoDias { get; set; }
        public DateTime? DataTPUObra { get; set; }
        public decimal? ValObraPI { get; set; }
        public DateTime? DataI0 { get; set; }
        public decimal? ValContrOrigPI { get; set; }
        public decimal? ValContrPI { get; set; }
        public decimal? ValTamPI { get; set; }
        public string NumUltMed { get; set; }
        public decimal? ValMedPI { get; set; }
        public decimal? ValReajMed { get; set; }
        public decimal? ValMedTot { get; set; }
        public decimal? ValReajProj { get; set; }
        public DateTime? DataRefReaj { get; set; }
        public float? PorcFin { get; set; }
        public decimal? ValPagMed94 { get; set; }
        public decimal? ValPagMed98 { get; set; }
        public decimal? ValPagMedTot { get; set; }
        public decimal? ValPagCor94 { get; set; }
        public decimal? ValPagCor98 { get; set; }
        public decimal? ValPagCorTot { get; set; }
        public decimal? ValDifAcao { get; set; }
        public decimal? ValConsolCPA { get; set; }
        public decimal? ValDivida { get; set; }
        public decimal? ValDesconto { get; set; }
        public decimal? ValMedOrig { get; set; }
        public decimal? ValPagOrig { get; set; }
        public decimal? ValMedOrigSemCpa { get; set; }
        public decimal? ValPagOrigSemCpa { get; set; }
        public string IndCPA { get; set; }
        public string CodNaturObra { get; set; }
        public DateTime? DTPNS { get; set; }
        public int? Extensao { get; set; }
        public DateTime? DataAtualiz { get; set; }
        public DateTime? DataRecebeProv { get; set; }

    }

}
