﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entity.DER
{
    public class tab_rodovia
    {
         public string rod_sp{get;set;}
         public decimal rod_km_inicial{get;set;}
         public decimal rod_km_final{get;set;}
         public decimal rod_extensao{get;set;}
         public string rod_descricao_inicial{get;set;}
         public string rod_descricao_final{get;set;}
         public string rod_uf{get;set;}
         public string rod_municipio{get;set;}
         public string rod_jurisdicao{get;set;}
         public string rod_administracao{get;set;}
         public string rod_conservacao{get;set;}
         public string rod_denominacao{get;set;}
    }
}
