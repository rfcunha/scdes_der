using System;
using System.Collections.Generic;
using Entity.SCDES.Models;

namespace Entity.DER
{
    public partial class tab_topografia
    {
        public tab_topografia()
        {
            this.tab_desapropriacao_individual = new List<tab_desapropriacao_individual>();
        }

        public int top_id { get; set; }
        public string top_nome { get; set; }
        public bool top_ativo { get; set; }
        public System.DateTime top_dt_cadastro { get; set; }
        public bool top_deletado { get; set; }
        public string userCreate { get; set; }
        public Nullable<System.DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<System.DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<System.DateTime> dtDelete { get; set; }
        public virtual ICollection<tab_desapropriacao_individual> tab_desapropriacao_individual { get; set; }
    }
}
