﻿using System;
using System.Collections.Generic;
using Entity.SCDES.Models;

namespace Entity.DER
{
    public partial class tab_regional
    {
        public tab_regional()
        {
            this.tab_desapropriacao = new List<tab_desapropriacao>();
        }

        public int reg_id { get; set; }
        public string reg_nome { get; set; }
        public bool reg_ativo { get; set; }
        public DateTime reg_dt_cadastro { get; set; }
        public bool reg_deletado { get; set; }
        public virtual ICollection<tab_desapropriacao> tab_desapropriacao { get; set; }
    }
}
