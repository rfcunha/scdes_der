using Entity.Utils;

namespace Entity.DER.Utils
{
    public class tab_estado_filtro:DefaultFiltro
    {
        public int? est_codigo_ibge { get; set; }
        public string est_nome { get; set; }
        public string est_uf { get; set; }
        public string est_regiao { get; set; }
    }
}
