using Entity.Utils;

namespace Entity.DER.Utils.Filtro
{
    public class tab_agente_financiador_filtro:DefaultFiltro
    {
        public string afi_nome { get; set; }
        public bool? afi_ativo { get; set; }
    }
}
