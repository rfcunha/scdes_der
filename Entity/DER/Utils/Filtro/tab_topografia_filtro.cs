using Entity.Utils;

namespace Entity.DER.Utils.Filtro
{
    public class tab_topografia_filtro : DefaultFiltro
    {
        public string top_nome { get; set; }
        public bool? top_ativo { get; set; }
    }
}
