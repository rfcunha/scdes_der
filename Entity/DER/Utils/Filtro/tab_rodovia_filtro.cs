﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entity.DER.Utils.Filtro
{
    public class tab_rodovia_filtro
    {
        public string rod_sp { get; set; }
        public decimal? rod_km_inicial { get; set; }
        public decimal? rod_km_final { get; set; }
    }
}
