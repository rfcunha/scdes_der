using Entity.Utils;

namespace Entity.DER.Utils.Filtro
{
    public class tab_solo_condicao_filtro : DefaultFiltro
    {

        public string sco_nome { get; set; }
        public bool? sco_ativo { get; set; }

    }
}
