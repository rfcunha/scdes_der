using Entity.Utils;

namespace Entity.DER.Utils.Filtro
{
    public class tab_zoneamento_tipo_filtro:DefaultFiltro
    {
        public string zti_nome { get; set; }
        public bool? zti_ativo { get; set; }
    }
}
