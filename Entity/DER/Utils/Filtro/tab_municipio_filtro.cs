using Entity.Utils;

namespace Entity.DER.Utils
{
    public class tab_municipio_filtro:DefaultFiltro
    {
        public string mun_nome { get; set; }
        public string mun_codigo_ibge { get; set; }
        public string mun_uf { get; set; }
        public bool? mun_ativo { get; set; }
        public int? est_id { get; set; }
    }
}
