using Entity.Utils;

namespace Entity.DER.Utils.Filtro
{
    public class tab_empresa_filtro : DefaultFiltro
    {
        public string emp_razao_social { get; set; }
        public string emp_nome_fantasia { get; set; }
        public string emp_cnpj { get; set; }
        public bool? emp_ativo { get; set; }

    }
}
