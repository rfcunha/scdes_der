using Entity.Utils;

namespace Entity.DER.Utils.Filtro
{
    public class tab_comarca_filtro : DefaultFiltro
    {
        public string com_nome { get; set; }
        public bool? com_ativo { get; set; }
    }
}
