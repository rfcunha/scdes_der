using Entity.Utils;

namespace Entity.DER.Utils.Filtro
{
    public class tab_solo_uso_filtro : DefaultFiltro
    {
        public string sus_nome { get; set; }
        public bool? sus_ativo { get; set; }
    }
}
