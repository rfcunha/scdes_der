using System;

namespace Entity.GERENCIADORPORTAL
{
    public partial class tab_perfil_menu_role
    {
        public tab_perfil_menu_role()
        {
            tab_menu_role = new tab_menu_role();
            tab_perfil = new tab_perfil();
            acao = new Enumeradores.Acao();
        }

        public int pmr_id { get; set; }
        public DateTime pmr_dt_cadastro { get; set; }
        public int per_id { get; set; }
        public int mro_id { get; set; }
        public virtual tab_menu_role tab_menu_role { get; set; }
        public virtual tab_perfil tab_perfil { get; set; }
        public Enumeradores.Acao acao { get; set; } 
    }
}
