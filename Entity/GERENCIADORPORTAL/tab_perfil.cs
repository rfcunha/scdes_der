using System;
using System.Collections.Generic;

namespace Entity.GERENCIADORPORTAL
{
    public partial class tab_perfil
    {
        public tab_perfil()
        {
            this.tab_perfil_menu_role = new List<tab_perfil_menu_role>();
            this.tab_usuario_perfil_sistema = new List<tab_usuario_perfil_sistema>();
        }


        /// <summary>
        /// propriedade so para atualizar ou desativar ou remover perfil de um sistema
        /// </summary>
        public int sis_id { get; set; }


        public int per_id { get; set; }
        public string per_nome { get; set; }
        public bool per_ativo { get; set; }
        public DateTime per_dt_cadastro { get; set; }
        public bool per_deletado { get; set; }
        public string userCreate { get; set; }
        public DateTime? dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public DateTime? dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public DateTime? dtDelete { get; set; }
        public bool per_update_xml { get; set; }
        public virtual List<tab_perfil_menu_role> tab_perfil_menu_role { get; set; }
        public virtual List<tab_usuario_perfil_sistema> tab_usuario_perfil_sistema { get; set; }
    }
}
