using System;
using System.Collections.Generic;

namespace Entity.GERENCIADORPORTAL
{
    public partial class tab_usuario
    {
        public tab_usuario()
        {
            tab_usuario_perfil_sistema = new List<tab_usuario_perfil_sistema>();
        }

        public int usu_id { get; set; }
        public int pes_id { get; set; }
        public string usu_nome { get; set; }
        public string usu_senha { get; set; }
        public string usu_login { get; set; }
        public DateTime usu_dt_cadastro { get; set; }
        public bool usu_ativo { get; set; }
        public bool usu_deletado { get; set; }
        public virtual ICollection<tab_usuario_perfil_sistema> tab_usuario_perfil_sistema { get; set; }
        public string userCreate { get; set; }
        public DateTime? dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public DateTime? dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public DateTime? dtDelete { get; set; }
        public bool usu_trocar_senha { get; set; }
        public string usu_nome_login { get; set; }
        public byte[] usu_imagem { get; set; }
        public string usu_url_imagem { get; set; } 
        public virtual tab_perfil tab_perfil { get; set; }
    }
}
