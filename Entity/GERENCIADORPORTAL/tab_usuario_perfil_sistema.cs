using System;

namespace Entity.GERENCIADORPORTAL
{
    public partial class tab_usuario_perfil_sistema
    {
        public int ups_id { get; set; }
        public int sis_id { get; set; }
        public int usu_id { get; set; }
        public int per_id { get; set; }
        public DateTime ups_dt_cadastro { get; set; }
        public bool ups_ativo { get; set; }
        public bool ups_deletado { get; set; }
        public virtual tab_sistema tab_sistema { get; set; }
        public virtual tab_usuario tab_usuario { get; set; }
    }
}
