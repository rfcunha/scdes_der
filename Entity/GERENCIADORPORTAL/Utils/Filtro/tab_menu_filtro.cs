namespace Entity.GERENCIADORPORTAL.Utils.Filtro
{
    public partial class tab_menu_filtro
    { 
        public int men_id { get; set; }
        public string men_menu { get; set; } 
        public bool? men_ativo { get; set; }

        public int pageIndex { get; set; }
        public int pageSize { get; set; }
        public int pageCount { get; set; }
    }
}
