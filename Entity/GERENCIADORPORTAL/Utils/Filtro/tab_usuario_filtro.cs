namespace Entity.GERENCIADORPORTAL.Utils.Filtro
{
    public partial class tab_usuario_filtro
    { 
        public int usu_id { get; set; }
        public string usu_nome { get; set; }
        public string usu_senha { get; set; }
        public string usu_login { get; set; }
        public bool? usu_ativo { get; set; }
        public int? pes_id { get; set; }

        public int pageIndex { get; set; }
        public int pageSize { get; set; }
        public int pageCount { get; set; }
    }
}
