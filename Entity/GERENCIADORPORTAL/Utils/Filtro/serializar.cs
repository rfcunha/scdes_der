﻿using System;

namespace Entity.GERENCIADORPORTAL.Utils.Filtro
{
    [Serializable]
    public partial class tab_menu_filtro { }
    [Serializable]
    public partial class tab_menu_role_filtro { }
    [Serializable]
    public partial class tab_perfil_filtro { }
    [Serializable]
    public partial class tab_perfil_menu_role_filtro { }
    [Serializable]
    public partial class tab_sistema_filtro { }
    [Serializable]
    public partial class tab_usuario_filtro { }
    [Serializable]
    public partial class tab_usuario_perfil_sistema_filtro { }
}
