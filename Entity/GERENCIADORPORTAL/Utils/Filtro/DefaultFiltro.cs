﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entity.GERENCIADORPORTAL.Utils.Filtro
{
    public partial class DefaultFiltro
    { 

        public int pageIndex { get; set; }
        public int pageSize { get; set; }
        public int pageCount { get; set; }
    }
}
