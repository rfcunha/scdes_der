namespace Entity.GERENCIADORPORTAL.Utils.Filtro
{
    public partial class tab_menu_role_filtro
    { 
        public int? mro_id { get; set; }
        public string mro_nome { get; set; } 
        public bool? mro_ativo { get; set; }
        public int? men_id { get; set; }

        public int pageIndex { get; set; }
        public int pageSize { get; set; }
        public int pageCount { get; set; }
    }
}
