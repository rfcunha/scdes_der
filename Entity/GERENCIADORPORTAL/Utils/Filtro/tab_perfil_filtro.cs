namespace Entity.GERENCIADORPORTAL.Utils.Filtro
{
    public partial class tab_perfil_filtro
    { 
        public int per_id { get; set; }
        public string per_nome { get; set; }
        public bool? per_ativo { get; set; }

        public int pageIndex { get; set; }
        public int pageSize { get; set; }
        public int pageCount { get; set; }
    }
}
