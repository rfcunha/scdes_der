namespace Entity.GERENCIADORPORTAL.Utils.Filtro
{
    public partial class tab_perfil_menu_role_filtro
    {
        public int pmr_id { get; set; }
        public int per_id { get; set; }

        public int pageIndex { get; set; }
        public int pageSize { get; set; }
        public int pageCount { get; set; }
    }
}
