namespace Entity.GERENCIADORPORTAL.Utils.Filtro
{
    public partial class tab_usuario_perfil_sistema_filtro
    {
        public int? ups_id { get; set; }
        public int? sis_id { get; set; }
        public int? usu_id { get; set; }
        public int? per_id { get; set; }

        public int pageIndex { get; set; }
        public int pageSize { get; set; }
        public int pageCount { get; set; }
    }
}
