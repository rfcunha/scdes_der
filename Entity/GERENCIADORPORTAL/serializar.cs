﻿using System;

namespace Entity.GERENCIADORPORTAL
{
    [Serializable]
    public partial class tab_menu { }
    [Serializable]
    public partial class tab_menu_role { }
    [Serializable]
    public partial class tab_perfil { }
    [Serializable]
    public partial class tab_perfil_menu_role { }
    [Serializable]
    public partial class tab_sistema { }
    [Serializable]
    public partial class tab_usuario { }
    [Serializable]
    public partial class tab_usuario_perfil_sistema { }
}
