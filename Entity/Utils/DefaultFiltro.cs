﻿using System;

namespace Entity.Utils
{
    public class DefaultFiltro
    {
        protected DefaultFiltro()
        {
            pageSize = 10;
            pageIndex = 1;
        }

        public int pageSize { get; set; }
        public int pageIndex { get; set; }
        public int pageCount { get; set; }
        public DateTime? dt_cadastro_inicial { get; set; }
        public DateTime? dt_cadastro_final { get; set; }
    }
}
