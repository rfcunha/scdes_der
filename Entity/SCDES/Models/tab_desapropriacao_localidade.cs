using System;
using System.Collections.Generic;
using Entity.DER;

namespace Entity.SCDES.Models
{
    public partial class tab_desapropriacao_localidade
    {
        public int dlo_id { get; set; }
        public System.DateTime dlo_cadastro_dt { get; set; }
        public int des_id { get; set; }
        public int mun_id { get; set; }

        //public virtual tab_municipio tab_municipio { get; set; }

        public string dlo_complemento { get; set; }

        public virtual tab_desapropriacao tab_desapropriacao { get; set; }

        public string userCreate { get; set; }
        public Nullable<System.DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<System.DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<System.DateTime> dtDelete { get; set; }
        
        public dlo_acao dlo_acao { get; set; }
    }

    public enum dlo_acao
    {
        Insert = 1,
        Update = 2,
        Delete = 3
    }
}
