using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.AccessControl;

namespace Entity.SCDES.Models
{
    public partial class tab_desapropriacao_emitente
    {
        public int dee_id { get; set; }

        public System.DateTime dee_cadastro_dt { get; set; }

        public int emp_id { get; set; }

        public int des_id { get; set; }

        public string userCreate { get; set; }

        public Nullable<System.DateTime> dtCreate { get; set; }

        public string userLastUpdate { get; set; }

        public Nullable<System.DateTime> dtLastUpdate { get; set; }

        public string userDelete { get; set; }

        public Nullable<System.DateTime> dtDelete { get; set; }

        public virtual tab_desapropriacao tab_desapropriacao { get; set; }

        [NotMapped]
        public dee_acao dee_acao { get; set; }

    }

    public enum dee_acao
    {
        Insert = 1,
        Update = 2,
        Delete = 3
    }
}
