﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.SCDES.Models
{
    [Table("tab_anexo_identificador", Schema = "dbo")]
    public class tab_anexo_identificador
    {
        [Key]
        public int aid_id { get; set; }
        
        public string aid_tipo { get; set; }
        public string aid_guia { get; set; }
        public string aid_form { get; set; }

        [InverseProperty("tab_anexo_identificador")]
        public virtual List<tab_anexo> tab_anexo { get; set; }
    }
}
