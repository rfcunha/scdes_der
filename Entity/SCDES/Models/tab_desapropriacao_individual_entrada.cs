using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Entity.SCDES.Enum;

namespace Entity.SCDES.Models
{
    public partial class tab_desapropriacao_individual_entrada
    {
        public int ien_id { get; set; }
        public string ien_tipo { get; set; }
        public string ien_conservacao_estado { get; set; }
        public string ien_dimensao { get; set; }
        public string ien_observacao { get; set; }
        public decimal ien_valor_total { get; set; }
        public System.DateTime ien_cadastro_dt { get; set; }
        public int din_id { get; set; }
        public string userCreate { get; set; }
        public Nullable<System.DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<System.DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<System.DateTime> dtDelete { get; set; }
        public virtual tab_desapropriacao_individual tab_desapropriacao_individual { get; set; }

        public int? ien_anexo_identificador_id { get; set; }
        [ForeignKey("ien_anexo_identificador_id")]
        public tab_anexo_identificador tab_anexo_identificador { get; set; }

        [NotMapped]
        public acao ien_acao { get; set; }

    }
}
