using System;
using System.Collections.Generic;

namespace Entity.SCDES.Models
{
    public partial class tab_desapropriacao_individual_status_evento_tipo
    {
        public tab_desapropriacao_individual_status_evento_tipo()
        {
            this.tab_desapropriacao_individual_status_evento = new List<tab_desapropriacao_individual_status_evento>();
        }

        public int set_id { get; set; }
        public string set_nome { get; set; }
        public bool set_ativo { get; set; }
        public bool set_valor { get; set; }
        public DateTime? set_data { get; set; }
        public System.DateTime set_cadastro_dt { get; set; }
        public string userCreate { get; set; }
        public Nullable<System.DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<System.DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<System.DateTime> dtDelete { get; set; }
        public virtual ICollection<tab_desapropriacao_individual_status_evento> tab_desapropriacao_individual_status_evento { get; set; }
    }
}
