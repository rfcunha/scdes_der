using System;
using System.Collections.Generic;

namespace Entity.SCDES.Models
{
    public partial class tab_desapropriacao
    {
        public tab_desapropriacao()
        {
            this.tab_desapropriacao_emitente = new List<tab_desapropriacao_emitente>();
            this.tab_desapropriacao_empreendimento = new List<tab_desapropriacao_empreendimento>();
            this.tab_desapropriacao_localidade = new List<tab_desapropriacao_localidade>();
            this.tab_desapropriacao_observacao = new List<tab_desapropriacao_observacao>();
            this.tab_desapropriacao_recurso_fonte = new List<tab_desapropriacao_recurso_fonte>();
            this.tab_desapropriacao_historico = new List<tab_desapropriacao_historico>();
        }

        public int des_id { get; set; }
        public string des_projeto_codigo { get; set; }
        public string des_nome { get; set; }
        public int dde_id { get; set; }
        public string des_descricao { get; set; }
        public string des_objetivo { get; set; }
        public Nullable<System.DateTime> des_inicio { get; set; }
        public Nullable<System.DateTime> des_fim { get; set; }
        public Nullable<decimal> des_valor_estimado { get; set; }
        public int dst_id { get; set; }
        public System.DateTime des_cadastro_dt { get; set; }
        public int afi_id { get; set; }
        public int reg_id { get; set; }

        public string userCreate { get; set; }
        public Nullable<System.DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<System.DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<System.DateTime> dtDelete { get; set; }

        public virtual tab_desapropriacao_departamento tab_desapropriacao_departamento { get; set; }
        public virtual tab_desapropriacao_status tab_desapropriacao_status { get; set; }
        
        public virtual ICollection<tab_desapropriacao_emitente> tab_desapropriacao_emitente { get; set; }
        public virtual ICollection<tab_desapropriacao_empreendimento> tab_desapropriacao_empreendimento { get; set; }
        public virtual ICollection<tab_desapropriacao_localidade> tab_desapropriacao_localidade { get; set; }
        public virtual ICollection<tab_desapropriacao_observacao> tab_desapropriacao_observacao { get; set; }
        public virtual ICollection<tab_desapropriacao_recurso_fonte> tab_desapropriacao_recurso_fonte { get; set; }
        public virtual ICollection<tab_desapropriacao_historico> tab_desapropriacao_historico { get; set; }
    }
}
