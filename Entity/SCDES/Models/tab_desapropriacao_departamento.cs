using System;
using System.Collections.Generic;

namespace Entity.SCDES.Models
{
    public partial class tab_desapropriacao_departamento
    {
        public tab_desapropriacao_departamento()
        {
            this.tab_desapropriacao = new List<tab_desapropriacao>();
        }

        public int dde_id { get; set; }
        public string dde_nome { get; set; }
        public bool dde_ativo { get; set; }
        public DateTime dde_cadastro_dt { get; set; }
        
        public string userCreate { get; set; }
        public Nullable<System.DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<System.DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<System.DateTime> dtDelete { get; set; }
        public virtual ICollection<tab_desapropriacao> tab_desapropriacao { get; set; }
    }
}
