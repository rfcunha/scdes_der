using System;
using System.Collections.Generic;

namespace Entity.SCDES.Models
{
    public class tab_desapropriacao_status
    {

        public int dst_id { get; set; }
        public string dst_nome { get; set; }
        public bool dst_ativo { get; set; }
        public System.DateTime dst_cadastro_dt { get; set; }
        public string userCreate { get; set; }
        public Nullable<System.DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<System.DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<System.DateTime> dtDelete { get; set; }
        public virtual ICollection<tab_desapropriacao> tab_desapropriacao { get; set; }
    }
}
