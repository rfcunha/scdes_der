using System;
using System.Collections.Generic;

namespace Entity.SCDES.Models
{
    public partial class tab_desapropriacao_individual_pendencia
    {
        public int ipe_id { get; set; }
        public string ipe_descricao { get; set; }
        public string ipe_cadastro_usuario { get; set; }
        public System.DateTime ipe_cadastro_dt { get; set; }
        public short ipe_status { get; set; }
        public string ipe_concluido_usuario { get; set; }
        public Nullable<System.DateTime> ipe_concluido_dt { get; set; }
        public int din_id { get; set; }
        public int usu_id { get; set; }
        public string userCreate { get; set; }
        public Nullable<System.DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<System.DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<System.DateTime> dtDelete { get; set; }
        public virtual tab_desapropriacao_individual tab_desapropriacao_individual { get; set; }

        public ipe_acao ipe_acao { get; set; }
    }

    public enum ipe_acao
    {
        Insert = 1,
        Update = 2,
        Delete = 3
    }
}
