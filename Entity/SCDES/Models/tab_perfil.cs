using System;
using System.Collections.Generic;

namespace Entity.SCDES.Models
{
    public partial class tab_perfil
    {
        public tab_perfil()
        {
            this.tab_perfil_menu_role = new List<tab_perfil_menu_role>();
        }

        public int per_id { get; set; }
        public string per_nome { get; set; }
        public bool per_update_xml { get; set; }
        public bool per_ativo { get; set; }
        public System.DateTime per_dt_cadastro { get; set; }
        public bool per_deletado { get; set; }
        public string userCreate { get; set; }
        public Nullable<System.DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<System.DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<System.DateTime> dtDelete { get; set; }
        public virtual ICollection<tab_perfil_menu_role> tab_perfil_menu_role { get; set; }
    }
}
