using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Entity.SCDES.Enum;

namespace Entity.SCDES.Models
{
    public partial class tab_desapropriacao_individual_assistente_tecnico
    {
        public tab_desapropriacao_individual_assistente_tecnico()
        {
            this.tab_desapropriacao_individual = new List<tab_desapropriacao_individual>();
        }

        public int iat_id { get; set; }
        public string iat_nome { get; set; }
        public string iat_email { get; set; }
        public string iat_telefone { get; set; }
        public string iat_celular { get; set; }
        public string iat_observacao { get; set; }
        public bool iat_ativo { get; set; }
        public System.DateTime iat_cadastro_dt { get; set; }

        [NotMapped]
        public acao iat_acao { get; set; }

        public string userCreate { get; set; }
        public Nullable<System.DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<System.DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<System.DateTime> dtDelete { get; set; }
        public virtual ICollection<tab_desapropriacao_individual> tab_desapropriacao_individual { get; set; }
    }
}
