using System;
using System.Collections.Generic;

namespace Entity.SCDES.Models
{
    public partial class tab_desapropriacao_individual_situacao
    {
        public tab_desapropriacao_individual_situacao()
        {
            this.tab_desapropriacao_individual = new List<tab_desapropriacao_individual>();
        }

        public int isi_id { get; set; }
        public string isi_nome { get; set; }
        public bool isi_ativo { get; set; }
        public System.DateTime isi_cadastro_dt { get; set; }
        public string userCreate { get; set; }
        public Nullable<System.DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<System.DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<System.DateTime> dtDelete { get; set; }
        public virtual ICollection<tab_desapropriacao_individual> tab_desapropriacao_individual { get; set; }
    }
}
