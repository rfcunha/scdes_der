using System;
using System.ComponentModel.DataAnnotations.Schema;
using Entity.SCDES.Enum;

namespace Entity.SCDES.Models
{
    public partial class tab_desapropriacao_individual_status_evento
    {
        public int ise_id { get; set; }
        public string ise_data { get; set; }
        public Nullable<decimal> ise_valor { get; set; }
        public System.DateTime ise_cadastro_dt { get; set; }
        public int set_id { get; set; }
        public int din_id { get; set; }
        public string userCreate { get; set; }
        public Nullable<System.DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<System.DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<System.DateTime> dtDelete { get; set; }
        public virtual tab_desapropriacao_individual tab_desapropriacao_individual { get; set; }
        public virtual tab_desapropriacao_individual_status_evento_tipo tab_desapropriacao_individual_status_evento_tipo { get; set; }

        [NotMapped]
        public acao ise_acao { get; set; }
    }
}
