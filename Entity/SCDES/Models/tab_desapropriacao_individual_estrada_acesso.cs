using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Entity.SCDES.Enum;

namespace Entity.SCDES.Models
{
    public partial class tab_desapropriacao_individual_estrada_acesso
    {
        public int iea_id { get; set; }
        public string iea_tipo { get; set; }
        public string iea_conservacao_estado { get; set; }
        public string iea_estaca_inicial { get; set; }
        public string iea_dimensao { get; set; }
        public string iea_pavimento { get; set; }
        public string iea_observacao { get; set; }
        public decimal iea_valor_total { get; set; }
        public System.DateTime iea_cadastro_dt { get; set; }
        public int din_id { get; set; }
        public string userCreate { get; set; }
        public Nullable<System.DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<System.DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<System.DateTime> dtDelete { get; set; }
        public virtual tab_desapropriacao_individual tab_desapropriacao_individual { get; set; }

        public int? iea_anexo_identificador_id { get; set; }
        [ForeignKey("iea_anexo_identificador_id")]
        public tab_anexo_identificador tab_anexo_identificador { get; set; }

        [NotMapped]
        public acao iea_acao { get; set; }
    }
}
