using System;

namespace Entity.SCDES.Models
{
    public partial class tab_desapropriacao_observacao
    {
        public int dob_id { get; set; }
        public string dob_descricao { get; set; }
        public string dob_cadastro_usuario { get; set; }
        public DateTime dob_cadastro_dt { get; set; }
        public int des_id { get; set; }
        public int usu_id { get; set; }

        public string userCreate { get; set; }
        public DateTime? dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public DateTime? dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public DateTime? dtDelete { get; set; }

        public virtual tab_desapropriacao tab_desapropriacao { get; set; }

        public dob_acao dob_acao { get; set; }
    }

    public enum dob_acao
    {
        Insert = 1,
        Update = 2,
        Delete = 3
    }
}
