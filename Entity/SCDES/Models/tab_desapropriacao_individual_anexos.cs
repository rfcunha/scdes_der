using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Entity.SCDES.Enum;

namespace Entity.SCDES.Models
{
    public partial class tab_desapropriacao_individual_anexos
    {
        public int ian_id { get; set; }
        public string ian_anexo { get; set; }
        public string dia_anexo_nome { get; set; }
        public short ian_tipo { get; set; }
        public System.DateTime ian_cadastro_dt { get; set; }
        public int din_id { get; set; }

        [NotMapped]
        public acao ian_acao { get; set; }

        public int? aid_id { get; set; }
        [ForeignKey("aid_id")]
        public tab_anexo_identificador tab_anexo_identificador { get; set; }

        public string userCreate { get; set; }
        public Nullable<System.DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<System.DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<System.DateTime> dtDelete { get; set; }
        public virtual tab_desapropriacao_individual tab_desapropriacao_individual { get; set; }
    }

}
