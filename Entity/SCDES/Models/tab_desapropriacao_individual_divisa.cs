using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.AccessControl;
using Entity.SCDES.Enum;

namespace Entity.SCDES.Models
{
    public partial class tab_desapropriacao_individual_divisa
    {
        public int idi_id { get; set; }
        public string idi_tipo { get; set; }
        public string idi_descricao { get; set; }
        public string idi_quantidade { get; set; }
        public string idi_unidade { get; set; }
        public string idi_observacao { get; set; }
        public decimal idi_valor_total { get; set; }
        public System.DateTime idi_cadastro_dt { get; set; }

        public int? idi_anexo_identificador_id { get; set; }
        [ForeignKey("idi_anexo_identificador_id")]
        public virtual tab_anexo_identificador tab_anexo_identificador { get; set; }
        
        [NotMapped]
        public acao idi_acao { get; set; }

        public int din_id { get; set; }
        public string userCreate { get; set; }
        public Nullable<System.DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<System.DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<System.DateTime> dtDelete { get; set; }

        public virtual tab_desapropriacao_individual tab_desapropriacao_individual { get; set; }
    }
}
