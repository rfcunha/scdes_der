using System.Data.Entity.ModelConfiguration;

namespace Entity.SCDES.Models.Mapping
{
    public class tab_emitenteMap : EntityTypeConfiguration<tab_emitente>
    {
        public tab_emitenteMap()
        {
            // Primary Key
            this.HasKey(t => t.emi_id);

            // Properties
            this.Property(t => t.emi_nome)
                .IsRequired()
                .HasMaxLength(150);

            this.Property(t => t.emi_email)
                .HasMaxLength(150);

            this.Property(t => t.emi_telefone)
                .HasMaxLength(11);

            this.Property(t => t.emi_celular)
                .HasMaxLength(11);

            this.Property(t => t.userCreate)
                .HasMaxLength(150);

            this.Property(t => t.userLastUpdate)
                .HasMaxLength(150);

            this.Property(t => t.userDelete)
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("tab_emitente");
            this.Property(t => t.emi_id).HasColumnName("emi_id");
            this.Property(t => t.emi_nome).HasColumnName("emi_nome");
            this.Property(t => t.emi_email).HasColumnName("emi_email");
            this.Property(t => t.emi_telefone).HasColumnName("emi_telefone");
            this.Property(t => t.emi_celular).HasColumnName("emi_celular");
            this.Property(t => t.emi_ativo).HasColumnName("emi_ativo");
            this.Property(t => t.userCreate).HasColumnName("userCreate");
            this.Property(t => t.dtCreate).HasColumnName("dtCreate");
            this.Property(t => t.userLastUpdate).HasColumnName("userLastUpdate");
            this.Property(t => t.dtLastUpdate).HasColumnName("dtLastUpdate");
            this.Property(t => t.userDelete).HasColumnName("userDelete");
            this.Property(t => t.dtDelete).HasColumnName("dtDelete");
        }
    }
}
