﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity.ModelConfiguration;

namespace Entity.SCDES.Models.Mapping
{
    public class tab_select_helper_guiaMap : EntityTypeConfiguration<tab_select_helper_guia>
    {
        public tab_select_helper_guiaMap()
        {
            this.HasKey(t => t.shg_id)
                .Property(t => t.shg_id)
                .HasColumnName("shg_id");


            this.Property(t => t.shg_nome)
                .HasMaxLength(50)
                .HasColumnName("shg_nome");

            this.Property(t => t.shg_ativo)
                .HasColumnName("shg_ativo");
            
            this.ToTable("tab_select_helper_guia");
        }
    }
}
