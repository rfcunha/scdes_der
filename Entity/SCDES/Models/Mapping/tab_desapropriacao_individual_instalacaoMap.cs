using System.Data.Entity.ModelConfiguration;

namespace Entity.SCDES.Models.Mapping
{
    public class tab_desapropriacao_individual_instalacaoMap : EntityTypeConfiguration<tab_desapropriacao_individual_instalacao>
    {
        public tab_desapropriacao_individual_instalacaoMap()
        {
            // Primary Key
            this.HasKey(t => t.iin_id);

            // Properties
            this.Property(t => t.iin_tipo)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.iin_conservacao_estado)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.iin_extensao)
                .HasMaxLength(20);

            this.Property(t => t.iin_observacao)
                .HasMaxLength(500);

            this.Property(t => t.userCreate)
                .HasMaxLength(150);

            this.Property(t => t.userLastUpdate)
                .HasMaxLength(150);

            this.Property(t => t.userDelete)
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("tab_desapropriacao_individual_instalacao");
            this.Property(t => t.iin_id).HasColumnName("iin_id");
            this.Property(t => t.iin_tipo).HasColumnName("iin_tipo");
            this.Property(t => t.iin_conservacao_estado).HasColumnName("iin_conservacao_estado");
            this.Property(t => t.iin_extensao).HasColumnName("iin_extensao");
            this.Property(t => t.iin_observacao).HasColumnName("iin_observacao");
            this.Property(t => t.iin_valor_total).HasColumnName("iin_valor_total");
            this.Property(t => t.iin_cadastro_dt).HasColumnName("iin_cadastro_dt");
            this.Property(t => t.din_id).HasColumnName("din_id");
            this.Property(t => t.userCreate).HasColumnName("userCreate");
            this.Property(t => t.dtCreate).HasColumnName("dtCreate");
            this.Property(t => t.userLastUpdate).HasColumnName("userLastUpdate");
            this.Property(t => t.dtLastUpdate).HasColumnName("dtLastUpdate");
            this.Property(t => t.userDelete).HasColumnName("userDelete");
            this.Property(t => t.dtDelete).HasColumnName("dtDelete");

            // Relationships
            this.HasRequired(t => t.tab_desapropriacao_individual)
                .WithMany(t => t.tab_desapropriacao_individual_instalacao)
                .HasForeignKey(d => d.din_id);

        }
    }
}
