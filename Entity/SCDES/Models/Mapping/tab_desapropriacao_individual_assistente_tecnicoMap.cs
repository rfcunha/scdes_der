using System.Data.Entity.ModelConfiguration;

namespace Entity.SCDES.Models.Mapping
{
    public class tab_desapropriacao_individual_assistente_tecnicoMap : EntityTypeConfiguration<tab_desapropriacao_individual_assistente_tecnico>
    {
        public tab_desapropriacao_individual_assistente_tecnicoMap()
        {
            // Primary Key
            this.HasKey(t => t.iat_id);

            // Properties
            this.Property(t => t.iat_nome)
                .IsRequired()
                .HasMaxLength(150);

            this.Property(t => t.iat_email)
                .HasMaxLength(150);

            this.Property(t => t.iat_telefone)
                .HasMaxLength(11);

            this.Property(t => t.iat_celular)
                .HasMaxLength(11);

            this.Property(t => t.userCreate)
                .HasMaxLength(150);

            this.Property(t => t.userLastUpdate)
                .HasMaxLength(150);

            this.Property(t => t.userDelete)
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("tab_desapropriacao_individual_assistente_tecnico");
            this.Property(t => t.iat_id).HasColumnName("iat_id");
            this.Property(t => t.iat_nome).HasColumnName("iat_nome");
            this.Property(t => t.iat_email).HasColumnName("iat_email");
            this.Property(t => t.iat_telefone).HasColumnName("iat_telefone");
            this.Property(t => t.iat_celular).HasColumnName("iat_celular");
            this.Property(t => t.iat_ativo).HasColumnName("iat_ativo");
            this.Property(t => t.iat_cadastro_dt).HasColumnName("iat_cadastro_dt");
            this.Property(t => t.userCreate).HasColumnName("userCreate");
            this.Property(t => t.dtCreate).HasColumnName("dtCreate");
            this.Property(t => t.userLastUpdate).HasColumnName("userLastUpdate");
            this.Property(t => t.dtLastUpdate).HasColumnName("dtLastUpdate");
            this.Property(t => t.userDelete).HasColumnName("userDelete");
            this.Property(t => t.dtDelete).HasColumnName("dtDelete");
        }
    }
}
