using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entity.SCDES.Models.Mapping
{
    public class tab_desapropriacao_localidadeMap : EntityTypeConfiguration<tab_desapropriacao_localidade>
    {
        public tab_desapropriacao_localidadeMap()
        {
            // Primary Key
            this.HasKey(t => t.dlo_id);

            // Properties
            this.Property(t => t.userCreate)
                .HasMaxLength(150);

            this.Property(t => t.userLastUpdate)
                .HasMaxLength(150);

            this.Property(t => t.userDelete)
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("tab_desapropriacao_localidade");
            this.Property(t => t.dlo_id).HasColumnName("dlo_id");
            this.Property(t => t.dlo_cadastro_dt).HasColumnName("dlo_cadastro_dt");
            this.Property(t => t.des_id).HasColumnName("des_id");
            this.Property(t => t.mun_id).HasColumnName("mun_id");
            this.Property(t => t.userCreate).HasColumnName("userCreate");
            this.Property(t => t.dtCreate).HasColumnName("dtCreate");
            this.Property(t => t.userLastUpdate).HasColumnName("userLastUpdate");
            this.Property(t => t.dtLastUpdate).HasColumnName("dtLastUpdate");
            this.Property(t => t.userDelete).HasColumnName("userDelete");
            this.Property(t => t.dtDelete).HasColumnName("dtDelete");

            // Relationships
            this.HasRequired(t => t.tab_desapropriacao)
                .WithMany(t => t.tab_desapropriacao_localidade)
                .HasForeignKey(d => d.des_id);


            this.Ignore(x => x.dlo_acao);
        }
    }
}
