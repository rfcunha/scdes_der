using System.Data.Entity.ModelConfiguration;

namespace Entity.SCDES.Models.Mapping
{
    public class tab_desapropriacao_individual_situacaoMap : EntityTypeConfiguration<tab_desapropriacao_individual_situacao>
    {
        public tab_desapropriacao_individual_situacaoMap()
        {
            // Primary Key
            this.HasKey(t => t.isi_id);

            // Properties
            this.Property(t => t.isi_nome)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.userCreate)
                .HasMaxLength(150);

            this.Property(t => t.userLastUpdate)
                .HasMaxLength(150);

            this.Property(t => t.userDelete)
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("tab_desapropriacao_individual_situacao");
            this.Property(t => t.isi_id).HasColumnName("isi_id");
            this.Property(t => t.isi_nome).HasColumnName("isi_nome");
            this.Property(t => t.isi_ativo).HasColumnName("isi_ativo");
            this.Property(t => t.isi_cadastro_dt).HasColumnName("isi_cadastro_dt");
            this.Property(t => t.userCreate).HasColumnName("userCreate");
            this.Property(t => t.dtCreate).HasColumnName("dtCreate");
            this.Property(t => t.userLastUpdate).HasColumnName("userLastUpdate");
            this.Property(t => t.dtLastUpdate).HasColumnName("dtLastUpdate");
            this.Property(t => t.userDelete).HasColumnName("userDelete");
            this.Property(t => t.dtDelete).HasColumnName("dtDelete");
        }
    }
}
