using System.Data.Entity.ModelConfiguration;


namespace Entity.SCDES.Models.Mapping
{
    public class tab_auditMap : EntityTypeConfiguration<tab_audit>
    {
        public tab_auditMap()
        {
            // Primary Key
            this.HasKey(t => t.AuditID);

            // Properties
            this.Property(t => t.Type)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.TableName)
                .HasMaxLength(128);

            this.Property(t => t.PrimaryKeyField)
                .HasMaxLength(1000);

            this.Property(t => t.PrimaryKeyValue)
                .HasMaxLength(1000);

            this.Property(t => t.FieldName)
                .HasMaxLength(128);

            this.Property(t => t.OldValue)
                .HasMaxLength(8000);

            this.Property(t => t.NewValue)
                .HasMaxLength(8000);

            this.Property(t => t.UserName)
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("tab_audit");
            this.Property(t => t.AuditID).HasColumnName("AuditID");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.TableName).HasColumnName("TableName");
            this.Property(t => t.PrimaryKeyField).HasColumnName("PrimaryKeyField");
            this.Property(t => t.PrimaryKeyValue).HasColumnName("PrimaryKeyValue");
            this.Property(t => t.FieldName).HasColumnName("FieldName");
            this.Property(t => t.OldValue).HasColumnName("OldValue");
            this.Property(t => t.NewValue).HasColumnName("NewValue");
            this.Property(t => t.UpdateDate).HasColumnName("UpdateDate");
            this.Property(t => t.UserName).HasColumnName("UserName");
        }
    }
}
