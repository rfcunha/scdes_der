using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entity.SCDES.Models.Mapping
{
    public class tab_desapropriacao_individual_registro_fotograficoMap : EntityTypeConfiguration<tab_desapropriacao_individual_registro_fotografico>
    {
        public tab_desapropriacao_individual_registro_fotograficoMap()
        {
            // Primary Key
            this.HasKey(t => t.irf_id);

            // Properties
            this.Property(t => t.irf_descricao)
                .HasMaxLength(200);

            this.Property(t => t.irf_anexo)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.irf_anexo_nome)
                .IsRequired()
                .HasMaxLength(200);
            
            this.Property(t => t.userCreate)
                .HasMaxLength(150);

            this.Property(t => t.userLastUpdate)
                .HasMaxLength(150);

            this.Property(t => t.userDelete)
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("tab_desapropriacao_individual_registro_fotografico");
            this.Property(t => t.irf_id).HasColumnName("irf_id");
            this.Property(t => t.irf_data).HasColumnName("irf_data");
            this.Property(t => t.irf_ordem).HasColumnName("irf_ordem");
            this.Property(t => t.irf_descricao).HasColumnName("irf_descricao");
            this.Property(t => t.irf_anexo).HasColumnName("irf_anexo");
            this.Property(t => t.irf_anexo_nome).HasColumnName("irf_anexo_nome");
            this.Property(t => t.irf_cadastro_dt).HasColumnName("irf_cadastro_dt");
            this.Property(t => t.din_id).HasColumnName("din_id");
            this.Property(t => t.userCreate).HasColumnName("userCreate");
            this.Property(t => t.dtCreate).HasColumnName("dtCreate");
            this.Property(t => t.userLastUpdate).HasColumnName("userLastUpdate");
            this.Property(t => t.dtLastUpdate).HasColumnName("dtLastUpdate");
            this.Property(t => t.userDelete).HasColumnName("userDelete");
            this.Property(t => t.dtDelete).HasColumnName("dtDelete");

            // Relationships
            this.HasRequired(t => t.tab_desapropriacao_individual)
                .WithMany(t => t.tab_desapropriacao_individual_registro_fotografico)
                .HasForeignKey(d => d.din_id);

        }
    }
}
