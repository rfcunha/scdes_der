using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entity.SCDES.Models.Mapping
{
    public class tab_desapropriacao_statusMap : EntityTypeConfiguration<tab_desapropriacao_status>
    {
        public tab_desapropriacao_statusMap()
        {
            // Primary Key
            this.HasKey(t => t.dst_id);

            // Properties
            this.Property(t => t.dst_nome)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.userCreate)
                .HasMaxLength(150);

            this.Property(t => t.userLastUpdate)
                .HasMaxLength(150);

            this.Property(t => t.userDelete)
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("tab_desapropriacao_status");
            this.Property(t => t.dst_id).HasColumnName("dst_id");
            this.Property(t => t.dst_nome).HasColumnName("dst_nome");
            this.Property(t => t.dst_ativo).HasColumnName("dst_ativo");
            this.Property(t => t.dst_cadastro_dt).HasColumnName("dst_cadastro_dt");
            this.Property(t => t.userCreate).HasColumnName("userCreate");
            this.Property(t => t.dtCreate).HasColumnName("dtCreate");
            this.Property(t => t.userLastUpdate).HasColumnName("userLastUpdate");
            this.Property(t => t.dtLastUpdate).HasColumnName("dtLastUpdate");
            this.Property(t => t.userDelete).HasColumnName("userDelete");
            this.Property(t => t.dtDelete).HasColumnName("dtDelete");
        }
    }
}
