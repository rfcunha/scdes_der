using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entity.SCDES.Models.Mapping
{
    public class tab_empresa_usuarioMap : EntityTypeConfiguration<tab_empresa_usuario>
    {
        public tab_empresa_usuarioMap()
        {
            // Primary Key
            this.HasKey(t => t.eus_id);

            // Properties
            // Table & Column Mappings
            this.ToTable("tab_empresa_usuario");
            this.Property(t => t.eus_id).HasColumnName("eus_id");
            this.Property(t => t.eus_cadastro_dt).HasColumnName("eus_cadastro_dt");
            this.Property(t => t.emp_id).HasColumnName("emp_id");
            this.Property(t => t.usu_id).HasColumnName("usu_id");
        }
    }
}
