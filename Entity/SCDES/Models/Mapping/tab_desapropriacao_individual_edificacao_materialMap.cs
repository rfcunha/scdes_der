using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entity.SCDES.Models.Mapping
{
    public class tab_desapropriacao_individual_edificacao_materialMap : EntityTypeConfiguration<tab_desapropriacao_individual_edificacao_material>
    {
        public tab_desapropriacao_individual_edificacao_materialMap()
        {
            // Primary Key
            this.HasKey(t => t.iem_id);

            // Properties
            this.Property(t => t.iem_material)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.iem_quantidade)
                .IsRequired()
                .HasMaxLength(5);

            this.Property(t => t.iem_descricao)
                .HasMaxLength(50);

            this.Property(t => t.userCreate)
                .HasMaxLength(150);

            this.Property(t => t.userLastUpdate)
                .HasMaxLength(150);

            this.Property(t => t.userDelete)
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("tab_desapropriacao_individual_edificacao_material");
            this.Property(t => t.iem_id).HasColumnName("iem_id");
            this.Property(t => t.iem_material).HasColumnName("iem_material");
            this.Property(t => t.iem_quantidade).HasColumnName("iem_quantidade");
            this.Property(t => t.iem_descricao).HasColumnName("iem_descricao");
            this.Property(t => t.iem_valor_total).HasColumnName("iem_valor_total");
            this.Property(t => t.iem_cadastro_dt).HasColumnName("iem_cadastro_dt");
            this.Property(t => t.ied_id).HasColumnName("ied_id");
            this.Property(t => t.userCreate).HasColumnName("userCreate");
            this.Property(t => t.dtCreate).HasColumnName("dtCreate");
            this.Property(t => t.userLastUpdate).HasColumnName("userLastUpdate");
            this.Property(t => t.dtLastUpdate).HasColumnName("dtLastUpdate");
            this.Property(t => t.userDelete).HasColumnName("userDelete");
            this.Property(t => t.dtDelete).HasColumnName("dtDelete");

            // Relationships
            this.HasRequired(t => t.tab_desapropriacao_individual_edificacao)
                .WithMany(t => t.tab_desapropriacao_individual_edificacao_material)
                .HasForeignKey(d => d.ied_id);

        }
    }
}
