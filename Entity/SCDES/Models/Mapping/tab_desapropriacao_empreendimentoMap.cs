using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entity.SCDES.Models.Mapping
{
    public class tab_desapropriacao_empreendimentoMap : EntityTypeConfiguration<tab_desapropriacao_empreendimento>
    {
        public tab_desapropriacao_empreendimentoMap()
        {
            // Primary Key
            this.HasKey(t => t.dem_id);

            // Properties
            this.Property(t => t.dem_auto_decreto)
                .IsRequired()
                .HasMaxLength(30);

            this.Property(t => t.dem_rodovia_sp)
                .IsRequired()
                .HasMaxLength(20);

            this.Property(t => t.dem_rodovia_nome)
                .HasMaxLength(300);

            this.Property(t => t.dem_rodovia_trecho)
                .HasMaxLength(300);

            this.Property(t => t.dem_rodovia_km_inicial)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.dem_rodovia_km_final)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.dem_dup_codigo)
                .HasMaxLength(20);

            this.Property(t => t.dem_nota_reserva)
                .HasMaxLength(20);

            this.Property(t => t.dem_anexo)
                .HasMaxLength(150);

            this.Property(t => t.dem_anexo_nome)
                .HasMaxLength(150);

            this.Property(t => t.dem_anexo_pasta)
                .HasMaxLength(50);

            this.Property(t => t.userCreate)
                .HasMaxLength(150);

            this.Property(t => t.userLastUpdate)
                .HasMaxLength(150);

            this.Property(t => t.userDelete)
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("tab_desapropriacao_empreendimento");
            this.Property(t => t.dem_id).HasColumnName("dem_id");
            this.Property(t => t.dem_auto_decreto).HasColumnName("dem_auto_decreto");
            this.Property(t => t.dem_lote).HasColumnName("dem_lote");
            this.Property(t => t.dem_rodovia_sp).HasColumnName("dem_rodovia_sp");
            this.Property(t => t.dem_rodovia_nome).HasColumnName("dem_rodovia_nome");
            this.Property(t => t.dem_rodovia_trecho).HasColumnName("dem_rodovia_trecho");
            this.Property(t => t.dem_rodovia_km_inicial).HasColumnName("dem_rodovia_km_inicial");
            this.Property(t => t.dem_rodovia_km_final).HasColumnName("dem_rodovia_km_final");
            this.Property(t => t.dem_dup_dt).HasColumnName("dem_dup_dt");
            this.Property(t => t.dem_dup_codigo).HasColumnName("dem_dup_codigo");
            this.Property(t => t.dem_nota_reserva).HasColumnName("dem_nota_reserva");
            this.Property(t => t.des_id).HasColumnName("des_id");
            this.Property(t => t.dem_anexo).HasColumnName("dem_anexo");
            this.Property(t => t.dem_anexo_nome).HasColumnName("dem_anexo_nome");
            this.Property(t => t.dem_anexo_pasta).HasColumnName("dem_anexo_pasta");
            this.Property(t => t.dem_cadastro_dt).HasColumnName("dem_cadastro_dt");
            this.Property(t => t.userCreate).HasColumnName("userCreate");
            this.Property(t => t.dtCreate).HasColumnName("dtCreate");
            this.Property(t => t.userLastUpdate).HasColumnName("userLastUpdate");
            this.Property(t => t.dtLastUpdate).HasColumnName("dtLastUpdate");
            this.Property(t => t.userDelete).HasColumnName("userDelete");
            this.Property(t => t.dtDelete).HasColumnName("dtDelete");

            // Relationships
            this.HasRequired(t => t.tab_desapropriacao)
                .WithMany(t => t.tab_desapropriacao_empreendimento)
                .HasForeignKey(d => d.des_id);

            this.Ignore(t => t.dem_acao);
            this.Ignore(t => t.dem_file_upload);
        }
    }
}
