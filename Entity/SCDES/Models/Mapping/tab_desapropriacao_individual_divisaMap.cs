using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entity.SCDES.Models.Mapping
{
    public class tab_desapropriacao_individual_divisaMap : EntityTypeConfiguration<tab_desapropriacao_individual_divisa>
    {
        public tab_desapropriacao_individual_divisaMap()
        {
            // Primary Key
            this.HasKey(t => t.idi_id);

            // Properties
            this.Property(t => t.idi_tipo)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.idi_descricao)
                .HasMaxLength(200);

            this.Property(t => t.idi_quantidade)
                .IsRequired()
                .HasMaxLength(20);

            this.Property(t => t.idi_unidade)
                .HasMaxLength(50);

            this.Property(t => t.idi_observacao)
                .HasMaxLength(500);

            this.Property(t => t.userCreate)
                .HasMaxLength(150);

            this.Property(t => t.userLastUpdate)
                .HasMaxLength(150);

            this.Property(t => t.userDelete)
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("tab_desapropriacao_individual_divisa");
            this.Property(t => t.idi_id).HasColumnName("idi_id");
            this.Property(t => t.idi_tipo).HasColumnName("idi_tipo");
            this.Property(t => t.idi_descricao).HasColumnName("idi_descricao");
            this.Property(t => t.idi_quantidade).HasColumnName("idi_quantidade");
            this.Property(t => t.idi_unidade).HasColumnName("idi_unidade");
            this.Property(t => t.idi_observacao).HasColumnName("idi_observacao");
            this.Property(t => t.idi_valor_total).HasColumnName("idi_valor_total");
            this.Property(t => t.idi_cadastro_dt).HasColumnName("idi_cadastro_dt");
            this.Property(t => t.din_id).HasColumnName("din_id");
            this.Property(t => t.userCreate).HasColumnName("userCreate");
            this.Property(t => t.dtCreate).HasColumnName("dtCreate");
            this.Property(t => t.userLastUpdate).HasColumnName("userLastUpdate");
            this.Property(t => t.dtLastUpdate).HasColumnName("dtLastUpdate");
            this.Property(t => t.userDelete).HasColumnName("userDelete");
            this.Property(t => t.dtDelete).HasColumnName("dtDelete");

            // Relationships
            this.HasRequired(t => t.tab_desapropriacao_individual)
                .WithMany(t => t.tab_desapropriacao_individual_divisa)
                .HasForeignKey(d => d.din_id);

        }
    }
}
