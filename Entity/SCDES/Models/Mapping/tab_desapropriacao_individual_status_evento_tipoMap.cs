using System.Data.Entity.ModelConfiguration;

namespace Entity.SCDES.Models.Mapping
{
    public class tab_desapropriacao_individual_status_evento_tipoMap : EntityTypeConfiguration<tab_desapropriacao_individual_status_evento_tipo>
    {
        public tab_desapropriacao_individual_status_evento_tipoMap()
        {
            // Primary Key
            this.HasKey(t => t.set_id);

            // Properties
            this.Property(t => t.set_nome)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.userCreate)
                .HasMaxLength(150);

            this.Property(t => t.userLastUpdate)
                .HasMaxLength(150);

            this.Property(t => t.userDelete)
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("tab_desapropriacao_individual_status_evento_tipo");
            this.Property(t => t.set_id).HasColumnName("set_id");
            this.Property(t => t.set_nome).HasColumnName("set_nome");
            this.Property(t => t.set_ativo).HasColumnName("set_ativo");
            this.Property(t => t.set_valor).HasColumnName("set_valor");
            this.Property(t => t.set_cadastro_dt).HasColumnName("set_cadastro_dt");
            this.Property(t => t.userCreate).HasColumnName("userCreate");
            this.Property(t => t.dtCreate).HasColumnName("dtCreate");
            this.Property(t => t.userLastUpdate).HasColumnName("userLastUpdate");
            this.Property(t => t.dtLastUpdate).HasColumnName("dtLastUpdate");
            this.Property(t => t.userDelete).HasColumnName("userDelete");
            this.Property(t => t.dtDelete).HasColumnName("dtDelete");
        }
    }
}
