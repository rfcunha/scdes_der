using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entity.SCDES.Models.Mapping
{
    public class tab_desapropriacao_individual_entradaMap : EntityTypeConfiguration<tab_desapropriacao_individual_entrada>
    {
        public tab_desapropriacao_individual_entradaMap()
        {
            // Primary Key
            this.HasKey(t => t.ien_id);

            // Properties
            this.Property(t => t.ien_tipo)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.ien_conservacao_estado)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.ien_dimensao)
                .HasMaxLength(20);

            this.Property(t => t.ien_observacao)
                .HasMaxLength(500);

            this.Property(t => t.userCreate)
                .HasMaxLength(150);

            this.Property(t => t.userLastUpdate)
                .HasMaxLength(150);

            this.Property(t => t.userDelete)
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("tab_desapropriacao_individual_entrada");
            this.Property(t => t.ien_id).HasColumnName("ien_id");
            this.Property(t => t.ien_tipo).HasColumnName("ien_tipo");
            this.Property(t => t.ien_conservacao_estado).HasColumnName("ien_conservacao_estado");
            this.Property(t => t.ien_dimensao).HasColumnName("ien_dimensao");
            this.Property(t => t.ien_observacao).HasColumnName("ien_observacao");
            this.Property(t => t.ien_valor_total).HasColumnName("ien_valor_total");
            this.Property(t => t.ien_cadastro_dt).HasColumnName("ien_cadastro_dt");
            this.Property(t => t.din_id).HasColumnName("din_id");
            this.Property(t => t.userCreate).HasColumnName("userCreate");
            this.Property(t => t.dtCreate).HasColumnName("dtCreate");
            this.Property(t => t.userLastUpdate).HasColumnName("userLastUpdate");
            this.Property(t => t.dtLastUpdate).HasColumnName("dtLastUpdate");
            this.Property(t => t.userDelete).HasColumnName("userDelete");
            this.Property(t => t.dtDelete).HasColumnName("dtDelete");

            // Relationships
            this.HasRequired(t => t.tab_desapropriacao_individual)
                .WithMany(t => t.tab_desapropriacao_individual_entrada)
                .HasForeignKey(d => d.din_id);

        }
    }
}
