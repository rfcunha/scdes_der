using System.Data.Entity.ModelConfiguration;

namespace Entity.SCDES.Models.Mapping
{
    public class tab_desapropriacao_individual_perito_judicialMap : EntityTypeConfiguration<tab_desapropriacao_individual_perito_judicial>
    {
        public tab_desapropriacao_individual_perito_judicialMap()
        {
            // Primary Key
            this.HasKey(t => t.ipj_id);

            // Properties
            this.Property(t => t.ipj_nome)
                .IsRequired()
                .HasMaxLength(150);

            this.Property(t => t.ipj_email)
                .HasMaxLength(150);

            this.Property(t => t.ipj_telefone)
                .HasMaxLength(11);

            this.Property(t => t.ipj_celular)
                .HasMaxLength(11);

            this.Property(t => t.userCreate)
                .HasMaxLength(150);

            this.Property(t => t.userLastUpdate)
                .HasMaxLength(150);

            this.Property(t => t.userDelete)
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("tab_desapropriacao_individual_perito_judicial");
            this.Property(t => t.ipj_id).HasColumnName("ipj_id");
            this.Property(t => t.ipj_nome).HasColumnName("ipj_nome");
            this.Property(t => t.ipj_email).HasColumnName("ipj_email");
            this.Property(t => t.ipj_telefone).HasColumnName("ipj_telefone");
            this.Property(t => t.ipj_celular).HasColumnName("ipj_celular");
            this.Property(t => t.ipj_ativo).HasColumnName("ipj_ativo");
            this.Property(t => t.ipj_cadastro_dt).HasColumnName("ipj_cadastro_dt");
            this.Property(t => t.userCreate).HasColumnName("userCreate");
            this.Property(t => t.dtCreate).HasColumnName("dtCreate");
            this.Property(t => t.userLastUpdate).HasColumnName("userLastUpdate");
            this.Property(t => t.dtLastUpdate).HasColumnName("dtLastUpdate");
            this.Property(t => t.userDelete).HasColumnName("userDelete");
            this.Property(t => t.dtDelete).HasColumnName("dtDelete");
        }
    }
}
