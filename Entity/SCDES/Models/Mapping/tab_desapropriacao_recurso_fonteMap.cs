using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entity.SCDES.Models.Mapping
{
    public class tab_desapropriacao_recurso_fonteMap : EntityTypeConfiguration<tab_desapropriacao_recurso_fonte>
    {
        public tab_desapropriacao_recurso_fonteMap()
        {
            // Primary Key
            this.HasKey(t => t.drf_id);

            // Properties
            this.Property(t => t.drf_contrato_numero).HasMaxLength(10);

            this.Property(t => t.userCreate).HasMaxLength(150);

            this.Property(t => t.userLastUpdate).HasMaxLength(150);

            this.Property(t => t.userDelete).HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("tab_desapropriacao_recurso_fonte");
            this.Property(t => t.drf_id).HasColumnName("drf_id");
            this.Property(t => t.drf_percentual).HasColumnName("drf_percentual");
            this.Property(t => t.drf_valor).HasColumnName("drf_valor");
            this.Property(t => t.drf_contrato_numero).HasColumnName("drf_contrato_numero");
            this.Property(t => t.drf_assinatura_dt).HasColumnName("drf_assinatura_dt");
            this.Property(t => t.drf_cadastro_dt).HasColumnName("drf_cadastro_dt");
            this.Property(t => t.des_id).HasColumnName("des_id");
            this.Property(t => t.userCreate).HasColumnName("userCreate");
            this.Property(t => t.dtCreate).HasColumnName("dtCreate");
            this.Property(t => t.userLastUpdate).HasColumnName("userLastUpdate");
            this.Property(t => t.dtLastUpdate).HasColumnName("dtLastUpdate");
            this.Property(t => t.userDelete).HasColumnName("userDelete");
            this.Property(t => t.dtDelete).HasColumnName("dtDelete");

            // Relationships
            this.HasRequired(t => t.tab_desapropriacao).WithMany(t => t.tab_desapropriacao_recurso_fonte).HasForeignKey(d => d.des_id);


            this.Ignore(x => x.drf_acao);
        }
    }
}
