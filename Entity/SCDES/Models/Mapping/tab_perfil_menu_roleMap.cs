using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entity.SCDES.Models.Mapping
{
    public class tab_perfil_menu_roleMap : EntityTypeConfiguration<tab_perfil_menu_role>
    {
        public tab_perfil_menu_roleMap()
        {
            // Primary Key
            this.HasKey(t => t.pmr_id);

            // Properties
            // Table & Column Mappings
            this.ToTable("tab_perfil_menu_role");
            this.Property(t => t.pmr_id).HasColumnName("pmr_id");
            this.Property(t => t.pmr_dt_cadastro).HasColumnName("pmr_dt_cadastro");
            this.Property(t => t.per_id).HasColumnName("per_id");
            this.Property(t => t.mro_id).HasColumnName("mro_id");

            // Relationships
            this.HasRequired(t => t.tab_menu_role)
                .WithMany(t => t.tab_perfil_menu_role)
                .HasForeignKey(d => d.mro_id);
            this.HasRequired(t => t.tab_perfil)
                .WithMany(t => t.tab_perfil_menu_role)
                .HasForeignKey(d => d.per_id);

        }
    }
}
