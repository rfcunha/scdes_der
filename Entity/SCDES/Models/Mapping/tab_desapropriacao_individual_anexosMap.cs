using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entity.SCDES.Models.Mapping
{
    public class tab_desapropriacao_individual_anexosMap : EntityTypeConfiguration<tab_desapropriacao_individual_anexos>
    {
        public tab_desapropriacao_individual_anexosMap()
        {
            // Primary Key
            this.HasKey(t => t.ian_id);

            // Properties
            this.Property(t => t.ian_anexo)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.dia_anexo_nome)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.userCreate)
                .HasMaxLength(150);

            this.Property(t => t.userLastUpdate)
                .HasMaxLength(150);

            this.Property(t => t.userDelete)
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("tab_desapropriacao_individual_anexos");
            this.Property(t => t.ian_id).HasColumnName("ian_id");
            this.Property(t => t.ian_anexo).HasColumnName("ian_anexo");
            this.Property(t => t.dia_anexo_nome).HasColumnName("dia_anexo_nome");
            this.Property(t => t.ian_tipo).HasColumnName("ian_tipo");
            this.Property(t => t.ian_cadastro_dt).HasColumnName("ian_cadastro_dt");
            this.Property(t => t.din_id).HasColumnName("din_id");
            this.Property(t => t.userCreate).HasColumnName("userCreate");
            this.Property(t => t.dtCreate).HasColumnName("dtCreate");
            this.Property(t => t.userLastUpdate).HasColumnName("userLastUpdate");
            this.Property(t => t.dtLastUpdate).HasColumnName("dtLastUpdate");
            this.Property(t => t.userDelete).HasColumnName("userDelete");
            this.Property(t => t.dtDelete).HasColumnName("dtDelete");

            // Relationships
            this.HasRequired(t => t.tab_desapropriacao_individual)
                .WithMany(t => t.tab_desapropriacao_individual_anexos)
                .HasForeignKey(d => d.din_id);

        }
    }
}
