using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entity.SCDES.Models.Mapping
{
    public class tab_configuracao_sistemaMap : EntityTypeConfiguration<tab_configuracao_sistema>
    {
        public tab_configuracao_sistemaMap()
        {
            // Primary Key
            this.HasKey(t => t.csi_id);

            // Properties
            this.Property(t => t.csi_id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.csi_modulo)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.csi_nome)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.csi_funcao)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.csi_valor)
                .IsRequired()
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("tab_configuracao_sistema");
            this.Property(t => t.csi_id).HasColumnName("csi_id");
            this.Property(t => t.csi_modulo).HasColumnName("csi_modulo");
            this.Property(t => t.csi_nome).HasColumnName("csi_nome");
            this.Property(t => t.csi_funcao).HasColumnName("csi_funcao");
            this.Property(t => t.csi_valor).HasColumnName("csi_valor");
        }
    }
}
