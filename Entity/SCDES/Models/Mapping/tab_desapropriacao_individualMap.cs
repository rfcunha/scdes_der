using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entity.SCDES.Models.Mapping
{
    public class tab_desapropriacao_individualMap : EntityTypeConfiguration<tab_desapropriacao_individual>
    {
        public tab_desapropriacao_individualMap()
        {
            // Primary Key
            this.HasKey(t => t.din_id);


            this.Property(t => t.din_guid)
                .HasMaxLength(50);

            // Properties
            this.Property(t => t.din_geral_documento_numero)
                .IsRequired()
                .HasMaxLength(35);

            this.Property(t => t.din_geral_revisao)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.din_geral_elaborador)
                .HasMaxLength(150);

            this.Property(t => t.din_geral_responsavel_tecnico)
                .HasMaxLength(150);

            this.Property(t => t.din_geral_objeto)
                .HasMaxLength(100);

            /*
            this.Property(t => t.din_empreendimento_rodovia_sp)
                .HasMaxLength(20);

            this.Property(t => t.din_empreendimento_rodovia_nome)
                .HasMaxLength(300);

            this.Property(t => t.din_empreendimento_rodovia_trecho)
                .HasMaxLength(300);

            this.Property(t => t.din_empreendimento_rodovia_km_inicial)
                .HasMaxLength(10);

            this.Property(t => t.din_empreendimento_rodovia_km_final)
                .HasMaxLength(10);

            this.Property(t => t.din_empreendimento_dup_codigo)
                .HasMaxLength(20);

            this.Property(t => t.din_empreendimento_auto_decreto)
                .HasMaxLength(30);
            */

            this.Property(t => t.din_propriedade_cep)
                .HasMaxLength(8);

            this.Property(t => t.din_propriedade_logradouro_numero)
                .HasMaxLength(10);

            this.Property(t => t.din_propriedade_logradouro)
                .HasMaxLength(150);

            this.Property(t => t.din_propriedade_logradouro_complemento)
                .HasMaxLength(100);

            this.Property(t => t.din_propriedade_bairro)
                .HasMaxLength(100);

            this.Property(t => t.din_propriedade_contribuinte_numero)
                .HasMaxLength(50);

            this.Property(t => t.din_propriedade_cartorio)
                .HasMaxLength(150);

            this.Property(t => t.din_propriedade_matricula)
                .HasMaxLength(50);

            this.Property(t => t.din_propriedade_estaca_inicial)
                .HasMaxLength(30);

            this.Property(t => t.din_propriedade_estaca_final)
                .HasMaxLength(30);

            this.Property(t => t.din_propriedade_km_inicial)
                .HasMaxLength(10);

            this.Property(t => t.din_propriedade_km_final)
                .HasMaxLength(10);

            this.Property(t => t.din_propriedade_latitude)
                .HasMaxLength(100);

            this.Property(t => t.din_propriedade_longitude)
                .HasMaxLength(100);

            this.Property(t => t.din_propriedade_autos_numero)
                .HasMaxLength(18);
            
            this.Property(t => t.din_imovel_infraestrutura)
                .HasMaxLength(150);

            this.Property(t => t.din_imovel_equipamento)
                .HasMaxLength(150);

            this.Property(t => t.din_imovel_servico)
                .HasMaxLength(150);

            this.Property(t => t.din_imovel_area_terreno)
                .HasMaxLength(20);

            this.Property(t => t.din_imovel_area_necessaria)
                .HasMaxLength(20);

            this.Property(t => t.din_imovel_area_porcentagem)
                .HasMaxLength(5);

            this.Property(t => t.din_imovel_perimetro)
                .HasMaxLength(50);

            this.Property(t => t.din_imovel_restricao_uso)
                .HasMaxLength(50);

            this.Property(t => t.din_imovel_curso_dagua)
                .HasMaxLength(70);

            this.Property(t => t.din_imovel_recurso_minerais)
                .HasMaxLength(70);

            this.Property(t => t.din_imovel_terreno_ocupacoes)
                .HasMaxLength(150);

            this.Property(t => t.din_imovel_delimitacao)
                .HasMaxLength(100);

            this.Property(t => t.din_imovel_atividade_economica)
                .HasMaxLength(100);

            this.Property(t => t.din_imovel_posicionamento)
                .HasMaxLength(150);

            this.Property(t => t.din_imovel_frontal)
                .HasMaxLength(100);

            this.Property(t => t.din_imovel_fundo)
                .HasMaxLength(100);

            this.Property(t => t.din_imovel_lateral_esquerda)
                .HasMaxLength(100);

            this.Property(t => t.din_imovel_lateral_direita)
                .HasMaxLength(100);

            this.Property(t => t.userCreate)
                .HasMaxLength(150);

            this.Property(t => t.userLastUpdate)
                .HasMaxLength(150);

            this.Property(t => t.userDelete)
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("tab_desapropriacao_individual");
            this.Property(t => t.din_id).HasColumnName("din_id");
            this.Property(t => t.din_geral_documento_numero).HasColumnName("din_geral_documento_numero");
            this.Property(t => t.din_geral_revisao).HasColumnName("din_geral_revisao");
            this.Property(t => t.din_geral_emissao_dt).HasColumnName("din_geral_emissao_dt");
            this.Property(t => t.din_geral_emp_id).HasColumnName("din_geral_emp_id");
            this.Property(t => t.din_geral_elaborador).HasColumnName("din_geral_elaborador");
            this.Property(t => t.din_geral_responsavel_tecnico).HasColumnName("din_geral_responsavel_tecnico");
            this.Property(t => t.din_geral_objeto).HasColumnName("din_geral_objeto");

            /*
            this.Property(t => t.din_empreendimento_rodovia_sp).HasColumnName("din_empreendimento_rodovia_sp");
            this.Property(t => t.din_empreendimento_rodovia_nome).HasColumnName("din_empreendimento_rodovia_nome");
            this.Property(t => t.din_empreendimento_rodovia_trecho).HasColumnName("din_empreendimento_rodovia_trecho");
            this.Property(t => t.din_empreendimento_rodovia_km_inicial).HasColumnName("din_empreendimento_rodovia_km_inicial");
            this.Property(t => t.din_empreendimento_rodovia_km_final).HasColumnName("din_empreendimento_rodovia_km_final");
            this.Property(t => t.din_empreendimento_dup_dt).HasColumnName("din_empreendimento_dup_dt");
            this.Property(t => t.din_empreendimento_dup_codigo).HasColumnName("din_empreendimento_dup_codigo");
            this.Property(t => t.din_empreendimento_auto_decreto).HasColumnName("din_empreendimento_auto_decreto");
            */

            this.Property(t => t.din_propriedade_cep).HasColumnName("din_propriedade_cep");
            this.Property(t => t.din_propriedade_logradouro_numero).HasColumnName("din_propriedade_logradouro_numero");
            this.Property(t => t.din_propriedade_logradouro).HasColumnName("din_propriedade_logradouro");
            this.Property(t => t.din_propriedade_logradouro_complemento).HasColumnName("din_propriedade_logradouro_complemento");
            this.Property(t => t.din_propriedade_mun_id).HasColumnName("din_propriedade_mun_id");
            this.Property(t => t.din_propriedade_bairro).HasColumnName("din_propriedade_bairro");
            this.Property(t => t.din_propriedade_contribuinte_tipo).HasColumnName("din_propriedade_contribuinte_tipo");
            this.Property(t => t.din_propriedade_contribuinte_numero).HasColumnName("din_propriedade_contribuinte_numero");
            this.Property(t => t.din_propriedade_cartorio).HasColumnName("din_propriedade_cartorio");
            this.Property(t => t.din_propriedade_matricula).HasColumnName("din_propriedade_matricula");
            this.Property(t => t.din_propriedade_desapropriacao).HasColumnName("din_propriedade_desapropriacao");
            this.Property(t => t.din_propriedade_comarca).HasColumnName("din_propriedade_comarca");
            this.Property(t => t.din_propriedade_estaca_inicial).HasColumnName("din_propriedade_estaca_inicial");
            this.Property(t => t.din_propriedade_estaca_final).HasColumnName("din_propriedade_estaca_final");
            this.Property(t => t.din_propriedade_km_inicial).HasColumnName("din_propriedade_km_inicial");
            this.Property(t => t.din_propriedade_km_final).HasColumnName("din_propriedade_km_final");
            this.Property(t => t.din_propriedade_latitude).HasColumnName("din_propriedade_latitude");
            this.Property(t => t.din_propriedade_longitude).HasColumnName("din_propriedade_longitude");
            this.Property(t => t.din_propriedade_dominio).HasColumnName("din_propriedade_dominio");
            this.Property(t => t.din_propriedade_prioridade).HasColumnName("din_propriedade_prioridade");
            this.Property(t => t.din_propriedade_autos_numero).HasColumnName("din_propriedade_autos_numero");
            this.Property(t => t.din_propriedade_autos_dt).HasColumnName("din_propriedade_autos_dt");

            this.Property(t => t.din_conducao_anuencia).HasColumnName("din_conducao_anuencia");
            this.Property(t => t.din_conducao_valor_total).HasColumnName("din_conducao_valor_total");
            this.Property(t => t.din_conducao_isi_id).HasColumnName("din_conducao_isi_id");
            this.Property(t => t.din_conducao_ist_id).HasColumnName("din_conducao_ist_id");
            this.Property(t => t.din_conducao_ipj_id).HasColumnName("din_conducao_ipj_id");
            this.Property(t => t.din_conducao_iat_id).HasColumnName("din_conducao_iat_id");
            
            this.Property(t => t.din_imovel_ipe_id).HasColumnName("din_imovel_ipe_id");
            this.Property(t => t.din_imovel_zti_id).HasColumnName("din_imovel_zti_id");
            this.Property(t => t.din_imovel_infraestrutura).HasColumnName("din_imovel_infraestrutura");
            this.Property(t => t.din_imovel_equipamento).HasColumnName("din_imovel_equipamento");
            this.Property(t => t.din_imovel_servico).HasColumnName("din_imovel_servico");
            this.Property(t => t.din_imovel_area_terreno).HasColumnName("din_imovel_area_terreno");
            this.Property(t => t.din_imovel_area_necessaria).HasColumnName("din_imovel_area_necessaria");
            this.Property(t => t.din_imovel_area_porcentagem).HasColumnName("din_imovel_area_porcentagem");
            this.Property(t => t.din_imovel_perimetro).HasColumnName("din_imovel_perimetro");
            this.Property(t => t.din_imovel_top_id).HasColumnName("din_imovel_top_id");
            this.Property(t => t.din_imovel_sco_id).HasColumnName("din_imovel_sco_id");
            this.Property(t => t.din_imovel_restricao_uso).HasColumnName("din_imovel_restricao_uso");
            this.Property(t => t.din_imovel_curso_dagua).HasColumnName("din_imovel_curso_dagua");
            this.Property(t => t.din_imovel_recurso_minerais).HasColumnName("din_imovel_recurso_minerais");
            this.Property(t => t.din_imovel_terreno_ocupacoes).HasColumnName("din_imovel_terreno_ocupacoes");
            this.Property(t => t.din_imovel_sus).HasColumnName("din_imovel_sus");
            this.Property(t => t.din_imovel_delimitacao).HasColumnName("din_imovel_delimitacao");
            this.Property(t => t.din_imovel_atividade_economica).HasColumnName("din_imovel_atividade_economica");
            this.Property(t => t.din_imovel_posicionamento).HasColumnName("din_imovel_posicionamento");
            this.Property(t => t.din_imovel_frontal).HasColumnName("din_imovel_frontal");
            this.Property(t => t.din_imovel_fundo).HasColumnName("din_imovel_fundo");
            this.Property(t => t.din_imovel_lateral_esquerda).HasColumnName("din_imovel_lateral_esquerda");
            this.Property(t => t.din_imovel_lateral_direita).HasColumnName("din_imovel_lateral_direita");

            this.Property(t => t.din_benfeitorias).HasColumnName("din_benfeitorias");

            this.Property(t => t.din_memorial_descritivo).HasColumnName("din_memorial_descritivo");

            this.Property(t => t.din_planta_propriedade).HasColumnName("din_planta_propriedade");

            this.Property(t => t.din_laudo_avaliacao).HasColumnName("din_laudo_avaliacao");

            this.Property(t => t.din_cadastro_dt).HasColumnName("din_cadastro_dt");
            this.Property(t => t.userCreate).HasColumnName("userCreate");
            this.Property(t => t.dtCreate).HasColumnName("dtCreate");
            this.Property(t => t.userLastUpdate).HasColumnName("userLastUpdate");
            this.Property(t => t.dtLastUpdate).HasColumnName("dtLastUpdate");
            this.Property(t => t.userDelete).HasColumnName("userDelete");
            this.Property(t => t.dtDelete).HasColumnName("dtDelete");

            // Relationships
            this.HasRequired(t => t.tab_desapropriacao_empreendimento)
                .WithMany(t => t.tab_desapropriacao_individual)
                .HasForeignKey(d => d.dem_id);
            this.HasOptional(t => t.tab_desapropriacao_individual_assistente_tecnico)
                .WithMany(t => t.tab_desapropriacao_individual)
                .HasForeignKey(d => d.din_conducao_iat_id);
            this.HasOptional(t => t.tab_desapropriacao_individual_perito_judicial)
                .WithMany(t => t.tab_desapropriacao_individual)
                .HasForeignKey(d => d.din_conducao_ipj_id);
            this.HasOptional(t => t.tab_desapropriacao_individual_predominante_entorno)
                .WithMany(t => t.tab_desapropriacao_individual)
                .HasForeignKey(d => d.din_imovel_ipe_id);
            this.HasOptional(t => t.tab_desapropriacao_individual_situacao)
                .WithMany(t => t.tab_desapropriacao_individual)
                .HasForeignKey(d => d.din_conducao_isi_id);
            this.HasOptional(t => t.tab_desapropriacao_individual_status)
                .WithMany(t => t.tab_desapropriacao_individual)
                .HasForeignKey(d => d.din_conducao_ist_id);

        }
    }
}
