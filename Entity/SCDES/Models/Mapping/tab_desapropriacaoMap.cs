using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entity.SCDES.Models.Mapping
{
    public class tab_desapropriacaoMap : EntityTypeConfiguration<tab_desapropriacao>
    {
        public tab_desapropriacaoMap()
        {
            // Primary Key
            this.HasKey(t => t.des_id);

            // Properties
            this.Property(t => t.des_projeto_codigo)
                .HasMaxLength(10);

            this.Property(t => t.des_nome)
                .HasMaxLength(70);

            this.Property(t => t.userCreate)
                .HasMaxLength(150);

            this.Property(t => t.userLastUpdate)
                .HasMaxLength(150);

            this.Property(t => t.userDelete)
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("tab_desapropriacao");
            this.Property(t => t.des_id).HasColumnName("des_id");
            this.Property(t => t.des_projeto_codigo).HasColumnName("des_projeto_codigo");
            this.Property(t => t.des_nome).HasColumnName("des_nome");
            this.Property(t => t.dde_id).HasColumnName("dde_id");
            this.Property(t => t.des_descricao).HasColumnName("des_descricao");
            this.Property(t => t.des_objetivo).HasColumnName("des_objetivo");
            this.Property(t => t.des_inicio).HasColumnName("des_inicio");
            this.Property(t => t.des_fim).HasColumnName("des_fim");
            this.Property(t => t.des_valor_estimado).HasColumnName("des_valor_estimado");
            this.Property(t => t.dst_id).HasColumnName("dst_id");
            this.Property(t => t.des_cadastro_dt).HasColumnName("des_cadastro_dt");
            this.Property(t => t.afi_id).HasColumnName("afi_id");
            this.Property(t => t.reg_id).HasColumnName("reg_id");
            this.Property(t => t.userCreate).HasColumnName("userCreate");
            this.Property(t => t.dtCreate).HasColumnName("dtCreate");
            this.Property(t => t.userLastUpdate).HasColumnName("userLastUpdate");
            this.Property(t => t.dtLastUpdate).HasColumnName("dtLastUpdate");
            this.Property(t => t.userDelete).HasColumnName("userDelete");
            this.Property(t => t.dtDelete).HasColumnName("dtDelete");

            // Relationships
            this.HasRequired(t => t.tab_desapropriacao_departamento)
                .WithMany(t => t.tab_desapropriacao)
                .HasForeignKey(d => d.dde_id);
            this.HasRequired(t => t.tab_desapropriacao_status)
                .WithMany(t => t.tab_desapropriacao)
                .HasForeignKey(d => d.dst_id);
        }
    }
}
