using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entity.SCDES.Models.Mapping
{
    public class tab_desapropriacao_individual_estrada_acessoMap : EntityTypeConfiguration<tab_desapropriacao_individual_estrada_acesso>
    {
        public tab_desapropriacao_individual_estrada_acessoMap()
        {
            // Primary Key
            this.HasKey(t => t.iea_id);

            // Properties
            this.Property(t => t.iea_tipo)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.iea_conservacao_estado)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.iea_estaca_inicial)
                .HasMaxLength(20);

            this.Property(t => t.iea_dimensao)
                .HasMaxLength(20);

            this.Property(t => t.iea_pavimento)
                .HasMaxLength(50);

            this.Property(t => t.iea_observacao)
                .HasMaxLength(500);

            this.Property(t => t.userCreate)
                .HasMaxLength(150);

            this.Property(t => t.userLastUpdate)
                .HasMaxLength(150);

            this.Property(t => t.userDelete)
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("tab_desapropriacao_individual_estrada_acesso");
            this.Property(t => t.iea_id).HasColumnName("iea_id");
            this.Property(t => t.iea_tipo).HasColumnName("iea_tipo");
            this.Property(t => t.iea_conservacao_estado).HasColumnName("iea_conservacao_estado");
            this.Property(t => t.iea_estaca_inicial).HasColumnName("iea_estaca_inicial");
            this.Property(t => t.iea_dimensao).HasColumnName("iea_dimensao");
            this.Property(t => t.iea_pavimento).HasColumnName("iea_pavimento");
            this.Property(t => t.iea_observacao).HasColumnName("iea_observacao");
            this.Property(t => t.iea_valor_total).HasColumnName("iea_valor_total");
            this.Property(t => t.iea_cadastro_dt).HasColumnName("iea_cadastro_dt");
            this.Property(t => t.din_id).HasColumnName("din_id");
            this.Property(t => t.userCreate).HasColumnName("userCreate");
            this.Property(t => t.dtCreate).HasColumnName("dtCreate");
            this.Property(t => t.userLastUpdate).HasColumnName("userLastUpdate");
            this.Property(t => t.dtLastUpdate).HasColumnName("dtLastUpdate");
            this.Property(t => t.userDelete).HasColumnName("userDelete");
            this.Property(t => t.dtDelete).HasColumnName("dtDelete");

            // Relationships
            this.HasRequired(t => t.tab_desapropriacao_individual)
                .WithMany(t => t.tab_desapropriacao_individual_estrada_acesso)
                .HasForeignKey(d => d.din_id);

        }
    }
}
