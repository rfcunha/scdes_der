using System.Data.Entity.ModelConfiguration;

namespace Entity.SCDES.Models.Mapping
{
    public class tab_desapropriacao_individual_status_eventoMap : EntityTypeConfiguration<tab_desapropriacao_individual_status_evento>
    {
        public tab_desapropriacao_individual_status_eventoMap()
        {
            // Primary Key
            this.HasKey(t => t.ise_id);

            // Properties
            this.Property(t => t.ise_data)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.userCreate)
                .HasMaxLength(150);

            this.Property(t => t.userLastUpdate)
                .HasMaxLength(150);

            this.Property(t => t.userDelete)
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("tab_desapropriacao_individual_status_evento");
            this.Property(t => t.ise_id).HasColumnName("ise_id");
            this.Property(t => t.ise_data).HasColumnName("ise_data");
            this.Property(t => t.ise_valor).HasColumnName("ise_valor");
            this.Property(t => t.ise_cadastro_dt).HasColumnName("ise_cadastro_dt");
            this.Property(t => t.set_id).HasColumnName("set_id");
            this.Property(t => t.din_id).HasColumnName("din_id");
            this.Property(t => t.userCreate).HasColumnName("userCreate");
            this.Property(t => t.dtCreate).HasColumnName("dtCreate");
            this.Property(t => t.userLastUpdate).HasColumnName("userLastUpdate");
            this.Property(t => t.dtLastUpdate).HasColumnName("dtLastUpdate");
            this.Property(t => t.userDelete).HasColumnName("userDelete");
            this.Property(t => t.dtDelete).HasColumnName("dtDelete");

            // Relationships
            this.HasRequired(t => t.tab_desapropriacao_individual)
                .WithMany(t => t.tab_desapropriacao_individual_status_evento)
                .HasForeignKey(d => d.din_id);
            this.HasRequired(t => t.tab_desapropriacao_individual_status_evento_tipo)
                .WithMany(t => t.tab_desapropriacao_individual_status_evento)
                .HasForeignKey(d => d.set_id);

        }
    }
}
