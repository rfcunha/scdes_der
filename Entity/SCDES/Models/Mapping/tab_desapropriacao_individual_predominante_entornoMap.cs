using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entity.SCDES.Models.Mapping
{
    public class tab_desapropriacao_individual_predominante_entornoMap : EntityTypeConfiguration<tab_desapropriacao_individual_predominante_entorno>
    {
        public tab_desapropriacao_individual_predominante_entornoMap()
        {
            // Primary Key
            this.HasKey(t => t.ipe_id);

            // Properties
            this.Property(t => t.ipe_nome)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.userCreate)
                .HasMaxLength(150);

            this.Property(t => t.userLastUpdate)
                .HasMaxLength(150);

            this.Property(t => t.userDelete)
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("tab_desapropriacao_individual_predominante_entorno");
            this.Property(t => t.ipe_id).HasColumnName("ipe_id");
            this.Property(t => t.ipe_nome).HasColumnName("ipe_nome");
            this.Property(t => t.ipe_ativo).HasColumnName("ipe_ativo");
            this.Property(t => t.ipe_cadastro_dt).HasColumnName("ipe_cadastro_dt");
            this.Property(t => t.userCreate).HasColumnName("userCreate");
            this.Property(t => t.dtCreate).HasColumnName("dtCreate");
            this.Property(t => t.userLastUpdate).HasColumnName("userLastUpdate");
            this.Property(t => t.dtLastUpdate).HasColumnName("dtLastUpdate");
            this.Property(t => t.userDelete).HasColumnName("userDelete");
            this.Property(t => t.dtDelete).HasColumnName("dtDelete");
        }
    }
}
