using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entity.SCDES.Models.Mapping
{
    public class tab_desapropriacao_historicoMap : EntityTypeConfiguration<tab_desapropriacao_historico>
    {
        public tab_desapropriacao_historicoMap()
        {
            // Primary Key
            this.HasKey(t => t.dhi_id);

            // Properties
            this.Property(t => t.dhi_descricao)
                .IsRequired()
                .HasMaxLength(500);

            this.Property(t => t.dhi_usuario)
                .IsRequired()
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("tab_desapropriacao_historico");
            this.Property(t => t.dhi_id).HasColumnName("dhi_id");
            this.Property(t => t.dhi_descricao).HasColumnName("dhi_descricao");
            this.Property(t => t.dhi_usuario).HasColumnName("dhi_usuario");
            this.Property(t => t.dhi_cadastro_dt).HasColumnName("dhi_cadastro_dt");
            this.Property(t => t.des_id).HasColumnName("des_id");

            // Relationships
            this.HasRequired(t => t.tab_desapropriacao)
                .WithMany(t => t.tab_desapropriacao_historico)
                .HasForeignKey(d => d.des_id);

        }
    }
}
