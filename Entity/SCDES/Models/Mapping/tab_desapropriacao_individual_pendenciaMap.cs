using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entity.SCDES.Models.Mapping
{
    public class tab_desapropriacao_individual_pendenciaMap : EntityTypeConfiguration<tab_desapropriacao_individual_pendencia>
    {
        public tab_desapropriacao_individual_pendenciaMap()
        {
            // Primary Key
            this.HasKey(t => t.ipe_id);

            // Properties
            this.Property(t => t.ipe_descricao)
                .IsRequired()
                .HasMaxLength(500);

            this.Property(t => t.ipe_cadastro_usuario)
                .IsRequired()
                .HasMaxLength(150);

            this.Property(t => t.ipe_concluido_usuario)
                .HasMaxLength(150);

            this.Property(t => t.userCreate)
                .HasMaxLength(150);

            this.Property(t => t.userLastUpdate)
                .HasMaxLength(150);

            this.Property(t => t.userDelete)
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("tab_desapropriacao_individual_pendencia");
            this.Property(t => t.ipe_id).HasColumnName("ipe_id");
            this.Property(t => t.ipe_descricao).HasColumnName("ipe_descricao");
            this.Property(t => t.ipe_cadastro_usuario).HasColumnName("ipe_cadastro_usuario");
            this.Property(t => t.ipe_cadastro_dt).HasColumnName("ipe_cadastro_dt");
            this.Property(t => t.ipe_status).HasColumnName("ipe_status");
            this.Property(t => t.ipe_concluido_usuario).HasColumnName("ipe_concluido_usuario");
            this.Property(t => t.ipe_concluido_dt).HasColumnName("ipe_concluido_dt");
            this.Property(t => t.din_id).HasColumnName("din_id");
            this.Property(t => t.usu_id).HasColumnName("usu_id");
            this.Property(t => t.userCreate).HasColumnName("userCreate");
            this.Property(t => t.dtCreate).HasColumnName("dtCreate");
            this.Property(t => t.userLastUpdate).HasColumnName("userLastUpdate");
            this.Property(t => t.dtLastUpdate).HasColumnName("dtLastUpdate");
            this.Property(t => t.userDelete).HasColumnName("userDelete");
            this.Property(t => t.dtDelete).HasColumnName("dtDelete");

            // Relationships
            this.HasRequired(t => t.tab_desapropriacao_individual)
                .WithMany(t => t.tab_desapropriacao_individual_pendencia)
                .HasForeignKey(d => d.din_id);

            this.Ignore(x => x.ipe_acao);

        }
    }
}
