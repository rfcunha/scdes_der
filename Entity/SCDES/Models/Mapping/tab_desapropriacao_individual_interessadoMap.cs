using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entity.SCDES.Models.Mapping
{
    public class tab_desapropriacao_individual_interessadoMap : EntityTypeConfiguration<tab_desapropriacao_individual_interessado>
    {
        public tab_desapropriacao_individual_interessadoMap()
        {
            // Primary Key
            this.HasKey(t => t.iin_id);

            // Properties
            this.Property(t => t.iin_proprietario)
                .IsRequired()
                .HasMaxLength(150);

            this.Property(t => t.iin_logradouro)
                .HasMaxLength(200);

            this.Property(t => t.iin_logradouro_complemento)
    .HasMaxLength(200);

            this.Property(t => t.iin_rg_rne)
                .HasMaxLength(15);

            this.Property(t => t.iin_cpf_cnpj)
                .HasMaxLength(15);

            this.Property(t => t.iin_email)
                .HasMaxLength(200);

            this.Property(t => t.iin_telefone)
                .HasMaxLength(11);

            this.Property(t => t.iin_celular)
                .HasMaxLength(11);

            this.Property(t => t.iin_observacao)
                .HasMaxLength(500);

            this.Property(t => t.userCreate)
                .HasMaxLength(150);

            this.Property(t => t.userLastUpdate)
                .HasMaxLength(150);

            this.Property(t => t.userDelete)
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("tab_desapropriacao_individual_interessado");
            this.Property(t => t.iin_id).HasColumnName("iin_id");
            this.Property(t => t.iin_responsavel).HasColumnName("iin_responsavel");
            this.Property(t => t.iin_proprietario).HasColumnName("iin_proprietario");
            this.Property(t => t.iin_logradouro).HasColumnName("iin_logradouro");
            this.Property(t => t.iin_logradouro_complemento).HasColumnName("iin_logradouro_complemento");
            this.Property(t => t.iin_rg_rne).HasColumnName("iin_rg_rne");
            this.Property(t => t.iin_cpf_cnpj).HasColumnName("iin_cpf_cnpj");
            this.Property(t => t.iin_email).HasColumnName("iin_email");
            this.Property(t => t.iin_telefone).HasColumnName("iin_telefone");
            this.Property(t => t.iin_celular).HasColumnName("iin_celular");
            this.Property(t => t.iin_observacao).HasColumnName("iin_observacao");
            this.Property(t => t.iin_cadastro_dt).HasColumnName("iin_cadastro_dt");
            this.Property(t => t.din_id).HasColumnName("din_id");
            this.Property(t => t.userCreate).HasColumnName("userCreate");
            this.Property(t => t.dtCreate).HasColumnName("dtCreate");
            this.Property(t => t.userLastUpdate).HasColumnName("userLastUpdate");
            this.Property(t => t.dtLastUpdate).HasColumnName("dtLastUpdate");
            this.Property(t => t.userDelete).HasColumnName("userDelete");
            this.Property(t => t.dtDelete).HasColumnName("dtDelete");

            // Relationships
            this.HasRequired(t => t.tab_desapropriacao_individual)
                .WithMany(t => t.tab_desapropriacao_individual_interessado)
                .HasForeignKey(d => d.din_id);

        }
    }
}
