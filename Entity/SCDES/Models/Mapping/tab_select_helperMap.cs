﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity.ModelConfiguration;

namespace Entity.SCDES.Models.Mapping
{
    public class tab_select_helperMap : EntityTypeConfiguration<tab_select_helper>
    {
        public tab_select_helperMap()
        {
            // Primary Key
            this.HasKey(t => t.seh_id);

            // Properties
            this.Property(t => t.seh_nome_exibicao)
                .HasMaxLength(100);

            this.Property(t => t.seh_value)
                .HasMaxLength(150);

            this.Property(t => t.UserCreate)
                .HasMaxLength(50);

            this.Property(t => t.CreationTime)
                .HasColumnType("datetime2");

            // Table & Column Mappings
            this.ToTable("tab_select_helper");
            this.Property(t => t.seh_id).HasColumnName("seh_id");
            this.Property(t => t.seh_nome_exibicao).HasColumnName("seh_nome_exibicao");
            this.Property(t => t.seh_ativo).HasColumnName("seh_ativo");
            this.Property(t => t.seh_value).HasColumnName("seh_value");
            this.Property(t => t.CreationTime).HasColumnName("CreationTime");
            this.Property(t => t.UserCreate).HasColumnName("UserCreate");
            this.Property(t => t.shg_id).HasColumnName("shg_id");

            this.HasRequired(t => t.tab_select_helper_guia)
                .WithMany(t => t.tab_select_helper_campo)
                .HasForeignKey(d => d.shg_id);
        }
    }
}
