using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entity.SCDES.Models.Mapping
{
    public class tab_desapropriacao_departamentoMap : EntityTypeConfiguration<tab_desapropriacao_departamento>
    {
        public tab_desapropriacao_departamentoMap()
        {
            // Primary Key
            this.HasKey(t => t.dde_id);

            // Properties
            this.Property(t => t.dde_nome)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.userCreate)
                .HasMaxLength(150);

            this.Property(t => t.userLastUpdate)
                .HasMaxLength(150);

            this.Property(t => t.userDelete)
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("tab_desapropriacao_departamento");
            this.Property(t => t.dde_id).HasColumnName("dde_id");
            this.Property(t => t.dde_nome).HasColumnName("dde_nome");
            this.Property(t => t.dde_ativo).HasColumnName("dde_ativo");
            this.Property(t => t.dde_cadastro_dt).HasColumnName("dde_cadastro_dt");
            this.Property(t => t.userCreate).HasColumnName("userCreate");
            this.Property(t => t.dtCreate).HasColumnName("dtCreate");
            this.Property(t => t.userLastUpdate).HasColumnName("userLastUpdate");
            this.Property(t => t.dtLastUpdate).HasColumnName("dtLastUpdate");
            this.Property(t => t.userDelete).HasColumnName("userDelete");
            this.Property(t => t.dtDelete).HasColumnName("dtDelete");
        }
    }
}
