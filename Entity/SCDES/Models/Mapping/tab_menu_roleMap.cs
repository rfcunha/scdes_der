using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entity.SCDES.Models.Mapping
{
    public class tab_menu_roleMap : EntityTypeConfiguration<tab_menu_role>
    {
        public tab_menu_roleMap()
        {
            // Primary Key
            this.HasKey(t => t.mro_id);

            // Properties
            this.Property(t => t.mro_nome)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.mro_controle)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("tab_menu_role");
            this.Property(t => t.mro_id).HasColumnName("mro_id");
            this.Property(t => t.mro_nome).HasColumnName("mro_nome");
            this.Property(t => t.mro_controle).HasColumnName("mro_controle");
            this.Property(t => t.mro_ativo).HasColumnName("mro_ativo");
            this.Property(t => t.mro_dt_cadastro).HasColumnName("mro_dt_cadastro");
            this.Property(t => t.men_id).HasColumnName("men_id");

            // Relationships
            this.HasRequired(t => t.tab_menu)
                .WithMany(t => t.tab_menu_role)
                .HasForeignKey(d => d.men_id);

        }
    }
}
