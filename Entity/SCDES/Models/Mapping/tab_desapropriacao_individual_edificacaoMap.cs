using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entity.SCDES.Models.Mapping
{
    public class tab_desapropriacao_individual_edificacaoMap : EntityTypeConfiguration<tab_desapropriacao_individual_edificacao>
    {
        public tab_desapropriacao_individual_edificacaoMap()
        {
            // Primary Key
            this.HasKey(t => t.ied_id);

            // Properties
            this.Property(t => t.ied_tipologia)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.ied_pavimento_numero)
                .HasMaxLength(5);

            this.Property(t => t.ied_area_construida)
                .HasMaxLength(20);

            this.Property(t => t.ied_implantacao)
                .HasMaxLength(50);

            this.Property(t => t.ied_conservacao_estado)
                .HasMaxLength(50);

            this.Property(t => t.ied_padrao_construtivo)
                .HasMaxLength(50);

            this.Property(t => t.userCreate)
                .HasMaxLength(150);

            this.Property(t => t.userLastUpdate)
                .HasMaxLength(150);

            this.Property(t => t.userDelete)
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("tab_desapropriacao_individual_edificacao");
            this.Property(t => t.ied_id).HasColumnName("ied_id");
            this.Property(t => t.ied_tipologia).HasColumnName("ied_tipologia");
            this.Property(t => t.ied_pavimento_numero).HasColumnName("ied_pavimento_numero");
            this.Property(t => t.ied_area_construida).HasColumnName("ied_area_construida");
            this.Property(t => t.ied_valor).HasColumnName("ied_valor");
            this.Property(t => t.ied_implantacao).HasColumnName("ied_implantacao");
            this.Property(t => t.ied_conservacao_estado).HasColumnName("ied_conservacao_estado");
            this.Property(t => t.ied_padrao_construtivo).HasColumnName("ied_padrao_construtivo");
            this.Property(t => t.ied_cadastro_dt).HasColumnName("ied_cadastro_dt");
            this.Property(t => t.din_id).HasColumnName("din_id");
            this.Property(t => t.userCreate).HasColumnName("userCreate");
            this.Property(t => t.dtCreate).HasColumnName("dtCreate");
            this.Property(t => t.userLastUpdate).HasColumnName("userLastUpdate");
            this.Property(t => t.dtLastUpdate).HasColumnName("dtLastUpdate");
            this.Property(t => t.userDelete).HasColumnName("userDelete");
            this.Property(t => t.dtDelete).HasColumnName("dtDelete");

            // Relationships
            this.HasRequired(t => t.tab_desapropriacao_individual)
                .WithMany(t => t.tab_desapropriacao_individual_edificacao)
                .HasForeignKey(d => d.din_id);

        }
    }
}
