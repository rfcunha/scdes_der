using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entity.SCDES.Models.Mapping
{
    public class tab_desapropriacao_individual_divisa_anexoMap : EntityTypeConfiguration<tab_desapropriacao_individual_divisa_anexo>
    {
        public tab_desapropriacao_individual_divisa_anexoMap()
        {
            // Primary Key
            this.HasKey(t => t.ida_id);

            // Properties
            this.Property(t => t.ida_anexo)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.ida_anexo_nome)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.ida_anexo_pasta)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.userCreate)
                .HasMaxLength(150);

            this.Property(t => t.userLastUpdate)
                .HasMaxLength(150);

            this.Property(t => t.userDelete)
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("tab_desapropriacao_individual_divisa_anexo");
            this.Property(t => t.ida_id).HasColumnName("ida_id");
            this.Property(t => t.ida_anexo).HasColumnName("ida_anexo");
            this.Property(t => t.ida_anexo_nome).HasColumnName("ida_anexo_nome");
            this.Property(t => t.ida_anexo_pasta).HasColumnName("ida_anexo_pasta");
            this.Property(t => t.ida_ordem).HasColumnName("ida_ordem");
            this.Property(t => t.ida_cadastro_dt).HasColumnName("ida_cadastro_dt");
            this.Property(t => t.idi_id).HasColumnName("idi_id");
            this.Property(t => t.userCreate).HasColumnName("userCreate");
            this.Property(t => t.dtCreate).HasColumnName("dtCreate");
            this.Property(t => t.userLastUpdate).HasColumnName("userLastUpdate");
            this.Property(t => t.dtLastUpdate).HasColumnName("dtLastUpdate");
            this.Property(t => t.userDelete).HasColumnName("userDelete");
            this.Property(t => t.dtDelete).HasColumnName("dtDelete");

            // Relationships
            this.HasRequired(t => t.tab_desapropriacao_individual_divisa)
                .WithMany(t => t.tab_desapropriacao_individual_divisa_anexo)
                .HasForeignKey(d => d.idi_id);

        }
    }
}
