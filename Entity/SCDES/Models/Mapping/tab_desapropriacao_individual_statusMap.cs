using System.Data.Entity.ModelConfiguration;

namespace Entity.SCDES.Models.Mapping
{
    public class tab_desapropriacao_individual_statusMap : EntityTypeConfiguration<tab_desapropriacao_individual_status>
    {
        public tab_desapropriacao_individual_statusMap()
        {
            // Primary Key
            this.HasKey(t => t.ist_id);

            // Properties
            this.Property(t => t.ist_nome)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.userCreate)
                .HasMaxLength(150);

            this.Property(t => t.userLastUpdate)
                .HasMaxLength(150);

            this.Property(t => t.userDelete)
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("tab_desapropriacao_individual_status");
            this.Property(t => t.ist_id).HasColumnName("ist_id");
            this.Property(t => t.ist_nome).HasColumnName("ist_nome");
            this.Property(t => t.ist_ativo).HasColumnName("ist_ativo");
            this.Property(t => t.ist_cadastro_dt).HasColumnName("ist_cadastro_dt");
            this.Property(t => t.userCreate).HasColumnName("userCreate");
            this.Property(t => t.dtCreate).HasColumnName("dtCreate");
            this.Property(t => t.userLastUpdate).HasColumnName("userLastUpdate");
            this.Property(t => t.dtLastUpdate).HasColumnName("dtLastUpdate");
            this.Property(t => t.userDelete).HasColumnName("userDelete");
            this.Property(t => t.dtDelete).HasColumnName("dtDelete");
        }
    }
}
