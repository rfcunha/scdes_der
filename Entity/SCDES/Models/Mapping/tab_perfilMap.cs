using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entity.SCDES.Models.Mapping
{
    public class tab_perfilMap : EntityTypeConfiguration<tab_perfil>
    {
        public tab_perfilMap()
        {
            // Primary Key
            this.HasKey(t => t.per_id);

            // Properties
            this.Property(t => t.per_nome)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.userCreate)
                .HasMaxLength(50);

            this.Property(t => t.userLastUpdate)
                .HasMaxLength(50);

            this.Property(t => t.userDelete)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("tab_perfil");
            this.Property(t => t.per_id).HasColumnName("per_id");
            this.Property(t => t.per_nome).HasColumnName("per_nome");
            this.Property(t => t.per_update_xml).HasColumnName("per_update_xml");
            this.Property(t => t.per_ativo).HasColumnName("per_ativo");
            this.Property(t => t.per_dt_cadastro).HasColumnName("per_dt_cadastro");
            this.Property(t => t.per_deletado).HasColumnName("per_deletado");
            this.Property(t => t.userCreate).HasColumnName("userCreate");
            this.Property(t => t.dtCreate).HasColumnName("dtCreate");
            this.Property(t => t.userLastUpdate).HasColumnName("userLastUpdate");
            this.Property(t => t.dtLastUpdate).HasColumnName("dtLastUpdate");
            this.Property(t => t.userDelete).HasColumnName("userDelete");
            this.Property(t => t.dtDelete).HasColumnName("dtDelete");
        }
    }
}
