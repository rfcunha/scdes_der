using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entity.SCDES.Models.Mapping
{
    public class tab_desapropriacao_individual_revisaoMap : EntityTypeConfiguration<tab_desapropriacao_individual_revisao>
    {
        public tab_desapropriacao_individual_revisaoMap()
        {
            // Primary Key
            this.HasKey(t => t.ire_id);

            // Properties
            this.Property(t => t.ire_id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.ire_descricao)
                .IsRequired()
                .HasMaxLength(500);

            this.Property(t => t.ire_revisor)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("tab_desapropriacao_individual_revisao");
            this.Property(t => t.ire_id).HasColumnName("ire_id");
            this.Property(t => t.ire_descricao).HasColumnName("ire_descricao");
            this.Property(t => t.ire_revisor).HasColumnName("ire_revisor");
            this.Property(t => t.ire_cadastro_dt).HasColumnName("ire_cadastro_dt");
            this.Property(t => t.din_id).HasColumnName("din_id");

            // Relationships
            this.HasRequired(t => t.tab_desapropriacao_individual)
                .WithMany(t => t.tab_desapropriacao_individual_revisao)
                .HasForeignKey(d => d.din_id);

        }
    }
}
