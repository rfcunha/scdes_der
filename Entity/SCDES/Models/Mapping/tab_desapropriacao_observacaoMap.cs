using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entity.SCDES.Models.Mapping
{
    public class tab_desapropriacao_observacaoMap : EntityTypeConfiguration<tab_desapropriacao_observacao>
    {
        public tab_desapropriacao_observacaoMap()
        {
            // Primary Key
            this.HasKey(t => t.dob_id);

            // Properties
            this.Property(t => t.dob_descricao).IsRequired().HasMaxLength(500);

            this.Property(t => t.dob_cadastro_usuario).HasMaxLength(150);

            this.Property(t => t.userCreate).HasMaxLength(150);

            this.Property(t => t.userLastUpdate).HasMaxLength(150);

            this.Property(t => t.userDelete).HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("tab_desapropriacao_observacao");
            this.Property(t => t.dob_id).HasColumnName("dob_id");
            this.Property(t => t.dob_descricao).HasColumnName("dob_descricao");
            this.Property(t => t.dob_cadastro_usuario).HasColumnName("dob_cadastro_usuario");
            this.Property(t => t.dob_cadastro_dt).HasColumnName("dob_cadastro_dt");
            this.Property(t => t.des_id).HasColumnName("des_id");
            this.Property(t => t.usu_id).HasColumnName("usu_id");
            this.Property(t => t.userCreate).HasColumnName("userCreate");
            this.Property(t => t.dtCreate).HasColumnName("dtCreate");
            this.Property(t => t.userLastUpdate).HasColumnName("userLastUpdate");
            this.Property(t => t.dtLastUpdate).HasColumnName("dtLastUpdate");
            this.Property(t => t.userDelete).HasColumnName("userDelete");
            this.Property(t => t.dtDelete).HasColumnName("dtDelete");

            // Relationships
            this.HasRequired(t => t.tab_desapropriacao).WithMany(t => t.tab_desapropriacao_observacao).HasForeignKey(d => d.des_id);

            this.Ignore(t => t.dob_acao);
        }
    }
}
