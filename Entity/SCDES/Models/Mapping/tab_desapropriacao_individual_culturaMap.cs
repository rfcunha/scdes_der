using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entity.SCDES.Models.Mapping
{
    public class tab_desapropriacao_individual_culturaMap : EntityTypeConfiguration<tab_desapropriacao_individual_cultura>
    {
        public tab_desapropriacao_individual_culturaMap()
        {
            // Primary Key
            this.HasKey(t => t.icu_id);

            // Properties
            this.Property(t => t.icu_tipo)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.icu_exploracao_estagio)
                .HasMaxLength(100);

            this.Property(t => t.icu_cultivo)
                .HasMaxLength(50);

            this.Property(t => t.icu_area)
                .HasMaxLength(20);

            this.Property(t => t.icu_observacao)
                .HasMaxLength(500);

            this.Property(t => t.userCreate)
                .HasMaxLength(150);

            this.Property(t => t.userLastUpdate)
                .HasMaxLength(150);

            this.Property(t => t.userDelete)
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("tab_desapropriacao_individual_cultura");
            this.Property(t => t.icu_id).HasColumnName("icu_id");
            this.Property(t => t.icu_tipo).HasColumnName("icu_tipo");
            this.Property(t => t.icu_exploracao_estagio).HasColumnName("icu_exploracao_estagio");
            this.Property(t => t.icu_cultivo).HasColumnName("icu_cultivo");
            this.Property(t => t.icu_area).HasColumnName("icu_area");
            this.Property(t => t.icu_valor_total).HasColumnName("icu_valor_total");
            this.Property(t => t.icu_observacao).HasColumnName("icu_observacao");
            this.Property(t => t.icu_cadastro_dt).HasColumnName("icu_cadastro_dt");
            this.Property(t => t.din_id).HasColumnName("din_id");
            this.Property(t => t.userCreate).HasColumnName("userCreate");
            this.Property(t => t.dtCreate).HasColumnName("dtCreate");
            this.Property(t => t.userLastUpdate).HasColumnName("userLastUpdate");
            this.Property(t => t.dtLastUpdate).HasColumnName("dtLastUpdate");
            this.Property(t => t.userDelete).HasColumnName("userDelete");
            this.Property(t => t.dtDelete).HasColumnName("dtDelete");

            // Relationships
            this.HasOptional(t => t.tab_desapropriacao_individual)
                .WithMany(t => t.tab_desapropriacao_individual_cultura)
                .HasForeignKey(d => d.din_id);

        }
    }
}
