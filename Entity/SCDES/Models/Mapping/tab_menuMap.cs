using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entity.SCDES.Models.Mapping
{
    public class tab_menuMap : EntityTypeConfiguration<tab_menu>
    {
        public tab_menuMap()
        {
            // Primary Key
            this.HasKey(t => t.men_id);

            // Properties
            this.Property(t => t.men_menu)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.men_hierarquia)
                .HasMaxLength(100);

            this.Property(t => t.men_pagina)
                .HasMaxLength(50);

            this.Property(t => t.men_url)
                .HasMaxLength(100);

            this.Property(t => t.men_icon)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("tab_menu");
            this.Property(t => t.men_id).HasColumnName("men_id");
            this.Property(t => t.men_menu).HasColumnName("men_menu");
            this.Property(t => t.men_hierarquia).HasColumnName("men_hierarquia");
            this.Property(t => t.men_pagina).HasColumnName("men_pagina");
            this.Property(t => t.men_url).HasColumnName("men_url");
            this.Property(t => t.men_ordem).HasColumnName("men_ordem");
            this.Property(t => t.men_pai).HasColumnName("men_pai");
            this.Property(t => t.men_ativo).HasColumnName("men_ativo");
            this.Property(t => t.men_dt_cadastro).HasColumnName("men_dt_cadastro");
            this.Property(t => t.men_icon).HasColumnName("men_icon");
            this.Property(t => t.men_exibir).HasColumnName("men_exibir");
        }
    }
}
