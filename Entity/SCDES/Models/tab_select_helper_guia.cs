﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.SCDES.Models
{
    [Table("tab_select_helper_guia", Schema = "dbo")]
    public class tab_select_helper_guia
    {
        [Key]
        public int shg_id { get; set; }

        public string shg_nome { get; set; }

        public bool shg_ativo { get; set; }

        [MaxLength(50)]
        public string shg_nome_exibicao { get; set; }

        [InverseProperty("tab_select_helper_guia")]
        public virtual List<tab_select_helper> tab_select_helper_campo { get; set; }
    }
}
