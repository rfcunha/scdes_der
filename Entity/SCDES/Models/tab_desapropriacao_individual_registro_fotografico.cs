using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Entity.SCDES.Enum;

namespace Entity.SCDES.Models
{
    public partial class tab_desapropriacao_individual_registro_fotografico
    {
        public int irf_id { get; set; }
        public DateTime irf_data { get; set; }
        public string irf_descricao { get; set; }
        public short irf_ordem { get; set; }
        public string irf_anexo { get; set; }
        public string irf_anexo_nome { get; set; }
        public System.DateTime irf_cadastro_dt { get; set; }
        public int din_id { get; set; }

        public int? aid_id { get; set; }
        [ForeignKey("aid_id")]
        public tab_anexo_identificador tab_anexo_identificador { get; set; }

        [NotMapped]
        public acao irf_acao { get; set; }
        public string userCreate { get; set; }
        public Nullable<System.DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<System.DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<System.DateTime> dtDelete { get; set; }

        [ForeignKey("din_id")]
        public virtual tab_desapropriacao_individual tab_desapropriacao_individual { get; set; }
    }
}
