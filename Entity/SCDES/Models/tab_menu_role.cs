using System;
using System.Collections.Generic;

namespace Entity.SCDES.Models
{
    public partial class tab_menu_role
    {
        public tab_menu_role()
        {
            this.tab_perfil_menu_role = new List<tab_perfil_menu_role>();
        }

        public int mro_id { get; set; }
        public string mro_nome { get; set; }
        public string mro_controle { get; set; }
        public bool mro_ativo { get; set; }
        public System.DateTime mro_dt_cadastro { get; set; }
        public int men_id { get; set; }
        public virtual tab_menu tab_menu { get; set; }
        public virtual ICollection<tab_perfil_menu_role> tab_perfil_menu_role { get; set; }
    }
}
