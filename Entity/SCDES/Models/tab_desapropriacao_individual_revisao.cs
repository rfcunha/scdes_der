using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Entity.SCDES.Enum;

namespace Entity.SCDES.Models
{
    public partial class tab_desapropriacao_individual_revisao
    {
        [Key]
        public int ire_id { get; set; }
        public string ire_descricao { get; set; }
        public string ire_revisor { get; set; }
        public System.DateTime ire_cadastro_dt { get; set; }
        public int din_id { get; set; }
        public virtual tab_desapropriacao_individual tab_desapropriacao_individual { get; set; }

        [NotMapped]
        public acao ire_acao { get; set; }
    }
}
