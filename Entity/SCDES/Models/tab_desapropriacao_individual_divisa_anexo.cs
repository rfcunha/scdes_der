using System;
using System.Collections.Generic;

namespace Entity.SCDES.Models
{
    public partial class tab_desapropriacao_individual_divisa_anexo
    {
        public int ida_id { get; set; }
        public string ida_anexo { get; set; }
        public string ida_anexo_nome { get; set; }
        public string ida_anexo_pasta { get; set; }
        public short ida_ordem { get; set; }
        public System.DateTime ida_cadastro_dt { get; set; }
        public int idi_id { get; set; }
        public string userCreate { get; set; }
        public Nullable<System.DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<System.DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<System.DateTime> dtDelete { get; set; }
        public virtual tab_desapropriacao_individual_divisa tab_desapropriacao_individual_divisa { get; set; }
    }
}
