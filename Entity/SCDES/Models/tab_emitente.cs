using System;

namespace Entity.SCDES.Models
{
    public partial class tab_emitente
    {
        public int emi_id { get; set; }
        public string emi_nome { get; set; }
        public string emi_email { get; set; }
        public string emi_telefone { get; set; }
        public string emi_celular { get; set; }
        public string emi_observacao { get; set; }
        public bool emi_ativo { get; set; }
        public string userCreate { get; set; }
        public Nullable<DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<DateTime> dtDelete { get; set; }
    }
}
