using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Entity.SCDES.Enum;

namespace Entity.SCDES.Models
{
    public partial class tab_desapropriacao_individual_interessado
    {
        public int iin_id { get; set; }
        public bool iin_responsavel { get; set; }
        public string iin_proprietario { get; set; }
        public string iin_logradouro { get; set; }
        public string iin_logradouro_complemento { get; set; }
        public string iin_rg_rne { get; set; }
        public string iin_cpf_cnpj { get; set; }
        public string iin_email { get; set; }
        public string iin_telefone { get; set; }
        public string iin_celular { get; set; }
        public string iin_observacao { get; set; }
        public System.DateTime iin_cadastro_dt { get; set; }
        public int din_id { get; set; }
        public string userCreate { get; set; }
        public Nullable<System.DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<System.DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<System.DateTime> dtDelete { get; set; }
        public virtual tab_desapropriacao_individual tab_desapropriacao_individual { get; set; }

        [NotMapped]
        public acao iin_acao { get; set; }
    }
}
