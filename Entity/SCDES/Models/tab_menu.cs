using System;
using System.Collections.Generic;

namespace Entity.SCDES.Models
{
    public partial class tab_menu
    {
        public tab_menu()
        {
            this.tab_menu_role = new List<tab_menu_role>();
        }

        public int men_id { get; set; }
        public string men_menu { get; set; }
        public string men_hierarquia { get; set; }
        public string men_pagina { get; set; }
        public string men_url { get; set; }
        public int men_ordem { get; set; }
        public Nullable<int> men_pai { get; set; }
        public bool men_ativo { get; set; }
        public System.DateTime men_dt_cadastro { get; set; }
        public string men_icon { get; set; }
        public Nullable<bool> men_exibir { get; set; }
        public virtual ICollection<tab_menu_role> tab_menu_role { get; set; }
    }
}
