using System;
using System.Collections.Generic;

namespace Entity.SCDES.Models
{
    public partial class tab_perfil_menu_role
    {
        public int pmr_id { get; set; }
        public System.DateTime pmr_dt_cadastro { get; set; }
        public int per_id { get; set; }
        public int mro_id { get; set; }
        public virtual tab_menu_role tab_menu_role { get; set; }
        public virtual tab_perfil tab_perfil { get; set; }
    }
}
