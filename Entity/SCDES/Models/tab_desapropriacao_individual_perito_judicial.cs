using System;
using System.Collections.Generic;

namespace Entity.SCDES.Models
{
    public partial class tab_desapropriacao_individual_perito_judicial
    {
        public tab_desapropriacao_individual_perito_judicial()
        {
            this.tab_desapropriacao_individual = new List<tab_desapropriacao_individual>();
        }

        public int ipj_id { get; set; }
        public string ipj_nome { get; set; }
        public string ipj_email { get; set; }
        public string ipj_telefone { get; set; }
        public string ipj_celular { get; set; }
        public string ipj_observacao { get; set; }
        public bool ipj_ativo { get; set; }
        public System.DateTime ipj_cadastro_dt { get; set; }
        public string userCreate { get; set; }
        public Nullable<System.DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<System.DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<System.DateTime> dtDelete { get; set; }
        public virtual ICollection<tab_desapropriacao_individual> tab_desapropriacao_individual { get; set; }
    }
}
