using System;
using System.Collections.Generic;

namespace Entity.SCDES.Models
{
    public partial class tab_desapropriacao_recurso_fonte
    {
        public int drf_id { get; set; }
        public decimal drf_percentual { get; set; }
        public decimal drf_valor { get; set; }
        public string drf_contrato_numero { get; set; }
        public Nullable<System.DateTime> drf_assinatura_dt { get; set; }
        public System.DateTime drf_cadastro_dt { get; set; }
        public int des_id { get; set; }
        public string userCreate { get; set; }
        public Nullable<System.DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<System.DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<System.DateTime> dtDelete { get; set; }
        public virtual tab_desapropriacao tab_desapropriacao { get; set; }

        public drf_acao drf_acao { get; set; }
    }

    public enum drf_acao
    {
        Insert = 1,
        Update = 2,
        Delete = 3
    }
}
