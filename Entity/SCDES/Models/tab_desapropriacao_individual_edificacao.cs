using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Entity.SCDES.Enum;

namespace Entity.SCDES.Models
{
    public partial class tab_desapropriacao_individual_edificacao
    {
        public tab_desapropriacao_individual_edificacao()
        {
            //this.tab_desapropriacao_individual_edificacao_material = new List<tab_desapropriacao_individual_edificacao_material>();
        }

        public int ied_id { get; set; }
        public string ied_tipologia { get; set; }
        public string ied_pavimento_numero { get; set; }
        public string ied_area_construida { get; set; }
        public decimal ied_valor { get; set; }
        public string ied_implantacao { get; set; }
        public string ied_conservacao_estado { get; set; }
        public string ied_padrao_construtivo { get; set; }
        public System.DateTime ied_cadastro_dt { get; set; }

        [NotMapped]
        public acao ied_acao { get; set; }

        public int din_id { get; set; }
        public string userCreate { get; set; }
        public Nullable<System.DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<System.DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<System.DateTime> dtDelete { get; set; }
        public virtual tab_desapropriacao_individual tab_desapropriacao_individual { get; set; }
        public virtual ICollection<tab_desapropriacao_individual_edificacao_material> tab_desapropriacao_individual_edificacao_material { get; set; }
    }
}
