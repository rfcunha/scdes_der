using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Entity.SCDES.Enum;

namespace Entity.SCDES.Models
{
    public partial class tab_desapropriacao_individual
    {
        public tab_desapropriacao_individual()
        {
            this.tab_desapropriacao_individual_anexos = new List<tab_desapropriacao_individual_anexos>();
            this.tab_desapropriacao_individual_cultura = new List<tab_desapropriacao_individual_cultura>();
            this.tab_desapropriacao_individual_divisa = new List<tab_desapropriacao_individual_divisa>();
            this.tab_desapropriacao_individual_edificacao = new List<tab_desapropriacao_individual_edificacao>();
            this.tab_desapropriacao_individual_entrada = new List<tab_desapropriacao_individual_entrada>();
            this.tab_desapropriacao_individual_instalacao = new List<tab_desapropriacao_individual_instalacao>();
            this.tab_desapropriacao_individual_interessado = new List<tab_desapropriacao_individual_interessado>();
            this.tab_desapropriacao_individual_pendencia = new List<tab_desapropriacao_individual_pendencia>();
            this.tab_desapropriacao_individual_registro_fotografico = new List<tab_desapropriacao_individual_registro_fotografico>();
            this.tab_desapropriacao_individual_revisao = new List<tab_desapropriacao_individual_revisao>();
            this.tab_desapropriacao_individual_estrada_acesso = new List<tab_desapropriacao_individual_estrada_acesso>();
            this.tab_desapropriacao_individual_status_evento = new List<tab_desapropriacao_individual_status_evento>();
            this.tab_desapropriacao_individual_planta_propriedade = new List<tab_desapropriacao_individual_planta_propriedade>();

            din_guid = Guid.NewGuid().ToString();
            din_cadastro_dt = DateTime.Now;
            dtCreate = DateTime.Now;
        }

        public tab_desapropriacao_individual(tab_desapropriacao_empreendimento dem) : this()
        {
            this.dem_id = dem.dem_id;
            this.tab_desapropriacao_empreendimento = dem;
        }

        public int din_id { get; set; }
        public string din_guid { get; set; }
        public string din_geral_documento_numero { get; set; }
        public string din_geral_revisao { get; set; }
        public DateTime? din_geral_emissao_dt { get; set; }
        public Nullable<int> din_geral_emp_id { get; set; }
        public string din_geral_elaborador { get; set; }
        public string din_geral_responsavel_tecnico { get; set; }
        public string din_geral_objeto { get; set; }

        public int dem_id { get; set; }

        public string din_propriedade_cep { get; set; }
        public string din_propriedade_logradouro_numero { get; set; }
        public string din_propriedade_logradouro { get; set; }
        public string din_propriedade_logradouro_complemento { get; set; }
        public Nullable<int> din_propriedade_mun_id { get; set; }
        public string din_propriedade_bairro { get; set; }
        public Nullable<short> din_propriedade_contribuinte_tipo { get; set; }
        public string din_propriedade_contribuinte_numero { get; set; }
        public string din_propriedade_cartorio { get; set; }
        public string din_propriedade_matricula { get; set; }
        public Nullable<short> din_propriedade_desapropriacao { get; set; }
        public string din_propriedade_comarca { get; set; }
        public string din_propriedade_estaca_inicial { get; set; }
        public string din_propriedade_estaca_final { get; set; }
        public string din_propriedade_km_inicial { get; set; }
        public string din_propriedade_km_final { get; set; }
        public string din_propriedade_latitude { get; set; }
        public string din_propriedade_longitude { get; set; }
        public Nullable<short> din_propriedade_dominio { get; set; }
        public Nullable<short> din_propriedade_prioridade { get; set; }
        public string din_propriedade_autos_numero { get; set; }
        public Nullable<System.DateTime> din_propriedade_autos_dt { get; set; }

        public Nullable<bool> din_conducao_anuencia { get; set; }
        public Nullable<decimal> din_conducao_valor_total { get; set; }
        public Nullable<int> din_conducao_isi_id { get; set; }
        public string din_conducao_isi_obs { get; set; }
        public Nullable<int> din_conducao_ist_id { get; set; }
        public Nullable<int> din_conducao_ipj_id { get; set; }
        public Nullable<int> din_conducao_iat_id { get; set; }
        public string din_conducao_numero_processo_unificado { get; set; }

        public Nullable<int> din_imovel_ipe_id { get; set; }
        public Nullable<int> din_imovel_zti_id { get; set; }
        public string din_imovel_infraestrutura { get; set; }
        public string din_imovel_equipamento { get; set; }
        public string din_imovel_servico { get; set; }
        public string din_imovel_area_terreno { get; set; }
        public string din_imovel_area_necessaria { get; set; }
        public string din_imovel_area_porcentagem { get; set; }
        public string din_imovel_perimetro { get; set; }
        public Nullable<int> din_imovel_top_id { get; set; }
        public Nullable<int> din_imovel_sco_id { get; set; }
        public string din_imovel_restricao_uso { get; set; }
        public string din_imovel_curso_dagua { get; set; }
        public string din_imovel_recurso_minerais { get; set; }
        public string din_imovel_terreno_ocupacoes { get; set; }
        public string din_imovel_sus { get; set; } // Mudar para string
        public string din_imovel_delimitacao { get; set; }
        public string din_imovel_atividade_economica { get; set; }
        public string din_imovel_posicionamento { get; set; }
        public string din_imovel_frontal { get; set; }
        public string din_imovel_fundo { get; set; }
        public string din_imovel_lateral_esquerda { get; set; }
        public string din_imovel_lateral_direita { get; set; }

        public string din_benfeitorias { get; set; }

        public string din_memorial_descritivo { get; set; }

        public string din_planta_propriedade { get; set; }

        public string din_laudo_avaliacao { get; set; }

        public System.DateTime din_cadastro_dt { get; set; }
        public string userCreate { get; set; }
        public Nullable<System.DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<System.DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<System.DateTime> dtDelete { get; set; }

        [NotMapped]
        public acao din_acao { get; set; }

        public virtual tab_desapropriacao_empreendimento tab_desapropriacao_empreendimento { get; set; }

        public virtual ICollection<tab_desapropriacao_individual_anexos> tab_desapropriacao_individual_anexos { get; set; }
        public virtual ICollection<tab_desapropriacao_individual_cultura> tab_desapropriacao_individual_cultura { get; set; }
        public virtual ICollection<tab_desapropriacao_individual_divisa> tab_desapropriacao_individual_divisa { get; set; }
        public virtual ICollection<tab_desapropriacao_individual_edificacao> tab_desapropriacao_individual_edificacao { get; set; }
        public virtual ICollection<tab_desapropriacao_individual_entrada> tab_desapropriacao_individual_entrada { get; set; }
        public virtual ICollection<tab_desapropriacao_individual_instalacao> tab_desapropriacao_individual_instalacao { get; set; }
        public virtual ICollection<tab_desapropriacao_individual_interessado> tab_desapropriacao_individual_interessado { get; set; }
        public virtual ICollection<tab_desapropriacao_individual_pendencia> tab_desapropriacao_individual_pendencia { get; set; }
        public virtual ICollection<tab_desapropriacao_individual_planta_propriedade> tab_desapropriacao_individual_planta_propriedade { get; set; }
        public virtual ICollection<tab_desapropriacao_individual_registro_fotografico> tab_desapropriacao_individual_registro_fotografico { get; set; }
        public virtual ICollection<tab_desapropriacao_individual_revisao> tab_desapropriacao_individual_revisao { get; set; }
        public virtual ICollection<tab_desapropriacao_individual_estrada_acesso> tab_desapropriacao_individual_estrada_acesso { get; set; }
        public virtual ICollection<tab_desapropriacao_individual_status_evento> tab_desapropriacao_individual_status_evento { get; set; }
        public virtual tab_desapropriacao_individual_assistente_tecnico tab_desapropriacao_individual_assistente_tecnico { get; set; }
        public virtual tab_desapropriacao_individual_perito_judicial tab_desapropriacao_individual_perito_judicial { get; set; }
        public virtual tab_desapropriacao_individual_predominante_entorno tab_desapropriacao_individual_predominante_entorno { get; set; }
        public virtual tab_desapropriacao_individual_situacao tab_desapropriacao_individual_situacao { get; set; }
        public virtual tab_desapropriacao_individual_status tab_desapropriacao_individual_status { get; set; }

    }
}
