using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;

namespace Entity.SCDES.Models
{
    public partial class tab_desapropriacao_empreendimento
    {
        public tab_desapropriacao_empreendimento()
        {
            this.tab_desapropriacao_individual = new List<tab_desapropriacao_individual>();
        }

        public int dem_id { get; set; }
        public string dem_auto_decreto { get; set; }
        public short dem_lote { get; set; }
        public string dem_rodovia_sp { get; set; }
        public string dem_rodovia_nome { get; set; }
        public string dem_rodovia_trecho { get; set; }
        public string dem_rodovia_km_inicial { get; set; }
        public string dem_rodovia_km_final { get; set; }
        public Nullable<System.DateTime> dem_dup_dt { get; set; }
        public string dem_dup_codigo { get; set; }
        public string dem_nota_reserva { get; set; }
        public int des_id { get; set; }
        public string dem_anexo { get; set; }
        public string dem_anexo_nome { get; set; }
        public string dem_anexo_pasta { get; set; }
        public System.DateTime dem_cadastro_dt { get; set; }
        public string userCreate { get; set; }
        public Nullable<System.DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<System.DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<System.DateTime> dtDelete { get; set; }
        public virtual tab_desapropriacao tab_desapropriacao { get; set; }

        public dem_acao dem_acao { get; set; }

        [NotMapped]
        public bool dem_update_anexo { get; set; }

        public HttpPostedFileBase dem_file_upload { get; set; }

        public virtual ICollection<tab_desapropriacao_individual> tab_desapropriacao_individual { get; set; }
    }

    public enum dem_acao
    {
        Insert = 1,
        Update = 2,
        Delete = 3
    }
}
