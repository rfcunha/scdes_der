﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using Entity.SCDES.Enum;

namespace Entity.SCDES.Models
{
    [Table("tab_desapropriacao_individual_planta_propriedade", Schema = "dbo")]
    public class tab_desapropriacao_individual_planta_propriedade
    {
        [Key]
        public int ipp_id { get; set; }

        [StringLength(100)]
        public string ipp_descricao { get; set; }

        public string ipp_anexo { get; set; }

        public string ipp_anexo_nome { get; set; }

        public string ipp_anexo_pasta { get; set; }

        public DateTime? ipp_anexo_dt { get; set; }

        public int? ipp_anexo_identificador_id { get; set; }

        [ForeignKey("ipp_anexo_identificador_id")]
        public virtual tab_anexo_identificador tab_anexo_identificador { get; set; }

        public int din_id { get; set; }
        [ForeignKey("din_id")]
        public virtual tab_desapropriacao_individual tab_desapropriacao_individual { get; set; }

        [NotMapped]
        public acao ipp_acao { get; set; }

        public string userCreate { get; set; }
        public Nullable<System.DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<System.DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<System.DateTime> dtDelete { get; set; }
    }
}
