using System;
using System.Collections.Generic;

namespace Entity.SCDES.Models
{
    public partial class tab_empresa_usuario
    {
        public int eus_id { get; set; }
        public System.DateTime eus_cadastro_dt { get; set; }
        public int emp_id { get; set; }
        public int usu_id { get; set; }
    }
}
