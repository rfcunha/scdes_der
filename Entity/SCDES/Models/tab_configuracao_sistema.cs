using System;
using System.Collections.Generic;

namespace Entity.SCDES.Models
{
    public partial class tab_configuracao_sistema
    {
        public int csi_id { get; set; }
        public string csi_modulo { get; set; }
        public string csi_nome { get; set; }
        public string csi_funcao { get; set; }
        public string csi_valor { get; set; }
    }
}
