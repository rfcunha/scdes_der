﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.SCDES.Models
{
    [Table("tab_anexo", Schema = "dbo")]
    public class tab_anexo
    {
        [Key]
        public int ane_id { get; set; }

        public int aid_id { get; set; }

        [ForeignKey("aid_id")]
        public tab_anexo_identificador tab_anexo_identificador { get; set; }

        public string ane_nome_original { get; set; }
        public string ane_nome { get; set; }
        public string ane_pasta { get; set; }
        public string ane_mime_type { get; set; }
        public int ane_ordem { get; set; }
        public string ane_legenda { get; set; }



        [NotMapped]
        public HttpPostedFileBase ane_arquivo { get; set; }

        public string userCreate { get; set; }
        public Nullable<DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<DateTime> dtDelete { get; set; }
    }
}
