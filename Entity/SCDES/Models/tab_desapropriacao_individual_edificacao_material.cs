using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Entity.SCDES.Enum;

namespace Entity.SCDES.Models
{
    public partial class tab_desapropriacao_individual_edificacao_material
    {
        public int iem_id { get; set; }
        public string iem_material { get; set; }
        public string iem_quantidade { get; set; }
        public string iem_descricao { get; set; }
        public decimal iem_valor_total { get; set; }
        public System.DateTime iem_cadastro_dt { get; set; }
        public int ied_id { get; set; }

        [NotMapped]
        public acao iem_acao { get; set; }

        public string userCreate { get; set; }
        public Nullable<System.DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<System.DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<System.DateTime> dtDelete { get; set; }

        public virtual tab_desapropriacao_individual_edificacao tab_desapropriacao_individual_edificacao { get; set; }
    }
}
