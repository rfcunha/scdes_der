using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Entity.SCDES.Enum;

namespace Entity.SCDES.Models
{
    public partial class tab_desapropriacao_individual_cultura
    {
        public int icu_id { get; set; }
        public string icu_tipo { get; set; }
        public string icu_exploracao_estagio { get; set; }
        public string icu_cultivo { get; set; }
        public string icu_area { get; set; }
        public decimal icu_valor_total { get; set; }
        public string icu_observacao { get; set; }
        public System.DateTime icu_cadastro_dt { get; set; }

        public int? icu_anexo_identificador_id { get; set; }
        [ForeignKey("icu_anexo_identificador_id")]
        public tab_anexo_identificador tab_anexo_identificador { get; set; }

        [NotMapped]
        public acao icu_acao { get; set; }

        public Nullable<int> din_id { get; set; }
        public string userCreate { get; set; }
        public Nullable<System.DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<System.DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<System.DateTime> dtDelete { get; set; }
        public virtual tab_desapropriacao_individual tab_desapropriacao_individual { get; set; }
    }
}
