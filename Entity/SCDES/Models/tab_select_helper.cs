﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.SCDES.Models
{
    [Table("tab_select_helper", Schema = "dbo")]
    public class tab_select_helper
    {
        [Key]
        public int seh_id { get; set; }

        public string seh_value { get; set; }

        public string seh_nome_exibicao { get; set; }

        public string seh_nome_campo { get; set; }

        public bool seh_ativo { get; set; }

        public int shg_id { get; set; }

        [ForeignKey("shg_id")]
        public virtual tab_select_helper_guia tab_select_helper_guia { get; set; }
        
        public string UserCreate { get; set; }
        public DateTime? CreationTime { get; set; }        
    }
}
