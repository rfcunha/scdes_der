﻿using System;

namespace Entity.SCDES.Utils.Gdv
{
    public class tab_desapropriacao_individual_assistente_tecnico_gdv
    {
        public int iat_id { get; set; }
        public string iat_nome { get; set; }
        public string iat_email { get; set; }
        public string iat_telefone { get; set; }
        public string iat_celular { get; set; }
        public bool iat_ativo { get; set; }
        public DateTime iat_cadastro_dt { get; set; }
        
        
        public string userCreate { get; set; }
        public Nullable<DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<DateTime> dtDelete { get; set; }
    }
}
