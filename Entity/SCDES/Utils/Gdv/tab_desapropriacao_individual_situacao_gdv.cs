﻿using System;

namespace Entity.SCDES.Utils.Gdv
{
    public class tab_desapropriacao_individual_situacao_gdv
    {
        public int isi_id { get; set; }
        public string isi_nome { get; set; }
        public bool isi_ativo { get; set; }
        public DateTime isi_cadastro_dt { get; set; }
        public string userCreate { get; set; }
        public Nullable<DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<DateTime> dtDelete { get; set; }
    }
}
