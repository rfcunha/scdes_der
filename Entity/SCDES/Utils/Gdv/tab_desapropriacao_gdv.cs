using System;

namespace Entity.SCDES.Utils.Gdv
{
    public class tab_desapropriacao_gdv
    {
        public int des_id { get; set; }
        public string des_projeto_codigo { get; set; }
        public string des_nome { get; set; }
        public DateTime des_inicio { get; set; }
        public DateTime des_fim { get; set; }
        public Decimal? des_valor_estimado { get; set; }
        public DateTime des_cadastro_dt { get; set; }
        public string dde_nome { get; set; }
        public string dge_nome { get; set; }
        //public string dti_nome { get; set; }
        public string dst_nome { get; set; }
        public string reg_nome { get; set; }
    }
}
