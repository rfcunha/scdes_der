﻿using System;

namespace Entity.SCDES.Utils.Gdv
{
    public class tab_desapropriacao_individual_status_evento_tipo_gdv
    {
        public int set_id { get; set; }
        public string set_nome { get; set; }
        public bool set_valor { get; set; }
        public DateTime? set_data { get; set; }
        public bool set_ativo { get; set; }
        public DateTime set_cadastro_dt { get; set; }
        public string userCreate { get; set; }
        public Nullable<DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<DateTime> dtDelete { get; set; }
    }
}
