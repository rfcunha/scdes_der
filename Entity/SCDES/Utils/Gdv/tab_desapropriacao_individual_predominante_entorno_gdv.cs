using System;

namespace Entity.SCDES.Utils.Gdv
{
    public class tab_desapropriacao_individual_predominante_entorno_gdv
    {
        public int ipe_id { get; set; }
        public string ipe_nome { get; set; }
        public bool ipe_ativo { get; set; }
        public DateTime ipe_cadastro_dt { get; set; }
        public string userCreate { get; set; }
        public DateTime? dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public DateTime? dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public DateTime? dtDelete { get; set; }
    }
}
