using System;

namespace Entity.SCDES.Utils.Gdv
{
    public class tab_desapropriacao_departamento_gdv
    {
        public int dde_id { get; set; }
        public string dde_nome { get; set; }
        public bool dde_ativo { get; set; }
        public DateTime dde_cadastro_dt { get; set; }
        public bool dde_deletado { get; set; }
        public string userCreate { get; set; }
        public DateTime? dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public DateTime? dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public DateTime? dtDelete { get; set; }
    }
}
