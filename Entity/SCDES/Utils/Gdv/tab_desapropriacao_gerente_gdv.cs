using System;

namespace Entity.SCDES.Utils.Gdv
{
    public class tab_desapropriacao_gerente_gdv
    {
        public int dge_id { get; set; }
        public string dge_nome { get; set; }
        public bool dge_ativo { get; set; }
        public DateTime dge_cadastro_dt { get; set; }
        public string userCreate { get; set; }
        public DateTime? dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public DateTime? dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public DateTime? dtDelete { get; set; }
    }
}
