﻿using System;

namespace Entity.SCDES.Utils.Gdv
{
    public class tab_audit_gdv
    {
        public int AuditID { get; set; }
        public string Type { get; set; }
        public string TableName { get; set; }
        public string PrimaryKeyField { get; set; }
        public string PrimaryKeyValue { get; set; }
        public string FieldName { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
        public Nullable<DateTime> UpdateDate { get; set; }
        public string UserName { get; set; }
    }
}
