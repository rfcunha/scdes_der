﻿using System;

namespace Entity.SCDES.Utils.Gdv
{
    public class tab_desapropriacao_individual_perito_judicial_gdv
    {
        public int ipj_id { get; set; }
        public string ipj_nome { get; set; }
        public string ipj_email { get; set; }
        public string ipj_telefone { get; set; }
        public string ipj_celular { get; set; }
        public bool ipj_ativo { get; set; }
        public DateTime ipj_cadastro_dt { get; set; }
        
        
        public string userCreate { get; set; }
        public Nullable<DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<DateTime> dtDelete { get; set; }
    }
}
