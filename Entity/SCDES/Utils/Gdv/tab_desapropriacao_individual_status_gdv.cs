using System;

namespace Entity.SCDES.Utils.Gdv
{
    public class tab_desapropriacao_individual_status_gdv
    {
        public int ist_id { get; set; }
        public string ist_nome { get; set; }
        public bool ist_ativo { get; set; }
        public System.DateTime ist_cadastro_dt { get; set; }
        public string userCreate { get; set; }
        public Nullable<System.DateTime> dtCreate { get; set; }
        public string userLastUpdate { get; set; }
        public Nullable<System.DateTime> dtLastUpdate { get; set; }
        public string userDelete { get; set; }
        public Nullable<System.DateTime> dtDelete { get; set; }

    }
}
