﻿using System;

namespace Entity.SCDES.Utils.Gdv
{
    public class tab_desapropriacao_individual_gdv
    {
        public int din_id { get; set; }
        public string din_geral_documento_numero { get; set; }
        public string din_geral_elaborador { get; set; }
        public string din_geral_objeto { get; set; }
        public int? din_conducao_ist_id { get; set; }
        public DateTime? din_geral_emissao_dt { get; set; }
        public string din_empreendimento_rodovia_sp { get; set; }
        public string din_empreendimento_rodovia_nome { get; set; }
    }
}