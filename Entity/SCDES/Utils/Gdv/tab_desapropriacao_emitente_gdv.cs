﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entity.SCDES.Models;

namespace Entity.SCDES.Utils.Gdv
{
    public class tab_desapropriacao_emitente_gdv
    {
        
        public int dee_id { get; set; }

        public DateTime dee_cadastro_dt { get; set; }

        public int emp_id { get; set; }

        public string emp_nome { get; set; }

        public int des_id { get; set; }

        public string userCreate { get; set; }

        public Nullable<DateTime> dtCreate { get; set; }

        public string userLastUpdate { get; set; }

        public Nullable<DateTime> dtLastUpdate { get; set; }

        public string userDelete { get; set; }

        public Nullable<DateTime> dtDelete { get; set; }
    }
}
