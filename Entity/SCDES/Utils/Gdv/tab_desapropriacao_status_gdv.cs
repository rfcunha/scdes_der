using System;

namespace Entity.SCDES.Utils.Gdv
{
    public class tab_desapropriacao_status_gdv
    {
        public int dst_id { get; set; }
        public string dst_nome { get; set; }
        public bool dst_ativo { get; set; }
        public DateTime dst_cadastro_dt { get; set; }

    }
}
