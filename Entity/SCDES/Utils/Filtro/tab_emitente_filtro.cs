using Entity.GERENCIADORPORTAL.Utils.Filtro;

namespace Entity.SCDES.Utils.Filtro
{
    public class tab_emitente_filtro : DefaultFiltro
    {
        public string emi_nome { get; set; }
        public bool? emi_ativo { get; set; }
    }
}
