using Entity.Utils;

namespace Entity.SCDES.Utils.Filtro
{
    public class tab_desapropriacao_departamento_filtro:DefaultFiltro
    {
        public string dde_nome { get; set; }
        public bool? dde_ativo { get; set; }
    }
}
