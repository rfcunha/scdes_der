using Entity.Utils;

namespace Entity.SCDES.Utils.Filtro
{
    public class tab_desapropriacao_status_filtro:DefaultFiltro
    {
        public string dst_nome { get; set; }
        public bool? dst_ativo { get; set; }
    }
}
