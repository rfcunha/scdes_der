using Entity.Utils;

namespace Entity.SCDES.Utils.Filtro
{
    public class tab_desapropriacao_individual_status_filtro:DefaultFiltro
    {
        public string ist_nome { get; set; }
        public bool? ist_ativo { get; set; }
    }
}
