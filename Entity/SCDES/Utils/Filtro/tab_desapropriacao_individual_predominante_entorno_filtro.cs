using Entity.Utils;

namespace Entity.SCDES.Utils.Filtro
{
    public class tab_desapropriacao_individual_predominante_entorno_filtro : DefaultFiltro
    {
        public string ipe_nome { get; set; }
        public bool? ipe_ativo { get; set; }
    }
}
