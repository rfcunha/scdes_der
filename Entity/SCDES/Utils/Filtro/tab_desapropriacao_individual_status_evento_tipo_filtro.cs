﻿using Entity.Utils;
using System;

namespace Entity.SCDES.Utils.Filtro
{
    public class tab_desapropriacao_individual_status_evento_tipo_filtro : DefaultFiltro
    {
        public string set_nome { get; set; }
        public bool? set_valor { get; set; }
        public DateTime? set_data { get; set; }
        public bool? set_ativo { get; set; }
    }
}
