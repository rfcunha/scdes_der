using Entity.GERENCIADORPORTAL.Utils.Filtro;

namespace Entity.SCDES.Utils.Filtro
{
    public class tab_desapropriacao_individual_assistente_tecnico_filtro : DefaultFiltro
    {
        public string iat_nome { get; set; }
        public bool? iat_ativo { get; set; }
    }
}
