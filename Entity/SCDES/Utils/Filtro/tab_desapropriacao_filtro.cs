using System;
using System.Collections.Generic;
using Entity.SCDES.Models;
using Entity.Utils;

namespace Entity.SCDES.Utils.Filtro
{
    public class tab_desapropriacao_filtro:DefaultFiltro
    {
        public string des_projeto_codigo { get; set; }
        public string des_nome { get; set; }
        public Nullable<int> dde_id { get; set; }
        public Nullable<decimal> des_valor_estimado { get; set; }
        public Nullable<int> dti_id { get; set; }
        public Nullable<int> dst_id { get; set; }
        public Nullable<int> reg_id { get; set; }
        public Nullable<int> afi_id { get; set; }
        public Nullable<DateTime>dem_dup_dt { get; set; }
        public string dem_dup_codigo { get; set; }
        public string dem_nota_reserva { get; set; }

    }
}
