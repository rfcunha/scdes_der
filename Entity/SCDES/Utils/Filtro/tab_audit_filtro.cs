﻿using Entity.Utils;

namespace Entity.SCDES.Utils.Filtro
{
    public class tab_audit_filtro : DefaultFiltro
    {
        public string Type { get; set; }
        public string TableName { get; set; }
        public string PrimaryKeyField { get; set; }
        public string PrimaryKeyValue { get; set; }
        public string FieldName { get; set; }
        public string UserName { get; set; }
    }
}
