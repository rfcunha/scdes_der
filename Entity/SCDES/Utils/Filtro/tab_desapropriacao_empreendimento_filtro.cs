﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entity.Utils;

namespace Entity.SCDES.Utils.Filtro
{
    public class tab_desapropriacao_empreendimento_filtro : DefaultFiltro
    {
        public int dem_id { get; set; }

        public string dem_auto_decreto  { get; set; }

        public short dem_lote { get; set; }
    }
}
