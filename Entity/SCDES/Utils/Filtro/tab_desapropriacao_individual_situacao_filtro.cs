using Entity.GERENCIADORPORTAL.Utils.Filtro;

namespace Entity.SCDES.Utils.Filtro
{
    public partial class tab_desapropriacao_individual_situacao_filtro : DefaultFiltro
    {
        public string isi_nome { get; set; }
        public bool? isi_ativo { get; set; }
    }
}
