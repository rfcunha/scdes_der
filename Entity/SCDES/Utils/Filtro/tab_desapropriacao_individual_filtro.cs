﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entity.Utils;

namespace Entity.SCDES.Utils.Filtro
{
    public class tab_desapropriacao_individual_filtro : DefaultFiltro
    {
        public string din_geral_documento_numero { get; set; }
        public string din_geral_elaborador { get; set; }
        public string din_propriedade_autos_numero { get; set; }
        public short? din_propriedade_prioridade { get; set; }
        public string din_interessados_proprietario { get; set; }
        public string din_geral_objeto { get; set; }
        public DateTime? din_geral_emissao_dt { get; set; }
        public int? afi_id { get; set; }
        public string din_empreendimento_auto_decreto { get; set; }
        public string din_empreendimento_dup_codigo { get; set; }
        public DateTime? din_empreendimento_dup_dt { get; set; }
        public int? din_conducao_ist_id { get; set; }
        public int? reg_id { get; set; }
        public string din_empreendimento_rodovia_sp { get; set; }
        public string din_empreendimento_rodovia_nome { get; set; }
    }
}
