using Entity.GERENCIADORPORTAL.Utils.Filtro;

namespace Entity.SCDES.Utils.Filtro
{
    public partial class tab_desapropriacao_individual_perito_judicial_filtro:DefaultFiltro
    {
        public string ipj_nome { get; set; }
        public bool? ipj_ativo { get; set; }
    }
}
