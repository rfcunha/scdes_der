﻿using System.ComponentModel.DataAnnotations;

namespace Entity.SCDES.Enum
{
    public enum tab_desapropriacao_individual_contribuinte_tipo
    {
        [Display(Name = "Incra")]
        Incra = 1,
        [Display(Name = "Iptu")]
        Iptu = 2
    }
}
