﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Entity.SCDES.Enum
{
    public class tab_desapropriacao_individual_propriedade
    {

        public enum propriedade_dominio
        {
            [Display(Name = "Público")]
            Publico = 1,
            [Display(Name = "Privado")]
            Privado = 2
        }

        public enum propriedade_desapropriacao
        {
            [Display(Name = "Parcial")]
            Parcial = 1,
            [Display(Name = "Total")]
            Total = 2
        }


        public enum propriedade_prioridade
        {
            [Display(Name = "Baixa")]
            Baixa = 1,
            [Display(Name = "Média")]
            Média = 2,
            [Display(Name = "Alta")]
            Alta = 3,
            [Display(Name = "Crítico")]
            Critico = 4
        }


    }
}
