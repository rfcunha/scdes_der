﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Dao.CONTEXT;
using Entity.SCDES.Models;
using Entity.SCDES.Utils.Filtro;
using Entity.SCDES.Utils.Gdv;
using Suporte.Exceptions;
using Suporte.Formatacao;
using Suporte.MsgBox;

namespace Dao.SCDES
{
    public class tab_desapropriacao_individual_status_evento_tipo_dao : DefaultDao<tab_desapropriacao_individual_status_evento_tipo, SCDESContext>
    {
        public override bool DELETE(tab_desapropriacao_individual_status_evento_tipo obj)
        {
            try
            {
                using (var _ctx = new SCDESContext())
                {
                    _ctx.tab_desapropriacao_individual_status_evento_tipo.AddOrUpdate(obj);
                    _ctx.SaveChanges();
                }


                using (var _ctx = new SCDESContext())
                {
                    _ctx.Entry(obj).State = EntityState.Deleted;
                    _ctx.SaveChanges();
                }

                return true;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao excluir os dados. <br/>Entre em contato com o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public List<tab_desapropriacao_individual_status_evento_tipo> FIND_ASSETS()
        {
            try
            {
                using (var dbContext = new SCDESContext())
                {
                    return dbContext.tab_desapropriacao_individual_status_evento_tipo.Where(s => s.set_ativo).ToList();
                }
            }
            catch (Exception ex)
            {

                new CustomException("Ocorreu um erro ao buscar os dados. <br/>Entre em contato com o suporte.", bootStrapMessageType.TYPE_DANGER);
            }

            return new List<tab_desapropriacao_individual_status_evento_tipo>();
            
        }

        /// <summary>
        /// PESQUISA PARA GRIVIEW
        /// </summary>
        /// <param name="filtro"></param>
        /// <returns></returns>
        public List<tab_desapropriacao_individual_status_evento_tipo_gdv> PESQUISA_GDV(tab_desapropriacao_individual_status_evento_tipo_filtro filtro)
        {
            try
            {
                SqlParameter[] param =
                {
                    new SqlParameter("@set_nome", SqlDbType.VarChar) { Value   = FormartaDados.SetType<string>(filtro.set_nome) },
                    new SqlParameter("@set_valor", SqlDbType.Bit) { Value      = FormartaDados.SetType<bool?>(filtro.set_valor) },
                    new SqlParameter("@set_data", SqlDbType.Date) { Value      = FormartaDados.SetType<DateTime?>(filtro.set_data) },
                    new SqlParameter("@set_ativo", SqlDbType.Bit) { Value      = FormartaDados.SetType<bool?>(filtro.set_ativo) },
                    new SqlParameter("@PageSize", SqlDbType.Int) { Value       = FormartaDados.SetType<int>(filtro.pageSize) },
                    new SqlParameter("@PageIndex", SqlDbType.Int) { Value      = FormartaDados.SetType<int>(filtro.pageIndex) },
                    new SqlParameter("@PageCount", SqlDbType.Int) { Direction  = ParameterDirection.Output }
                };

                using (var _ctxContext = new SCDESContext())
                {
                    using (var cmd = _ctxContext.Database.Connection.CreateCommand())
                    {
                        cmd.CommandText = "STP_GDV_DESAPROPRIACAO_INDIVIDUAL_STATUS_EVENTO_TIPO";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddRange(param);

                        try
                        {
                            cmd.Connection.Open();

                            var reader = cmd.ExecuteReader();

                            return ((IObjectContextAdapter)_ctxContext).ObjectContext.Translate<tab_desapropriacao_individual_status_evento_tipo_gdv>(reader).ToList();
                        }
                        finally
                        {
                            cmd.Connection.Close();
                            filtro.pageCount = (int)param[param.Count() - 1].Value;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
