﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using Dao.CONTEXT;
using Entity.SCDES.Models;
using Entity.SCDES.Utils.Gdv;
using System.Data.SqlClient;
using System.Runtime.CompilerServices;
using Suporte.Formatacao;

namespace Dao.SCDES
{
    public class tab_desapropriacao_emitente_dao : DefaultDao<tab_desapropriacao_emitente, SCDESContext>
    {
        public List<tab_desapropriacao_emitente_gdv> PESQUISA_GDV_EMITENTES(int desId)
        {
            try
            {
                using (var _dbContext = new SCDESContext())
                {

                    var param = new SqlParameter[1];

                    param[0] = new SqlParameter("@des_id", SqlDbType.Int)
                    {
                        Value = FormartaDados.SetType<int>(desId)
                    };


                    using (var cmd = _dbContext.Database.Connection.CreateCommand())
                    {
                        cmd.CommandText = "STP_GDV_DESAPROPRIACAO_EMITENTE";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddRange(param);

                        try
                        {
                            cmd.Connection.Open();

                            var reader = cmd.ExecuteReader();

                            return ((IObjectContextAdapter)_dbContext).ObjectContext.Translate<tab_desapropriacao_emitente_gdv>(reader).ToList();
                        }
                        finally
                        {
                            cmd.Connection.Close();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
    }
}
