using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using Dao.CONTEXT;
using Entity.SCDES.Models;
using Entity.SCDES.Utils.Filtro;
using Entity.SCDES.Utils.Gdv;
using Suporte.Exceptions;
using Suporte.Formatacao;
using Suporte.MsgBox;

namespace Dao.SCDES
{
    public class tab_emitente_dao : DefaultDao<tab_emitente, SCDESContext>
    {
        /// <summary>
        /// PESQUISA PARA GRIVIEW
        /// </summary>
        /// <param name="filtro"></param>
        /// <returns></returns>
        public List<tab_emitente_gdv> PESQUISA_GDV(tab_emitente_filtro filtro)
        {
            try
            {
                SqlParameter[] param =
                {
                    new SqlParameter("@emi_nome", SqlDbType.VarChar) {Value   = FormartaDados.SetType<string>(filtro.emi_nome)},
                    new SqlParameter("@emi_ativo", SqlDbType.Bit) {Value      = FormartaDados.SetType<bool?>(filtro.emi_ativo)},
                    new SqlParameter("@PageSize", SqlDbType.Int) {Value       = FormartaDados.SetType<int>(filtro.pageSize)},
                    new SqlParameter("@PageIndex", SqlDbType.Int) {Value      = FormartaDados.SetType<int>(filtro.pageIndex)},
                    new SqlParameter("@PageCount", SqlDbType.Int) {Direction  = ParameterDirection.Output}
                };

                using (var _ctxContext = new SCDESContext())
                {
                    using (var cmd = _ctxContext.Database.Connection.CreateCommand())
                    {
                        cmd.CommandText = "STP_GDV_EMITENTE";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddRange(param);

                        try
                        {
                            cmd.Connection.Open();

                            var reader = cmd.ExecuteReader();

                            return ((IObjectContextAdapter)_ctxContext).ObjectContext.Translate<tab_emitente_gdv>(reader).ToList();
                        }
                        finally
                        {
                            cmd.Connection.Close();
                            filtro.pageCount = (int)param[param.Count() - 1].Value;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
