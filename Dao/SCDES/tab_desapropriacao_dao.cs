using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using Dao.CONTEXT;
using Entity.SCDES.Models;
using Entity.SCDES.Utils.Filtro;
using Entity.SCDES.Utils.Gdv;
using Suporte.Exceptions;
using Suporte.Formatacao;
using Suporte.MsgBox;
using System.Data.Entity.Validation;

namespace Dao.SCDES
{
    public class tab_desapropriacao_dao : DefaultDao<tab_desapropriacao, SCDESContext>
    {
        public override bool UPDATE(tab_desapropriacao obj)
        {
            try
            {
                using (var _ctx = new SCDESContext())
                {
                    obj.tab_desapropriacao_empreendimento.ToList().ForEach(t =>
                    {
                        t.des_id = obj.des_id;

                        switch ((int)t.dem_acao)
                        {
                            case 1:
                            case 2:
                            case 3:
                                _ctx.Configuration.ValidateOnSaveEnabled = false;
                                _ctx.Configuration.AutoDetectChangesEnabled = true;
                                _ctx.tab_desapropriacao_empreendimento.AddOrUpdate(t);

                                break;
                        }
                    });


                    obj.tab_desapropriacao_localidade.ToList().ForEach(t =>
                    {
                        t.des_id = obj.des_id;

                        switch ((int)t.dlo_acao)
                        {
                            case 1:
                            case 3:
                                _ctx.Configuration.ValidateOnSaveEnabled = false;
                                _ctx.Configuration.AutoDetectChangesEnabled = true;
                                _ctx.tab_desapropriacao_localidade.AddOrUpdate(t);

                                break;
                        }
                    });

                    obj.tab_desapropriacao_recurso_fonte.ToList().ForEach(t =>
                    {
                        t.des_id = obj.des_id;

                        switch ((int)t.drf_acao)
                        {
                            case 1:
                            case 2:
                            case 3:
                                _ctx.Configuration.ValidateOnSaveEnabled = false;
                                _ctx.Configuration.AutoDetectChangesEnabled = true;
                                _ctx.tab_desapropriacao_recurso_fonte.AddOrUpdate(t);

                                break;
                        }
                    });


                    obj.tab_desapropriacao_observacao.ToList().ForEach(t =>
                    {
                        t.des_id = obj.des_id;

                        switch ((int)t.dob_acao)
                        {
                            case 1:
                            case 2:
                            case 3:
                                _ctx.Configuration.ValidateOnSaveEnabled = false;
                                _ctx.Configuration.AutoDetectChangesEnabled = true;
                                _ctx.tab_desapropriacao_observacao.AddOrUpdate(t);

                                break;
                        }
                    });

                    obj.tab_desapropriacao_emitente.ToList().ForEach(t =>
                    {
                        t.des_id = obj.des_id;

                        switch ((int)t.dee_acao)
                        {
                            case 1:
                            case 2:
                            case 3:
                                _ctx.Configuration.ValidateOnSaveEnabled = false;
                                _ctx.Configuration.AutoDetectChangesEnabled = true;
                                _ctx.tab_desapropriacao_emitente.AddOrUpdate(t);
                                break;
                        }
                    });

                    //obj.tab_desapropriacao_individual.ToList().ForEach(t =>
                    //{
                    //    t.des_id = obj.des_id;

                    //    switch ((int)t.din_acao)
                    //    {
                    //        case 1:
                    //        case 2:
                    //        case 3:
                    //            _ctx.Configuration.ValidateOnSaveEnabled = false;
                    //            _ctx.Configuration.AutoDetectChangesEnabled = true;
                    //            _ctx.tab_desapropriacao_individual.AddOrUpdate(t);
                    //            break;
                    //    }
                    //});


                    _ctx.tab_desapropriacao.AddOrUpdate(obj);
                    _ctx.SaveChanges();
                }


                #region | DELETAR OS ITENS P/ LOG|

                using (var _ctx = new SCDESContext())
                {
                    obj.tab_desapropriacao_empreendimento.Where(pendencia => pendencia.dem_acao == dem_acao.Delete).ToList().ForEach(t => { _ctx.Entry(t).State = EntityState.Deleted; });
                    obj.tab_desapropriacao_localidade.Where(pendencia => pendencia.dlo_acao == dlo_acao.Delete).ToList().ForEach(t => { _ctx.Entry(t).State = EntityState.Deleted; });
                    obj.tab_desapropriacao_recurso_fonte.Where(pendencia => pendencia.drf_acao == drf_acao.Delete).ToList().ForEach(t => { _ctx.Entry(t).State = EntityState.Deleted; });
                    obj.tab_desapropriacao_observacao.Where(pendencia => pendencia.dob_acao == dob_acao.Delete).ToList().ForEach(t => { _ctx.Entry(t).State = EntityState.Deleted; });
                    obj.tab_desapropriacao_emitente.Where(pendencia => pendencia.dee_acao == dee_acao.Delete).ToList().ForEach(t => { _ctx.Entry(t).State = EntityState.Deleted; });

                    _ctx.SaveChanges();
                }

                #endregion


                return true;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao alterar os dados. <br/>Entre em contato com o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public bool DELETAR(int id, string userDelete)
        {
            try
            {

                using (var dbContext = new SCDESContext())
                {

                    var des_obj = dbContext.tab_desapropriacao.Where(t => t.des_id == id)
                        .Include(t => t.tab_desapropriacao_empreendimento).FirstOrDefault();

                    des_obj.tab_desapropriacao_empreendimento.ToList().ForEach(
                        t =>
                        {
                            t.userDelete = userDelete;
                            t.dtDelete = DateTime.Now;
                        });

                    des_obj.userDelete = userDelete;
                    des_obj.dtDelete = DateTime.Now;


                    dbContext.Entry(des_obj).State = EntityState.Modified;

                    dbContext.SaveChanges();

                    return true;
                }

                //var param = new SqlParameter[2];
                //param[0] = new SqlParameter("@des_id", SqlDbType.Int) { Value = FormartaDados.SetType<int>(id) };
                //param[1] = new SqlParameter("@userDelete", SqlDbType.VarChar) { Value = FormartaDados.SetType<string>(userDelete) };


                //using (var _ctxContext = new SCDESContext())
                //{
                //    using (var cmd = _ctxContext.Database.Connection.CreateCommand())
                //    {
                //        cmd.CommandText = "STP_DEL_DESAPROPRIACAO";
                //        cmd.CommandType = CommandType.StoredProcedure;
                //        cmd.Parameters.AddRange(param);

                //        try
                //        {
                //            cmd.Connection.Open();

                //            var reader = cmd.ExecuteReader();

                //            if (reader.RecordsAffected > 0)
                //                return true;
                //            else
                //                return false;
                //        }
                //        finally
                //        {
                //            cmd.Connection.Close();
                //        }
                //    }
                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// PESQUISAR PARA GRIDVIEW
        /// </summary>
        /// <param name="filtro"></param>
        /// <returns></returns>
        public List<tab_desapropriacao_gdv> PESQUISA_GDV_SIMPLES(tab_desapropriacao_filtro filtro)
        {
            try
            {
                var param = new SqlParameter[10];
                //param[0] = new SqlParameter("@boa_numero_geral", SqlDbType.VarChar) { Value = FormartaDados.SetType<string>(filtro.boa_numero_geral) };
                //param[1] = new SqlParameter("@boa_dt_fato", SqlDbType.Bit) { Value = FormartaDados.SetType<DateTime?>(filtro.boa_dt_fato) };
                //param[2] = new SqlParameter("@boa_auditado", SqlDbType.Bit) { Value = FormartaDados.SetType<bool?>(filtro.boa_auditado) };
                //param[3] = new SqlParameter("@boa_ativo", SqlDbType.Bit) { Value = FormartaDados.SetType<bool?>(filtro.boa_ativo) };
                //param[4] = new SqlParameter("@bbc_id", SqlDbType.Int) { Value = FormartaDados.SetType<int?>(filtro.bbc_id) };
                param[5] = new SqlParameter("@dt_cadastro_inicial", SqlDbType.DateTime) { Value = FormartaDados.SetType<DateTime?>(filtro.dt_cadastro_inicial) };
                param[6] = new SqlParameter("@dt_cadastro_final", SqlDbType.DateTime) { Value = FormartaDados.SetType<DateTime?>(filtro.dt_cadastro_final) };

                param[7] = new SqlParameter("@PageSize", SqlDbType.Int) { Value = FormartaDados.SetType<int>(filtro.pageSize) };
                param[8] = new SqlParameter("@PageIndex", SqlDbType.Int) { Value = FormartaDados.SetType<int>(filtro.pageIndex) };
                param[9] = new SqlParameter("@PageCount", SqlDbType.Int) { Direction = ParameterDirection.Output };

                using (var _ctxContext = new SCDESContext())
                {
                    using (var cmd = _ctxContext.Database.Connection.CreateCommand())
                    {
                        cmd.CommandText = "STP_GDV_DESAPROPRIACAO_SIMPLES";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddRange(param);

                        try
                        {
                            cmd.Connection.Open();

                            var reader = cmd.ExecuteReader();

                            return ((IObjectContextAdapter)_ctxContext).ObjectContext.Translate<tab_desapropriacao_gdv>(reader).ToList();
                        }
                        finally
                        {
                            cmd.Connection.Close();
                            filtro.pageCount = (int)param[param.Count() - 1].Value;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// PESQUISAR PARA GRIDVIEW
        /// </summary>
        /// <param name="filtro"></param>
        /// <returns></returns>
        public List<tab_desapropriacao_gdv> PESQUISA_GDV(tab_desapropriacao_filtro filtro)
        {
            try
            {
                SqlParameter[] param =
                {
                    new SqlParameter("@des_projeto_codigo", SqlDbType.VarChar) { Value = FormartaDados.SetType<String>(filtro.des_projeto_codigo) },
                    new SqlParameter("@des_nome", SqlDbType.VarChar) { Value = FormartaDados.SetType<String>(filtro.des_nome) },
                    new SqlParameter("@dst_id", SqlDbType.Int) { Value = FormartaDados.SetType<int?>(filtro.dst_id) },
                    new SqlParameter("@afi_id", SqlDbType.Int) { Value = FormartaDados.SetType<int?>(filtro.afi_id) },
                    new SqlParameter("@reg_id", SqlDbType.Decimal) { Value = FormartaDados.SetType<int?>(filtro.reg_id) },
                    new SqlParameter("@dem_dup_dt", SqlDbType.DateTime) { Value = FormartaDados.SetType<DateTime?>(filtro.dem_dup_dt) },
                    new SqlParameter("@dem_dup_codigo", SqlDbType.VarChar) { Value = FormartaDados.SetType<String>(filtro.dem_dup_codigo) },
                    new SqlParameter("@dem_nota_reserva", SqlDbType.VarChar) { Value = FormartaDados.SetType<String>(filtro.dem_nota_reserva) },

                    new SqlParameter("@PageSize", SqlDbType.Int) { Value = FormartaDados.SetType<int>(filtro.pageSize) },
                    new SqlParameter("@PageIndex", SqlDbType.Int) { Value = FormartaDados.SetType<int>(filtro.pageIndex) },
                    new SqlParameter("@PageCount", SqlDbType.Int) { Direction = ParameterDirection.Output }
                };


                using (var _ctxContext = new SCDESContext())
                {
                    using (var cmd = _ctxContext.Database.Connection.CreateCommand())
                    {
                        cmd.CommandText = "STP_GDV_DESAPROPRIACAO";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddRange(param);

                        try
                        {
                            cmd.Connection.Open();

                            DbDataReader reader;

                            reader = cmd.ExecuteReader();
                            
                            return ((IObjectContextAdapter)_ctxContext).ObjectContext.Translate<tab_desapropriacao_gdv>(reader).ToList();
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                        finally
                        {
                            cmd.Connection.Close();
                            filtro.pageCount = (int)param[param.Count() - 1].Value;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
