using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using Dao.CONTEXT;
using Entity.SCDES.Models;
using Entity.SCDES.Utils.Filtro;
using Entity.SCDES.Utils.Gdv;
using Suporte.Exceptions;
using Suporte.Formatacao;
using Suporte.MsgBox;

namespace Dao.SCDES
{
    public class tab_desapropriacao_status_dao : DefaultDao<tab_desapropriacao_status, SCDESContext>
    {

        public override bool DELETE(tab_desapropriacao_status obj)
        {
            try
            {
                using (var _ctx = new SCDESContext())
                {
                    _ctx.tab_desapropriacao_status.AddOrUpdate(obj);
                    _ctx.SaveChanges();
                }



                using (var _ctx = new SCDESContext())
                {
                    _ctx.Entry(obj).State = EntityState.Deleted;
                    _ctx.SaveChanges();
                }

                return true;
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro ao excluir os dados. <br/>Entre em contato com o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        /// <summary>
        /// PESQUISA PARA GRIVIEW
        /// </summary>
        /// <param name="filtro"></param>
        /// <returns></returns>
        public List<tab_desapropriacao_status_gdv> PESQUISA_GDV(tab_desapropriacao_status_filtro filtro)
        {
            try
            {
                SqlParameter[] param =
                {
                    new SqlParameter("@dst_nome", SqlDbType.VarChar) {Value   = FormartaDados.SetType<string>(filtro.dst_nome)},
                    new SqlParameter("@dst_ativo", SqlDbType.Bit) {Value      = FormartaDados.SetType<bool?>(filtro.dst_ativo)},
                    new SqlParameter("@PageSize", SqlDbType.Int) {Value       = FormartaDados.SetType<int>(filtro.pageSize)},
                    new SqlParameter("@PageIndex", SqlDbType.Int) {Value      = FormartaDados.SetType<int>(filtro.pageIndex)},
                    new SqlParameter("@PageCount", SqlDbType.Int) {Direction  = ParameterDirection.Output}
                };

                using (var _ctxContext = new SCDESContext())
                {
                    using (var cmd = _ctxContext.Database.Connection.CreateCommand())
                    {
                        cmd.CommandText = "STP_GDV_DESAPROPRIACAO_STATUS";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddRange(param);

                        try
                        {
                            cmd.Connection.Open();

                            var reader = cmd.ExecuteReader();

                            return ((IObjectContextAdapter)_ctxContext).ObjectContext.Translate<tab_desapropriacao_status_gdv>(reader).ToList();
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                        finally
                        {
                            cmd.Connection.Close();
                            filtro.pageCount = (int)param[param.Count() - 1].Value;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
