using System;
using System.Collections.Generic;
using System.Linq;
using Dao.CONTEXT;
using Entity.SCDES.Models;
using Entity.SCDES.Utils.Filtro;
using Suporte.Exceptions;
using Suporte.MsgBox;

namespace Dao.SCDES
{
    public class tab_desapropriacao_empreendimento_dao : DefaultDao<tab_desapropriacao_empreendimento, SCDESContext>
    {

        public List<tab_desapropriacao_empreendimento> LISTAR_EMPREENDIMENTOS()
        {

            try
            {
                using (var ctx = new SCDESContext())
                {
                    return ctx.tab_desapropriacao_empreendimento.Where(t => string.IsNullOrEmpty(t.userDelete)).ToList();
                }
            }
            catch (Exception e)
            {
                throw new CustomException("Erro: " + e, bootStrapMessageType.TYPE_DANGER);
                //throw new CustomException("Ocorreu um erro ao alterar os dados. <br/>Entre em contato com o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
        }

        public tab_desapropriacao_empreendimento BUSCAR_EMPREENDIMENTO(tab_desapropriacao_empreendimento_filtro filtro)
        {

            try
            {
                using (var dbContext = new SCDESContext())
                {
                   return dbContext.tab_desapropriacao_empreendimento.FirstOrDefault(x =>
                        x.dem_lote == filtro.dem_lote && x.dem_auto_decreto.Equals(filtro.dem_auto_decreto));
                }
            }
            catch (Exception e)
            {
                throw new CustomException("Erro: " + e, bootStrapMessageType.TYPE_DANGER);
            }
        }

    }
}
