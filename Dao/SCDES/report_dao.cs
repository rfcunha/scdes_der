﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Conexao.AdoProvider.Repositorio;
using Entity.DER;
using Entity.DER.Utils.Filtro;
using Suporte.Criptografia;
using Suporte.Exceptions;
using Suporte.Formatacao;
using Suporte.MsgBox;
using System.Data;

namespace Dao.SCDES
{
    public class report_dao
    {
        private DbBase objetoBase { get; set; }

        public report_dao()
        {
            objetoBase = new DbBase(Cripto.Decrypt(ConfigurationManager.ConnectionStrings["CONNECTION_DEFAULT"].ConnectionString, ConfigurationManager.AppSettings["Cripto"]));
        }

        public DataSet RelatorioDesapropriacao(string numeroAutoDecreto, short? lote)
        {
            try
            {
                var obj = new tab_contratos();

                objetoBase.SetParameter("@NumeroAutoDecreto", FormartaDados.SetType<string>(numeroAutoDecreto));
                objetoBase.SetParameter("@Lote", FormartaDados.SetType<short?>(lote));

                objetoBase.CommandType = CommandType.StoredProcedure;
                objetoBase.CommandText = "STP_REL_DESAPROPRIACAO";

                System.Data.DataSet ds = objetoBase.ExecuteDataSet();

                return ds;
            }
            catch (CustomException ce)
            {
                throw new CustomException(ce.Message, bootStrapMessageType.TYPE_DANGER);
            }
            catch (Exception ex)
            {
                throw new CustomException("Ocorreu um erro desconhecido no sistema.</br>Entre em contato com o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
        }
    }
}
