using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using Dao.CONTEXT;
using Entity.SCDES.Models;
using Entity.SCDES.Utils.Filtro;
using Entity.SCDES.Utils.Gdv;
using Suporte.Exceptions;
using Suporte.Formatacao;

namespace Dao.SCDES
{
    public class tab_desapropriacao_gerente_dao : DefaultDao<tab_desapropriacao_gerente, SCDESContext>
    {
        /// <summary>
        /// PESQUISA PARA GRIVIEW
        /// </summary>
        /// <param name="filtro"></param>
        /// <returns></returns>
        public List<tab_desapropriacao_gerente_gdv> PESQUISA_GDV(tab_desapropriacao_gerente_filtro filtro)
        {
            try
            {
                SqlParameter[] param =
                {
                    new SqlParameter("@dge_nome", SqlDbType.VarChar) {Value   = FormartaDados.SetType<string>(filtro.dge_nome)},
                    new SqlParameter("@dge_ativo", SqlDbType.Bit) {Value      = FormartaDados.SetType<bool?>(filtro.dge_ativo)},
                    new SqlParameter("@PageSize", SqlDbType.Int) {Value       = FormartaDados.SetType<int>(filtro.pageSize)},
                    new SqlParameter("@PageIndex", SqlDbType.Int) {Value      = FormartaDados.SetType<int>(filtro.pageIndex)},
                    new SqlParameter("@PageCount", SqlDbType.Int) {Direction  = ParameterDirection.Output}
                };

                using (var _ctxContext = new SCDESContext())
                {
                    using (var cmd = _ctxContext.Database.Connection.CreateCommand())
                    {
                        cmd.CommandText = "STP_GDV_DESAPROPRIACAO_GERENTE";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddRange(param);

                        try
                        {
                            cmd.Connection.Open();

                            var reader = cmd.ExecuteReader();

                            return ((IObjectContextAdapter)_ctxContext).ObjectContext.Translate<tab_desapropriacao_gerente_gdv>(reader).ToList();
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                        finally
                        {
                            cmd.Connection.Close();
                            filtro.pageCount = (int)param[param.Count() - 1].Value;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
    }
}
