using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using Dao.CONTEXT;
using Entity.SCDES.Models;
using Entity.SCDES.Utils.Filtro;
using Entity.SCDES.Utils.Gdv;
using Suporte.Formatacao;

namespace Dao.SCDES
{
    public class tab_audit_dao : DefaultDao<tab_audit, SCDESContext>
    {
        /// <summary>
        /// PESQUISAR PARA GRIDVIEW
        /// </summary>
        /// <param name="filtro"></param>
        /// <returns></returns>
        public List<tab_audit_gdv> PESQUISA_GDV(tab_audit_filtro filtro)
        {
            try
            {
                SqlParameter[] param =
                {
                    new SqlParameter("@FieldName", SqlDbType.VarChar){Value            = FormartaDados.SetType<string>(filtro.FieldName)},
                    new SqlParameter("@TableName", SqlDbType.VarChar){Value            = FormartaDados.SetType<string>(filtro.TableName)},
                    new SqlParameter("@PrimaryKeyValue", SqlDbType.VarChar){Value      = FormartaDados.SetType<string>(filtro.PrimaryKeyValue)},
                    new SqlParameter("@PrimaryKeyField", SqlDbType.VarChar){Value      = FormartaDados.SetType<string>(filtro.PrimaryKeyField)},
                    new SqlParameter("@Type", SqlDbType.Char) {Value                   = FormartaDados.SetType<string>(filtro.Type)},
                    new SqlParameter("@UserName", SqlDbType.VarChar){Value             = FormartaDados.SetType<string>(filtro.UserName)},
                    new SqlParameter("@dt_cadastro_final", SqlDbType.DateTime){Value   = FormartaDados.SetType<DateTime?>(filtro.dt_cadastro_final)},
                    new SqlParameter("@dt_cadastro_inicial", SqlDbType.DateTime){Value = FormartaDados.SetType<DateTime?>(filtro.dt_cadastro_inicial)},
                    
                    new SqlParameter("@PageSize", SqlDbType.Int) { Value               = FormartaDados.SetType<int>(filtro.pageSize) },
                    new SqlParameter("@PageIndex", SqlDbType.Int) { Value              = FormartaDados.SetType<int>(filtro.pageIndex) },
                    new SqlParameter("@PageCount", SqlDbType.Int) { Direction          = ParameterDirection.Output }
                };

                using (var _ctxContext = new SCDESContext())
                {
                    using (var cmd = _ctxContext.Database.Connection.CreateCommand())
                    {
                        cmd.CommandText = "STP_GDV_AUDIT";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddRange(param);

                        try
                        {
                            cmd.Connection.Open();

                            var reader = cmd.ExecuteReader();

                            return ((IObjectContextAdapter)_ctxContext).ObjectContext.Translate<tab_audit_gdv>(reader).ToList();
                        }
                        finally
                        {
                            cmd.Connection.Close();
                            filtro.pageCount = (int)param[param.Count() - 1].Value;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
