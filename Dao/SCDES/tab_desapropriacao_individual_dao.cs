using System;
using System.Data.Entity.Validation;
using Dao.CONTEXT;
using Entity.SCDES.Models;
using Suporte.Exceptions;
using Suporte.MsgBox;
using System.Collections.Generic;
using Entity.SCDES.Utils.Gdv;
using Entity.SCDES.Utils.Filtro;
using System.Data.SqlClient;
using System.Data;
using Suporte.Formatacao;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Runtime.CompilerServices;
using Entity.SCDES.Enum;

namespace Dao.SCDES
{
    public class tab_desapropriacao_individual_dao : DefaultDao<tab_desapropriacao_individual, SCDESContext>
    {
        public tab_desapropriacao_individual FIND(int id)
        {
            try
            {
                using (var dbContext = new SCDESContext())
                {
                    return dbContext.tab_desapropriacao_individual
                        .Include(t => t.tab_desapropriacao_empreendimento)

                        .Include(t => t.tab_desapropriacao_individual_anexos)
                        .Include(t => t.tab_desapropriacao_individual_anexos.Select(d => d.tab_anexo_identificador))
                        .Include(t => t.tab_desapropriacao_individual_anexos.Select(d => d.tab_anexo_identificador).Select(p => p.tab_anexo))

                        .Include(t => t.tab_desapropriacao_individual_cultura)
                        .Include(t => t.tab_desapropriacao_individual_cultura.Select(d => d.tab_anexo_identificador))
                        .Include(t => t.tab_desapropriacao_individual_cultura.Select(d => d.tab_anexo_identificador).Select(p => p.tab_anexo))

                        .Include(t => t.tab_desapropriacao_individual_divisa)
                        .Include(t => t.tab_desapropriacao_individual_divisa.Select(d => d.tab_anexo_identificador))
                        .Include(t => t.tab_desapropriacao_individual_divisa.Select(d => d.tab_anexo_identificador).Select(p => p.tab_anexo))

                        .Include(t => t.tab_desapropriacao_individual_edificacao)
                        .Include(t => t.tab_desapropriacao_individual_edificacao.Select(d => d.tab_desapropriacao_individual_edificacao_material))

                        .Include(t => t.tab_desapropriacao_individual_entrada)
                        .Include(t => t.tab_desapropriacao_individual_entrada.Select(d => d.tab_anexo_identificador))
                        .Include(t => t.tab_desapropriacao_individual_entrada.Select(d => d.tab_anexo_identificador).Select(p => p.tab_anexo))
                        
                        .Include(t => t.tab_desapropriacao_individual_instalacao)
                        .Include(t => t.tab_desapropriacao_individual_instalacao.Select(d => d.tab_anexo_identificador))
                        .Include(t => t.tab_desapropriacao_individual_instalacao.Select(d => d.tab_anexo_identificador).Select(p => p.tab_anexo))

                        .Include(t => t.tab_desapropriacao_individual_interessado)
                        .Include(t => t.tab_desapropriacao_individual_pendencia)
                        .Include(t => t.tab_desapropriacao_individual_registro_fotografico)
                        .Include(t => t.tab_desapropriacao_individual_registro_fotografico.Select(d => d.tab_anexo_identificador))
                        .Include(t => t.tab_desapropriacao_individual_registro_fotografico.Select(d => d.tab_anexo_identificador).Select(p => p.tab_anexo))
                        .Include(t => t.tab_desapropriacao_individual_revisao)

                        .Include(t => t.tab_desapropriacao_individual_estrada_acesso)
                        .Include(t => t.tab_desapropriacao_individual_estrada_acesso.Select(d => d.tab_anexo_identificador))
                        .Include(t => t.tab_desapropriacao_individual_estrada_acesso.Select(d => d.tab_anexo_identificador).Select(p => p.tab_anexo))

                        .Include(t => t.tab_desapropriacao_individual_planta_propriedade)
                        .Include(t => t.tab_desapropriacao_individual_planta_propriedade.Select(d => d.tab_anexo_identificador))
                        .Include(t => t.tab_desapropriacao_individual_planta_propriedade.Select(d => d.tab_anexo_identificador).Select(p => p.tab_anexo))

                        .Include(t => t.tab_desapropriacao_individual_status_evento)
                        .Include(t => t.tab_desapropriacao_individual_perito_judicial)
                        .Include(t => t.tab_desapropriacao_individual_situacao)
                        .Include(t => t.tab_desapropriacao_individual_predominante_entorno)
                        .Include(t => t.tab_desapropriacao_individual_status)
                        .Include(t => t.tab_desapropriacao_individual_assistente_tecnico)                            
                        .FirstOrDefault(t => t.din_id == id && string.IsNullOrEmpty(t.userDelete));

                    //return obj;
                }
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public bool UPDATE(tab_desapropriacao_individual obj)
        {

            try
            {
                using (var dbContext = new SCDESContext())
                {
                
                    obj.tab_desapropriacao_individual_cultura.ToList().ForEach(item =>
                    {
                        item.din_id = obj.din_id;

                        switch ((int)item.icu_acao)
                        {
                            case 1:
                            case 2:
                            case 3:

                                dbContext.Configuration.ValidateOnSaveEnabled = false;
                                dbContext.Configuration.AutoDetectChangesEnabled = true;
                                dbContext.tab_desapropriacao_individual_cultura.AddOrUpdate(item);
                                break;
                        }
                    });

                    obj.tab_desapropriacao_individual_anexos.ToList().ForEach(item =>
                    {
                        item.din_id = obj.din_id;

                        switch ((int)item.ian_acao)
                        {
                            case 1:
                            case 3:

                                dbContext.Configuration.ValidateOnSaveEnabled = false;
                                dbContext.Configuration.AutoDetectChangesEnabled = true;
                                dbContext.tab_desapropriacao_individual_anexos.AddOrUpdate(item);
                                break;
                        }
                    });

                    obj.tab_desapropriacao_individual_planta_propriedade.ToList().ForEach(item =>
                    {
                        item.din_id = obj.din_id;

                        switch ((int)item.ipp_acao)
                        {
                            case 1:
                            case 3:

                                dbContext.Configuration.ValidateOnSaveEnabled = false;
                                dbContext.Configuration.AutoDetectChangesEnabled = true;
                                dbContext.tab_desapropriacao_individual_planta_propriedade.AddOrUpdate(item);
                                break;
                        }
                    });

                    obj.tab_desapropriacao_individual_divisa.ToList().ForEach(item =>
                    {
                        item.din_id = obj.din_id;

                        switch ((int)item.idi_acao)
                        {
                            case 1:
                            case 2:
                            case 3:

                                dbContext.Configuration.ValidateOnSaveEnabled = false;
                                dbContext.Configuration.AutoDetectChangesEnabled = true;
                                dbContext.tab_desapropriacao_individual_divisa.AddOrUpdate(item);
                                break;
                        }
                    });

                    obj.tab_desapropriacao_individual_edificacao.ToList().ForEach(item =>
                    {
                        item.din_id = obj.din_id;

                        switch ((int)item.ied_acao)
                        {
                            case 1:
                            case 2:
                            case 3:

                                dbContext.Configuration.ValidateOnSaveEnabled = false;
                                dbContext.Configuration.AutoDetectChangesEnabled = true;
                                dbContext.tab_desapropriacao_individual_edificacao.AddOrUpdate(item);
                                break;
                        }
                    });


                    obj.tab_desapropriacao_individual_entrada.ToList().ForEach(item =>
                    {
                        item.din_id = obj.din_id;

                        switch ((int)item.ien_acao)
                        {
                            case 1:
                            case 2:
                            case 3:

                                dbContext.Configuration.ValidateOnSaveEnabled = false;
                                dbContext.Configuration.AutoDetectChangesEnabled = true;
                                dbContext.tab_desapropriacao_individual_entrada.AddOrUpdate(item);
                                break;
                        }
                    });

                    obj.tab_desapropriacao_individual_instalacao.ToList().ForEach(item =>
                    {
                        item.din_id = obj.din_id;

                        switch ((int)item.iin_acao)
                        {
                            case 1:
                            case 2:
                            case 3:

                                dbContext.Configuration.ValidateOnSaveEnabled = false;
                                dbContext.Configuration.AutoDetectChangesEnabled = true;
                                dbContext.tab_desapropriacao_individual_instalacao.AddOrUpdate(item);
                                break;
                        }
                    });


                    obj.tab_desapropriacao_individual_interessado.ToList().ForEach(item =>
                    {
                        item.din_id = obj.din_id;

                        switch ((int)item.iin_acao)
                        {
                            case 1:
                            case 2:
                            case 3:

                                dbContext.Configuration.ValidateOnSaveEnabled = false;
                                dbContext.Configuration.AutoDetectChangesEnabled = true;
                                dbContext.tab_desapropriacao_individual_interessado.AddOrUpdate(item);
                                break;
                        }
                    });

                    obj.tab_desapropriacao_individual_pendencia.ToList().ForEach(item =>
                    {
                        item.din_id = obj.din_id;

                        switch ((int)item.ipe_acao)
                        {
                            case 1:
                            case 3:

                                dbContext.Configuration.ValidateOnSaveEnabled = false;
                                dbContext.Configuration.AutoDetectChangesEnabled = true;
                                dbContext.tab_desapropriacao_individual_pendencia.AddOrUpdate(item);
                                break;
                        }
                    });


                    obj.tab_desapropriacao_individual_registro_fotografico.ToList().ForEach(item =>
                    {
                        item.din_id = obj.din_id;

                        switch ((int)item.irf_acao)
                        {
                            case 1:
                            case 3:

                                dbContext.Configuration.ValidateOnSaveEnabled = false;
                                dbContext.Configuration.AutoDetectChangesEnabled = true;
                                dbContext.tab_desapropriacao_individual_registro_fotografico.AddOrUpdate(item);
                                break;
                        }
                    });

                    obj.tab_desapropriacao_individual_revisao.ToList().ForEach(item =>
                    {
                        item.din_id = obj.din_id;

                        switch ((int)item.ire_acao)
                        {
                            case 1:
                            case 3:

                                dbContext.Configuration.ValidateOnSaveEnabled = false;
                                dbContext.Configuration.AutoDetectChangesEnabled = true;
                                dbContext.tab_desapropriacao_individual_revisao.AddOrUpdate(item);
                                break;
                        }
                    });

                    obj.tab_desapropriacao_individual_estrada_acesso.ToList().ForEach(item =>
                    {
                        item.din_id = obj.din_id;

                        switch ((int)item.iea_acao)
                        {
                            case 1:
                            case 2:
                            case 3:

                                dbContext.Configuration.ValidateOnSaveEnabled = false;
                                dbContext.Configuration.AutoDetectChangesEnabled = true;
                                dbContext.tab_desapropriacao_individual_estrada_acesso.AddOrUpdate(item);
                                break;
                        }
                    });


                    obj.tab_desapropriacao_individual_status_evento.ToList().ForEach(item =>
                    {
                        item.din_id = obj.din_id;

                        switch ((int)item.ise_acao)
                        {
                            case 1:
                            case 3:

                                dbContext.Configuration.ValidateOnSaveEnabled = false;
                                dbContext.Configuration.AutoDetectChangesEnabled = true;
                                dbContext.tab_desapropriacao_individual_status_evento.AddOrUpdate(item);
                                break;
                        }
                    });

               
                    dbContext.tab_desapropriacao_individual.AddOrUpdate(obj);
                    dbContext.SaveChanges();
                }


                using (var _ctx = new SCDESContext())
                {
                    obj.tab_desapropriacao_individual_cultura.Where(pendencia => pendencia.icu_acao == acao.Delete).ToList().ForEach(t => { _ctx.Entry(t).State = EntityState.Deleted; });
                    obj.tab_desapropriacao_individual_anexos.Where(pendencia => pendencia.ian_acao == acao.Delete).ToList().ForEach(t => { _ctx.Entry(t).State = EntityState.Deleted; });
                    obj.tab_desapropriacao_individual_divisa.Where(pendencia => pendencia.idi_acao == acao.Delete).ToList().ForEach(t => { _ctx.Entry(t).State = EntityState.Deleted; });
                    obj.tab_desapropriacao_individual_edificacao.Where(pendencia => pendencia.ied_acao == acao.Delete).ToList().ForEach(t => { _ctx.tab_desapropriacao_individual_edificacao.Attach(t); _ctx.tab_desapropriacao_individual_edificacao.Remove(t); });
                    obj.tab_desapropriacao_individual_entrada.Where(pendencia => pendencia.ien_acao == acao.Delete).ToList().ForEach(t => { _ctx.Entry(t).State = EntityState.Deleted; });
                    obj.tab_desapropriacao_individual_instalacao.Where(pendencia => pendencia.iin_acao == acao.Delete).ToList().ForEach(t => { _ctx.Entry(t).State = EntityState.Deleted; });
                    obj.tab_desapropriacao_individual_interessado.Where(pendencia => pendencia.iin_acao == acao.Delete).ToList().ForEach(t => { _ctx.Entry(t).State = EntityState.Deleted; });
                    obj.tab_desapropriacao_individual_pendencia.Where(pendencia => pendencia.ipe_acao == ipe_acao.Delete).ToList().ForEach(t => { _ctx.Entry(t).State = EntityState.Deleted; });
                    obj.tab_desapropriacao_individual_registro_fotografico.Where(pendencia => pendencia.irf_acao == acao.Delete).ToList().ForEach(t => { _ctx.Entry(t).State = EntityState.Deleted; });
                    obj.tab_desapropriacao_individual_revisao.Where(pendencia => pendencia.ire_acao == acao.Delete).ToList().ForEach(t => { _ctx.Entry(t).State = EntityState.Deleted; });
                    obj.tab_desapropriacao_individual_estrada_acesso.Where(pendencia => pendencia.iea_acao == acao.Delete).ToList().ForEach(t => { _ctx.Entry(t).State = EntityState.Deleted; });
                    obj.tab_desapropriacao_individual_status_evento.Where(pendencia => pendencia.ise_acao == acao.Delete).ToList().ForEach(t => { _ctx.Entry(t).State = EntityState.Deleted; });
                    obj.tab_desapropriacao_individual_planta_propriedade.Where(pendencia => pendencia.ipp_acao == acao.Delete).ToList().ForEach(t => { _ctx.Entry(t).State = EntityState.Deleted; });

                    _ctx.SaveChanges();
                }

            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
            catch (Exception ex)
            {
                throw new CustomException("Erro do update: " + ex, bootStrapMessageType.TYPE_DANGER);
            }


            return true;
        }

        public bool DELETAR(int id, string userDelete)
        {
            try
            {
                using (var dbContext = new SCDESContext())
                {
                    var obj = dbContext.tab_desapropriacao_individual.Where(t => t.din_id == id)
                        .Include(t => t.tab_desapropriacao_individual_cultura)
                        .Include(t => t.tab_desapropriacao_individual_anexos)
                        .Include(t => t.tab_desapropriacao_individual_divisa)
                        .Include(t => t.tab_desapropriacao_individual_edificacao)
                        .Include(t => t.tab_desapropriacao_individual_entrada)
                        .Include(t => t.tab_desapropriacao_individual_instalacao)
                        .Include(t => t.tab_desapropriacao_individual_interessado)
                        .Include(t => t.tab_desapropriacao_individual_pendencia)
                        .Include(t => t.tab_desapropriacao_individual_registro_fotografico)
                        .Include(t => t.tab_desapropriacao_individual_estrada_acesso)
                        .Include(t => t.tab_desapropriacao_individual_status_evento)
                        .FirstOrDefault();

                    obj.userDelete = userDelete;
                    obj.dtDelete = DateTime.Now;

                    obj.tab_desapropriacao_individual_cultura.ToList().ForEach(t => { t.userDelete = userDelete; t.dtDelete = DateTime.Now; });
                    obj.tab_desapropriacao_individual_anexos.ToList().ForEach(t => { t.userDelete = userDelete; t.dtDelete = DateTime.Now; });
                    obj.tab_desapropriacao_individual_divisa.ToList().ForEach(t => { t.userDelete = userDelete; t.dtDelete = DateTime.Now; });
                    obj.tab_desapropriacao_individual_edificacao.ToList().ForEach(t => { t.userDelete = userDelete; t.dtDelete = DateTime.Now; });
                    obj.tab_desapropriacao_individual_entrada.ToList().ForEach(t => { t.userDelete = userDelete; t.dtDelete = DateTime.Now; });
                    obj.tab_desapropriacao_individual_instalacao.ToList().ForEach(t => { t.userDelete = userDelete; t.dtDelete = DateTime.Now; });
                    obj.tab_desapropriacao_individual_interessado.ToList().ForEach(t => { t.userDelete = userDelete; t.dtDelete = DateTime.Now; });
                    obj.tab_desapropriacao_individual_pendencia.ToList().ForEach(t => { t.userDelete = userDelete; t.dtDelete = DateTime.Now; });
                    obj.tab_desapropriacao_individual_registro_fotografico.ToList().ForEach(t => { t.userDelete = userDelete; t.dtDelete = DateTime.Now; });
                    //obj.tab_desapropriacao_individual_revisao.ToList().ForEach(t => { t.userDelete = userDelete; t.dtDelete = DateTime.Now; });
                    obj.tab_desapropriacao_individual_estrada_acesso.ToList().ForEach(t => { t.userDelete = userDelete; t.dtDelete = DateTime.Now; });
                    obj.tab_desapropriacao_individual_status_evento.ToList().ForEach(t => { t.userDelete = userDelete; t.dtDelete = DateTime.Now; });
                    obj.tab_desapropriacao_individual_planta_propriedade.ToList().ForEach(t => { t.userDelete = userDelete; t.dtDelete = DateTime.Now; });


                    dbContext.Entry(obj).State = EntityState.Modified;

                    dbContext.SaveChanges();

                    return true;
                }
            }
            catch (Exception e)
            {
                throw new CustomException("Erro ao remover a desapropriação individual", bootStrapMessageType.TYPE_DANGER);
            }
        }

        /// <summary>
        /// PESQUISAR PARA GRIDVIEW
        /// </summary>
        /// <param name="filtro"></param>
        /// <returns></returns>
        public List<tab_desapropriacao_individual_gdv> PESQUISA_GDV(tab_desapropriacao_individual_filtro filtro)
        {
            try
            {
                SqlParameter[] param =
                {
                    new SqlParameter("@din_geral_documento_numero", SqlDbType.VarChar) { Value = FormartaDados.SetType<string>(filtro.din_geral_documento_numero) },
                    new SqlParameter("@din_geral_elaborador", SqlDbType.VarChar) { Value = FormartaDados.SetType<string>(filtro.din_geral_elaborador) },
                    new SqlParameter("@din_propriedade_autos_numero", SqlDbType.VarChar) { Value = FormartaDados.SetType<string>(filtro.din_propriedade_autos_numero) },
                    new SqlParameter("@din_propriedade_prioridade", SqlDbType.SmallInt) { Value = FormartaDados.SetType<short?>(filtro.din_propriedade_prioridade) },
                    new SqlParameter("@din_interessados_proprietario", SqlDbType.VarChar) { Value = FormartaDados.SetType<string>(filtro.din_interessados_proprietario) },
                    new SqlParameter("@din_geral_objeto", SqlDbType.VarChar) { Value = FormartaDados.SetType<string>(filtro.din_geral_objeto) },
                    new SqlParameter("@din_geral_emissao_dt", SqlDbType.DateTime) { Value = FormartaDados.SetType<DateTime?>(filtro.din_geral_emissao_dt) },
                    new SqlParameter("@afi_id", SqlDbType.Int) { Value = FormartaDados.SetType<int?>(filtro.afi_id) },
                    new SqlParameter("@din_empreendimento_auto_decreto", SqlDbType.VarChar) { Value = FormartaDados.SetType<string>(filtro.din_empreendimento_auto_decreto) },
                    new SqlParameter("@des_empreendimento_dup_codigo", SqlDbType.VarChar) { Value = FormartaDados.SetType<string>(filtro.din_empreendimento_dup_codigo) },
                    new SqlParameter("@des_empreendimento_dup_dt", SqlDbType.DateTime) { Value = FormartaDados.SetType<DateTime?>(filtro.din_empreendimento_dup_dt) },
                    new SqlParameter("@din_conducao_ist_id", SqlDbType.Int) { Value = FormartaDados.SetType<int?>(filtro.din_conducao_ist_id) },
                    new SqlParameter("@reg_id", SqlDbType.Int) { Value = FormartaDados.SetType<int?>(filtro.reg_id) },
                    new SqlParameter("@din_empreendimento_rodovia_sp", SqlDbType.VarChar) { Value = FormartaDados.SetType<string>(filtro.din_empreendimento_rodovia_sp) },
                    new SqlParameter("@din_empreendimento_rodovia_nome", SqlDbType.VarChar) { Value = FormartaDados.SetType<string>(filtro.din_empreendimento_rodovia_nome) },

                    new SqlParameter("@PageSize", SqlDbType.Int) { Value = FormartaDados.SetType<int>(filtro.pageSize) },
                    new SqlParameter("@PageIndex", SqlDbType.Int) { Value = FormartaDados.SetType<int>(filtro.pageIndex) },
                    new SqlParameter("@PageCount", SqlDbType.Int) { Direction = ParameterDirection.Output }
                };


                using (var _ctxContext = new SCDESContext())
                {
                    using (var cmd = _ctxContext.Database.Connection.CreateCommand())
                    {
                        cmd.CommandText = "STP_GDV_DESAPROPRIACAO_INDIVIDUAL";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddRange(param);

                        try
                        {
                            cmd.Connection.Open();

                            DbDataReader reader;

                            reader = cmd.ExecuteReader();

                            return ((IObjectContextAdapter)_ctxContext).ObjectContext.Translate<tab_desapropriacao_individual_gdv>(reader).ToList();
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                        finally
                        {
                            cmd.Connection.Close();
                            filtro.pageCount = (int)param[param.Count() - 1].Value;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
