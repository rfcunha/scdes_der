﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dao.CONTEXT;
using Entity.SCDES.Models;
using System.Data;
using System.Data.SqlClient;
using Suporte.Formatacao;
using System.Data.Common;
using System.Data.Entity.Infrastructure;

namespace Dao.SCDES
{
    public class tab_select_helper_dao : DefaultDao<tab_select_helper, SCDESContext>
    {


        

        public List<tab_select_helper> LISTAR_GUIA_HELPERS(int guia_id)
        {
            using (var _ctxContext = new SCDESContext())
            {
                try
                {
                    var list = _ctxContext.tab_select_helper.Where(t => t.seh_ativo && t.shg_id == guia_id).ToList();
                    return list;
                }
                catch (Exception ex)
                {                    
                    throw ex;
                }
            }
        }
    }
}
