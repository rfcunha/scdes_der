using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using Conexao.AdoProvider.Repositorio;
using Entity.DER;
using Entity.DER.Utils.Filtro;
using Suporte.Criptografia;
using Suporte.Exceptions;
using Suporte.Formatacao;
using Suporte.MsgBox;

namespace Dao.DER
{
    public class tab_zoneamento_tipo_dao
    {
        private DbBase objetoBase { get; set; }

        public tab_zoneamento_tipo_dao()
        {
            objetoBase = new DbBase(Cripto.Decrypt(ConfigurationManager.ConnectionStrings["CONNECTION_DER"].ConnectionString), ConfigurationManager.AppSettings["Cripto"]);  
        } 
        
        public tab_zoneamento_tipo GET_BY(int id)
        {
            try
            {
                var obj = new tab_zoneamento_tipo();
                
                objetoBase.SetParameter("@zti_id", FormartaDados.SetType<int>(id));
                
                objetoBase.CommandType = CommandType.StoredProcedure;
                objetoBase.CommandText = "STP_GET_BY_ZONEAMENTO_TIPO";
                
                var reader = objetoBase.ExecuteReader();

                while (reader.Read())
                    obj = ToObject(reader);

                reader.Close();

                return obj;
            }
            catch (CustomException ce)
            {
                throw new CustomException(ce.Message, bootStrapMessageType.TYPE_DANGER);
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro desconhecido no sistema.</br>Entre em contato com o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
        }

        private List<tab_zoneamento_tipo> _PESQUISA(tab_zoneamento_tipo_filtro filtro)
        {
            try
            {               
                var listObj = new List<tab_zoneamento_tipo>();

                objetoBase.SetParameter("@zti_nome ", FormartaDados.SetType<string>(filtro.zti_nome));
                objetoBase.SetParameter("@zti_ativo", FormartaDados.SetType<bool?>(filtro.zti_ativo));  
                
                objetoBase.CommandType = CommandType.StoredProcedure;
                objetoBase.CommandText = "STP_PES_ZONEAMENTO_TIPO";
                
                var reader = objetoBase.ExecuteReader();
                
                while (reader.Read())
                {
                    var obj = new tab_zoneamento_tipo
                    {
                        zti_id = FormartaDados.Int(reader["zti_id"]),
                        zti_nome = FormartaDados.String(reader["zti_nome"]),
                        zti_dt_cadastro = FormartaDados.Data(reader["zti_dt_cadastro"]),
                        zti_ativo = FormartaDados.Booleano(reader["zti_ativo"]),
                        zti_deletado = FormartaDados.Booleano(reader["zti_deletado"])
                    };

                    listObj.Add(obj);
                }

                reader.Close();

                return listObj;
            }
            catch (CustomException ce)
            {
                throw new CustomException(ce.Message, bootStrapMessageType.TYPE_DANGER);
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro desconhecido no sistema.</br>Entre em contato com o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
        }

        public List<tab_zoneamento_tipo> PESQUISA(tab_zoneamento_tipo_filtro filtro)
        {
            return _PESQUISA(filtro);
        }
        
        private tab_zoneamento_tipo ToObject(IDataReader dr)
        {
            try
            {
                var obj = new tab_zoneamento_tipo
                {
                    zti_id = FormartaDados.Int(dr["zti_id"]),
                    zti_nome = FormartaDados.String(dr["zti_nome"]),
                    zti_dt_cadastro = FormartaDados.Data(dr["zti_dt_cadastro"]),
                    zti_ativo = FormartaDados.Booleano(dr["zti_ativo"]),
                    zti_deletado = FormartaDados.Booleano(dr["zti_deletado"])
                };

                return obj;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
        }
    }
}
