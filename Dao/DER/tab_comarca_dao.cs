using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using Conexao.AdoProvider.Repositorio;
using Entity.DER;
using Entity.DER.Utils.Filtro;
using Suporte.Criptografia;
using Suporte.Exceptions;
using Suporte.Formatacao;
using Suporte.MsgBox;

namespace Dao.DER
{
    public class tab_comarca_dao
    {
        private DbBase objetoBase { get; set; }

        public tab_comarca_dao()
        {
            objetoBase = new DbBase(Cripto.Decrypt(ConfigurationManager.ConnectionStrings["CONNECTION_DER"].ConnectionString, ConfigurationManager.AppSettings["Cripto"]));  
        } 
        
        public tab_comarca GET_BY(int id)
        {
            try
            {
                var obj = new tab_comarca();
                
                objetoBase.SetParameter("@com_id", FormartaDados.SetType<int>(id));
                
                objetoBase.CommandType = CommandType.StoredProcedure;
                objetoBase.CommandText = "STP_GET_BY_COMARCA";
                
                var reader = objetoBase.ExecuteReader();

                while (reader.Read())
                    obj = ToObject(reader);

                reader.Close();

                return obj;
            }
            catch (CustomException ce)
            {
                throw new CustomException(ce.Message, bootStrapMessageType.TYPE_DANGER);
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro desconhecido no sistema.</br>Entre em contato com o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
        }

        private List<tab_comarca> _PESQUISA(tab_comarca_filtro filtro)
        {
            try
            {               
                var listObj = new List<tab_comarca>();

                objetoBase.SetParameter("@com_nome ", FormartaDados.SetType<string>(filtro.com_nome));
                objetoBase.SetParameter("@com_ativo", FormartaDados.SetType<bool?>(filtro.com_ativo));  
                
                objetoBase.CommandType = CommandType.StoredProcedure;
                objetoBase.CommandText = "STP_PES_COMARCA";
                
                var reader = objetoBase.ExecuteReader();
                
                while (reader.Read())
                {
                    var obj = new tab_comarca
                    {
                        com_id = FormartaDados.Int(reader["com_id"]),
                        com_nome = FormartaDados.String(reader["com_nome"]),
                        com_dt_cadastro = FormartaDados.Data(reader["com_dt_cadastro"]),
                        com_ativo = FormartaDados.Booleano(reader["com_ativo"]),
                        com_deletado = FormartaDados.Booleano(reader["com_deletado"])
                    };

                    listObj.Add(obj);
                }

                reader.Close();

                return listObj;
            }
            catch (CustomException ce)
            {
                throw new CustomException(ce.Message, bootStrapMessageType.TYPE_DANGER);
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro desconhecido no sistema.</br>Entre em contato com o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
        }

        public List<tab_comarca> PESQUISA(tab_comarca_filtro filtro)
        {
            return _PESQUISA(filtro);
        }
        
        private tab_comarca ToObject(IDataReader dr)
        {
            try
            {
                var obj = new tab_comarca
                {
                    com_id = FormartaDados.Int(dr["com_id"]),
                    com_nome = FormartaDados.String(dr["com_nome"]),
                    com_dt_cadastro = FormartaDados.Data(dr["com_dt_cadastro"]),
                    com_ativo = FormartaDados.Booleano(dr["com_ativo"]),
                    com_deletado = FormartaDados.Booleano(dr["com_deletado"])
                };

                return obj;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
        }
    }
}
