﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using Conexao.AdoProvider.Repositorio;
using Entity.DER;
using Entity.DER.Utils.Filtro;
using Suporte.Criptografia;
using Suporte.Exceptions;
using Suporte.Formatacao;
using Suporte.MsgBox;

namespace Dao.DER
{
    public class tab_rodovia_dao
    {
        private DbBase objetoBase { get; set; }

        public tab_rodovia_dao()
        {
            objetoBase = new DbBase(Cripto.Decrypt(ConfigurationManager.ConnectionStrings["CONNECTION_DER"].ConnectionString, ConfigurationManager.AppSettings["Cripto"]));  
        }

        public tab_rodovia GET_BY_RODOVIA(string rod_sp, decimal rod_km_inicial, decimal rod_km_final)
        {
            try
            {
                objetoBase.SetParameter("@rod_sp", FormartaDados.SetType<string>(rod_sp));
                objetoBase.SetParameter("@rod_km_inicial", FormartaDados.SetType<decimal>(rod_km_inicial));
                objetoBase.SetParameter("@rod_km_final", FormartaDados.SetType<decimal>(rod_km_final));

                objetoBase.CommandType = CommandType.StoredProcedure;
                objetoBase.CommandText = "STP_GET_BY_RODOVIA";

                var reader = objetoBase.ExecuteReader();

                var obj = new tab_rodovia();

                while (reader.Read())
                    obj = ToObject(reader);

                reader.Close();

                return obj;
            }
            catch (CustomException ce)
            {
                throw new CustomException(ce.Message, bootStrapMessageType.TYPE_DANGER);
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro desconhecido no sistema.</br>Entre em contato com o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
        }

        private List<string> _PESQUISA_RODOVIA_SP(string rod_sp)
        {
            try
            {
                objetoBase.SetParameter("@rod_sp", FormartaDados.SetType<string>(rod_sp));

                objetoBase.CommandType = CommandType.StoredProcedure;
                objetoBase.CommandText = "STP_PES_RODOVIA_SP";

                var reader = objetoBase.ExecuteReader();

                var listObj = new List<string>();

                while (reader.Read())
                {
                    listObj.Add(FormartaDados.GetType<string>(reader["rod_sp"]));
                }

                reader.Close();

                return listObj;
            }
            catch (CustomException ce)
            {
                throw new CustomException(ce.Message, bootStrapMessageType.TYPE_DANGER);
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro desconhecido no sistema.</br>Entre em contato com o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
        }

        public List<string> PESQUISA_RODOVIA_SP(string rod_sp)
        {
            return _PESQUISA_RODOVIA_SP(rod_sp);
        }

        private List<tab_rodovia> _PESQUISA_RODOVIA(tab_rodovia_filtro filtro)
        {
            try
            {
                var listObj = new List<tab_rodovia>();

                objetoBase.SetParameter("@rod_sp", FormartaDados.SetType<string>(filtro.rod_sp));
                objetoBase.SetParameter("@rod_km_inicial", FormartaDados.SetType<decimal?>(filtro.rod_km_inicial));
                objetoBase.SetParameter("@rod_km_final", FormartaDados.SetType<decimal?>(filtro.rod_km_final));

                objetoBase.CommandType = CommandType.StoredProcedure;
                objetoBase.CommandText = "STP_PES_RODOVIA_DENOMINACAO";

                var reader = objetoBase.ExecuteReader();

                while (reader.Read())
                {
                    var obj = new tab_rodovia();

                    obj.rod_sp = FormartaDados.GetType<string>(reader["rod_sp"]);
                    obj.rod_denominacao = FormartaDados.GetType<string>(reader["rod_denominacao"]);
                    obj.rod_km_inicial = FormartaDados.GetType<decimal>(reader["rod_km_inicial"]);
                    obj.rod_km_final = FormartaDados.GetType<decimal>(reader["rod_km_final"]);
                    obj.rod_extensao = FormartaDados.GetType<decimal>(reader["rod_extensao"]);
                    obj.rod_jurisdicao = FormartaDados.GetType<string>(reader["rod_jurisdicao"]);
                    obj.rod_administracao = FormartaDados.GetType<string>(reader["rod_administracao"]);
                    obj.rod_conservacao = FormartaDados.GetType<string>(reader["rod_conservacao"]);
                    obj.rod_descricao_inicial = FormartaDados.GetType<string>(reader["rod_descricao_inicial"]);
                    obj.rod_descricao_final = FormartaDados.GetType<string>(reader["rod_descricao_final"]);
                    obj.rod_sp = FormartaDados.GetType<string>(reader["rod_sp"]);
                    obj.rod_municipio = FormartaDados.GetType<string>(reader["rod_municipio"]);

                    listObj.Add(obj);
                }

                reader.Close();

                return listObj;
            }
            catch (CustomException ce)
            {
                throw new CustomException(ce.Message, bootStrapMessageType.TYPE_DANGER);
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro desconhecido no sistema.</br>Entre em contato com o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
        }

        public List<tab_rodovia> PESQUISA_RODOVIA(tab_rodovia_filtro filtro)
        {
            return _PESQUISA_RODOVIA(filtro);
        }

        private tab_rodovia ToObject(IDataReader dr)
        {
            try
            {
                var obj = new tab_rodovia();

                obj.rod_sp = FormartaDados.GetType<string>(dr["rod_sp"]);
                obj.rod_denominacao = FormartaDados.GetType<string>(dr["rod_denominacao"]);
                obj.rod_km_inicial = FormartaDados.GetType<decimal>(dr["rod_km_inicial"]);
                obj.rod_km_final = FormartaDados.GetType<decimal>(dr["rod_km_final"]);
                obj.rod_extensao = FormartaDados.GetType<decimal>(dr["rod_extensao"]);
                obj.rod_jurisdicao = FormartaDados.GetType<string>(dr["rod_jurisdicao"]);
                obj.rod_administracao = FormartaDados.GetType<string>(dr["rod_administracao"]);
                obj.rod_conservacao = FormartaDados.GetType<string>(dr["rod_conservacao"]);
                obj.rod_descricao_inicial = FormartaDados.GetType<string>(dr["rod_descricao_inicial"]);
                obj.rod_descricao_final = FormartaDados.GetType<string>(dr["rod_descricao_final"]);
                obj.rod_sp = FormartaDados.GetType<string>(dr["rod_sp"]);
                obj.rod_municipio = FormartaDados.GetType<string>(dr["rod_municipio"]);

                return obj;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
        }
    }
}
