using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using Conexao.AdoProvider.Repositorio;
using Entity.DER;
using Entity.DER.Utils.Filtro;
using Suporte.Criptografia;
using Suporte.Exceptions;
using Suporte.Formatacao;
using Suporte.MsgBox;

namespace Dao.DER
{
    public class tab_solo_uso_dao
    {
        private DbBase objetoBase { get; set; }

        public tab_solo_uso_dao()
        {
            objetoBase = new DbBase(Cripto.Decrypt(ConfigurationManager.ConnectionStrings["CONNECTION_DER"].ConnectionString), ConfigurationManager.AppSettings["Cripto"]);  
        } 
        
        public tab_solo_uso GET_BY(int id)
        {
            try
            {
                var obj = new tab_solo_uso();
                
                objetoBase.SetParameter("@sus_id", FormartaDados.SetType<int>(id));
                
                objetoBase.CommandType = CommandType.StoredProcedure;
                objetoBase.CommandText = "STP_GET_BY_SOLO_USO";
                
                var reader = objetoBase.ExecuteReader();

                while (reader.Read())
                    obj = ToObject(reader);

                reader.Close();

                return obj;
            }
            catch (CustomException ce)
            {
                throw new CustomException(ce.Message, bootStrapMessageType.TYPE_DANGER);
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro desconhecido no sistema.</br>Entre em contato com o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
        }

        private List<tab_solo_uso> _PESQUISA(tab_solo_uso_filtro filtro)
        {
            try
            {               
                var listObj = new List<tab_solo_uso>();

                objetoBase.SetParameter("@sus_nome ", FormartaDados.SetType<string>(filtro.sus_nome));
                objetoBase.SetParameter("@sus_ativo", FormartaDados.SetType<bool?>(filtro.sus_ativo));  
                
                objetoBase.CommandType = CommandType.StoredProcedure;
                objetoBase.CommandText = "STP_PES_SOLO_USO";
                
                var reader = objetoBase.ExecuteReader();
                
                while (reader.Read())
                {
                    var obj = new tab_solo_uso
                    {
                        sus_id = FormartaDados.Int(reader["sus_id"]),
                        sus_nome = FormartaDados.String(reader["sus_nome"]),
                        sus_dt_cadastro = FormartaDados.Data(reader["sus_dt_cadastro"]),
                        sus_ativo = FormartaDados.Booleano(reader["sus_ativo"]),
                        sus_deletado = FormartaDados.Booleano(reader["sus_deletado"])
                    };

                    listObj.Add(obj);
                }

                reader.Close();

                return listObj;
            }
            catch (CustomException ce)
            {
                throw new CustomException(ce.Message, bootStrapMessageType.TYPE_DANGER);
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro desconhecido no sistema.</br>Entre em contato com o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
        }

        public List<tab_solo_uso> PESQUISA(tab_solo_uso_filtro filtro)
        {
            return _PESQUISA(filtro);
        }
        
        private tab_solo_uso ToObject(IDataReader dr)
        {
            try
            {
                var obj = new tab_solo_uso
                {
                    sus_id = FormartaDados.Int(dr["sus_id"]),
                    sus_nome = FormartaDados.String(dr["sus_nome"]),
                    sus_dt_cadastro = FormartaDados.Data(dr["sus_dt_cadastro"]),
                    sus_ativo = FormartaDados.Booleano(dr["sus_ativo"]),
                    sus_deletado = FormartaDados.Booleano(dr["sus_deletado"])
                };

                return obj;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
        }
    }
}
