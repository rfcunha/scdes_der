using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using Conexao.AdoProvider.Repositorio;
using Entity.DER;
using Entity.DER.Utils.Filtro;
using Suporte.Criptografia;
using Suporte.Exceptions;
using Suporte.Formatacao;
using Suporte.MsgBox;

namespace Dao.DER
{
    public class tab_solo_condicao_dao
    {
        private DbBase objetoBase { get; set; }

        public tab_solo_condicao_dao()
        {
            objetoBase = new DbBase(Cripto.Decrypt(ConfigurationManager.ConnectionStrings["CONNECTION_DER"].ConnectionString), ConfigurationManager.AppSettings["Cripto"]);  
        } 
        
        public tab_solo_condicao GET_BY(int id)
        {
            try
            {
                var obj = new tab_solo_condicao();
                
                objetoBase.SetParameter("@sco_id", FormartaDados.SetType<int>(id));
                
                objetoBase.CommandType = CommandType.StoredProcedure;
                objetoBase.CommandText = "STP_GET_BY_SOLO_CONDICAO";
                
                var reader = objetoBase.ExecuteReader();

                while (reader.Read())
                    obj = ToObject(reader);

                reader.Close();

                return obj;
            }
            catch (CustomException ce)
            {
                throw new CustomException(ce.Message, bootStrapMessageType.TYPE_DANGER);
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro desconhecido no sistema.</br>Entre em contato com o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
        }

        private List<tab_solo_condicao> _PESQUISA(tab_solo_condicao_filtro filtro)
        {
            try
            {               
                var listObj = new List<tab_solo_condicao>();

                objetoBase.SetParameter("@sco_nome ", FormartaDados.SetType<string>(filtro.sco_nome));
                objetoBase.SetParameter("@sco_ativo", FormartaDados.SetType<bool?>(filtro.sco_ativo));  
                
                objetoBase.CommandType = CommandType.StoredProcedure;
                objetoBase.CommandText = "STP_PES_SOLO_CONDICAO";
                
                var reader = objetoBase.ExecuteReader();
                
                while (reader.Read())
                {
                    var obj = new tab_solo_condicao
                    {
                        sco_id = FormartaDados.Int(reader["sco_id"]),
                        sco_nome = FormartaDados.String(reader["sco_nome"]),
                        sco_dt_cadastro = FormartaDados.Data(reader["sco_dt_cadastro"]),
                        sco_ativo = FormartaDados.Booleano(reader["sco_ativo"]),
                        sco_deletado = FormartaDados.Booleano(reader["sco_deletado"])
                    };

                    listObj.Add(obj);
                }

                reader.Close();

                return listObj;
            }
            catch (CustomException ce)
            {
                throw new CustomException(ce.Message, bootStrapMessageType.TYPE_DANGER);
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro desconhecido no sistema.</br>Entre em contato com o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
        }

        public List<tab_solo_condicao> PESQUISA(tab_solo_condicao_filtro filtro)
        {
            return _PESQUISA(filtro);
        }
        
        private tab_solo_condicao ToObject(IDataReader dr)
        {
            try
            {
                var obj = new tab_solo_condicao
                {
                    sco_id = FormartaDados.Int(dr["sco_id"]),
                    sco_nome = FormartaDados.String(dr["sco_nome"]),
                    sco_dt_cadastro = FormartaDados.Data(dr["sco_dt_cadastro"]),
                    sco_ativo = FormartaDados.Booleano(dr["sco_ativo"]),
                    sco_deletado = FormartaDados.Booleano(dr["sco_deletado"])
                };

                return obj;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
        }
    }
}
