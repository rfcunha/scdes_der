﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using Conexao.AdoProvider.Repositorio;
using Entity.DER;
using Entity.DER.Utils.Filtro;
using Suporte.Criptografia;
using Suporte.Exceptions;
using Suporte.Formatacao;
using Suporte.MsgBox;

namespace Dao.DER
{
    public class tab_regional_dao
    {
        private DbBase objetoBase { get; set; }

        public tab_regional_dao()
        {
            objetoBase = new DbBase(Cripto.Decrypt(ConfigurationManager.ConnectionStrings["CONNECTION_DER"].ConnectionString, ConfigurationManager.AppSettings["Cripto"]));  
        } 

        public tab_regional GET_BY(int id)
        {
            try
            {
                var obj = new tab_regional();

                objetoBase.SetParameter("@reg_id", FormartaDados.SetType<int>(id));

                objetoBase.CommandType = CommandType.StoredProcedure;
                objetoBase.CommandText = "STP_GET_BY_REGIONAL";

                var reader = objetoBase.ExecuteReader();

                while (reader.Read())
                    obj = ToObject(reader);

                reader.Close();

                return obj;
            }
            catch (CustomException ce)
            {
                throw new CustomException(ce.Message, bootStrapMessageType.TYPE_DANGER);
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro desconhecido no sistema.</br>Entre em contato com o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
        }

        private List<tab_regional> _PESQUISA(tab_regional_filtro filtro)
        {
            try
            {
                var listObj = new List<tab_regional>();

                objetoBase.SetParameter("@reg_nome",FormartaDados.SetType<string>(filtro.reg_nome));
                objetoBase.SetParameter("@reg_ativo", FormartaDados.SetType<bool?>(filtro.reg_ativo));

                objetoBase.CommandType = CommandType.StoredProcedure;
                objetoBase.CommandText = "STP_PES_REGIONAL";

                var reader = objetoBase.ExecuteReader();

                while (reader.Read())
                {
                    var obj = new tab_regional();

                    obj.reg_id = FormartaDados.GetType<Int32>(reader["reg_id"]);
                    obj.reg_nome = FormartaDados.GetType<String>(reader["reg_nome"]);
                    obj.reg_ativo = FormartaDados.GetType<Boolean>(reader["reg_ativo"]);
                    obj.reg_dt_cadastro = FormartaDados.GetType<DateTime>(reader["reg_dt_cadastro"]);

                    listObj.Add(obj);
                }

                reader.Close();

                return listObj;
            }
            catch (CustomException ce)
            {
                throw new CustomException(ce.Message, bootStrapMessageType.TYPE_DANGER);
            }
            catch (Exception ex)
            {
                throw new CustomException("Ocorreu um erro desconhecido no sistema.</br>Entre em contato com o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
        }

        public List<tab_regional> PESQUISA(tab_regional_filtro filtro)
        {
            return _PESQUISA(filtro);
        }

        private tab_regional ToObject(IDataReader dr)
        {
            try
            {
                var obj = new tab_regional();

                obj.reg_id = FormartaDados.GetType<Int32>(dr["reg_id"]);
                obj.reg_nome = FormartaDados.GetType<String>(dr["reg_nome"]);
                obj.reg_ativo = FormartaDados.GetType<Boolean>(dr["reg_ativo"]);
                obj.reg_dt_cadastro = FormartaDados.GetType<DateTime>(dr["reg_dt_cadastro"]);

                return obj;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
        }
    }
}
