﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using Conexao.AdoProvider.Repositorio;
using Entity.DER;
using Entity.DER.Utils;
using Suporte.Criptografia;
using Suporte.Exceptions;
using Suporte.Formatacao;
using Suporte.MsgBox;

namespace Dao.DER
{
    public class tab_municipio_dao
    {
        private DbBase objetoBase { get; set; }

        public tab_municipio_dao()
        {
            objetoBase = new DbBase(Cripto.Decrypt(ConfigurationManager.ConnectionStrings["CONNECTION_DER"].ConnectionString, ConfigurationManager.AppSettings["Cripto"])); 
        } 

        public tab_municipio GET_BY(int id)
        {
            try
            {
                var obj = new tab_municipio();

                objetoBase.SetParameter("@mun_id", FormartaDados.SetType<int>(id));

                objetoBase.CommandType = CommandType.StoredProcedure;
                objetoBase.CommandText = "STP_GET_BY_MUNICIPIO";

                var reader = objetoBase.ExecuteReader();

                while (reader.Read())
                    obj = ToObject(reader);

                reader.Close();

                return obj;
            }
            catch (CustomException ce)
            {
                throw new CustomException(ce.Message, bootStrapMessageType.TYPE_DANGER);
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro desconhecido no sistema.</br>Entre em contato com o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
        }

        private List<tab_municipio> _PESQUISA(tab_municipio_filtro filtro)
        {
            try
            {
                var listObj = new List<tab_municipio>();

                objetoBase.SetParameter("@mun_nome", FormartaDados.SetType<string>(filtro.mun_nome));
                objetoBase.SetParameter("@mun_codigo_ibge", FormartaDados.SetType<int?>(filtro.mun_codigo_ibge));
                objetoBase.SetParameter("@mun_uf", FormartaDados.SetType<string>(filtro.mun_uf));
                objetoBase.SetParameter("@mun_ativo", FormartaDados.SetType<bool?>(filtro.mun_ativo));
                objetoBase.SetParameter("@est_id", FormartaDados.SetType<int?>(filtro.est_id));

                objetoBase.CommandType = CommandType.StoredProcedure;
                objetoBase.CommandText = "STP_PES_MUNICIPIO";

                var reader = objetoBase.ExecuteReader();

                while (reader.Read())
                {
                    var obj = new tab_municipio();

                    obj.mun_id          = FormartaDados.GetType<Int32>(reader["mun_id"]);
                    obj.mun_nome        = FormartaDados.GetType<String>(reader["mun_nome"]);
                    obj.mun_codigo_ibge = FormartaDados.GetType<String>(reader["mun_codigo_ibge"]);
                    obj.mun_uf          = FormartaDados.GetType<String>(reader["mun_uf"]);
                    obj.mun_ativo       = FormartaDados.GetType<Boolean>(reader["mun_ativo"]);
                    obj.mun_dt_cadastro = FormartaDados.GetType<DateTime>(reader["mun_dt_cadastro"]);
                    obj.mun_deletado    = FormartaDados.GetType<Boolean>(reader["mun_deletado"]);
                    obj.est_id          = FormartaDados.GetType<Int32>(reader["est_id"]);

                    listObj.Add(obj);
                }

                reader.Close();

                return listObj;
            }
            catch (CustomException ce)
            {
                throw new CustomException(ce.Message, bootStrapMessageType.TYPE_DANGER);
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro desconhecido no sistema.</br>Entre em contato com o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
        }

        public List<tab_municipio> PESQUISA(tab_municipio_filtro filtro)
        {
            return _PESQUISA(filtro);
        }

        private tab_municipio ToObject(IDataReader dr)
        {
            try
            {
                var obj = new tab_municipio();

                obj.mun_id          = FormartaDados.GetType<Int32>(dr["mun_id"]);
                obj.mun_nome        = FormartaDados.GetType<String>(dr["mun_nome"]);
                obj.mun_codigo_ibge = FormartaDados.GetType<String>(dr["mun_codigo_ibge"]);
                obj.mun_uf          = FormartaDados.GetType<String>(dr["mun_uf"]);
                obj.mun_ativo       = FormartaDados.GetType<Boolean>(dr["mun_ativo"]);
                obj.mun_dt_cadastro = FormartaDados.GetType<DateTime>(dr["mun_dt_cadastro"]);
                obj.mun_deletado    = FormartaDados.GetType<Boolean>(dr["mun_deletado"]);
                obj.est_id          = FormartaDados.GetType<Int32>(dr["est_id"]);

                return obj;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
        }
    }
}