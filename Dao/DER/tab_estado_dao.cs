﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using Conexao.AdoProvider.Repositorio;
using Entity.DER;
using Entity.DER.Utils;
using Suporte.Criptografia;
using Suporte.Exceptions;
using Suporte.Formatacao;
using Suporte.MsgBox;

namespace Dao.DER
{
    public class tab_estado_dao
    {
        private DbBase objetoBase { get; set; }

        public tab_estado_dao()
        {
            objetoBase = new DbBase(Cripto.Decrypt(ConfigurationManager.ConnectionStrings["CONNECTION_DER"].ConnectionString, ConfigurationManager.AppSettings["Cripto"]));  
        } 

        public tab_estado GET_BY(int id)
        {
            try
            {
                var obj = new tab_estado();

                objetoBase.SetParameter("@est_id", FormartaDados.SetType<int>(id));

                objetoBase.CommandType = CommandType.StoredProcedure;
                objetoBase.CommandText = "STP_GET_BY_ESTADO";

                var reader = objetoBase.ExecuteReader();

                while (reader.Read())
                    obj = ToObject(reader);

                reader.Close();

                return obj;
            }
            catch (CustomException ce)
            {
                throw new CustomException(ce.Message, bootStrapMessageType.TYPE_DANGER);
            }
            catch (Exception ex)
            {
                throw new CustomException("Ocorreu um erro desconhecido no sistema.</br>Entre em contato com o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
        }

        private List<tab_estado> _PESQUISA(tab_estado_filtro filtro)
        {
            try
            {
                var listObj = new List<tab_estado>();

                objetoBase.SetParameter("@est_nome",FormartaDados.SetType<string>(filtro.est_nome));
                objetoBase.SetParameter("@est_codigo_ibge", FormartaDados.SetType<int?>(filtro.est_codigo_ibge));
                objetoBase.SetParameter("@est_regiao", FormartaDados.SetType<string>(filtro.est_regiao));
                objetoBase.SetParameter("@est_uf", FormartaDados.SetType<string>(filtro.est_uf));

                objetoBase.CommandType = CommandType.StoredProcedure;
                objetoBase.CommandText = "STP_PES_ESTADO";

                var reader = objetoBase.ExecuteReader();

                while (reader.Read())
                {
                    var obj = new tab_estado();

                    obj.est_id          = FormartaDados.GetType<Int32>(reader["est_id"]);
                    obj.est_nome        = FormartaDados.GetType<String>(reader["est_nome"]);
                    obj.est_regiao      = FormartaDados.GetType<String>(reader["est_regiao"]);
                    obj.est_uf          = FormartaDados.GetType<String>(reader["est_uf"]);
                    obj.est_codigo_ibge = FormartaDados.GetType<Int32>(reader["est_codigo_ibge"]);

                    listObj.Add(obj);
                }

                reader.Close();

                return listObj;
            }
            catch (CustomException ce)
            {
                throw new CustomException(ce.Message, bootStrapMessageType.TYPE_DANGER);
            }
            catch (Exception ex)
            {
                throw new CustomException("Ocorreu um erro desconhecido no sistema.</br>Entre em contato com o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
        }

        public List<tab_estado> PESQUISA(tab_estado_filtro filtro)
        {
            return _PESQUISA(filtro);
        }

        private tab_estado ToObject(IDataReader dr)
        {
            try
            {
                var obj = new tab_estado();

                obj.est_id = FormartaDados.GetType<Int32>(dr["est_id"]);
                obj.est_nome = FormartaDados.GetType<String>(dr["est_nome"]);
                obj.est_regiao = FormartaDados.GetType<String>(dr["est_regiao"]);
                obj.est_uf = FormartaDados.GetType<String>(dr["est_uf"]);
                obj.est_codigo_ibge = FormartaDados.GetType<Int32>(dr["est_codigo_ibge"]);

                return obj;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
        }
    }
}