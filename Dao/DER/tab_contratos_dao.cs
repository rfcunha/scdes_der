﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using Conexao.AdoProvider.Repositorio;
using Entity.DER;
using Entity.DER.Utils.Filtro;
using Suporte.Criptografia;
using Suporte.Exceptions;
using Suporte.Formatacao;
using Suporte.MsgBox;

namespace Dao.DER
{
    public class tab_contratos_dao
    {
        private DbBase objetoBase { get; set; }

        public tab_contratos_dao()
        {
            objetoBase = new DbBase(Cripto.Decrypt(ConfigurationManager.ConnectionStrings["CONNECTION_DER"].ConnectionString, ConfigurationManager.AppSettings["Cripto"]));
        }

        public tab_contratos GET_BY(string numContr)
        {
            try
            {
                var obj = new tab_contratos();

                objetoBase.SetParameter("@NumContr", FormartaDados.SetType<string>(numContr));

                objetoBase.CommandType = CommandType.StoredProcedure;
                objetoBase.CommandText = "STP_GET_BY_CONTRATO";

                var reader = objetoBase.ExecuteReader();

                while (reader.Read())
                    obj = ToObject(reader);

                reader.Close();

                return obj;
            }
            catch (CustomException ce)
            {
                throw new CustomException(ce.Message, bootStrapMessageType.TYPE_DANGER);
            }
            catch (Exception ex)
            {
                throw new CustomException("Ocorreu um erro desconhecido no sistema.</br>Entre em contato com o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
        }

        private tab_contratos ToObject(IDataReader dr)
        {
            try
            {
                var obj = new tab_contratos();

                obj.con_id = FormartaDados.GetType<int>(dr["con_id"]);
                obj.AnoContr = FormartaDados.GetType<string>(dr["AnoContr"]);
                obj.CodOrgContr = FormartaDados.GetType<string>(dr["CodOrgContr"]);
                obj.NumContr = FormartaDados.GetType<string>(dr["NumContr"]);
                obj.FlagContrFct = FormartaDados.GetType<string>(dr["FlagContrFct"]);
                obj.FlagRepact = FormartaDados.GetType<string>(dr["FlagRepact"]);
                obj.NumObra = FormartaDados.GetType<string>(dr["NumObra"]);
                obj.CodMun = FormartaDados.GetType<string>(dr["CodMun"]);
                obj.CodRegional = FormartaDados.GetType<string>(dr["CodRegional"]);
                obj.NumCFP = FormartaDados.GetType<string>(dr["NumCFP"]);
                obj.CodSituacao = FormartaDados.GetType<string>(dr["CodSituacao"]);
                obj.DescrObjetoContr1 = FormartaDados.GetType<string>(dr["DescrObjetoContr1"]);
                obj.DescrObjetoContr2 = FormartaDados.GetType<string>(dr["DescrObjetoContr2"]);
                obj.DescrObjetoContr3 = FormartaDados.GetType<string>(dr["DescrObjetoContr3"]);
                obj.DescrObjetoContr4 = FormartaDados.GetType<string>(dr["DescrObjetoContr4"]);
                obj.DescrRazaoSocial = FormartaDados.GetType<string>(dr["DescrRazaoSocial"]);
                obj.NumCGC = FormartaDados.GetType<string>(dr["NumCGC"]);
                obj.DataInicio = FormartaDados.GetType<DateTime?>(dr["DataInicio"]);
                obj.DataFim = FormartaDados.GetType<DateTime?>(dr["DataFim"]);
                obj.PrazoMes = FormartaDados.GetType<short?>(dr["PrazoMes"]);
                obj.PrazoDias = FormartaDados.GetType<short?>(dr["PrazoDias"]);
                obj.DataTPUObra = FormartaDados.GetType<DateTime?>(dr["DataTPUObra"]);
                obj.ValObraPI = FormartaDados.GetType<decimal?>(dr["ValObraPI"]);
                obj.DataI0 = FormartaDados.GetType<DateTime?>(dr["DataI0"]);
                obj.ValContrOrigPI = FormartaDados.GetType<decimal?>(dr["ValContrOrigPI"]);
                obj.ValContrPI = FormartaDados.GetType<decimal?>(dr["ValContrPI"]);
                obj.ValTamPI = FormartaDados.GetType<decimal?>(dr["ValTamPI"]);
                obj.NumUltMed = FormartaDados.GetType<string>(dr["NumUltMed"]);
                obj.ValMedPI = FormartaDados.GetType<decimal?>(dr["ValMedPI"]);
                obj.ValReajMed = FormartaDados.GetType<decimal?>(dr["ValReajMed"]);
                obj.ValMedTot = FormartaDados.GetType<decimal?>(dr["ValMedTot"]);
                obj.ValReajProj = FormartaDados.GetType<decimal?>(dr["ValReajProj"]);
                obj.DataRefReaj = FormartaDados.GetType<DateTime?>(dr["DataRefReaj"]);
                obj.PorcFin = FormartaDados.GetType<float?>(dr["PorcFin"]);
                obj.ValPagMed94 = FormartaDados.GetType<decimal?>(dr["ValPagMed94"]);
                obj.ValPagMed98 = FormartaDados.GetType<decimal?>(dr["ValPagMed98"]);
                obj.ValPagMedTot = FormartaDados.GetType<decimal?>(dr["ValPagMedTot"]);
                obj.ValPagCor94 = FormartaDados.GetType<decimal?>(dr["ValPagCor94"]);
                obj.ValPagCor98 = FormartaDados.GetType<decimal?>(dr["ValPagCor98"]);
                obj.ValPagCorTot = FormartaDados.GetType<decimal?>(dr["ValPagCorTot"]);
                obj.ValDifAcao = FormartaDados.GetType<decimal?>(dr["ValDifAcao"]);
                obj.ValConsolCPA = FormartaDados.GetType<decimal?>(dr["ValConsolCPA"]);
                obj.ValDivida = FormartaDados.GetType<decimal?>(dr["ValDivida"]);
                obj.ValDesconto = FormartaDados.GetType<decimal?>(dr["ValDesconto"]);
                obj.ValMedOrig = FormartaDados.GetType<decimal?>(dr["ValMedOrig"]);
                obj.ValPagOrig = FormartaDados.GetType<decimal?>(dr["ValPagOrig"]);
                obj.ValMedOrigSemCpa = FormartaDados.GetType<decimal?>(dr["ValMedOrigSemCpa"]);
                obj.ValPagOrigSemCpa = FormartaDados.GetType<decimal?>(dr["ValPagOrigSemCpa"]);
                obj.IndCPA = FormartaDados.GetType<string>(dr["IndCPA"]);
                obj.CodNaturObra = FormartaDados.GetType<string>(dr["CodNaturObra"]);
                obj.DTPNS = FormartaDados.GetType<DateTime?>(dr["DTPNS"]);
                obj.Extensao = FormartaDados.GetType<int>(dr["Extensao"]);
                obj.DataAtualiz = FormartaDados.GetType<DateTime?>(dr["DataAtualiz"]);
                obj.DataRecebeProv = FormartaDados.GetType<DateTime?>(dr["DataRecebeProv"]);

                return obj;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
        }
    }
}
