using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using Conexao.AdoProvider.Repositorio;
using Entity.DER;
using Entity.DER.Utils.Filtro;
using Suporte.Criptografia;
using Suporte.Exceptions;
using Suporte.Formatacao;
using Suporte.MsgBox;

namespace Dao.DER
{
    public class tab_agente_financiador_dao
    {
        private DbBase objetoBase { get; set; }

        public tab_agente_financiador_dao()
        {
            objetoBase = new DbBase(Cripto.Decrypt(ConfigurationManager.ConnectionStrings["CONNECTION_DER"].ConnectionString, ConfigurationManager.AppSettings["Cripto"]));  
        } 
        
        public tab_agente_financiador GET_BY(int id)
        {
            try
            {
                var obj = new tab_agente_financiador();
                
                objetoBase.SetParameter("@afi_id", FormartaDados.SetType<int>(id));
                
                objetoBase.CommandType = CommandType.StoredProcedure;
                objetoBase.CommandText = "STP_GET_BY_AGENTE_FINANCIADOR";
                
                var reader = objetoBase.ExecuteReader();

                while (reader.Read())
                    obj = ToObject(reader);

                reader.Close();

                return obj;
            }
            catch (CustomException ce)
            {
                throw new CustomException(ce.Message, bootStrapMessageType.TYPE_DANGER);
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro desconhecido no sistema.</br>Entre em contato com o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
        }

        private List<tab_agente_financiador> _PESQUISA(tab_agente_financiador_filtro filtro)
        {
            try
            {               
                var listObj = new List<tab_agente_financiador>();

                objetoBase.SetParameter("@afi_nome ", FormartaDados.SetType<string>(filtro.afi_nome));
                objetoBase.SetParameter("@afi_ativo", FormartaDados.SetType<bool?>(filtro.afi_ativo));  
                
                objetoBase.CommandType = CommandType.StoredProcedure;
                objetoBase.CommandText = "STP_PES_AGENTE_FINANCIADOR";
                
                var reader = objetoBase.ExecuteReader();
                
                while (reader.Read())
                {
                    var obj = new tab_agente_financiador
                    {
                        afi_id = FormartaDados.Int(reader["afi_id"]),
                        afi_nome = FormartaDados.String(reader["afi_nome"]),
                        afi_dt_cadastro = FormartaDados.Data(reader["afi_dt_cadastro"]),
                        afi_ativo = FormartaDados.Booleano(reader["afi_ativo"]),
                        afi_deletado = FormartaDados.Booleano(reader["afi_deletado"])
                    };

                    listObj.Add(obj);
                }

                reader.Close();

                return listObj;
            }
            catch (CustomException ce)
            {
                throw new CustomException(ce.Message, bootStrapMessageType.TYPE_DANGER);
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro desconhecido no sistema.</br>Entre em contato com o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
        }

        public List<tab_agente_financiador> PESQUISA(tab_agente_financiador_filtro filtro)
        {
            return _PESQUISA(filtro);
        }
        
        private tab_agente_financiador ToObject(IDataReader dr)
        {
            try
            {
                var obj = new tab_agente_financiador
                {
                    afi_id = FormartaDados.Int(dr["afi_id"]),
                    afi_nome = FormartaDados.String(dr["afi_nome"]),
                    afi_dt_cadastro = FormartaDados.Data(dr["afi_dt_cadastro"]),
                    afi_ativo = FormartaDados.Booleano(dr["afi_ativo"]),
                    afi_deletado = FormartaDados.Booleano(dr["afi_deletado"])
                };

                return obj;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
        }
    }
}
