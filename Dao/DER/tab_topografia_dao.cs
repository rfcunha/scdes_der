using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using Conexao.AdoProvider.Repositorio;
using Entity.DER;
using Entity.DER.Utils.Filtro;
using Suporte.Criptografia;
using Suporte.Exceptions;
using Suporte.Formatacao;
using Suporte.MsgBox;

namespace Dao.DER
{
    public class tab_topografia_dao
    {
        private DbBase objetoBase { get; set; }

        public tab_topografia_dao()
        {
            objetoBase = new DbBase(Cripto.Decrypt(ConfigurationManager.ConnectionStrings["CONNECTION_DER"].ConnectionString), ConfigurationManager.AppSettings["Cripto"]);  
        } 
        
        public tab_topografia GET_BY(int id)
        {
            try
            {
                var obj = new tab_topografia();
                
                objetoBase.SetParameter("@top_id", FormartaDados.SetType<int>(id));
                
                objetoBase.CommandType = CommandType.StoredProcedure;
                objetoBase.CommandText = "STP_GET_BY_TOPOLOGIA";
                
                var reader = objetoBase.ExecuteReader();

                while (reader.Read())
                    obj = ToObject(reader);

                reader.Close();

                return obj;
            }
            catch (CustomException ce)
            {
                throw new CustomException(ce.Message, bootStrapMessageType.TYPE_DANGER);
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro desconhecido no sistema.</br>Entre em contato com o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
        }

        private List<tab_topografia> _PESQUISA(tab_topografia_filtro filtro)
        {
            try
            {               
                var listObj = new List<tab_topografia>();

                objetoBase.SetParameter("@top_nome ", FormartaDados.SetType<string>(filtro.top_nome));
                objetoBase.SetParameter("@top_ativo", FormartaDados.SetType<bool?>(filtro.top_ativo));  
                
                objetoBase.CommandType = CommandType.StoredProcedure;
                objetoBase.CommandText = "STP_PES_TOPOLOGIA";
                
                var reader = objetoBase.ExecuteReader();
                
                while (reader.Read())
                {
                    var obj = new tab_topografia
                    {
                        top_id = FormartaDados.Int(reader["top_id"]),
                        top_nome = FormartaDados.String(reader["top_nome"]),
                        top_dt_cadastro = FormartaDados.Data(reader["top_dt_cadastro"]),
                        top_ativo = FormartaDados.Booleano(reader["top_ativo"]),
                        top_deletado = FormartaDados.Booleano(reader["top_deletado"])
                    };

                    listObj.Add(obj);
                }

                reader.Close();

                return listObj;
            }
            catch (CustomException ce)
            {
                throw new CustomException(ce.Message, bootStrapMessageType.TYPE_DANGER);
            }
            catch (Exception)
            {
                throw new CustomException("Ocorreu um erro desconhecido no sistema.</br>Entre em contato com o suporte.", bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
        }

        public List<tab_topografia> PESQUISA(tab_topografia_filtro filtro)
        {
            return _PESQUISA(filtro);
        }
        
        private tab_topografia ToObject(IDataReader dr)
        {
            try
            {
                var obj = new tab_topografia
                {
                    top_id = FormartaDados.Int(dr["top_id"]),
                    top_nome = FormartaDados.String(dr["top_nome"]),
                    top_dt_cadastro = FormartaDados.Data(dr["top_dt_cadastro"]),
                    top_ativo = FormartaDados.Booleano(dr["top_ativo"]),
                    top_deletado = FormartaDados.Booleano(dr["top_deletado"])
                };

                return obj;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
        }

    }
}
