﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using Conexao.EntityFramework.Repositorio;

namespace Dao
{
    /// <summary>
    /// CLASSE DEFAULT DE CRUD PARA ENTITY FRAMEWORK
    /// </summary>
    /// <typeparam name="TClass"></typeparam>
    /// <typeparam name="TCtx"></typeparam>
    public class DefaultDao<TClass, TCtx> : IDisposable
        where TClass : class, new()
        where TCtx : DbContext, new()
    {
        protected Repositorio<TClass, TCtx> _Repositorio;

        protected DefaultDao()
        {
            _Repositorio = new Repositorio<TClass, TCtx>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public virtual TClass INSERT(TClass obj)
        {
            try
            {
                _Repositorio.INSERT(obj);
                _Repositorio.SaveChanges();

                return obj;
            }
            catch (DbEntityValidationException ex)
            {
                var x = MostrarErro(ex);
                throw new Exception(ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public virtual bool INSERT(List<TClass> list)
        {
            try
            {
                foreach (TClass obj in list)
                {
                    _Repositorio.INSERT(obj);
                }

                _Repositorio.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public virtual bool UPDATE(TClass obj)
        {
            try
            {
                _Repositorio.UPDATE(obj);
                _Repositorio.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="properties"></param>
        /// <returns></returns>
        public virtual bool UPDATE(TClass obj, params Expression<Func<TClass, object>>[] properties)
        {
            try
            {
                _Repositorio.UPDATE(obj, properties);
                _Repositorio.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public virtual bool DELETE(TClass obj)
        {
            try
            {
                _Repositorio.DELETE(obj);
                _Repositorio.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Where"></param>
        /// <returns></returns>
        public IQueryable<TClass> FIND(Expression<Func<TClass, bool>> Where = null)
        {
            try
            {
                return _Repositorio.FIND(Where);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TClassX"></typeparam>
        /// <param name="Where"></param>
        /// <returns></returns>
        public IQueryable<TClassX> FIND<TClassX>(Expression<Func<TClassX, bool>> Where = null) where TClassX : class, new()
        {
            try
            {
                return _Repositorio.FIND(Where);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Where"></param>
        /// <returns></returns>
        public bool VALIDATE(Expression<Func<TClass, bool>> Where)
        {
            try
            {
                return _Repositorio.FIND(Where).Any();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public TClass FIND(int id)
        {
            try
            {
                return _Repositorio.FIND(id);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposing)
                return;

            if (disposing)
            {
                if (_Repositorio == null) return;
                _Repositorio.Dispose();
                GC.SuppressFinalize(this);
            }
        }

        //public virtual void SaveChanges()
        //{
        //    _Repositorio.SaveChanges();
        //}

        private List<string> MostrarErro(DbEntityValidationException ex)
        {
            // Retrieve the error messages as a list of strings. 
            List<string> errorMessages = new List<string>();
            foreach (DbEntityValidationResult validationResult in ex.EntityValidationErrors)
            {
                var entityName = validationResult.Entry.Entity.GetType().Name;

                errorMessages.AddRange(validationResult.ValidationErrors.Select(error => entityName + "." + error.PropertyName + ": " + error.ErrorMessage));
            }

            return errorMessages;
        }
    }

}

