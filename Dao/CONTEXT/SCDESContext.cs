using System.Configuration;
using System.Data.Entity;
using Entity.SCDES.Models;
using Entity.SCDES.Models.Mapping;
using Suporte.Criptografia;


namespace Dao.CONTEXT
{
    public partial class SCDESContext : DbContext
    {
        static SCDESContext()
        {
            Database.SetInitializer<SCDESContext>(null);
        }

        public SCDESContext(): base(Cripto.Decrypt(ConfigurationManager.ConnectionStrings["CONNECTION_DEFAULT"].ConnectionString,ConfigurationManager.AppSettings["Cripto"]))
        {
        }

        public DbSet<tab_anexo> tab_anexo { get; set; }
        public DbSet<tab_anexo_identificador> tab_anexo_identificador { get; set; }
        public DbSet<tab_audit> tab_audit { get; set; }
        public DbSet<tab_configuracao_sistema> tab_configuracao_sistema { get; set; }
        public DbSet<tab_desapropriacao> tab_desapropriacao { get; set; }
        public DbSet<tab_desapropriacao_departamento> tab_desapropriacao_departamento { get; set; }
        public DbSet<tab_emitente> tab_emitente { get; set; }
        public DbSet<tab_desapropriacao_emitente> tab_desapropriacao_emitente { get; set; }
        public DbSet<tab_desapropriacao_empreendimento> tab_desapropriacao_empreendimento { get; set; }
        public DbSet<tab_desapropriacao_historico> tab_desapropriacao_historico { get; set; }
        public DbSet<tab_desapropriacao_individual> tab_desapropriacao_individual { get; set; }
        public DbSet<tab_desapropriacao_individual_anexos> tab_desapropriacao_individual_anexos { get; set; }
        public DbSet<tab_desapropriacao_individual_assistente_tecnico> tab_desapropriacao_individual_assistente_tecnico { get; set; }
        public DbSet<tab_desapropriacao_individual_cultura> tab_desapropriacao_individual_cultura { get; set; }
        public DbSet<tab_desapropriacao_individual_divisa> tab_desapropriacao_individual_divisa { get; set; }
        public DbSet<tab_desapropriacao_individual_edificacao> tab_desapropriacao_individual_edificacao { get; set; }
        public DbSet<tab_desapropriacao_individual_edificacao_material> tab_desapropriacao_individual_edificacao_material { get; set; }
        public DbSet<tab_desapropriacao_individual_entrada> tab_desapropriacao_individual_entrada { get; set; }
        public DbSet<tab_desapropriacao_individual_estrada_acesso> tab_desapropriacao_individual_estrada_acesso { get; set; }
        public DbSet<tab_desapropriacao_individual_instalacao> tab_desapropriacao_individual_instalacao { get; set; }
        public DbSet<tab_desapropriacao_individual_interessado> tab_desapropriacao_individual_interessado { get; set; }
        public DbSet<tab_desapropriacao_individual_pendencia> tab_desapropriacao_individual_pendencia { get; set; }
        public DbSet<tab_desapropriacao_individual_perito_judicial> tab_desapropriacao_individual_perito_judicial { get; set; }
        public DbSet<tab_desapropriacao_individual_predominante_entorno> tab_desapropriacao_individual_predominante_entorno { get; set; }
        public DbSet<tab_desapropriacao_individual_planta_propriedade> tab_desapropriacao_individual_planta_propriedade { get; set; }
        public DbSet<tab_desapropriacao_individual_registro_fotografico> tab_desapropriacao_individual_registro_fotografico { get; set; }
        public DbSet<tab_desapropriacao_individual_revisao> tab_desapropriacao_individual_revisao { get; set; }
        public DbSet<tab_desapropriacao_individual_situacao> tab_desapropriacao_individual_situacao { get; set; }
        public DbSet<tab_desapropriacao_individual_status> tab_desapropriacao_individual_status { get; set; }
        public DbSet<tab_desapropriacao_individual_status_evento> tab_desapropriacao_individual_status_evento { get; set; }
        public DbSet<tab_desapropriacao_individual_status_evento_tipo> tab_desapropriacao_individual_status_evento_tipo { get; set; }
        public DbSet<tab_desapropriacao_localidade> tab_desapropriacao_localidade { get; set; }
        public DbSet<tab_desapropriacao_observacao> tab_desapropriacao_observacao { get; set; }
        public DbSet<tab_desapropriacao_recurso_fonte> tab_desapropriacao_recurso_fonte { get; set; }
        public DbSet<tab_desapropriacao_status> tab_desapropriacao_status { get; set; }
        public DbSet<tab_empresa_usuario> tab_empresa_usuario { get; set; }
        public DbSet<tab_menu> tab_menu { get; set; }
        public DbSet<tab_menu_role> tab_menu_role { get; set; }
        public DbSet<tab_perfil> tab_perfil { get; set; }
        public DbSet<tab_perfil_menu_role> tab_perfil_menu_role { get; set; }
        public DbSet<tab_select_helper> tab_select_helper { get; set; }
        public DbSet<tab_select_helper_guia> tab_select_helper_guia { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new tab_auditMap());
            modelBuilder.Configurations.Add(new tab_configuracao_sistemaMap());
            modelBuilder.Configurations.Add(new tab_desapropriacaoMap());
            modelBuilder.Configurations.Add(new tab_desapropriacao_departamentoMap());
            modelBuilder.Configurations.Add(new tab_emitenteMap());
            modelBuilder.Configurations.Add(new tab_desapropriacao_emitenteMap());
            modelBuilder.Configurations.Add(new tab_desapropriacao_empreendimentoMap());
            modelBuilder.Configurations.Add(new tab_desapropriacao_historicoMap());
            modelBuilder.Configurations.Add(new tab_desapropriacao_individualMap());
            modelBuilder.Configurations.Add(new tab_desapropriacao_individual_anexosMap());
            modelBuilder.Configurations.Add(new tab_desapropriacao_individual_assistente_tecnicoMap());
            modelBuilder.Configurations.Add(new tab_desapropriacao_individual_culturaMap());
            modelBuilder.Configurations.Add(new tab_desapropriacao_individual_divisaMap());
            modelBuilder.Configurations.Add(new tab_desapropriacao_individual_edificacaoMap());
            modelBuilder.Configurations.Add(new tab_desapropriacao_individual_edificacao_materialMap());
            modelBuilder.Configurations.Add(new tab_desapropriacao_individual_entradaMap());
            modelBuilder.Configurations.Add(new tab_desapropriacao_individual_estrada_acessoMap());
            modelBuilder.Configurations.Add(new tab_desapropriacao_individual_instalacaoMap());
            modelBuilder.Configurations.Add(new tab_desapropriacao_individual_interessadoMap());
            modelBuilder.Configurations.Add(new tab_desapropriacao_individual_pendenciaMap());
            modelBuilder.Configurations.Add(new tab_desapropriacao_individual_perito_judicialMap());
            modelBuilder.Configurations.Add(new tab_desapropriacao_individual_predominante_entornoMap());
            modelBuilder.Configurations.Add(new tab_desapropriacao_individual_registro_fotograficoMap());
            modelBuilder.Configurations.Add(new tab_desapropriacao_individual_revisaoMap());
            modelBuilder.Configurations.Add(new tab_desapropriacao_individual_situacaoMap());
            modelBuilder.Configurations.Add(new tab_desapropriacao_individual_statusMap());
            modelBuilder.Configurations.Add(new tab_desapropriacao_individual_status_eventoMap());
            modelBuilder.Configurations.Add(new tab_desapropriacao_individual_status_evento_tipoMap());
            modelBuilder.Configurations.Add(new tab_desapropriacao_localidadeMap());
            modelBuilder.Configurations.Add(new tab_desapropriacao_observacaoMap());
            modelBuilder.Configurations.Add(new tab_desapropriacao_recurso_fonteMap());
            modelBuilder.Configurations.Add(new tab_desapropriacao_statusMap());
            modelBuilder.Configurations.Add(new tab_empresa_usuarioMap());
            modelBuilder.Configurations.Add(new tab_menuMap());
            modelBuilder.Configurations.Add(new tab_menu_roleMap());
            modelBuilder.Configurations.Add(new tab_perfilMap());
            modelBuilder.Configurations.Add(new tab_perfil_menu_roleMap());
            //modelBuilder.Configurations.Add(new tab_select_helperMap());
            //modelBuilder.Configurations.Add(new tab_select_helper_campoMap());
            //modelBuilder.Configurations.Add(new tab_select_helper_guiaMap());
        }
    }

    
}