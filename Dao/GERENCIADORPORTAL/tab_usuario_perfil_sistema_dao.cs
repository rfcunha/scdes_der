using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using Conexao.AdoProvider.Repositorio;
using Entity.GERENCIADORPORTAL;
using Entity.GERENCIADORPORTAL.Utils.Filtro;
using Suporte.Criptografia;
using Suporte.Exceptions;
using Suporte.Formatacao;
using Suporte.MsgBox;

namespace Dao.GERENCIADORPORTAL
{
    public sealed class tab_usuario_perfil_sistema_dao
    {
        #region PARAMETROS
        public DbBase objetoBase { get; private set; }
        #endregion PARAMETROS

        #region CONSTRUTOR
        public tab_usuario_perfil_sistema_dao()
        {
            objetoBase = new DbBase(Cripto.Decrypt(ConfigurationManager.ConnectionStrings["CONNECTION_GERENCIADOR_CONTAS"].ConnectionString, ConfigurationManager.AppSettings["Cripto"]), ConfigurationManager.ConnectionStrings["CONNECTION_GERENCIADOR_CONTAS"].ProviderName);
        }

     
        #endregion CONSTRUTOR

        #region METODOS
        public int INSERT(tab_usuario_perfil_sistema obj)
        {
            try
            {
                objetoBase.SetParameter("@sis_id", obj.sis_id);
                objetoBase.SetParameter("@usu_id", obj.usu_id);
                objetoBase.SetParameter("@per_id", obj.per_id);

                objetoBase.CommandType = CommandType.StoredProcedure;

                objetoBase.CommandText = "STP_INS_USUARIO_PERFIL_SISTEMA";

                return Convert.ToInt32(objetoBase.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
        }

        public bool STATUS(tab_usuario_perfil_sistema obj)
        {
            bool retorno; 
            try
            {
                objetoBase.SetParameter("@ups_id", obj.ups_id); 
                objetoBase.SetParameter("@ups_ativo", obj.ups_ativo);

                objetoBase.CommandType = CommandType.StoredProcedure;

                objetoBase.CommandText = "STP_ACT_USUARIO_PERFIL_SISTEMA";

                objetoBase.ExecuteScalar();

                retorno = true;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
            return retorno;
        }

        public int UPDATE(tab_usuario_perfil_sistema obj)
        {
            try
            {
                objetoBase.SetParameter("@ups_id", obj.ups_id);
                objetoBase.SetParameter("@sis_id", obj.sis_id);
                objetoBase.SetParameter("@usu_id", obj.usu_id);
                objetoBase.SetParameter("@per_id", obj.per_id);

                objetoBase.CommandType = CommandType.StoredProcedure;

                objetoBase.CommandText = "STP_UPD_USUARIO_PERFIL_SISTEMA";

                return Convert.ToInt32(objetoBase.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
        }

        public int DELETE(tab_usuario_perfil_sistema obj)
        {
            try
            {
                objetoBase.SetParameter("@ups_id", obj.ups_id); 

                objetoBase.CommandType = CommandType.StoredProcedure;

                objetoBase.CommandText = "STP_DEL_USUARIO_PERFIL_SISTEMA";

                return Convert.ToInt32(objetoBase.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
        }
 
        public bool VERIFICAR_STATUS(tab_usuario_perfil_sistema obj)
        {
            bool retorno;
            try
            {
                objetoBase.SetParameter("@per_id", obj.per_id);
                objetoBase.SetParameter("@sis_id", obj.sis_id);
                objetoBase.SetParameter("@ups_ativo", obj.ups_ativo);

                objetoBase.CommandType = CommandType.StoredProcedure;

                objetoBase.CommandText = "STP_VERIFICAR_ACT_USUARIO_PERFIL_SISTEMA";

                objetoBase.ExecuteScalar();

                retorno = true;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
            return retorno;
        }
 
        public bool VERIFICAR_DELETE(tab_usuario_perfil_sistema obj)
        {
            bool retorno;
            try
            {

                objetoBase.SetParameter("@per_id", obj.per_id);
                objetoBase.SetParameter("@sis_id", obj.sis_id);

                objetoBase.CommandType = CommandType.StoredProcedure;

                objetoBase.CommandText = "STP_VERIFICAR_DEL_USUARIO_PERFIL_SISTEMA";

                objetoBase.ExecuteScalar(); 

                retorno = true;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
            return retorno;
        }


        /// <summary>
        /// Metodo n�o necessitado no momento
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tab_usuario_perfil_sistema GET_BY(int id)
        {
            throw new NotImplementedException();
        }

        public ICollection<tab_usuario_perfil_sistema> GET_ALL(tab_usuario_perfil_sistema_filtro obj)
        {
            try
            {
                var listObj = new List<tab_usuario_perfil_sistema>();
                objetoBase.SetParameter("@ups_id", obj.ups_id);
                objetoBase.SetParameter("@sis_id", obj.sis_id);
                objetoBase.SetParameter("@usu_id", obj.usu_id);
                objetoBase.SetParameter("@per_id", obj.per_id);

                objetoBase.CommandType = CommandType.StoredProcedure;

                objetoBase.CommandText = "STP_PES_USUARIO_PERFIL_SISTEMA";

                var reader = objetoBase.ExecuteReader();

                while (reader.Read())
                {
                    listObj.Add(ToObject(reader));
                }

                reader.Close();

                return listObj;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
        }
        #endregion METODOS

        /******************PARA LEITURA*****************/
        public tab_usuario_perfil_sistema ToObject(IDataReader dr)
        {
            try
            {
                var obj = new tab_usuario_perfil_sistema
                {
                    ups_id = FormartaDados.Int(dr["ups_id"]),
                    sis_id = FormartaDados.Int(dr["sis_id"]),
                    usu_id = FormartaDados.Int(dr["usu_id"]),
                    per_id = FormartaDados.Int(dr["per_id"]),
                    ups_dt_cadastro = FormartaDados.Data(dr["ups_dt_cadastro"]),
                    ups_ativo = FormartaDados.Booleano(dr["ups_ativo"]),
                    ups_deletado = FormartaDados.Booleano(dr["ups_deletado"]), 
                };
                return obj;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
        }
    }
}
