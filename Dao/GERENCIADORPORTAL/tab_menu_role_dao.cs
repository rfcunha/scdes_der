﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Conexao.AdoProvider.Repositorio;
using Entity.GERENCIADORPORTAL;
using Entity.GERENCIADORPORTAL.Utils.Filtro;
using Suporte.Criptografia;
using Suporte.Exceptions;
using Suporte.Formatacao;
using Suporte.MsgBox;

namespace Dao.GERENCIADORPORTAL
{
    public sealed class tab_menu_role_dao
    {
        #region PARAMETROS
        public DbBase objetoBase { get; private set; }
        #endregion PARAMETROS

        #region CONSTRUTOR
        public tab_menu_role_dao()
        {
            objetoBase = new DbBase(Cripto.Decrypt(ConfigurationManager.ConnectionStrings["CONNECTION_DEFAULT"].ConnectionString, ConfigurationManager.AppSettings["Cripto"]), ConfigurationManager.ConnectionStrings["CONNECTION_DEFAULT"].ProviderName);
        }

      
        #endregion CONSTRUTOR

        #region METODOS
        public int INSERT(tab_menu_role obj)
        {
            try
            {
                objetoBase.SetParameter("@mro_nome", obj.mro_nome);
                objetoBase.SetParameter("@mro_controle", obj.mro_controle);
                objetoBase.SetParameter("@men_id ", obj.men_id);

                objetoBase.CommandType = CommandType.StoredProcedure;

                objetoBase.CommandText = "STP_INS_MENU_ROLE";

                return Convert.ToInt32(objetoBase.ExecuteScalar());
            }
            catch (SqlException sqlex)
            {
                throw sqlex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objetoBase.Dispose();
            }
        }

        public int UPDATE(tab_menu_role obj)
        {
            try
            {
                objetoBase.SetParameter("@mro_id", obj.mro_id);
                objetoBase.SetParameter("@mro_nome", obj.mro_nome);
                objetoBase.SetParameter("@mro_controle", obj.mro_controle);
                objetoBase.SetParameter("@men_id ", obj.men_id);

                objetoBase.CommandType = CommandType.StoredProcedure;

                objetoBase.CommandText = "STP_UPD_MENU_ROLE";

                return Convert.ToInt32(objetoBase.ExecuteScalar());
            }
            catch (SqlException sqlex)
            {
                throw sqlex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objetoBase.Dispose();
            }
        }

        public int DELETE(tab_menu_role obj)
        {
            try
            {
                objetoBase.SetParameter("@mro_id", obj.mro_id);

                objetoBase.CommandType = CommandType.StoredProcedure;

                objetoBase.CommandText = "STP_DEL_MENU_ROLE";

                return Convert.ToInt32(objetoBase.ExecuteNonQuery());
            }
            catch (SqlException sqlex)
            {
                throw sqlex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objetoBase.Dispose();
            }
        }

        public bool STATUS(tab_menu_role obj)
        {
            throw new NotImplementedException();
        }
 
        public tab_menu_role GET_BY(int id)
        {
            try
            {
                objetoBase.SetParameter("@mro_id", id);

                objetoBase.CommandType = CommandType.StoredProcedure;

                objetoBase.CommandText = "STP_GET_BY_MENU_ROLE";

                var reader = objetoBase.ExecuteReader();

                var obj = new tab_menu_role();

                while (reader.Read())
                {
                    obj = ToObject(reader);
                }

                reader.Close();

                return obj;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
        }

        public ICollection<tab_menu_role> GET_ALL(tab_menu_role_filtro obj)
        {
            try
            {
                var listObj = new List<tab_menu_role>();
                objetoBase.SetParameter("@mro_nome", obj.mro_nome);
                objetoBase.SetParameter("@mro_ativo ", obj.mro_ativo);
                objetoBase.SetParameter("@men_id ", obj.men_id);

                objetoBase.CommandType = CommandType.StoredProcedure;

                objetoBase.CommandText = "STP_PES_MENU_ROLE";

                var reader = objetoBase.ExecuteReader();

                while (reader.Read())
                {
                    listObj.Add(ToObject(reader));
                }

                reader.Close();

                return listObj;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
        }
        #endregion METODOS

        /******************PARA LEITURA*****************/
        public tab_menu_role ToObject(IDataReader dr)
        {
            try
            {
                var obj = new tab_menu_role
                {
                    men_id = FormartaDados.Int(dr["men_id"]),
                    mro_nome = FormartaDados.String(dr["mro_nome"]),
                    mro_controle = FormartaDados.String(dr["mro_controle"]),
                    mro_ativo = FormartaDados.Booleano(dr["mro_ativo"]),
                    mro_dt_cadastro = FormartaDados.Data(dr["mro_dt_cadastro"]),
                    mro_id = FormartaDados.Int(dr["mro_id"])
                };
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
