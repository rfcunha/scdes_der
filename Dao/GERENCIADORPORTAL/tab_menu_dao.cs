﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Conexao.AdoProvider.Repositorio;
using Entity.GERENCIADORPORTAL;
using Entity.GERENCIADORPORTAL.Utils.Filtro;
using Suporte.Criptografia;
using Suporte.Exceptions;
using Suporte.Formatacao;
using Suporte.MsgBox;

namespace Dao.GERENCIADORPORTAL
{
    public sealed class tab_menu_dao
    {
        #region PARAMETROS
        public DbBase objetoBase { get; private set; }
        #endregion PARAMETROS

        #region CONSTRUTOR
        public tab_menu_dao()
        {
             objetoBase = new DbBase(Cripto.Decrypt(ConfigurationManager.ConnectionStrings["CONNECTION_DEFAULT"].ConnectionString, ConfigurationManager.AppSettings["Cripto"]), ConfigurationManager.ConnectionStrings["CONNECTION_DEFAULT"].ProviderName);  
        }

      
        #endregion CONSTRUTOR
 
        #region METODOS
        public int INSERT(tab_menu obj)
        {
            try
            {
                objetoBase.SetParameter("@men_menu", obj.men_menu, DbType.String);
                objetoBase.SetParameter("@men_hierarquia", obj.men_hierarquia, DbType.String);
                objetoBase.SetParameter("@men_pagina", obj.men_pagina, DbType.String);
                objetoBase.SetParameter("@men_url", obj.men_url, DbType.String);
                objetoBase.SetParameter("@men_icon", obj.men_icon, DbType.String);
                objetoBase.SetParameter("@men_pai", obj.men_pai);
                objetoBase.SetParameter("@men_exibir", obj.men_exibir,DbType.Boolean); 

                objetoBase.CommandType = CommandType.StoredProcedure;

                objetoBase.CommandText = "STP_INS_MENU";

                return Convert.ToInt32(objetoBase.ExecuteScalar());
            }
            catch (SqlException sqlex)
            {
                throw sqlex; 
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objetoBase.Dispose();
            }
        } 
       
        public int UPDATE(tab_menu obj)
        {
            try
            {
                objetoBase.SetParameter("@men_id", obj.men_id);
                objetoBase.SetParameter("@men_menu", obj.men_menu);
                objetoBase.SetParameter("@men_hierarquia", obj.men_hierarquia);
                objetoBase.SetParameter("@men_pagina", obj.men_pagina);
                objetoBase.SetParameter("@men_url", obj.men_url);
                objetoBase.SetParameter("@men_icon", obj.men_icon); 
                objetoBase.SetParameter("@men_pai", obj.men_pai);
                objetoBase.SetParameter("@men_exibir", obj.men_exibir);

                objetoBase.CommandType = CommandType.StoredProcedure;

                objetoBase.CommandText = "STP_UPD_MENU";

                return Convert.ToInt32(objetoBase.ExecuteScalar());
            }
            catch (SqlException sqlex)
            {
                throw sqlex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objetoBase.Dispose();
            }
        }

        public int DELETE(tab_menu obj)
        {
            try
            {
                objetoBase.SetParameter("@men_id", obj.men_id);

                objetoBase.CommandType = CommandType.StoredProcedure;

                objetoBase.CommandText = "STP_DEL_MENU";

                return Convert.ToInt32(objetoBase.ExecuteNonQuery());
            }
            catch (SqlException sqlex)
            {
                throw sqlex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objetoBase.Dispose();
            }
        }

        public bool STATUS(tab_menu obj)
        {
            bool retorno;

            try
            {
                objetoBase.SetParameter("@men_id", obj.men_id);
                objetoBase.SetParameter("@men_ativo", obj.men_ativo);

                objetoBase.CommandType = CommandType.StoredProcedure;

                objetoBase.CommandText = "STP_ACT_MENU";

                objetoBase.ExecuteScalar();

                retorno = true;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
            return retorno;
        }
         
        public tab_menu GET_BY(int id)
        {
            try
            {
                
                objetoBase.SetParameter("@men_id", id);

                objetoBase.CommandType = CommandType.StoredProcedure;

                objetoBase.CommandText = "STP_GET_BY_MENU";

                var reader = objetoBase.ExecuteReader();

                var obj = new tab_menu();

                while (reader.Read())
                {
                    obj = ToObject(reader);
                }

                reader.Close();

                return obj;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
        }

        public ICollection<tab_menu> GET_ALL(tab_menu_filtro obj)
        {
            try
            {
                objetoBase.SetParameter("@men_ativo ", obj.men_ativo);
                objetoBase.SetParameter("@men_menu ", obj.men_menu);  

                objetoBase.CommandType = CommandType.StoredProcedure;

                objetoBase.CommandText = "STP_PES_MENU";

                var reader = objetoBase.ExecuteReader();

                var listObj = new List<tab_menu>();

                while (reader.Read())
                {
                    listObj.Add(ToObject(reader));
                }

                reader.Close();

                return listObj;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
        }
        #endregion METODOS

        /******************PARA LEITURA*****************/ 
        public tab_menu ToObject(IDataReader dr)
        {
            try
            {
                var obj = new tab_menu
                {

                    men_id = FormartaDados.Int(dr["men_id"]),
                    men_menu = FormartaDados.String(dr["men_menu"]),
                    men_icon = FormartaDados.String(dr["men_icon"]),
                    men_hierarquia = FormartaDados.String(dr["men_hierarquia"]),
                    men_pagina = FormartaDados.String(dr["men_pagina"]),
                    men_url = FormartaDados.String(dr["men_url"]),
                    men_exibir = FormartaDados.Booleano(dr["men_exibir"]),
                    men_ordem = FormartaDados.Int(dr["men_ordem"]),
                    men_pai = FormartaDados.IntNull(dr["men_pai"]),
                    men_ativo = FormartaDados.Booleano(dr["men_ativo"]),
                    men_dt_cadastro = FormartaDados.Data(dr["men_dt_cadastro"]) 
                };
                return obj;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
        } 
    }
}
