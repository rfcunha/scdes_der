﻿using System;
using System.Configuration;
using System.Data;
using Conexao.AdoProvider.Repositorio;
using Suporte.Criptografia;
using Suporte.Exceptions;
using Suporte.Formatacao;
using Suporte.MsgBox;

namespace Dao.GERENCIADORPORTAL
{
    public sealed class tab_configuracao_sistema_dao
    {
        #region PARAMETROS
        public DbBase objetoBase { get; private set; }
        #endregion PARAMETROS

        #region CONSTRUTOR
        public tab_configuracao_sistema_dao()
        {
             objetoBase = new DbBase(Cripto.Decrypt(ConfigurationManager.ConnectionStrings["CONNECTION_DEFAULT"].ConnectionString, ConfigurationManager.AppSettings["Cripto"]), ConfigurationManager.ConnectionStrings["CONNECTION_DEFAULT"].ProviderName);  
        }

      
        #endregion CONSTRUTOR
 
        #region METODOS
        
        public int STP_GET_BY_ID_SISTEMA()
        {
            try
            {
                int obj = 0;

                objetoBase.CommandType = CommandType.StoredProcedure;

                objetoBase.CommandText = "STP_GET_BY_ID_SISTEMA";

                var reader = objetoBase.ExecuteReader();

                while (reader.Read())
                {
                    obj = FormartaDados.Int(reader["csi_valor"]);
                } 

                reader.Close();

                return obj;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
        }

        #endregion METODOS
         
    }
}
