﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using Conexao.AdoProvider.Repositorio;
using Entity.GERENCIADORPORTAL;
using Entity.GERENCIADORPORTAL.Utils.Filtro;
using Suporte.Criptografia;
using Suporte.Exceptions;
using Suporte.Formatacao;
using Suporte.MsgBox;

namespace Dao.GERENCIADORPORTAL
{
    public sealed class tab_usuario_dao 
    {
        #region PAREMETROS
        public DbBase objetoBase { get; private set; }
        #endregion PARAMETROS

        #region CONSTRUTOR
        public tab_usuario_dao()
        {
            objetoBase = new DbBase(Cripto.Decrypt(ConfigurationManager.ConnectionStrings["CONNECTION_GERENCIADOR_CONTAS"].ConnectionString, ConfigurationManager.AppSettings["Cripto"]), ConfigurationManager.ConnectionStrings["CONNECTION_GERENCIADOR_CONTAS"].ProviderName);
        }

       
        #endregion CONSTRUTOR

        #region METODOS
        public int INSERT(tab_usuario obj)
        {
            try
            {
                objetoBase.SetParameter("@usu_nome", obj.usu_nome);
                objetoBase.SetParameter("@pes_id", obj.pes_id);
                objetoBase.SetParameter("@usu_senha", obj.usu_senha);
                objetoBase.SetParameter("@usu_login", obj.usu_login);
                objetoBase.SetParameter("@userCreate", obj.userCreate);
                
                objetoBase.CommandType = CommandType.StoredProcedure;
                
                objetoBase.CommandText = "STP_INS_USUARIO";

                return Convert.ToInt32(objetoBase.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
        }

        public bool STATUS(tab_usuario obj)
        {
            bool retorno;

            try
            {
                objetoBase.SetParameter("@usu_id", obj.usu_id);
                objetoBase.SetParameter("@usu_ativo", obj.usu_ativo);
                objetoBase.SetParameter("@userLastUpdate", obj.userLastUpdate);
                objetoBase.CommandType = CommandType.StoredProcedure;
                objetoBase.CommandText = "STP_ACT_USUARIO";
                objetoBase.ExecuteScalar();
                retorno = true;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
            return retorno;
        }

        public int UPDATE(tab_usuario obj)
        {
            try
            {
                objetoBase.SetParameter("@usu_id", obj.usu_id);
                objetoBase.SetParameter("@pes_id", obj.pes_id);
                objetoBase.SetParameter("@usu_nome", obj.usu_nome);
                objetoBase.SetParameter("@usu_senha", obj.usu_senha);
                objetoBase.SetParameter("@usu_trocar_senha", obj.usu_trocar_senha);
                objetoBase.SetParameter("@usu_login", obj.usu_login);
                objetoBase.SetParameter("@userLastUpdate", obj.userLastUpdate);

                objetoBase.CommandType = CommandType.StoredProcedure;

                objetoBase.CommandText = "STP_UPD_USUARIO";

                return Convert.ToInt32(objetoBase.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
        }

        public int STP_UPD_USUARIO_SENHA(int usu_id, string usu_senha, string userLastUpdate, bool usu_trocar_senha)
        {
            try
            {

                objetoBase.SetParameter("@usu_id", usu_id);
                objetoBase.SetParameter("@usu_senha", usu_senha);
                objetoBase.SetParameter("@usu_trocar_senha", usu_trocar_senha);
                objetoBase.SetParameter("@userLastUpdate", userLastUpdate);

                objetoBase.CommandType = CommandType.StoredProcedure;

                objetoBase.CommandText = "STP_UPD_USUARIO_SENHA";

                return Convert.ToInt32(objetoBase.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
        }

        public int DELETE(tab_usuario obj)
        {
            try
            {
                objetoBase.SetParameter("@usu_id", obj.usu_id);
                objetoBase.SetParameter("@userLastUpdate", obj.userLastUpdate);

                objetoBase.CommandType = CommandType.StoredProcedure;

                objetoBase.CommandText = "STP_DEL_USUARIO";

                return Convert.ToInt32(objetoBase.ExecuteNonQuery());
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
        }

        public tab_usuario GET_BY(int id)
        {
            try
            {
                var obj = new tab_usuario();

                objetoBase.SetParameter("@usu_id", id);

                objetoBase.CommandType = CommandType.StoredProcedure;

                objetoBase.CommandText = "STP_GET_BY_USUARIO";

                var reader = objetoBase.ExecuteReader();

                while (reader.Read())
                {
                    obj = ToObject(reader);
                }

                reader.Close();

                return obj;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
        }

        public ICollection<tab_usuario> GET_ALL(tab_usuario_filtro obj)
        {
            try
            {
                objetoBase.SetParameter("@usu_nome", obj.usu_nome);
                objetoBase.SetParameter("@usu_login", obj.usu_login);
                objetoBase.SetParameter("@usu_ativo", obj.usu_ativo);
                
                objetoBase.CommandType = CommandType.StoredProcedure;
                
                objetoBase.CommandText = "STP_PES_USUARIO";
                
                var reader = objetoBase.ExecuteReader();

                var listObj = new List<tab_usuario>();
                
                while (reader.Read())
                {
                    listObj.Add(ToObject(reader));
                }

                reader.Close();
                
                return listObj;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
        }

        public ICollection<tab_usuario> GET_GRID(tab_usuario_filtro obj)
        {
            try
            {
                var listObj = new List<tab_usuario>();

                objetoBase.SetParameter("@usu_nome", obj.usu_nome);
                objetoBase.SetParameter("@usu_login", obj.usu_login);
                objetoBase.SetParameter("@usu_ativo", obj.usu_ativo);
                objetoBase.SetParameter("@PageSize", obj.pageSize);
                objetoBase.SetParameter("@PageIndex", obj.pageIndex);

                var pageCount = objetoBase.SetParameter("@PageCount", DbType.Int32, ParameterDirection.Output);

                objetoBase.CommandType = CommandType.StoredProcedure;

                objetoBase.CommandText = "STP_GRI_USUARIO";

                var reader = objetoBase.ExecuteReader();

                while (reader.Read())
                {
                    listObj.Add(ToObject(reader));
                }

                reader.Close();
                obj.pageCount = Convert.ToInt32(pageCount.Value);
                return listObj;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
        }

        public tab_usuario VALIDAR_USUARIO(tab_usuario_filtro objFiltro)
        {
            var obj = new tab_usuario();

            objetoBase.SetParameter("@usu_login", objFiltro.usu_login);
            objetoBase.SetParameter("@usu_senha", objFiltro.usu_senha);

            objetoBase.CommandType = CommandType.StoredProcedure;

            objetoBase.CommandText = "STP_VALIDA_USUARIO";

            var reader = objetoBase.ExecuteReader();

            while (reader.Read())
            {
                obj.usu_id = FormartaDados.Int(reader["usu_id"]);
                obj.usu_imagem = (byte[]) reader["usu_imagem"];
                obj.pes_id = FormartaDados.Int(reader["pes_id"]);
                obj.usu_nome = FormartaDados.String(reader["usu_nome"]);
                obj.usu_senha = FormartaDados.String(reader["usu_senha"]);
                obj.usu_login = FormartaDados.String(reader["usu_login"]);
                obj.usu_ativo = FormartaDados.Booleano(reader["usu_ativo"]);
                obj.usu_deletado = FormartaDados.Booleano(reader["usu_deletado"]);
                obj.usu_dt_cadastro = FormartaDados.Data(reader["usu_dt_cadastro"]);
                obj.userCreate = FormartaDados.String(reader["userCreate"]);
                obj.dtCreate = FormartaDados.DataNull(reader["dtCreate"]);
                obj.userLastUpdate = FormartaDados.String(reader["userLastUpdate"]);
                obj.dtLastUpdate = FormartaDados.DataNull(reader["dtLastUpdate"]);
                obj.userDelete = FormartaDados.String(reader["userDelete"]);
                obj.dtDelete = FormartaDados.DataNull(reader["dtDelete"]);
                obj.usu_trocar_senha = FormartaDados.Booleano(reader["usu_trocar_senha"]);
                obj.usu_nome_login = FormartaDados.String(reader["usu_nome"]) + "-" + FormartaDados.String(reader["usu_login"]);
            }

            reader.Close();

            return obj;
        }

        #endregion METODOS

        /******************PARA LEITURA*****************/
        public tab_usuario ToObject(IDataReader dr)
        {
            try
            {
                var obj = new tab_usuario
                {
                    usu_id = FormartaDados.Int(dr["usu_id"]),
                    usu_imagem = (byte[])dr["usu_imagem"],
                    pes_id = FormartaDados.Int(dr["pes_id"]),
                    usu_nome = FormartaDados.String(dr["usu_nome"]),
                    usu_senha = FormartaDados.String(dr["usu_senha"]),
                    usu_login = FormartaDados.String(dr["usu_login"]),
                    usu_ativo = FormartaDados.Booleano(dr["usu_ativo"]),
                    usu_deletado = FormartaDados.Booleano(dr["usu_deletado"]),
                    usu_dt_cadastro = FormartaDados.Data(dr["usu_dt_cadastro"]),
                    userCreate = FormartaDados.String(dr["userCreate"]),
                    dtCreate = FormartaDados.DataNull(dr["dtCreate"]),
                    userLastUpdate = FormartaDados.String(dr["userLastUpdate"]),
                    dtLastUpdate = FormartaDados.DataNull(dr["dtLastUpdate"]),
                    userDelete = FormartaDados.String(dr["userDelete"]),
                    dtDelete = FormartaDados.DataNull(dr["dtDelete"]),
                    usu_trocar_senha = FormartaDados.Booleano(dr["usu_trocar_senha"])
                };
                return obj;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
        } 
    }
}