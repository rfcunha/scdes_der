﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using Conexao.AdoProvider.Repositorio;
using Entity.GERENCIADORPORTAL;
using Entity.GERENCIADORPORTAL.Utils.Filtro;
using Suporte.Criptografia;
using Suporte.Exceptions;
using Suporte.Formatacao;
using Suporte.MsgBox;

namespace Dao.GERENCIADORPORTAL
{
    public sealed class tab_perfil_menu_role_dao 
    {
        #region PARAMETROS
        public DbBase objetoBase { get; private set; }
        #endregion PARAMETROS

        #region CONSTRUTOR
        public tab_perfil_menu_role_dao()
        {
            objetoBase = new DbBase(Cripto.Decrypt(ConfigurationManager.ConnectionStrings["CONNECTION_DEFAULT"].ConnectionString, ConfigurationManager.AppSettings["Cripto"]), ConfigurationManager.ConnectionStrings["CONNECTION_DEFAULT"].ProviderName);
        }

       
        #endregion PARAMETROS

        #region METODOS
        public int INSERT(tab_perfil_menu_role obj)
        {
            try
            {
                objetoBase.SetParameter("@mro_id", obj.mro_id);
                objetoBase.SetParameter("@per_id", obj.per_id); 
                objetoBase.CommandType = CommandType.StoredProcedure;

                objetoBase.CommandText = "STP_INS_PERFIL_MENU_ROLE";

                return Convert.ToInt32(objetoBase.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
        }

        public bool STATUS(tab_perfil_menu_role obj)
        {
            throw new NotImplementedException();
        }

        public int UPDATE(tab_perfil_menu_role obj)
        {
            throw new NotImplementedException();
        }

        public int DELETE(tab_perfil_menu_role obj)
        {
            try
            {
                objetoBase.SetParameter("@pmr_id", obj.pmr_id);
                objetoBase.CommandType = CommandType.StoredProcedure;

                objetoBase.CommandText = "STP_DEL_PERFIL_MENU_ROLE";

                return Convert.ToInt32(objetoBase.ExecuteNonQuery());
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
        }

        public int DELETE_ALL_ROLE(int per_id)
        {
            try
            {
                objetoBase.SetParameter("@per_id", per_id);

                objetoBase.CommandType = CommandType.StoredProcedure;

                objetoBase.CommandText = "STP_DEL_PERFIL_MENU_ROLE_ALL";

                return Convert.ToInt32(objetoBase.ExecuteNonQuery());
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
        }

        public tab_perfil_menu_role GET_BY(int id)
        {
            throw new NotImplementedException();
        }

        public ICollection<tab_perfil_menu_role> GET_ALL(tab_perfil_menu_role_filtro obj)
        {
            try
            {
                
                objetoBase.SetParameter("@per_id", obj.per_id);

                objetoBase.CommandType = CommandType.StoredProcedure;

                objetoBase.CommandText = "STP_PES_PERFIL_MENU_ROLE";

                var reader = objetoBase.ExecuteReader();

                var listObj = new List<tab_perfil_menu_role>();

                while (reader.Read())
                {
                    listObj.Add(ToObject(reader));
                }

                reader.Close();

                return listObj;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
        }
        #endregion METODOS

        /******************PARA LEITURA*****************/
        public tab_perfil_menu_role ToObject(IDataReader dr)
        {
            try
            {
                var obj = new tab_perfil_menu_role
                {
                    pmr_id = FormartaDados.Int(dr["pmr_id"]),
                    per_id = FormartaDados.Int(dr["per_id"]),
                    mro_id = FormartaDados.Int(dr["mro_id"]),
                    pmr_dt_cadastro = FormartaDados.Data(dr["pmr_dt_cadastro"])  
                };
                return obj;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
        }
    }
}