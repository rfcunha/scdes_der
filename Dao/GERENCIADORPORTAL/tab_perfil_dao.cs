﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using Conexao.AdoProvider.Repositorio;
using Entity.GERENCIADORPORTAL;
using Entity.GERENCIADORPORTAL.Utils.Filtro;
using Suporte.Criptografia;
using Suporte.Exceptions;
using Suporte.Formatacao;
using Suporte.MsgBox;

namespace Dao.GERENCIADORPORTAL
{
    public sealed class tab_perfil_dao 
    {

        #region PARAMETROS
        public DbBase objetoBase { get; private set; }
        #endregion PARAMETROS

        #region CONSTRUTOR
        public tab_perfil_dao()
        {
            objetoBase = new DbBase(Cripto.Decrypt(ConfigurationManager.ConnectionStrings["CONNECTION_DEFAULT"].ConnectionString, ConfigurationManager.AppSettings["Cripto"]));
        }

       
        #endregion CONSTRUTOR

        #region METODOS

        public bool UPDATE_XML(int per_id)
        {
            try
            {
                objetoBase.SetParameter("@per_id", per_id);

                objetoBase.CommandType = CommandType.StoredProcedure;

                objetoBase.CommandText = "STP_UPD_PERFIL_XML";

                objetoBase.ExecuteNonQuery();

                return true;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
        }

        public int INSERT(tab_perfil obj)
        {
            try
            {
                objetoBase.SetParameter("@per_nome", obj.per_nome);
                objetoBase.SetParameter("@userCreate", obj.userCreate);
                objetoBase.SetParameter("@per_update_xml", obj.per_update_xml); 

                objetoBase.CommandType = CommandType.StoredProcedure;

                objetoBase.CommandText = "STP_INS_PERFIL";

                return Convert.ToInt32(objetoBase.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
        }

        public bool STATUS(tab_perfil obj)
        {
            bool retorno;

            try
            {
                objetoBase.SetParameter("@per_id", obj.per_id);
                objetoBase.SetParameter("@sis_id", obj.sis_id);
                objetoBase.SetParameter("@per_ativo", obj.per_ativo);
                objetoBase.SetParameter("@userLastUpdate", obj.userLastUpdate);

                objetoBase.CommandType = CommandType.StoredProcedure;

                objetoBase.CommandText = "STP_ACT_PERFIL";

                objetoBase.ExecuteScalar();

                retorno = true;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
            return retorno;
        }

        public int UPDATE(tab_perfil obj)
        {
            try
            {
                objetoBase.SetParameter("@per_id", obj.per_id);
                objetoBase.SetParameter("@per_nome", obj.per_nome);
                objetoBase.SetParameter("@userLastUpdate", obj.userLastUpdate);
                objetoBase.SetParameter("@per_update_xml", obj.per_update_xml); 

                objetoBase.CommandType = CommandType.StoredProcedure;

                objetoBase.CommandText = "STP_UPD_PERFIL";

                return Convert.ToInt32(objetoBase.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
        }

        public int DELETE(tab_perfil obj)
        {
            try
            {
                objetoBase.SetParameter("@per_id", obj.per_id);
                objetoBase.SetParameter("@sis_id", obj.sis_id); 
                objetoBase.SetParameter("@userLastUpdate", obj.userLastUpdate);

                objetoBase.CommandType = CommandType.StoredProcedure;

                objetoBase.CommandText = "STP_DEL_PERFIL";

                return Convert.ToInt32(objetoBase.ExecuteNonQuery());
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
        }

        public tab_perfil GET_BY(int id)
        {
            try
            {
                var obj = new tab_perfil();

                objetoBase.SetParameter("@per_id", id);

                objetoBase.CommandType = CommandType.StoredProcedure;

                objetoBase.CommandText = "STP_GET_BY_PERFIL";

                var reader = objetoBase.ExecuteReader();

                while (reader.Read())
                {
                    obj = ToObject(reader);
                }

                reader.Close();
                return obj;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
        }

        public ICollection<tab_perfil> GET_ALL(tab_perfil_filtro obj)
        {
            try
            {
                var listObj = new List<tab_perfil>();

                objetoBase.SetParameter("@per_nome", obj.per_nome);
                objetoBase.SetParameter("@per_ativo", obj.per_ativo);

                objetoBase.CommandType = CommandType.StoredProcedure;

                objetoBase.CommandText = "STP_PES_PERFIL";

                var reader = objetoBase.ExecuteReader();

                while (reader.Read())
                {
                    listObj.Add(ToObject(reader));
                }

                reader.Close();

                return listObj;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }
        }

        public ICollection<tab_perfil> GET_GRID(tab_perfil_filtro obj)
        { 
            try
            {
                var listObj = new List<tab_perfil>();

                objetoBase.SetParameter("@per_nome", obj.per_nome);
                objetoBase.SetParameter("@per_ativo", obj.per_ativo);
                objetoBase.SetParameter("@PageSize", obj.pageSize);
                objetoBase.SetParameter("@PageIndex", obj.pageIndex);

                var pageCount = objetoBase.SetParameter("@PageCount", DbType.Int32, ParameterDirection.Output);

                objetoBase.CommandType = CommandType.StoredProcedure;

                objetoBase.CommandText = "STP_GRI_PERFIL";

                var reader = objetoBase.ExecuteReader();

                while (reader.Read())
                {
                    listObj.Add(ToObject(reader));
                }

                reader.Close();

                obj.pageCount = Convert.ToInt32(pageCount.Value);

                return listObj;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
            finally
            {
                objetoBase.Dispose();
            }  
        }
        #endregion METODOS

        /******************PARA LEITURA*****************/
        public tab_perfil ToObject(IDataReader dr)
        {
            try
            {
                var obj = new tab_perfil
                {
                    per_id = FormartaDados.Int(dr["per_id"]),
                    per_nome = FormartaDados.String(dr["per_nome"]),
                    per_dt_cadastro = FormartaDados.Data(dr["per_dt_cadastro"]),
                    per_ativo = FormartaDados.Booleano(dr["per_ativo"]),
                    per_deletado = FormartaDados.Booleano(dr["per_deletado"]),
                    userCreate = FormartaDados.String(dr["userCreate"]),
                    dtCreate = FormartaDados.DataNull(dr["dtCreate"]),
                    userLastUpdate = FormartaDados.String(dr["userLastUpdate"]),
                    dtLastUpdate = FormartaDados.DataNull(dr["dtLastUpdate"]),
                    userDelete = FormartaDados.String(dr["userDelete"]),
                    dtDelete = FormartaDados.DataNull(dr["dtDelete"]) ,
                    per_update_xml = FormartaDados.Booleano(dr["per_update_xml"])  
                };
                return obj;
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message, bootStrapMessageType.TYPE_DANGER);
            }
        }
    }
}
